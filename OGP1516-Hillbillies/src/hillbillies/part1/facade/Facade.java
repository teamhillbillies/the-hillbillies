package hillbillies.part1.facade;

import hillbillies.model.Unit;
import ogp.framework.util.ModelException;

public class Facade implements IFacade {

	@Override
	public Unit createUnit(String name, int[] initialPosition, int weight, int agility, int strength, int toughness,
			boolean enableDefaultBehavior) throws ModelException {
		//TODO
		//double[] truePosition = {0.5,0.5,0.5};
		//for (int i=0;i<initialPosition.length;i++) {
		//	truePosition[i] += initialPosition[i];
		//}
		//Unit someUnit = new Unit(name, truePosition, weight, agility,strength,toughness,enableDefaultBehavior);
		//return someUnit;
		return null;

	}

	@Override
	public double[] getPosition(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		return unit.getPosition();
	}

	@Override
	public int[] getCubeCoordinate(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		return unit.getOccupiedCube();
	}

	@Override
	public String getName(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		return unit.getName();
	}

	@Override
	public void setName(Unit unit, String newName) throws ModelException {
		// TODO Auto-generated method stub
		try {
			unit.setName(newName);
		}
		catch (IllegalArgumentException e) {
			throw new ModelException("You specified an invalid name");
		}
	}

	@Override
	public int getWeight(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		return unit.getWeight();
	}

	@Override
	public void setWeight(Unit unit, int newValue) throws ModelException {
		// TODO Auto-generated method stub
		try {
			unit.setWeight(newValue, false);
		}
		catch (IllegalArgumentException e) {
			throw new ModelException("you specified an invalid weight");
		}		
	}

	@Override
	public int getStrength(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		return unit.getStrength();
	}

	@Override
	public void setStrength(Unit unit, int newValue) throws ModelException {
		// TODO Auto-generated method stub
		try {
			unit.setStrength(newValue, false);
		}
		catch (IllegalArgumentException e) {
			throw new ModelException("you specified an invalid strength");
		}		
	}

	@Override
	public int getAgility(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		return unit.getAgility();
	}

	@Override
	public void setAgility(Unit unit, int newValue) throws ModelException {
		// TODO Auto-generated method stub
		unit.setAgility(newValue,false);
	}

	@Override
	public int getToughness(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		return unit.getToughness();
	}

	@Override
	public void setToughness(Unit unit, int newValue) throws ModelException {
		// TODO Auto-generated method stub
		try {
			unit.setToughness(newValue,false);
		}
		catch (IllegalArgumentException e) {
			throw new ModelException("you specified an invalid toughness");
		}		
	}

	@Override
	public int getMaxHitPoints(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		return unit.getMaxHitpoints();
	}

	@Override
	public int getCurrentHitPoints(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		return (int) Math.round(unit.getHitpoints());
	}

	@Override
	public int getMaxStaminaPoints(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		return unit.getMaxStamina();
	}

	@Override
	public int getCurrentStaminaPoints(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		return (int) Math.round(unit.getStamina());
	}

	@Override
	public void advanceTime(Unit unit, double dt) throws ModelException {
		// TODO Auto-generated method stub
		try {
			unit.advanceTime(dt);
		}
		catch (IllegalArgumentException e) {
			throw new ModelException("you specified an invalid duration");
		}		
	}

	@Override
	public void moveToAdjacent(Unit unit, int dx, int dy, int dz) throws ModelException {
		// TODO Auto-generated method stub
		double[] position = {dx,dy,dz};
		try {
			unit.moveToAdjacentFinal(position);
		}
		catch (IllegalArgumentException e) {
			throw new ModelException("that is not a valid position");
		}
	}

	@Override
	public double getCurrentSpeed(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		if (unit.isSprinting())
			return unit.getSprintSpeed();
		else
			return unit.getWalkingSpeed();
	}

	@Override
	public boolean isMoving(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		return unit.isMoving() || unit.isSprinting();
	}

	@Override
	public void startSprinting(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		unit.startSprinting();
	}

	@Override
	public void stopSprinting(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		unit.stopSprinting();
	}

	@Override
	public boolean isSprinting(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		return unit.isSprinting();
	}

	@Override
	public double getOrientation(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		return unit.getOrientation();
	}

	@Override
	public void moveTo(Unit unit, int[] cube) throws ModelException {
		// TODO Auto-generated method stub
		try {
			unit.moveTo(cube);
		}
		catch (IllegalArgumentException e) {
			throw new ModelException("you specified an invalid cube");
		}		
	}

	@Override
	public void work(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		unit.work(new int[]{0,0,0});
	}

	@Override
	public boolean isWorking(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		return unit.isWorking();
	}

	@Override
	public void fight(Unit attacker, Unit defender) throws ModelException {
		// TODO Auto-generated method stub
		if (attacker == null || defender == null) {
			throw new ModelException("you specified a nullpointer as Unit");
		}
		System.out.println(Math.abs(attacker.getPosition()[0] - defender.getPosition()[0]));
		System.out.println(Math.abs(attacker.getPosition()[1] - defender.getPosition()[1]));
		System.out.println(Math.abs(attacker.getPosition()[2] - defender.getPosition()[2]));
		if ((Math.abs(attacker.getPosition()[0] - defender.getPosition()[0]) > 1.1) || (Math.abs(attacker.getPosition()[1] - defender.getPosition()[1])) > 1.1 || (Math.abs(attacker.getPosition()[2] - defender.getPosition()[2])) > 1.1) {
			throw new ModelException("that unit is too far away");
		}
		if (attacker.attack(defender)) {
			defender.setAttackFlag(true);
			defender.addEnemy(attacker);
		}
	}

	@Override
	public boolean isAttacking(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		return unit.isAttacking();
	}

	@Override
	public void rest(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		unit.rest();
	}

	@Override
	public boolean isResting(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		return unit.isResting();
	}

	@Override
	public void setDefaultBehaviorEnabled(Unit unit, boolean value) throws ModelException {
		// TODO Auto-generated method stub
		unit.setDefaultBehaviour(value);
	}

	@Override
	public boolean isDefaultBehaviorEnabled(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		return unit.hasDefaultBehaviour();
	}
	
}

