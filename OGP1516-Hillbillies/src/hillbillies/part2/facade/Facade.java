package hillbillies.part2.facade;

import java.util.HashSet;
import java.util.Set;

import hillbillies.model.Boulder;
import hillbillies.model.Faction;
import hillbillies.model.Item;
import hillbillies.model.Log;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.model.TerrainType;
import hillbillies.part2.listener.TerrainChangeListener;
import ogp.framework.util.ModelException;

public class Facade implements IFacade {

	public Facade() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Unit createUnit(String name, int[] initialPosition, int weight, int agility, int strength, int toughness,
			boolean enableDefaultBehavior) throws ModelException {
		//double[] truePosition = {0.5,0.5,0.5};
		//for (int i=0;i<initialPosition.length;i++) {
		//	truePosition[i] += initialPosition[i];
		//}
		//Unit someUnit = world.createUnit
		//return someUnit;
		System.out.println("system tried to use createunit");
		return null;
	}

	@Override
	public double[] getPosition(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		return unit.getPosition();
	}

	@Override
	public int[] getCubeCoordinate(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		return unit.getOccupiedCube();
	}

	@Override
	public String getName(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		return unit.getName();
	}

	@Override
	public void setName(Unit unit, String newName) throws ModelException {
		// TODO Auto-generated method stub
		try {
			unit.setName(newName);
		}
		catch (IllegalArgumentException e) {
			throw new ModelException("You specified an invalid name");
		}
	}

	@Override
	public int getWeight(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		return unit.getWeight();
	}

	@Override
	public void setWeight(Unit unit, int newValue) throws ModelException {
		// TODO Auto-generated method stub
		try {
			unit.setWeight(newValue, false);
		}
		catch (IllegalArgumentException e) {
			throw new ModelException("you specified an invalid weight");
		}		
	}

	@Override
	public int getStrength(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		return unit.getStrength();
	}

	@Override
	public void setStrength(Unit unit, int newValue) throws ModelException {
		// TODO Auto-generated method stub
		try {
			unit.setStrength(newValue, false);
		}
		catch (IllegalArgumentException e) {
			throw new ModelException("you specified an invalid strength");
		}		
	}

	@Override
	public int getAgility(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		return unit.getAgility();
	}

	@Override
	public void setAgility(Unit unit, int newValue) throws ModelException {
		// TODO Auto-generated method stub
		unit.setAgility(newValue,false);
	}

	@Override
	public int getToughness(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		return unit.getToughness();
	}

	@Override
	public void setToughness(Unit unit, int newValue) throws ModelException {
		// TODO Auto-generated method stub
		try {
			unit.setToughness(newValue,false);
		}
		catch (IllegalArgumentException e) {
			throw new ModelException("you specified an invalid toughness");
		}		
	}

	@Override
	public int getMaxHitPoints(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		return unit.getMaxHitpoints();
	}

	@Override
	public int getCurrentHitPoints(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		return (int) Math.round(unit.getHitpoints());
	}

	@Override
	public int getMaxStaminaPoints(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		return unit.getMaxStamina();
	}

	@Override
	public int getCurrentStaminaPoints(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		return (int) Math.round(unit.getStamina());
	}

	@Override
	public void moveToAdjacent(Unit unit, int dx, int dy, int dz) throws ModelException {
		// TODO Auto-generated method stub
		double[] position = {dx,dy,dz};
		try {
			unit.moveToAdjacentFinal(position);
		}
		catch (IllegalArgumentException e) {
			throw new ModelException("that is not a valid position");
		}
	}

	@Override
	public double getCurrentSpeed(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		if (unit.isSprinting())
			return unit.getSprintSpeed();
		else
			return unit.getWalkingSpeed();
	}

	@Override
	public boolean isMoving(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		return unit.isMoving() || unit.isSprinting();
	}

	@Override
	public void startSprinting(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		unit.startSprinting();
	}

	@Override
	public void stopSprinting(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		unit.stopSprinting();
	}

	@Override
	public boolean isSprinting(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		return unit.isSprinting();
	}

	@Override
	public double getOrientation(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		return unit.getOrientation();
	}

	@Override
	public void moveTo(Unit unit, int[] cube) throws ModelException {
		// TODO Auto-generated method stub
		try {
			unit.moveTo(cube);
		}
		catch (IllegalArgumentException e) {
			throw new ModelException("you specified an invalid cube");
		}		
	}

	@Override
	public boolean isWorking(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		return unit.isWorking();
	}

	@Override
	public void fight(Unit attacker, Unit defender) throws ModelException {
		// TODO Auto-generated method stub
		if (attacker == null || defender == null) {
			throw new ModelException("you specified a nullpointer as Unit");
		}
		try {
			attacker.fight(defender);
		}
		catch (IllegalArgumentException e) {
			throw new ModelException("that unit is too far away or is a member of your faction");
		}
	}

	@Override
	public boolean isAttacking(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		return unit.isAttacking();
	}

	@Override
	public void rest(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		unit.rest();
	}

	@Override
	public boolean isResting(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		return unit.isResting();
	}

	@Override
	public void setDefaultBehaviorEnabled(Unit unit, boolean value) throws ModelException {
		// TODO Auto-generated method stub
		unit.setDefaultBehaviour(value);
	}

	@Override
	public boolean isDefaultBehaviorEnabled(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		return unit.hasDefaultBehaviour();
	}
	

	@Override
	public World createWorld(int[][][] terrainTypes, TerrainChangeListener modelListener) throws ModelException {
		World world = new World(terrainTypes,modelListener);
		return world;
	}

	@Override
	public int getNbCubesX(World world) throws ModelException {
		// TODO Auto-generated method stub
		return world.getNbCubesX();
	}

	@Override
	public int getNbCubesY(World world) throws ModelException {
		// TODO Auto-generated method stub
		return world.getNbCubesY();
	}

	@Override
	public int getNbCubesZ(World world) throws ModelException {
		// TODO Auto-generated method stub
		return world.getNbCubesZ();
	}

	@Override
	public void advanceTime(World world, double dt) throws ModelException {
		world.advanceTime(dt);
	}

	@Override
	public int getCubeType(World world, int x, int y, int z) throws ModelException {
		// TODO Auto-generated method stub
		int[] position = {x,y,z};
		TerrainType type = world.getTerrainType(position);
		switch (type) {
			case AIR:
				return 0;
			case ROCK:
				return 1;
			case TREE:
				return 2;
			case WORKSHOP:
				return 3;
			default:
				return 0;
		}
	}

	@Override
	public void setCubeType(World world, int x, int y, int z, int value) throws ModelException {
		int[] position = {x,y,z};
		switch (value) {
		case 0:
			world.setTerrainType(position, TerrainType.AIR);
			break;
		case 1:
			world.setTerrainType(position, TerrainType.ROCK);
			break;
		case 2:
			world.setTerrainType(position, TerrainType.TREE);
			break;
		case 3:
			world.setTerrainType(position, TerrainType.WORKSHOP);
			break;
		default:
			world.setTerrainType(position, TerrainType.AIR);
			break;
		}
	}

	@Override
	public boolean isSolidConnectedToBorder(World world, int x, int y, int z) throws ModelException {
		return world.isSolidConnectedToBorder(x, y, z);
	}

	@Override
	public Unit spawnUnit(World world, boolean enableDefaultBehavior) throws ModelException {
		return world.createUnit();
	}

	@Override
	public void addUnit(Unit unit, World world) throws ModelException {
		world.addUnit(unit);
	}

	@Override
	public Set<Unit> getUnits(World world) throws ModelException {
		return world.getAllUnits();
	}

	@Override
	public boolean isCarryingLog(Unit unit) throws ModelException {
		if (unit.getItem() instanceof Log)
			return true;
		return false;
	}

	@Override
	public boolean isCarryingBoulder(Unit unit) throws ModelException {
		if (unit.getItem() instanceof Boulder)
			return true;
		return false;
	}

	@Override
	public boolean isAlive(Unit unit) throws ModelException {
		return !unit.isDead();
	}

	@Override
	public int getExperiencePoints(Unit unit) throws ModelException {
		return unit.getExperiencePoints();
	}

	@Override
	public void workAt(Unit unit, int x, int y, int z) throws ModelException {
		int[] cubeToWorkAt = {x,y,z};
		unit.work(cubeToWorkAt);

	}

	@Override
	public Faction getFaction(Unit unit) throws ModelException {
		return unit.getFaction();
	}

	@Override
	public Set<Unit> getUnitsOfFaction(Faction faction) throws ModelException {
		return faction.getUnitsInFaction();
	}

	@Override
	public Set<Faction> getActiveFactions(World world) throws ModelException {
		return world.getAllActiveFactions();
	}

	@Override
	public double[] getPosition(Boulder boulder) throws ModelException {
		return boulder.getPosition();
	}

	@Override
	public Set<Boulder> getBoulders(World world) throws ModelException {
		Set<Item> items = world.getAllItems();
		Set<Boulder> boulders = new HashSet<Boulder>();
		for (Item item : items) {
			if (item instanceof Boulder) {
				boulders.add((Boulder) item);
			}
		}
		return boulders;
	}

	@Override
	public double[] getPosition(Log log) throws ModelException {
		return log.getPosition();
	}

	@Override
	public Set<Log> getLogs(World world) throws ModelException {
		Set<Item> items = world.getAllItems();
		Set<Log> logs = new HashSet<Log>();
		for (Item item : items) {
			if (item instanceof Log) {
				logs.add((Log) item);
			}
		}
		return logs;
	}

}
