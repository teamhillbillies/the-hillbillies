package hillbillies.model;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import hillbillies.part2.listener.DefaultTerrainChangeListener;
import ogp.framework.util.ModelException;

public class FactionTest {
	
	private static Faction faction;
	private static Unit firstUnit, midUnit, lastUnit, extraUnit;
	private World world;
	private int nbX;
	private int nbY;
	private int nbZ;
	private double[] position;

	@Before
	public void setUp() throws Exception {
		nbX = 10;
		nbY = 20;
		nbZ = 30;
		
		world = new World(new int[nbX][nbY][nbZ], new DefaultTerrainChangeListener());
		
		
		for (int x=0; x<nbX; x++){
			for (int y=0; y<nbY; y++){
				for (int z=0; z<nbZ; z++){
					double[] myPosition = {x,y,z};
					if (world.isValidUnitPosition(myPosition)){
					
						if (position==null){
							position = new double[] {x,y,z};
						}
					}
				}
			}
		}
		firstUnit = new Unit("Unit Low",position,25,25,25,25,false);
		midUnit = new Unit("Unit O'Middle",position, 50,50,50,50,false);
		lastUnit = new Unit("UnitHigh",position, 100,100,100,100,false);
		extraUnit = new Unit("Unit Low",position,25,25,25,25,false);
		world.addUnit(firstUnit);
		world.addUnit(midUnit);
		world.addUnit(extraUnit);
		world.addUnit(lastUnit);
		faction = firstUnit.getFaction();
	}

	//BLACKBOX
	@Test
	public void extendedConstructor_legalCase() throws Exception{
		Faction myFaction = new Faction(extraUnit);
		assertEquals(extraUnit.getFaction(), myFaction);
		assertTrue(myFaction.unitInFaction(extraUnit));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void extendedConstructor_falseCase() throws Exception{
		Faction myFaction = new Faction(null);
	}
	
	@Test
	public void extendedConstructor_changeFaction() throws Exception{
		Faction myFaction = new Faction(firstUnit);
		assertEquals(firstUnit.getFaction(), myFaction);
	}
	
	@Test
	public void getNbNumbers_testCase() throws Exception{
		for (int i=2; i<12; i++){
			Unit myUnit1 = new Unit("Unit O'Check",position, 25,50,70,100,false);
			faction.addUnitToFaction(myUnit1);
			assertEquals(faction.getNbUnits(),i);
		}
	}
	
	@Test
	public void getUnitsInFaction_testCase() throws Exception{
		faction.addUnitToFaction(extraUnit);
		faction.addUnitToFaction(midUnit);
		faction.addUnitToFaction(lastUnit);
		//Set unitSet = new HashSet();
		Set unitSet = faction.getUnitsInFaction();
		assertEquals(unitSet.size(),4);
		assertTrue(unitSet.contains(firstUnit));
		assertTrue(unitSet.contains(extraUnit));
		assertTrue(unitSet.contains(midUnit));
		assertTrue(unitSet.contains(lastUnit));
		
	}
	@Test
	public void hasProperUnits_testCase() throws Exception{
		for (int i=2; i<12; i++){
			Unit myUnit1 = new Unit("Unit O'Check",position, 25,50,70,100,false);
			faction.addUnitToFaction(myUnit1);
		}
		assertTrue(faction.hasProperUnits());
	}
	
	@Test
	public void addUnit_legalCase() throws Exception{
		faction.addUnitToFaction(extraUnit);
		assertTrue(faction.unitInFaction(extraUnit));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void addUnit_falseCase() throws Exception{
		faction.addUnitToFaction(null);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void addUnit_factionFull() throws Exception{
		for (int i=2; i<=50; i++){
			Unit myUnit1 = new Unit("Unit O'Check",position, 25,50,70,100,false);
			faction.addUnitToFaction(myUnit1);
			assertTrue(faction.unitInFaction(myUnit1));
		}
		faction.addUnitToFaction(extraUnit);
	}
	
	@Test(expected=ModelException.class)
	public void removeUnit_falseCase() throws Exception{
		faction.removeUnit(firstUnit);
	}
	
	@Test
	public void removeUnit_unitNotInFaction() throws Exception{
		assertFalse(faction.unitInFaction(midUnit));
		assertFalse(midUnit.isTerminated());
		faction.removeUnit(midUnit);
		assertFalse(midUnit.isTerminated());
	}
	
	@Test
	public void terminate_falseCase() throws Exception{
		faction.terminate();
		assertFalse(faction.isTerminated());
	}
	
	@Test
	public void terminate_trueCase() throws Exception{
		world.removeUnit(firstUnit);
		assertTrue(faction.isTerminated());
	}
	
	//WHITEBOX
	
	@Test
	public void canHaveAsUnit_legalcase() throws Exception{
		assertTrue(faction.canHaveAsUnit(extraUnit));
		assertTrue(faction.canHaveAsUnit(midUnit));
		assertTrue(faction.canHaveAsUnit(lastUnit));
	}
	
	@Test
	public void canHaveAsUnit_unitNull() throws Exception{
		Unit myUnit = null;
		assertFalse(faction.canHaveAsUnit(myUnit));
		
	}
	
	@Test
	public void canHaveAsUnit_unitTerminated() throws Exception{
		world.removeUnit(midUnit);
		assertFalse(faction.canHaveAsUnit(midUnit));
	}
	
	@Test
	public void canHaveAsUnit_factionTerminated() throws Exception{
		assertTrue(faction.canHaveAsUnit(midUnit));
		world.removeUnit(firstUnit);
		assertFalse(faction.canHaveAsUnit(midUnit));
	}
	
	@Test
	public void canHaveAsUnit_factionFull() throws Exception{
		for (int i=2; i<=50; i++){
			Unit myUnit1 = new Unit("Unit O'Check",position, 25,50,70,100,false);
			faction.addUnitToFaction(myUnit1);
		}
		assertFalse(faction.canHaveAsUnit(extraUnit));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void addUnit_cantHaveAsUnit() throws Exception{
		Unit myUnit = null;
		faction.addUnitToFaction(myUnit);
	}
	
	@Test
	public void addUnit_catchStatement() throws Exception{
		//never happens
	}
	
	@Test
	public void addUnit_firstUnit() throws Exception{
		Faction myFaction = new Faction(firstUnit);
		assertTrue(myFaction.getNbUnits()==1);
		assertTrue(myFaction.unitInFaction(firstUnit));
	}
	
	@Test
	public void addUnit_midUnit() throws Exception{
		for (int i=2; i<25; i++){
			Unit myUnit1 = new Unit("Unit O'Check",position, 25,50,70,100,false);
			faction.addUnitToFaction(myUnit1);
		}
		faction.addUnitToFaction(midUnit);
		assertTrue(faction.getNbUnits()==25);
		assertTrue(faction.unitInFaction(midUnit));
	}
	
	@Test
	public void addUnit_lastUnit() throws Exception{
		for (int i=2; i<50; i++){
			Unit myUnit1 = new Unit("Unit O'Check",position, 25,50,70,100,false);
			faction.addUnitToFaction(myUnit1);
		}
		faction.addUnitToFaction(lastUnit);
		assertTrue(faction.getNbUnits()==50);
		assertTrue(faction.unitInFaction(lastUnit));
	}
	
}



