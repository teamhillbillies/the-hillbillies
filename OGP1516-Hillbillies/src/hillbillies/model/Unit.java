package hillbillies.model;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;
import hillbillies.model.pathfinding.Position;
import ogp.framework.util.ModelException;
import ogp.framework.util.Util;
/**
 * 
 * A class of units that can conduct several activities. Each activity has an initiator 
 * that sets the currentActivity of the unit to that activity. The next time advanceTime 
 * is called the unit does that activity for the given time. When it finishes its activity 
 * it changes its currentActivity to WAITING. Between most state transitions the method 
 * cleanState() is called, this method resets instance variables that were needed for 
 * the previous activity. The moveTo() method works a bit differently, it sets a finalDestination that the 
 * Unit intents to reach. The next time the Unit is waiting it starts to move towards this 
 * finalDestination. This way the command can be issued without interrupting other activities. 
 * An attackFlag is used to notify the Unit whenever it is attacked. The next time advanceTime() 
 * is called it runs the defend() and cleanState() methods and sets the state of the Unit to waiting 
 * since defending interrupts any activity.
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 * @invar The name of each Unit must be a valid name for any Unit. |
 *        isValidname(getname())
 * @invar The position of each Unit must be a valid position for any Unit. |
 *        isValidPosition(getPosition())
 * @invar  The progress of each Unit must be a valid progress for any
 *         Unit.
 *       | isValidProgress(getProgress())
 * @invar  The timeSpendResting of each Unit must be a valid timeSpendResting for any
 *         Unit.
 *       | isValidtimeSpendResting(gettimeSpendResting())
 * @invar  The hitpoints of each Unit must be a valid hitpoints for any
 *         Unit.
 *       | isValidHitpoints(getHitpoints())
 * @invar  The stamina of each Unit must be a valid stamina for any
 *         Unit.
 *       | isValidStamina(getStamina())
 * @invar  The faction of each unit must be a valid faction for any
 *         unit.
 *       | isValidfaction(getfaction())
 *
 *       
 * @note when changing maxInitStrength, maxStrength, maxInitAgility, maxAgility,
 *       maxInitWeight and maxWeight keep the constraint set by
 *       weightConstraint() in mind.
 */
public class Unit {

	/**
	 * Initialize this new Unit with given name.
	 * @param name
	 *            The name for this new Unit.
	 * @param position
	 *            The position for this new Unit.
	 * @param weight
	 *            The weight for this new Unit.
	 * @param agility
	 *            The agility for this new Unit.            
	 * @param strength
	 *            The strength for this new Unit.
	 * @param toughness
	 *            The toughness for this new Unit.
	 * @param defaultBehaviour
	 * 			  A boolean to enable or disable the default behaviour of this new Unit.
	 * @effect The position of this new Unit is set to the given position.
	 *         |this.setPosition(position)
	 * @effect The name of this new Unit is set to the given name.
	 *         |this.setname(name)
	 * @effect The strength of this new Unit is set to the given strength
	 *         |this.setStrength(strength,true)
	 * @effect The agility of this new Unit is set to the given agility
	 *         |this.setAgility(agility,true)
	 * @effect The toughness of this new Unit is set to the given toughness
	 *         |this.setToughness(toughness,true)
	 * @effect The weight of this new Unit is set to the given weight
	 *         |this.setWeight(weight,true)
	 * @effect The defaultBehaviour of this new Unit is set to the given defaultBehaviour.
	 *         |this.setDefaultBehaviour(defaultBehaviour)
	 * @effect The maxHitpoints of this new Unit is set.
	 * 		   |this.setMaxHitpoints()
	 * @effect The hitpoints of this new Unit is set to the maximum hitpoints.
	 *         |this.setHitpoints(getMaxHitpoints())
	 * @effect The maxStamina of this new Unit is set.
	 *         |this.setMaxStamina()
	 * @effect The stamina of this new Unit is set to the maximum stamina.
	 *         |this.setStamina(getMaxStamina())
	 * @effect The orientation of this new Unit is set to the given orientation
	 * 		   |this.setOrientation(orientation)
	 * @effect The attackFlag of this new Unit is set to 0.
	 *         |this.setAttackFlag(false)
	 * @effect The enemies of this new Unit are set to null.
	 *         |this.setEnemies(null)
	 * @effect The timeSpendResting of this new Unit is set to 0.
	 *         |this.setTimeSpendResting(0)
	 * @effect The progress of this new Unit is set to 0.
	 *         |this.setProgress(0)
	 * @effect The currentActivity of this new Unit is set to Waiting.
	 *         |this.setCurrentActivity(Status.WAITING)
	 * @effect The newPosition of this new Unit is set to the given position.
	 *         |this.setNewPosition(position.clone())
	 * @effect The faction of this new unit is set to null.
	 *         |this.setFaction(null)
	 * @effect The item of this new unit is set to null.
	 *         |this.setItem(null)
	 * @effect The experience points of this new unit are set to 0.
	 *         |this.setExperiencePoints(0);
	 * @effect The path of this new unit is set to an empty arraylist.
	 *         |this.path = new ArrayList<int []>();
	 */

	public Unit(String name, double[] position, int weight, int agility, int strength, int toughness, boolean enabledefaultBehaviour)
			throws ModelException {
		
		try {
			setPosition(position);
		}
		catch (IllegalArgumentException e) {
			throw new ModelException("you specified an invalid position");
		}
		try {
			setName(name);
		}
		catch (IllegalArgumentException e) {
			throw new ModelException("you specified an invalid name");
		}		
		try {
			setStrength(strength, true);
		}
		catch (IllegalArgumentException e) {
			throw new ModelException("you specified an invalid strength");
		}		
		try {
			setAgility(agility, true);
		}
		catch (IllegalArgumentException e) {
			throw new ModelException("you specified an invalid agility");
		}		
		try {
			setToughness(toughness, true);
		}
		catch (IllegalArgumentException e) {
			throw new ModelException("you specified an invalid toughness");
		}		
		try {
			setWeight(weight, true);
		}
		catch (IllegalArgumentException e) {
			throw new ModelException("you specified an invalid weight");
		}		
		try {
			setDefaultBehaviour(enabledefaultBehaviour);
		}
		catch (IllegalArgumentException e) {
			throw new ModelException("you specified an invalid defaultBehaviour");
		}
		setMaxHitpoints();
		setHitpoints(getMaxHitpoints());
		setMaxStamina();
		setStamina(getMaxStamina());
		setOrientation(Math.PI/2);
		setAttackFlag(false);
		setEnemies(new ArrayList<Unit>());
		setTimeSpendResting(0);
		setProgress(0);
		this.currentActivity= Status.WAITING;
		setNewPosition(position.clone());
		setFaction(null);
		setItem(null);
		setExperiencePoints(0);
		this.path = new ArrayList<int []>();
	}
	

	/**
	 * Return the position of this Unit.
	 */
	@Basic
	@Raw
	public double[] getPosition() {
		return this.position;
	}

	/**
	 * Returns the cube that the Unit occupies.
	 */
	public int[] getOccupiedCube() {
		int[] cubePosition = { 0, 0, 0 };
		for (int i = 0; i < position.length; i++) {
			cubePosition[i] = (int) Math.floor(this.position[i]);
		}
		return cubePosition;
	}


	/**
	 * Set the position of this Unit to the given position.
	 * 
	 * @param position
	 *        The new position for this Unit.
	 * @post The position of this new Unit is equal to the given position.
	 *       |new.getPosition() == position
	 * @throws IllegalArgumentException()
	 *         The given position is not a valid position for any Unit.
	 *         |!isValidUnitPosition(getPosition())
	 */
	@Raw
	private void setPosition(double[] position) throws IllegalArgumentException {
		//if (!world.isValidUnitPosition(position))
		//	throw new IllegalArgumentException();
		this.position = position;
	}

	/**
	 * Variable registering the position of this Unit.
	 */
	private double[] position;

	/**
	 * Return the name of this Unit.
	 */
	@Basic @Raw
	public String getName() {
		return this.name;
	}

	/**
	 * Check whether the given name is a valid name for any Unit.
	 * 
	 * @param name
	 *         The name to check.
	 * @return result == return true if the given name is at least 2 characters
	 *         long, starts with an uppercase letter and if the name only
	 *         contains letters, quotes and spaces.
	 */
	public static boolean isValidName(String name) {
		if (name.length() < 2)
			return false;
		if (!Character.isLetter(name.charAt(0)) || !Character.isUpperCase(name.charAt(0)))
			return false;
		for (int i = 1; i < name.length(); i++) {
			if (!(Character.isLetter(name.charAt(i)) || Character.isWhitespace(name.charAt(i)) || name.charAt(i) == '"'
					|| name.charAt(i) == '\'' || (ALLOW_NUMBERS && Character.isDigit(name.charAt(i))))) {
				return false;
			}
		}
		return true;
	}
	// decides if numbers are allowed in the names of units
	private static boolean ALLOW_NUMBERS = true;

	/**
	 * Set the name of this Unit to the given name.
	 * 
	 * @param name
	 *        The new name for this Unit.
	 * @post The name of this new Unit is equal to the given name.
	 *       |new.getname() == name
	 * @throws IllegalArgumentException
	 *         The given name is not a valid name for any Unit. 
	 *         |!isValidname(getname())
	 */
	public void setName(String name) throws IllegalArgumentException {
		if (!isValidName(name))
			throw new IllegalArgumentException(name);
		this.name = name;
	}

	/**
	 * Variable registering the name of this Unit.
	 */
	private String name;

	/**
	 * Return the strength of this Unit.
	 */
	@Basic
	public int getStrength() {
		return this.strength;
	}

	/**
	 * Variable registering the strength of this Unit.
	 */
	/**
	 * Set the strength of this Unit to the given strength.
	 * 
	 * @param strength
	 *            The new strength for this Unit.
	 * @param init
	 *            A boolean flag to find out whether the Unit is newly
	 *            initialized or not
	 * @post If the given strength is a valid strength for a Unit, the strength
	 *       of this Unit is equal to the given strength. Otherwise, if the
	 *       given strength is too high the strength of this Unit is equal to
	 *       its maximum value. If the given strength is too low the strength of
	 *       this Unit is equal to its minimum value. The minimum and maximum
	 *       value depend on whether the Unit is newly initialized or not. 
	 *       |currentMaxStrength = init ? maxInitStrength : maxStrength 
	 *       |currentMinStrength = init ? minInitStrength : minStrength 
	 *       |if (strength < currentMinStrength) 
	 *       | 	then strength == currentMinStrength 
	 *       |else if (strength > currentMaxStrength) 
	 *       |	then strength == currentMaxStrength 
	 *       |new.strength == strength
	 */
	public void setStrength(int strength, boolean init) {
		int currentMaxStrength = init ? MAXINITSTRENGTH : MAXSTRENGTH;
		int currentMinStrength = init ? MININITSTRENGTH : MINSTRENGTH;
		if (strength < currentMinStrength || Double.isNaN(strength))
			strength = currentMinStrength;
		else if (strength > currentMaxStrength)
			strength = currentMaxStrength;
		this.strength = strength;
	}
	
	/**
	 * Returns the maximum number of strength a unit can possess upon initialization
	 */
	public static int getMaxInitStrength(){
		return MAXINITSTRENGTH;
	}
	
	/**
	 *Returns the minimum number of strength a unit can possess upon initialization
	 */
	public static int getMinInitStrength(){
		return MININITSTRENGTH;
	}
	
	/**
	 * A variable registering the strength of a unit
	 */
	private int strength;
	
	/**
	 * A final variable registering the maximum allowed strength of a unit
	 * upon initialization
	 */
	private final static int MAXINITSTRENGTH = 100;
	
	/**
	 * A final variable registering the maximum allowed strength of a unit
	 */
	private final static int MAXSTRENGTH = 200;
	
	/**
	 * A final variable registering the minimum allowed strength of a unit
	 * upon initialization
	 */
	private final static int MININITSTRENGTH = 25;
	
	/**
	 * A final variable registering the minimum allowed strength of a unit
	 */
	private final static int MINSTRENGTH = 1;

	/**
	 * Return the agility of this Unit.
	 */
	@Basic
	public int getAgility() {
		return this.agility;
	}

	/**
	 * Variable registering the agility of this Unit.
	 */
	/**
	 * Set the agility of this Unit to the given agility.
	 * 
	 * @param agility
	 *            The new agility for this Unit.
	 * @param init
	 *            A boolean flag to find out whether the Unit is newly
	 *            initialized or not
	 * @post If the given agility is a valid agility for a Unit, the agility of
	 *       this Unit is equal to the given agility. Otherwise, if the given
	 *       agility is too high the agility of this Unit is equal to its
	 *       maximum value. If the given agility is too low the agility of this
	 *       Unit is equal to its minimum value. The minimum and maximum value
	 *       depend on whether the Unit is newly initialized or not. 
	 *       |currentMaxAgility = init ? maxInitAgility : maxAgility 
	 *       |currentMinAgility = init ? minInitAgility : minAgility 
	 *       |if (agility < currentMinAgility) 
	 *       |	then agility == currentMinAgility 
	 *       |else if (agility > currentMaxAgility) 
	 *       |	then agility == currentMaxAgility | new.agility == agility
	 */
	public void setAgility(int agility, boolean init) {
		int currentMaxAgility = init ? MAXINITAGILITY : MAXAGILITY;
		int currentMinAgility = init ? MININITAGILITY : MINAGILITY;
		if (agility < currentMinAgility || Double.isNaN(strength))
			agility = currentMinAgility;
		else if (agility > currentMaxAgility)
			agility = currentMaxAgility;
		this.agility = agility;
	}
	
	/**
	 * Returns the maximum allowed agility of a unit upon initialization
	 */
	public static int getMaxInitAgility(){
		return MAXINITAGILITY;
	}
	
	/**
	 * Returns the minimum allowed agility of a unit upon initialization
	 */
	public static int getMinInitAgility(){
		return MININITAGILITY;
	}
	
	/**
	 * Variable registering the agility of a unit
	 */
	private int agility;
	
	/**
	 * The maximum agility a unit can have upon initialization
	 */
	private final static int MAXINITAGILITY = 100;
	
	/**
	 * The maximum agility a unit can have
	 */
	private final static int MAXAGILITY = 200;
	
	/**
	 * The minimum agility a unit can have upon initialization
	 */
	private final static int MININITAGILITY = 25;
	
	/**
	 * The minimum agility a unit can have
	 */
	private final static int MINAGILITY = 1;

	/**
	 * Return the toughness of this Unit.
	 */
	@Basic
	public int getToughness() {
		return this.toughness;
	}

	/**
	 * Variable registering the toughness of this Unit.
	 */
	/**
	 * Set the toughness of this Unit to the given toughness.
	 * 
	 * @param toughness
	 *            The new toughness for this Unit.
	 * @param init
	 *            A boolean flag to find out whether the Unit is newly
	 *            initialized or not
	 * @post If the given toughness is a valid toughness for a Unit, the
	 *       toughness of this Unit is equal to the given toughness. Otherwise,
	 *       if the given toughness is too high the toughness of this Unit is
	 *       equal to its maximum value. If the given toughness is too low the
	 *       toughness of this Unit is equal to its minimum value. The minimum
	 *       and maximum value depend on whether the Unit is newly initialized
	 *       or not. 
	 *       |currentMaxToughness = init ? maxInitToughness : maxToughness 
	 *       |currentMinToughness = init ? minInitToughness : minToughness 
	 *       |if (toughness < currentMinToughness) 
	 *       |	then toughness == currentMinToughness 
	 *       |else if (toughness > currentMaxToughness) 
	 *       | 	then toughness == currentMaxToughness
	 *       |new.toughness == toughness
	 */
	public void setToughness(int toughness, boolean init) {
		int currentMaxToughness = init ? MAXINITOUGHNESS : MAXTOUGHNESS;
		int currentMinToughness = init ? MININITTOUGHNESS : MINTOUGHNESS;
		if (toughness < currentMinToughness || Double.isNaN(strength))
			toughness = currentMinToughness;
		else if (toughness > currentMaxToughness)
			toughness = currentMaxToughness;
		this.toughness = toughness;
	}
	
	/**
	 * Returns the maximum thoughness a unit can have upon initialization
	 */
	public static int getMaxInitToughness(){
		return MAXINITOUGHNESS;
	}
	
	/**
	 * Returns the minimum thoughness a unit can have upon initialization
	 */
	public static int getMinInitToughness(){
		return MININITTOUGHNESS;
	}
	
	/**
	 * Variable registering the thoughness of a unit
	 */
	private int toughness;
	
	/**
	 * Variable registering the maximum thoughness a unit can have upon initialization
	 */
	private final static int MAXINITOUGHNESS = 100;
	
	/**
	 * Variable registering the maximum thoughness a unit can have
	 */
	private final static int MAXTOUGHNESS = 200;
	
	/**
	 * Variable registering the minimum thoughness a unit can have upon initialization
	 */
	private final static int MININITTOUGHNESS = 25;
	
	/**
	 * Variable registering the minimum thoughness a unit can have
	 */
	private final static int MINTOUGHNESS = 1;

	/**
	 * Return the weight of this Unit.
	 */
	@Basic
	public int getWeight() {
		return this.weight;
	}

	/**
	 * 
	 * @param weight
	 *            The new weight for this Unit.
	 * @return whether the weight is higher than or equal to the averages of
	 *         the strength and agility 
	 *         |weight >= ((this.strength + this.agility)/2)
	 * 
	 */
	private int weightConstraint() {
		return (this.strength + this.agility) / 2;
	}

	/**
	 * Set the weight of this Unit to the given weight.
	 * 
	 * @param weight
	 *            The new weight for this Unit.
	 * @param init
	 *            A boolean flag to find out whether the Unit is newly
	 *            initialized or not
	 * @post If the given weight is a valid weight for a Unit, the weight of
	 *       this Unit is equal to the given weight. Otherwise, if the given
	 *       weight is too high the weight of this Unit is equal to its maximum
	 *       value. If the given weight is lower than the average of the
	 *       strength and agility of the Unit the weight of this Unit is set to
	 *       this average. The minimum and maximum value depend on whether the
	 *       Unit is newly initialized or not. 
	 *       |currentMaxToughness = init ? maxInitToughness : maxToughness 
	 *       |currentMinToughness = init ? minInitToughness : minToughness 
	 *       |if (toughness < currentMinToughness) 
	 *       | 	then toughness == currentMinToughness 
	 *       |if (toughness < (this.strength + this.agility)/2) 
	 *       | 	then toughness == (this.strength + this.agility)/2 
	 *       |else if (toughness > currentMaxToughness) 
	 *       | 	then toughness == currentMaxToughness
	 *       |new.toughness == toughness
	 * @note At the moment the constraint regarding currentMinToughness is not
	 *       relevant because the constraint regarding the average of the
	 *       strength and agility is stronger, but if the minimum values on
	 *       those attributes are lowered it might become important.
	 */
	public void setWeight(int weight, boolean init) {
		int currentMaxWeight = init ? MAXINITWEIGHT : MAXWEIGHT;
		int currentMinWeight = init ? MININITWEIGHT : MINWEIGHT;
		if (weight < currentMinWeight || Double.isNaN(strength))
			weight = currentMinWeight;
		if (weight < weightConstraint())
			weight = weightConstraint();
		else if (weight > currentMaxWeight)
			weight = currentMaxWeight;
		this.weight = hasItem() ? weight + getItem().getWeight() : weight;
	}
	
	/**
	 * Returns the maximum weight a unit can have upon initialization
	 */
	public static int getMaxInitWeight(){
		return MAXINITWEIGHT;
	}
	
	/**
	 * Returns the minimum weight a unit can have upon initialization
	 * @return
	 */
	public static int getMinInitWeight(){
		return MININITWEIGHT;
	}

	/**
	 * Variable registering the weight of a unit
	 */
	private int weight;
	
	/**
	 * Variable registering the maximum weight a unit can have upon initialization
	 */
	private final static int MAXINITWEIGHT = 100;
	
	/**
	 * Variable registering the maximum weight a unit can have
	 */
	private final static int MAXWEIGHT = 200;
	
	/**
	 * Variable registering the minimum weight a unit can have upon initialization
	 */
	private final static int MININITWEIGHT = 25;
	
	/**
	 * Variable registering the minimum weight a unit can have
	 */
	private final static int MINWEIGHT = 1;

	/**
	 * @return returns the maximum number of hitpoints of the unit
	 * | result == this.maxHitpoints
	 */

	public int getMaxHitpoints() {
		return this.maxHitpoints;
	}

	/**
	 * @pre (minWeight < this.getWeight() < maxWeight()) && (minThoughness < this.getThougness < maxThougness)
	 * @post sets the maxHitpoints to the given formula: 
	 * 		|new.getMaxHitpoints == math.ceil(200*(this.weight/100)*(this.thoughness/100))
	 */
	private void setMaxHitpoints() {
		this.maxHitpoints = (int) Math.ceil(200 * ((double) this.getWeight() / 100) * ((double) this.getToughness() / 100));
	}

	/**
	 * @return returns the hitpoints of the unit
	 *        | result == this.hitpoints
	 */
	public double getHitpoints() {
		return this.hitpoints;
	}

	/**
	 * @pre the hitpoints has to be bigger than 0 
	 *      |new.getHitpoints > 0 + eps
	 * @pre the hitpoints can not be bigger than the maximum number of hitpoints
	 *      |new.getHitpoints <= this.getMaxHitpoints
	 * @post sets current hitpoint to the given input 
	 *      |new.getHitpoints = hitpoints
	 */
	private void setHitpoints(double hitpoints) {
		assert isValidHitpoints(hitpoints);
		this.hitpoints = hitpoints;
	}

	/**
	 * 
	 * @return true if and only if hitpoints is smaller than or equal to maxHitpoints and
	 *         bigger than or equal to 0
	 *         | result == ((hitpoints >= 0 + eps) && (hitpoints <= getMaxHitpoints - eps))
	 */
	public boolean isValidHitpoints(double hitpoints) {
		if ((Util.fuzzyGreaterThanOrEqualTo(hitpoints, 0)) && (Util.fuzzyLessThanOrEqualTo(hitpoints, (double) getMaxHitpoints()) && !Double.isNaN(hitpoints)))
			return true;
		return false;
	}

	/**
	 * @pre the number of hitpoints has to be bigger or equal to 1
	 * 		|this.hitpoints >= 1
	 * @post the number of hitpoints is reduced by 1 
	 * 		|new.hitpoints = this.hitpoints - 1
	 */
	public void consumeHitpoints() {
		assert this.getHitpoints() >= 1;
		this.setHitpoints(this.getHitpoints() - 1);
	}

	/**
	 * @param amount
	 * @pre the amount has to be bigger than 0 
	 * 		|amount > 0 + eps
	 * @pre the result of the amount substracted from the hitpoints has to be bigger than 0 
	 * 		|isValidHitpoints(this.getHitpoints - amount)
	 * @post the new number of hitpoints is equal to the number of hitpoints minus the amount 
	 * 		|new.getHitpoints = this.getHitpoints - amount
	 */
	public void consumeHitpoints(double amount) {
		if (getHitpoints() - amount > 0) {
			this.setHitpoints(this.getHitpoints() - amount);
		}
		else {
			setHitpoints(0);
			die();
		}
	}
	
	/**
	 * Variable registering the hitpoints of a unit
	 */
	private double hitpoints;
	
	/**
	 * Variable registering the maximum number of hitpoints the unit can have
	 */
	private int maxHitpoints;

	/**
	 * Return the maximum stamina of this Unit
	 */
	public int getMaxStamina() {

		return this.maxStamina;
	}

	/**
	 * @pre minWeight < this.getWeight() < maxWeight() && minThoughness < this.getThougness < maxThougness
	 * @post sets the maximum stamina to the given formula: 
	 * 		|new.getMaxHitpoints = math.ceil(200*(this.weight/100)*(this.thoughness/100))
	 */
	private void setMaxStamina() {
		this.maxStamina = (int) Math.ceil(200 * ((double) this.getWeight() / 100) * ((double) this.getToughness() / 100));

	}

	/**
	 * returns the stamina of the unit
	 */
	public double getStamina() {
		return this.stamina;
	}

	/**
	 * @pre the stamina has to be bigger than 0 
	 * 		|new.getStamina > 0 + eps
	 * @pre the stamina can not be bigger than the maximum stamina
	 *      |new.getStamina <= this.getMaxStamina
	 * @post sets current stamina to the given input |new.getStamina = stamina
	 */
	private void setStamina(double stamina) {
		assert isValidStamina(stamina);
		this.stamina = stamina;
	}

	/**
	 * 
	 * @return true if and only if the stamina is smaller than or equal to maxStamina and bigger
	 *         than or equal to 0
	 *         | result == ((stamina >= 0 - eps) && (stamina <= getMaxStamina + eps))
	 *         
	 */
	public boolean isValidStamina(double stamina) {
		if (Util.fuzzyGreaterThanOrEqualTo(stamina, 0) && (Util.fuzzyLessThanOrEqualTo(stamina, getMaxStamina()) && !Double.isNaN(stamina)))
			return true;
		return false;
	}

	/**
	 * @pre the number of stamina has to be bigger than or equal to 1 
	 * 		|this.stamina >= 1
	 * @post the number of stamina is reduced by 1 
	 * 		|new.stamina = this.stamina - 1
	 */
	public void consumeStamina() {
		assert this.getStamina() >= 1;
		this.setStamina(this.getStamina() - 1);
	}

	/**
	 * @param amount
	 * @pre the amount has to be bigger than 0 
	 *      |amount > 0
	 * @pre the result of the amount substracted from the stamina has to be
	 *      bigger than 0 
	 *      |isValidStamina(this.getStamina - amount)
	 * @post the new number of stamina is equal to the number of stamina minus
	 *       the amount 
	 *       |new.getStamina = this.getStamina - amount
	 */
	public void consumeStamina(int amount) {
		assert amount > 0;
		assert isValidStamina(this.getStamina() - amount);
		this.setStamina(this.getStamina() - amount);
	}
	
	/**
	 * Variable registering the current stamina of a unit
	 */
	private double stamina;
	
	/**
	 * Variable registering the maximum number of stamina a unit can have
	 */
	private int maxStamina;

	/**
	 * Return the orientation of this Unit
	 */
	@Basic 
	public double getOrientation() {
		return this.orientation;
	}

	 /**
	  * Set the orientation of this Unit to the given orientation.
	  * 
	  * @param  orientation
	  *         The new orientation for this Unit.
	  * @post   If the given orientation is between 0 and 2*PI, the orientation of this 
	  * 		   new Unit is equal to the given orientation. If the given orientation exceed 
	  *         2*PI; the orientation of this new Unit is set to the given orientation modulo 2*PI. 
	  *         If the given orientation is negative the orientation is set to 2*PI minus the given 
	  *         orientation's absolute value modulo 2*PI. If the given orientation is not a valid 
	  *         number the orientation of this new Unit is equal to 0.
	  *       | if (orientation >)
	  *       |   then new.getOrientation() == orientation % (2*PI)
	  *       | else if (orientation < 0)
	  *       |   then new.getOrientation() == 2*PI + (orientation % (2*PI))
	  *       | else new.getOrientation() == 0
	  */
	 public void setOrientation(double orientation) {
		 
		 if (orientation > 0)
			 orientation = orientation % (2*Math.PI);
		 else if (orientation < 0)
			 orientation = 2*Math.PI + (orientation % (2*Math.PI));
		 else
			 orientation = 0;
		 this.orientation = orientation;
	 	}

	private double orientation;	
	
	/**
	 * Returns the current activity of this Unit.
	 */
	@Basic @Raw
	public Status getCurrentActivity() {
		return this.currentActivity;
	}

	/**
	 * Set the current activity of this Unit to the given current activity.
	 * 
	 * @param  currentActivity
	 *         The new current activity for this Unit.
	 * @pre    The given input has to be defined in the Status enumeration
	 * @post   The current activity of this Unit is equal to the given
	 *         current activity.
	 *       | new.getCurrentActivity() == currentActivity
	 */
	
	@Raw
	private void setCurrentActivity(Status currentActivity) {
		cleanState();
		this.currentActivity = currentActivity;
	}
	
	/**
	 * Checks whether the given Status overrules the activity of the unit
	 * 
	 * @param 	currentActivity
	 * 			the activity to be checked
	 * @return	true if the unit is either falling, moving or resting and hasn't recovered hitpoints
	 * 			and the given activity is falling. If the given activity isn't falling and the unit
	 * 			doing the listed activities return false. Otherwise return true
	 * 			|if (isFalling() || isMoving() || (isResting() && !hasRecoveredHitpoint()))
	 * 			|	if (currentActivity == Status.FALLING) {
				|		result == true;
				|	else
				|		result == false;
				|else
				|	result == true
	 */
	private boolean overrulesCurrentActivity(Status currentActivity) {
		if (isFalling() || isMoving() || (isResting() && !hasRecoveredHitpoint())) {
			if (currentActivity == Status.FALLING) {
				return true;
			}
			return false;
		}
		return true;
	}

	/**
	 * Variable registering the current activity of this Unit.
	 */
	private Status currentActivity;
	

	/**
	 * @param		duration
	 * 				the duration with which to advance the time
	 * @effect    	if the unit is attacked, the unit will defend and then change it's status to waiting
	 * @effect    	if the unit is moving it continues to move for the given duration
	 * @effect    	if the unit is attacking it continues to attack for the given duration
	 * @effect    	if the unit is resting it continues to rest for the given duration
	 * @effect    	If the unit is working it continues to work for the given duration
	 * @effect    	If the unit was attacked or was waiting, the unit starts its next move if it has 
	 *          	not reached its final destination. If the unit has reached its final destination it stops 
	 *          	sprinting. If the default behaviour is enabled it is started.
	 * @effect   	If the gameTime passed a multiple of 180, the unit initiates resting
	 * @post    	The gameTime is increased by the given duration
	 * @throws		IllegalArgumentException
	 * 				the given duration is not a valid duration
	 * 				|! isValidDuration(duration)
	 * 
	 * 		
	 */
	public void advanceTime(double duration) throws ModelException{
		if (!isValidDuration(duration)){
			throw new ModelException();
		}
		if (isDead())
			return;
		if (isAttacked() == true) {
			for (Unit enemy : this.enemies) {
				defend(enemy);
			}
			if(hasTask()) {
				returnTask();
			}
			setAttackFlag(false);
			setEnemies(new ArrayList<Unit>());
			setCurrentActivity(Status.WAITING);
		}
		switch(getCurrentActivity()) {
		case MOVING: 
			move(duration);
			break;
		case FALLING:
			fall(duration);
			break;
		case RESTING: 
			rest(duration);
			break;	
		case WORKING: 
			work(duration);
			break;
		case FOLLOWING:
			move(duration);
			break;
		case WAITING:
			if(UnitToFollow!=null) {
				if(canStopFollowing()) {
					setUnitToFollow(null);
					if (isSprinting()) {
						stopSprinting();
					}
					if (hasDefaultBehaviour() == true)
						defaultBehaviour(duration);
				}
				else
					nextFollow();
				break;
			}
			if(hasReachedEndOfPath()) {
			//if (Util.fuzzyEquals(finalDestination[0], position[0]) && Util.fuzzyEquals(finalDestination[1], position[1]) && Util.fuzzyEquals(finalDestination[2], position[2])) {
				if (isSprinting()) {
					stopSprinting();
				}
				if (hasDefaultBehaviour() == true)
					defaultBehaviour(duration);
			}
			else 
				nextMove2();
			break;	
		case ATTACKING:
			attack(duration);
			break;
		}
		if ((gameTime % 180) + duration >= 180) {
			if(hasTask()) {
				returnTask();
			}
			rest();
		}
		this.gameTime += duration;

	}
	
	// this variable stores the time that this unit exists
	private double gameTime = 0;
		
	/**
	 * @return true if and only if the duration is smaller than or equal to 0.2 and bigger
	 *         than or equal to 0
	 *         | result == ((duration >= 0 - eps) && (duration <= 0.2 + eps))
	 */
	public static boolean isValidDuration(double duration) {
		return (Util.fuzzyGreaterThanOrEqualTo(duration, 0) && Util.fuzzyLessThanOrEqualTo(duration, 0.2) && !Double.isNaN(duration));
	}

	/**
	 * Return the base speed of this Unit.
	 * | result == 1.5*(this.getStrength()+this.getAgility())/(200*this.getWeight()/100)
	 */
	@Basic @Raw
	public double getBaseSpeed() {
		return 1.5*(this.getStrength()+this.getAgility())/(200*this.getWeight()/100);
	}

	/**
	 * Return the walking speed of this Unit.
	 */
	@Basic @Raw
	public double getWalkingSpeed() {
		return this.walkingSpeed;
	}

	/**
	 * Check whether the given walking speed is a valid walking speed for
	 * any Unit.
	 *  
	 * @param  walking speed
	 *         The walking speed to check.
	 * @return 
	 *       | result == true if walking speed > 0 + eps
	 *       | result == false if walking speed < 0 - eps
	*/
	public static boolean isValidWalkingSpeed(double walkingSpeed) {
		return (Util.fuzzyGreaterThanOrEqualTo(walkingSpeed, 0) && !Double.isNaN(walkingSpeed));
	}

	/**
	 * Set the walking speed of this Unit to the given walking speed.
	 * 
	 * @param  walkingSpeed
	 *         The new walking speed for this Unit.
	 * @post   	The walking speed of a unit moving from position (x,y,z) to the 
	 * 			targets position (x',y',z') is:
	 * 			0.5 times the base speed if the unit is walking upwards (z-coordinate)
	 * 			1.2 times the base speed if the unit is walking downwards (z-coordinate)
	 * 			the base speed if the unit is walking in the xy-plane
	 *       | if heightdifference == -1
	 *       |		then new.walkingSpeed == 0.5*this.getbaseSpeed()
	 *       | if heightdifference == 1
	 *       |		then new.walkingSpeed == 1.2*this.getBaseSpeed()
	 *       | else
	 *       |		new.walkingSpeed == this.getBaseSpeed()
	 *       |(with heightdifference == this.getOccupiedCube()[2] - newOccupiedCube[2])
	 */
	@Raw
	private void setWalkingSpeed(double[] newOccupiedCube) {
		
		int heightDifference = this.getOccupiedCube()[2] - (int) newOccupiedCube[2];
		
		if (heightDifference == -1){
			this.walkingSpeed = 0.5*this.getBaseSpeed();
		}
		else if (heightDifference == 1){
			this.walkingSpeed = 1.2*this.getBaseSpeed();
		}
		else {
			this.walkingSpeed = this.getBaseSpeed();
		}
		
	}

	/**
	 * Variable registering the walking speed of this Unit.
	 */
	private double walkingSpeed;
	
	
	/**
	 * Return the sprinting speed of this Unit. This is 2 times the walking speed
	 * |this.getSprintspeed == 2*getWalkingSpeed()
	 */
	@Basic @Raw
	public double getSprintSpeed() {
		return 2*getWalkingSpeed();
	}
	
	/**
	 * Check whether the given sprintSpeed is a valid sprintSpeed for
	 * any Unit.
	 *  
	 * @param  sprintSpeed
	 *         The sprintSpeed to check.
	 * @return 
	 *       | result == (sprintSpeed >= 0 + eps)
	*/
	public static boolean isValidSprintSpeed(double sprintSpeed) {
		return (Util.fuzzyGreaterThanOrEqualTo(sprintSpeed, 0) && !Double.isNaN(sprintSpeed));
	
	}
	
	/**
	 * @return	true if the given Unit is moving
	 * 			|return == (this.getCurrentActivity() == Status.MOVING)
	 */
	public boolean isMoving(){
		return (this.getCurrentActivity() == Status.MOVING);
	}
	
	/**
	 * @return	true if the given Unit is sprinting
	 * 			|return == this.sprintingFlag
	 */
	public boolean isSprinting(){
		return (this.sprintingFlag);
	}
	
	/**
	 * @post 	IF the unit is allowed to sprint, the Unit starts to sprint and starts to consume stamina
	 * 			|if this.isAllowedToSprint
	 * 			|	then ((this.sprintingFlag == true) && (this.consumeStamina()))
	 */
	public void startSprinting(){
		if (this.isAllowedToSprint()){
			this.sprintingFlag = true;
			this.consumeStamina();
		}	
	}
	
	/**
	 * @param	time
	 * 			The time the Unit sprints by invoking this method 1 time
	 * @post 	If the time added with the 0.1 modulus of the gametime, is equal or greater than 0.1 (with machine precision): 
	 * 			then the unit will sprint and consume stamina
	 * 			|if (this.gameTime % 0.1 + time >= 0.1 - eps)
	 * 			|	then consumeStamina
	 * @post	The unit will consume 2 stamina if the time added with the 0.2 modulus of the gametime, is equal or greater than
	 * 			0.2 (with machine precision) else the Unit will consume 1 stamina
	 * 			|if (this.gameTime % 0.2 + time >= 0.2 - eps)
				|	then consumeStamina(2)
				|else
				|	consumestamina(1)
	 */
	private void sprint(double time){
		if (Util.fuzzyGreaterThanOrEqualTo(this.gameTime % 0.1 + time, 0.1)) {
			if (Util.fuzzyGreaterThanOrEqualTo(this.gameTime % 0.2 + time, 0.2)) {
				consumeStamina(2);
			}
			else {
				consumeStamina(1);
			}

		}
	}
	
	/**
	 * @post 	The Unit will stop sprinting and will continue to walk
	 * 			|this.sprintingFlag == false	
	 */
	public void stopSprinting(){
		this.sprintingFlag = false;
	}
	
	/**
	 * Return 	True if the Unit is allowed to sprint, so if the Unit is either already sprinting or walking
	 * 			and the Unit's stamina is greater than or equal to 2
	 * 			|return == ((this.getCurrentActivity() == Status.MOVING) &&( this.getStamina() >= 2 - eps))
	 */
	public boolean isAllowedToSprint(){
		return ((this.getCurrentActivity() == Status.MOVING) &&(Util.fuzzyGreaterThanOrEqualTo(this.getStamina(), 2)));
	}
	
	public boolean sprintingFlag = false;
	
	
	
	/**
	 * Initiates movement to the given newPosition if it is a valid position.
	 * @param 	newPosition
	 * 			The adjacent position that the unit needs to reach
	 * @effect  The walking speed is set depending on the z level of the given newPosition
	 *          |setWalkingSpeed(newPosition) 	
	 * @post    If the unit is not currently walking or resting for his first hitpoint, 
	 * 			the newPosition of the unit is changed to the given newPosition and the current 
	 * 			activity is set to MOVING.
	 * @throws	IllegalArgumentException
	 * 			the given position is not a valid position for any Unit
	 * 			|! isValidUnitPosition(truePosition)
	 * 
	 */
	private void moveToAdjacent(double[] newPosition) throws IllegalArgumentException{
		setWalkingSpeed(newPosition);
		
		if (getWorld() == null)
			throw new IllegalArgumentException("Unit doesnt have a world yet");

		if (isMoving()|| (isResting() && !hasRecoveredHitpoint())) {
			// this should never happen, since when the method is called 
			// this constraint has either already been tested for or the Unit is waiting.
			return;
		}
		if (! getWorld().isValidUnitPosition(position)) {
			throw new IllegalArgumentException
				("this is not a valid unit position");
		}
		setNewPosition(newPosition);
		setCurrentActivity(Status.MOVING);
	}
	
	/**
	 * Return the newPosition of this Unit.
	 */
	@Basic
	@Raw
	public double[] getNewPosition() {
		return this.newPosition;
	}
	
	/**
	 * Set the newPosition of this Unit to the given newPosition.
	 * 
	 * @param newPosition
	 *        The new newPosition for this Unit.
	 * @post The newPosition of this new Unit is equal to the given newPosition.
	 *       |new.getNewPosition() == newPosition
	 * @throws IllegalArgumentException()
	 *         The given newPosition is not a valid position for any Unit.
	 *         |!isValidUnitPosition(getNewPosition())
	 */
	@Raw
	public void setNewPosition(double[] newPosition) throws IllegalArgumentException {
		if ((getWorld()!=null)&&(!getWorld().isValidUnitPosition(newPosition)))
			throw new IllegalArgumentException();
		this.newPosition = newPosition;
	}

	/*
	 * a variable that stores the position where the unit moves to next
	 */
	public double[] newPosition;

	/**
	 * A method that forms a bridge between the moveToAdjacent method in this class and the desired method in Facade. 
	 * It calls the moveToAdjacent method and makes the newPosition the final destination of the Unit.
	 * 
	 * @param  newPosition
	 * 		   The adjacent position that the unit needs to reach
	 * @effect If the given position is valid and the unit is not currently resting for his first hitpoint or walking, the moveToAdjacent
	 *         method is called with as argument the given newPosition + the current position of the unit.
	 * @post   If the given position is valid and the unit is not currently resting for his first hitpoint or walking,
	 *         the finalDestination is set equal to the given newPosition + the current position of the unit.
	 *         | new.finalDestination == (newPosition + this.getOccupiedCube())
	 * @throws IllegalArgumentException
	 * 		   the given position is not a valid position for any Unit
	 */
	public void moveToAdjacentFinal(double[] newPosition) throws IllegalArgumentException {
		if (!overrulesCurrentActivity(Status.MOVING)) {
			return;
		}
		int[] oldCube = getOccupiedCube();
		double[] position = {newPosition[0]+oldCube[0]+0.5,newPosition[1]+oldCube[1]+0.5,newPosition[2]+oldCube[2]+0.5};
		if ((getWorld()!=null)&&(! getWorld().isValidUnitPosition(position))) {
			throw new IllegalArgumentException
				("this is not a valid unit position");
		}
		moveToAdjacent(position);
	}
	
	/**
	 * A method that moves the Unit to the desired newPosition
	 * 
	 * @param   time
	 * 		    The time during which the unit moves
	 * @effect  If the unit is no longer allowed to sprint it stops sprinting
	 *          | if (isAllowedToSprint())
	 *          |   then sprint(time)
	 * @effect  If defaultBehaviour is enabled the unit has a 10% change to start sprinting if it is currently walking
	 *          | if (!isSprinting() && isAllowedToSprint() && hasDefaultBehaviour && Math.random() < 0.1)
	 *          |   then startSprinting()
	 * @effect  The orientation of the unit is set depending on his velocity in the x and y direction.
	 *          | setOrientation(atan2(velocity[1],velocity[0]))
	 * @post	If the unit reaches the newPosition in time, the new Position of the unit will be the position it wanted
	 * 			to reach. Else it will update it's given position with time*speed with the speed depending whether the
	 * 			unit is walking or sprinting
	 * 			|if speed*time >= distance + eps
	 * 			|	then new.getPosition() == this.newPosition
	 * 			|else
	 * 			|	then new.getPosition() == this.getPosition() + speed*time
	 */
	private void move(double time) {
		double speed;
		if (isSprinting()){
			if (isAllowedToSprint()) {
				sprint(time);
				speed = getSprintSpeed();
			}
			else {
				stopSprinting();
				speed = getWalkingSpeed();
			}
		}
		else {
			speed = this.getWalkingSpeed();
			if (isAllowedToSprint() && hasDefaultBehaviour()) {
				double startSprint = Math.random();
				if (startSprint<0.1) {
					startSprinting();
					speed = getSprintSpeed();
				}
			}
		}
		double normalizedDistance = Math.sqrt(Math.pow((this.getPosition()[0]-newPosition[0]),2) + Math.pow((this.getPosition()[1]-newPosition[1]),2) + Math.pow((this.getPosition()[2]-newPosition[2]),2));
		double[] velocity = {speed*(newPosition[0]-this.getPosition()[0])/normalizedDistance,speed*(newPosition[1]-this.getPosition()[1])/normalizedDistance,speed*(newPosition[2]-this.getPosition()[2])/normalizedDistance};
		setOrientation(Math.atan2(velocity[1],velocity[0]));
		if (Util.fuzzyGreaterThanOrEqualTo(speed*time, normalizedDistance)) {
			setPosition(newPosition);
			setCurrentActivity(Status.WAITING);
		}
		else {
			double [] updatePosition = {this.getPosition()[0]+velocity[0]*time,this.getPosition()[1]+velocity[1]*time,this.getPosition()[2]+velocity[2]*time};
			setPosition(updatePosition);
		}
	}
	/**
	 * This method finds the path to a far off destination by asking the world for a list of adjacent 
	 * positions that connect the unit's current position and it's target
	 * 
	 * @param  newPosition
	 *         The new position the unit wants to reach
	 * @effect If there exists a passable path towards the target, the unit asks the world to find this path and stores it.
	 *         | if (isValidUnitPosition(newPosition))
	 *         |    new.path = getWorld().findPath(getOccupiedCube(),newPosition);
	 * @throws	IllegalArgumentException
	 * 			The unit has no world or the target location is not a valid unit position
	 */
	public void moveTo(int[] newPosition) throws IllegalArgumentException {
		
		if (getWorld()==null)
			throw new IllegalArgumentException("Unit doesnt have a world yet");
		
		double[] truePosition = {0.5,0.5,0.5};
		for (int i=0;i<newPosition.length;i++) {
			truePosition[i] += newPosition[i];
		}
		if (! getWorld().isValidUnitPosition(truePosition)) {

			throw new IllegalArgumentException
				("this is not a valid unit position");
		}
		try {
			ArrayList<int[]> routeToFollow = getWorld().findPath(getOccupiedCube(),newPosition);
			this.path = routeToFollow;
		}
		catch(IllegalArgumentException e) {
		}
	}
	
	
	/**
	 * The path that this unit has to follow to its destination
	 */
	private ArrayList<int[]> path;
	
	
	/**
	 * The unit attempts to set its next target adjacent position to the next position in its path towards its final destination. 
	 * If it is not a valid position a new path towards the final target is sought. If no such path is found the current cube is 
	 * set as the final target. If the current cube is not a valid unit position the units looks for
	 * a cube below him that is a valid unit cube and sets it as the final target.
	 * 
	 * @effect  If the next position is a valid unit position, the next target position is set to this position
	 *         | if (isValidUnitPosition(nextPosition))
	 *         | then moveToAdjacent(path.remove(path.size()-1))
	 * @effect  If the next position is not a valid unit position, a new path is sought
	 *         | if (!misValidUnitPosition(nextPosition) && path exists)
	 *         |  then this.path = getWorld().findPath(getOccupiedCube(),path.remove(0))
	 * @effect  If the next position is not a valid unit position, and no new path could be found, the 
	 *          target position is set to the current centered position
	 *         | if (!misValidUnitPosition(nextPosition) && !path exists && isValidUnitPosition(getOccupiedCube()+0.5))
	 *         | then setNewPosition(getOccupiedCube()+0.5
	 * @effect  If the next position is not a valid unit position, and no new path could be found, and this centered position
	 *          is not a valid unit position, a new valid position is searched for below the current cube
	 *         | if (!misValidUnitPosition(nextPosition) && !path exists && !isValidUnitPosition(getOccupiedCube()+0.5))
	 *         | then setNewPosition(world.findAllowedPosition(getOccupiedCube()+0.5),false)
	 * @effect  If we still cannot find a valid position, we give up
	 * @throws ModelException
	 * 			The unit has no world yet
	 */
	private void nextMove2() throws ModelException{
		if (getWorld() == null)
			throw new ModelException("Unit doesnt have a world yet");
		int[] nextPosition = path.remove(path.size()-1);
		double[] nextPos = {0.5,0.5,0.5};
		for (int i = 0;i<nextPosition.length;i++) {
			nextPos[i] += nextPosition[i];
		}
		try {
			moveToAdjacent(nextPos);
		}
		catch (IllegalArgumentException e) {
			try {
				ArrayList<int[]> routeToFollow = getWorld().findPath(getOccupiedCube(),path.remove(0));
				this.path = routeToFollow;
			}
			catch(IllegalArgumentException | IndexOutOfBoundsException g) {
				this.path = new ArrayList<int []>();
				int[] newPosition = getOccupiedCube();
				try {
					setNewPosition(new double[]{newPosition[0]+0.5,newPosition[1]+0.5,newPosition[2]+0.5});
				}
				catch(IllegalArgumentException f) {
					try {
					setNewPosition(getWorld().findAllowedPosition(new double[]{newPosition[0]+0.5,newPosition[1]+0.5,newPosition[2]+0.5}, false));
					this.path = new ArrayList<int []>();
					}
					catch(IllegalArgumentException h) {
					}
				}
			}
		}
	}
	
	/**
	 * 
	 * @return true if the unit has reached the end of its path, false otherwise
	 */
	private boolean hasReachedEndOfPath() {
		return path.size() == 0;
	}
	
	/**
	 * We decide on which cube to move to next based on the current cube of the unit we are following
	 */
	private void nextFollow() {
		moveTo(getUnitToFollow().getOccupiedCube());
		try {
			nextMove2();
		} catch (ModelException e) {
		}
	}
	
	/**
	 * Return whether the unit can stop following
	 * 
	 * @return 	true if the unit to follow is dead or the unit has arrived
	 * 			|result == (UnitToFollow.isDead || hasArrived())
	 */
	private boolean canStopFollowing() {
		return (UnitToFollow.isDead || hasArrived());
	}
	
	/**
	 * 
	 * @return true if we are at an adjacent cube of the unit we are following, false otherwise
	 */
	private boolean hasArrived() {
		int[] thisCube = this.getOccupiedCube();
		int[] targetCube = UnitToFollow.getOccupiedCube();
		int dx = thisCube[0] - targetCube[0];
		int dy = thisCube[1] - targetCube[1];
		int dz = thisCube[2] - targetCube[2];
		if ( Math.abs(dx) <= 1 && Math.abs(dy) <= 1 && Math.abs(dz) <= 1) {
			return true;
		}
		return false;
	}
	
	/**
	 * follows the given unit
	 * @param	unit
	 * 			The unit that will be followed
	 * @post 	sets the given unit as the unit to follow
	 * 			|new.getUnitToFollow = unit
	 * @effect 	this unit moves to the position the given unit occupies
	 * 			|new.moveTo(unit.getOccupiedCube())
	 */
	public void follow(Unit unit) {
		setUnitToFollow(unit);
		moveTo(unit.getOccupiedCube());
	}
	
	/** 
	 * Return the UnitToFollow of this Unit.
	 *    A null reference is returned if this Unit is not following any Unit.
	 */
	@Basic @Raw
	public Unit getUnitToFollow() {
		return this.UnitToFollow;
	}

	/**
	 * Check whether this Unit can have the other Unit as its UnitToFollow
	 * 
	 *
	 * @param  other
	 *         The other Unit to check.
	 * @return True if the other Unit is not effective.
	 *       | if (other == null)
	 *       |   then result == true
	 *         Otherwise, false if this Unit or the other Unit
	 *         is terminated.
	 *       | else if ( this.isTerminated() || other.isTerminated() )
	 *       |   then result == false
	 *         Otherwise, true if and only if the units are in the same world
	 *       | else
	 *       |   result ==
	 *       |     this.getWorld() == other.getWorld()
	 */
	@Raw
	public boolean canHaveAsUnitToFollow(@Raw Unit other) {
		if (other == null)
			return true;
		else if ((other.isTerminated()) || (this.isTerminated()))
			return false;
		else
			return  (this.getWorld() == other.getWorld());
	}

	/** 
	 * Register the given Unit as the UnitToFollow of this Unit.
	 * 
	 * @param  Unit
	 *         The Unit to be registered as the UnitToFollow of this Unit.
	 * @post   The UnitToFollow of this Unit is the same as the given Unit.
	 *       | new.getUnitToFollow() == Unit
	 */
	@Raw
	private void setUnitToFollow(@Raw Unit Unit) {
		if(canHaveAsUnitToFollow(Unit))
			this.UnitToFollow = Unit;
	}

	/**
	 * Variable referencing the UnitToFollow of this Unit.
	 */
	private Unit UnitToFollow = null;
	
	/**
	 * Return 	True if the given Unit is working
	 * 			|return == ( this.getCurrentActivity() == Status.WORKING )
	 */
	public boolean isWorking() {
		return (getCurrentActivity() == Status.WORKING);
	}
	
	/**
	 * A method that advances the progress of the work that the Unit is doing by the given time. If the 
	 * unit is finished working it stops and starts waiting for its next task.
	 * 
	 * @param  time
	 *         The time with which to advance the work
	 * @effect If the Unit has not finished his work yet, the progress made during the given time is added to the total progress.
	 *         | setProgress(getProgress() + time*getStrength()/500)
	 * @effect if the Unit has finished its work, its progress is reset back to 0 using cleanState() and it starts waiting for its next task.
	 *         | if (getProgress() + time*getStrength()/500 > 1+ eps)
	 *         |    then cleanState(); setCurrentActivity(Status.WAITING)
	 *    
	 */
	
	private void work(double time) throws ModelException {
		if (this.getWorld() == null)
			throw new ModelException("Unit doesn't have a world yet");
		double newProgress = time * getStrength() / 500;
		if (Util.fuzzyGreaterThanOrEqualTo(getProgress() + newProgress,1)) {
			if (hasItem() && getWorld().getTerrainType(targetCube).isPassable()) {
				dropItem(targetCube);
			}
			else {
				Boulder aBoulder = null;
				Log aLog = null;
				Position positionObject = new Position(targetCube);
				if ( getWorld().getItemsAtPos(targetCube) != null) {
					List<Item> allItemsAtPos = getWorld().getItemsAtPos(targetCube);
					for (Item item : allItemsAtPos) {
						if (item instanceof Boulder && item.isFalling() == false) {
							aBoulder = (Boulder) item;
						}
						else if (item instanceof Log && item.isFalling() == false) 
							aLog = (Log) item;
					}
				}
				if ((getWorld().getTerrainType(targetCube) == TerrainType.WORKSHOP) && aLog != null && aBoulder != null && !hasItem()) {
					getWorld().removeItem(aLog, positionObject);
					getWorld().removeItem(aBoulder, positionObject);
					if (getWeight() < MAXSTRENGTH) 
						setWeight(getWeight() + 1,false);
					if (getToughness() < MAXTOUGHNESS)
						setToughness(getToughness() + 1,false);
				}
				else if (aBoulder != null && !hasItem()) {
					pickUpItem(aBoulder);
				}
				else if (aLog != null && !hasItem()) {
					pickUpItem(aLog);
				}
				else if (getWorld().getTerrainType(targetCube) == TerrainType.TREE) {
					getWorld().destroySolidBlock(targetCube);
				}
				else if (getWorld().getTerrainType(targetCube) == TerrainType.ROCK) {
					getWorld().destroySolidBlock(targetCube);
				}
				else {
					//everything failed, return without experience
					setCurrentActivity(Status.WAITING);
					return;
				}
			}
			increaseExperiencePoints(10);
			if (overrulesCurrentActivity(Status.WAITING)) {
				setCurrentActivity(Status.WAITING);
			}
			return;
		}
		setProgress(getProgress()+newProgress);
	}
	
	/**
	 * A method that initiates working if the Unit is not currently moving or resting for its first hitpoint.
	 * 
	 * @effect If the Unit is not currently moving or resting for its first hitpoint, temporary instance 
	 *         variables are reset and the state of the Unit is changed to working.
	 *         |if (!isMoving() && !(isResting() && !hasRecoveredHitpoint()))
	 *         |    then cleanState(); setCurrentActivity(Status.WORKING)
	 */
	public void work(int[] targetCube1) throws IllegalArgumentException {
		if (!overrulesCurrentActivity(Status.WORKING)) {
			return;
		}
		if(!isNextTo(targetCube1)) {
			throw new IllegalArgumentException("that cube is too far away");
		}
		setCurrentActivity(Status.WORKING);
		this.targetCube = targetCube1;
	}
	
	int[] targetCube;
	
	/**
	 * Checks whether the given cube position is next to this unit
	 * 
	 * @param 	targetCube1
	 * 			the cube to be checked
	 * @return	true if the given cube is next to this unit
	 */
	private boolean isNextTo(int[] targetCube1) {
		int[] thisCube = getOccupiedCube();
		double dx = targetCube1[0] - thisCube[0];
		double dy = targetCube1[1] - thisCube[1];
		double dz = targetCube1[2] - thisCube[2];
		if ( Math.abs(dx) <= 1 && Math.abs(dy) <= 1 && Math.abs(dz) <= 1) {
			return true;
		}
		return false;
	}
	
	/**
	 * This unit will attack a random enemy
	 * @post	The unit will attack a random enemy
	 * 			|new.fight(randomEnemy)
	 * @throws 	IllegalArgumentException
	 * 			if the unit has no world
	 */
	public void attackRandomEnemy() throws IllegalArgumentException {
		if (getWorld() == null)
			throw new IllegalArgumentException("Unit doesnt have a world yet");
		List<Unit> allAdjacentEnemies = getWorld().getAdjacentEnemies(this);
		if (!allAdjacentEnemies.isEmpty()) {
			Random randomizer = new Random();
			Unit enemy = allAdjacentEnemies.get(randomizer.nextInt(allAdjacentEnemies.size()));
			fight(enemy);
		}
	}
	
	/**
	 * The unit will fight the given unit
	 * 
	 * @param 	enemy
	 * 			the unit to fight with
	 * @post	The unit will attack the given enemy
	 * 			|new.attack(enemy)
	 * @throws 	IllegalArgumentException
	 * 			if this unit has no world or the given unit isn't an enemy
	 * 			|(getWorld==null || !getWorld.canAttackUnit(this, enemy))
	 */
	public void fight(Unit enemy) throws IllegalArgumentException{
		if (getWorld()==null)
			throw new IllegalArgumentException("Unit doesnt have a world yet");
		if ( getWorld().canAttackUnit(this, enemy)) {
			if (attack(enemy)) {
				enemy.setAttackFlag(true);
				enemy.addEnemy(this);
			}
		}
		else
			throw new IllegalArgumentException("you can not attack that unit");
	}
	
	/**
	 * Return 	True if the given Unit is attacked
	 * 			|return == ( this.getAttackFlag() == true )
	 */
	public boolean isAttacked() {
		return (this.getAttackFlag() == true);
	}

	
	/**
	 * A method that advances the attack progress with the given time. If the attacking process is 
	 * finished the Unit starts waiting for its next task. 
	 * 
	 * @param  time
	 *         The time with which to advance the attack progress
	 * @effect If the Unit has not finished attacking yet, the progress mad during the given time is added to the total progress.
	 *         | setProgress(getProgress() + time)
	 * @effect If the Unit has finished attacking, its progress is reset back to 0 using cleanState() and it starts waiting for its next task.
	 *         | if (getProgress() + time > 1 + eps)
	 *              then cleanState(); setCurrentActivity(Status.WAITING)
	 */
	private void attack(double time) {
		if (Util.fuzzyGreaterThanOrEqualTo(getProgress()+time,1)){
			// the time of war has passed, it's time to do something else
			setCurrentActivity(Status.WAITING);
			increaseExperiencePoints(20);
			return;
		}
		setProgress(getProgress()+time);
	}
	
	/**
	 * A method that initiates attacking if the Unit is not currently moving or resting for its first hitpoint.
	 * 
	 * @param enemy
	 *        The enemy that the Unit attacks.
	 * @effect If the Unit is not currently moving or resting for its first hitpoint, the orientation of the 
	 *         Unit is set to face the enemy, temporary instance variables are reset and the sate of the Unit is changed to attacking.
	 *         | if (!isMoving() && !(isResting() && !hasRecoveredHitpoint()))
	 *         |    then changeOrientation(enemy); cleanState(); setCurrentActivity(Status.ATTACKING)

	 * @return true if the Unit can attack the enemy, thus when it is not moving or resting for its first hitpoint.
	 *         | result == (!isMoving() && !(isResting() && !hasRecoveredHitpoint()))
	 */
	public boolean attack(Unit enemy) {
		if (!overrulesCurrentActivity(Status.FALLING)) {
			return false;
		}
		changeOrientation(enemy);
		setCurrentActivity(Status.ATTACKING);
		return true;
	}

	/**	
	 * Return the progress of this Unit.
	 */
	@Basic @Raw
	public double getProgress() {
		return this.progress;
	}
	
	/**
	 * Check whether the given progress is a valid progress for
	 * any Unit.
	 *  
	 * @param  progress
	 *         The progress to check.
	 * @return 
	 *       | result == (progress <= 1 +/- eps) && (progress >= 0 +/- eps)
	*/
	public static boolean isValidProgress(double progress) {
		return ( Util.fuzzyLessThanOrEqualTo(progress, 1) && Util.fuzzyGreaterThanOrEqualTo(progress, 0) && !Double.isNaN(progress));
	}
	
	/**
	 * Set the progress of this Unit to the given progress.
	 * 
	 * @param  progress
	 *         The new progress for this Unit.
	 * @post   The progress of this new Unit is equal to
	 *         the given progress.
	 *       | new.getProgress() == progress
	 * @throws IllegalArgumentException
	 *         The given progress is not a valid progress for any
	 *         Unit.
	 *       | ! isValidProgress(getProgress())
	 */
	@Raw
	private void setProgress(double progress) 
			throws IllegalArgumentException {
		if (! isValidProgress(progress))
			throw new IllegalArgumentException();
		this.progress = progress;
	}
	
	/**
	 * Variable registering the progress of this Unit.
	 */
	private double progress;

	/**
	 * 
	 * returns true if the Unit is currently attacking and false otherwise
	 *      | result == (getCurrentActivity() == Status.ATTACKING)
	 */
	public boolean isAttacking() {
		return (getCurrentActivity() == Status.ATTACKING);
	}
	
	/**
	 * This method runs the defense routine for the Unit with a given enemy. The Unit might 
	 * block the attack, block, or get hurt for a certain amount of hitpoints.
	 * 
	 * @param enemy
	 * @effect The Unit changes his orientation to face the enemy.
	 *         | changeOrientation(enemy)
	 * @effect The Unit tries to dodge the attack, if he succeeds the method returns without further effect
	 *         | if (dodge(enemy) == true)
	 *         |    then return;
	 * @effect The Unit tries to block the attack if he fails to dodge, if he succeeds the method returns without further effect
	 *         | if (block(enemy) == true)
	 *         |    then return;
	 * @effect If the unit fails to dodge or block the attack he takes damage equal to the 
	 *         enemy strength divided by 10. If the Unit has less hitpoints than this it dies.
	 *         |if (getHitpoints() > enemy.strength/10 && block(enemy) == false && dodge(enemy) == false)
	 *         |    then consumeHitpoints(enemy.strength/10)
	 *         |if (getHitpoints() <= enemy.strength/10 && block(enemy) == false && dodge(enemy) == false)
	 *         |    then setHitpoints(0); this.isDead == true
	 */
	public void defend(Unit enemy) {
		changeOrientation(enemy);
		boolean dodgeSuccess = dodge(enemy);
		if (dodgeSuccess == true) {
			increaseExperiencePoints(20);
			return;
		}
		boolean blockSuccess = block(enemy);
		if (blockSuccess == true) {
			increaseExperiencePoints(20);
			return;
		}
		consumeHitpoints(enemy.strength/10);
	}
	
	/**
	 * a variable to store whether the Unit is dead or not. Not yet implemented.
	 */
	private boolean isDead = false;
	
	/**
	 * True if the hitpoints of the unit are 0
	 * 
	 * @return true if this.isDead == true
	 */
	public boolean isDead(){
		return this.isDead;
	}
	
	/**
	 * The unit dies
	 * 
	 * @post The unit is dead
	 * @post If the unit had a item, the item is dropped at the cube he occupied.
	 */
	public void die() {
		isDead = true;
		setCurrentActivity(Status.WAITING);
		if (hasItem()) {
			try{
			dropItem(getOccupiedCube());
			}
			catch (ModelException e){
				//never happens
			}
		}
	}
	
	/**
	 * Changes the orientation of the Unit so that it faces its enemy.
	 * @param enemy
	 *        The enemy to face
	 * @effect Sets the orientation of the Unit so that it faces its enemy
	 *         | setOrientation(atan2(enemy.position[1]-this.position[1],enemy.position[0]-this.position[0]));
	 */
	private void changeOrientation(Unit enemy) {
		setOrientation(Math.atan2(enemy.position[1]-this.position[1],enemy.position[0]-this.position[0]));
	}
	
	/**
	 * This method returns whether the Unit managed to dodge the attack of his enemy. If he succeeds he moves to a random adjacent tile.
	 * 
	 * @param enemy
	 *        The enemy whose attack needs to be dodged
	 * @effect If the Unit manages to dodge the attack it moves to an adjacent tile.
	 *        | if (Math.random() <= 0.2*this.Agility()/enemy.getAgility)
	 *        |    then moveToRandomAdjacent();
	 * @return whether the Unit managed to dodge the attack or not.
	 *        | result == (Math.random() <= 0.2*this.getAgility()/enemy.getAgility())
	 */
	private boolean dodge(Unit enemy) {
		double chanceOfDodge = 0.2 * this.getAgility()/enemy.getAgility();
		if (Math.random() <= chanceOfDodge) {
			moveToRandomAdjacent();
			return true;
		}
		return false;
	}
	/**
	 * This method moves the Unit to a random valid adjacent tile.
	 * @effect The position of the Unit and its finalDestination change to a random valid adjacent position
	 *         | if (isValidUnitPosition({this.position[0] +/- 1, this.position[1] +/- 1, this.position[2]}))
	 *         |    then setPosition(newPosition); setFinalDestination(newPosition)
	 */
	private void moveToRandomAdjacent() throws IllegalArgumentException {
		if (getWorld() == null)
			throw new IllegalArgumentException("Unit doesnt have a world");

		double newXPosition = this.position[0] + (Math.random()*2-1);
		double newYPosition = this.position[1] + (Math.random()*2-1);
		double[] newPosition = {newXPosition,newYPosition,this.position[2]}; 
		if (getWorld().isValidUnitPosition(newPosition)) {
			setPosition(newPosition);
			this.path = new ArrayList<int []>();
		}
		else {
			moveToRandomAdjacent();
		}
	}
	
	/**
	 * This method returns whether the Unit managed to block the attack of his enemy.
	 * 
	 * @param enemy
	 *        The enemy that the Unit needs to Block.
	 * @return whether the Unit managed to block the attack or not.
	 *         | result == (Math.random() <= 0.25 * (this.getAgility()+this.getStrength())/(enemy.getAgility()+enemy.getStrength()))
	 */
	private boolean block(Unit enemy) {
		double chanceOfBlock = 0.25 * (this.getAgility()+this.getStrength())/(enemy.getAgility()+enemy.getStrength());
		return (Math.random() <= chanceOfBlock);
	}
	

	/**
	 * Return the attackFlag of this Unit.
	 */
	@Basic @Raw
	public boolean getAttackFlag() {
		return this.attackFlag;
	}
	
	/**
	 * Set the attackFlag of this Unit to the given attackFlag.
	 * 
	 * @param  attackFlag
	 *         The new attackFlag for this Unit.
	 * @post   The attackFlag of this new Unit is equal to
	 *         the given attackFlag.
	 *       | new.getAttackFlag() == attackFlag
	 */
	@Raw
	public void setAttackFlag(boolean attackFlag) {
		this.attackFlag = attackFlag;
	}
	
	/**
	 * Variable registering the attackFlag of this Unit.
	 */
	private boolean attackFlag;
	

	/**
	 * Return the enemies of this Unit.
	 */
	@Basic @Raw
	public List<Unit> getEnemies() {
		return this.enemies;
	}
	
	/**
	 * Set the enemies of this Unit to the given enemies.
	 * 
	 * @param  enemies
	 *         The new enemies for this Unit.
	 * @post   The enemies of this new Unit is equal to
	 *         the given enemies.
	 *       | new.getenemies() == enemies
	 */
	@Raw
	public void setEnemies(List<Unit> enemies) {
		List<Unit> newEnemies = new ArrayList<Unit>();
		for (Unit enemy : enemies) {
			if (enemy != null) {
				newEnemies.add(enemy);
			}
		}
		this.enemies = newEnemies;
	}
	
	
	/**
	 * adds an enemy unit to the list of enemies of this unit
	 * @param 	enemy
	 * 			The unit to be added
	 * @post	The given enemy is now an enemy of this unit
	 */
	public void addEnemy(Unit enemy) {
		if (enemy != null) {
			getEnemies().add(enemy);
		}
	}
	
	/**
	 * Variable registering the enemy of this Unit.
	 * 
	 * @invar	Each unit in enemies must be an enemy of this unit.
	 */
	private List<Unit> enemies;
	
	/**
	 * Changes the state of the unit to resting if the unit isn't moving
	 * 
	 * @effect	The status of the unit is changed to status.RESTING
	 */
	public void rest() {
		if (getCurrentActivity() == Status.MOVING) {
			return;
		}
		this.setCurrentActivity(Status.RESTING);
	}
	
	/**
	 * 
	 * This method lets the unit rest for a given time. If the time crosses a tick the unit restores some 
	 * hitpoints if he is out of hitpoints or else restores some stamina. If the received hitpoints or stamina 
	 * would put the Unit above the maximum, the hitpoints or stamina are set equal to the maximum. If both 
	 * the stamina and hitpoints are completely filled the Unit stops resting and starts waiting for its next command.
	 * 
	 * @param time
	 *        The time during which to rest
	 * @effect The timeSpendResting is increased by the given time.
	 *         | setTimeSpendResting(getTimeSpendResting()+time)
	 * @effect If the time crosses a tick and the Unit did not yet reach its maximum hitpoints, it recovers some hitpoints. 
	 *         If the recovered hitpoints would put it above the maximum its hitpoints are set equal to the maximum.
	 *         | if ((timeUntilTick-time <= 0+eps) && (getHitpoints != getMaxHitpoints +/- eps)) 
	 *         |     if (new.getHitpoints < this.getMaxHitpoints
	 *         |          then setHitpoints(getHitpoints() + addedHitpoints)
	 *         |     else
	 *         |          then setHitpoints(getMaxHitpoints())
	 * @effect If the time crosses a tick and the Unit did not yet reach its maximum stamina while it has already 
	 *         reached its maximum hitpoints, it recovers some stamina. 
	 *         If the recovered stamina would put it above the maximum its stamina are set equal to the maximum, 
	 *         temporary variables are cleaned and the current activity is set to waiting.
	 *         | if ((timeUntilTick-time <= 0+eps) && (getStamina != getMaxStamina +/- eps)) 
	 *         |     if (new.getStamina < this.getMaxStamina
	 *         |          then setStamina(getStamina() + addedStamina)
	 *         |     else
	 *         |          then setStamina(getMaxStamina()); cleanState(); setCurrentActivity(Status.WAITING)
	 */
	private void rest(double time) {
		double timeUntilTick = getTimeSpendResting() % 0.2;
		setTimeSpendResting(getTimeSpendResting()+time);
		if (Util.fuzzyLessThanOrEqualTo(timeUntilTick-time, 0)) {
			if (Util.fuzzyEquals(getHitpoints(), getMaxHitpoints())) {
				double addedStamina = getStaminaPerTick();
				if (Util.fuzzyGreaterThanOrEqualTo(getStamina()+addedStamina, getMaxStamina())) {
					setStamina(getMaxStamina());
					setCurrentActivity(Status.WAITING);
					return;
				}
				else {
					setStamina(getStamina()+addedStamina);
				}
			}
			else {
				double addedHitpoints = getHitpointsPerTick();
				if (Util.fuzzyGreaterThanOrEqualTo(getHitpoints()+addedHitpoints, getMaxHitpoints())) {
					setHitpoints(getMaxHitpoints());
					return;
				}
				else {
					setHitpoints(getHitpoints() + addedHitpoints);
				}
			}
		}
	}

	/**
	 * 
	 * @return whether the Unit is currently resting
	 *         | result == (getCurrentActivity() == Status.RESTING)
	 */
	public boolean isResting() {
		return (getCurrentActivity() == Status.RESTING);
	}
	/**
	 * Return the timeSpendResting of this Unit.
	 */
	@Basic @Raw
	private double getTimeSpendResting() {
		return this.timeSpendResting;
	}
	
	/**
	 * Check whether the given timeSpendResting is a valid timeSpendResting for
	 * any Unit.
	 *  
	 * @param  timeSpendResting
	 *         The timeSpendResting to check.
	 * @return 
	 *       | timeSpendResting > 0 + eps
	*/
	private static boolean isValidTimeSpendResting(double timeSpendResting) {
		return (Util.fuzzyGreaterThanOrEqualTo(timeSpendResting, 0) && !Double.isNaN(timeSpendResting));
	}
	
	/**
	 * Set the timeSpendResting of this Unit to the given timeSpendResting.
	 * 
	 * @param  timeSpendResting
	 *         The new timeSpendResting for this Unit.
	 * @post   If the given timeSpendResting is a valid timeSpendResting for any Unit,
	 *         the timeSpendResting of this new Unit is equal to the given
	 *         timeSpendResting.
	 *       | if (isValidTimeSpendResting(timeSpendResting))
	 *       |   then new.getTimeSpendResting() == timeSpendResting
	 */
	@Raw
	private void setTimeSpendResting(double timeSpendResting) {
		if (isValidTimeSpendResting(timeSpendResting))
			this.timeSpendResting = timeSpendResting;
	}
	/**
	 * 
	 * @return whether the Unit has already rested for a time equivalent to the time needed to recover one hitpoint.
	 *        | result == (getHitpointsPerTick() *Math.floor(timeSpendResting/TIME_PER_TICK) = 1 +/- eps)
	 */
	private boolean hasRecoveredHitpoint() {
		return Util.fuzzyGreaterThanOrEqualTo(getHitpointsPerTick() * Math.floor(timeSpendResting/TIME_PER_TICK),1);
	}
	
	/**
	 * Variable registering the timeSpendResting of this Unit.
	 */
	private double timeSpendResting;
	
	/**
	 * Static variable describing the time between two increases in hitpoints while the Unit is resting
	 */
	private static double TIME_PER_TICK = 0.2;
	
	/**
	 * 
	 * @return The amount of hitpoints that the Unit recovers per tick while resting
	 */
	private double getHitpointsPerTick() {
		return (double) toughness/200;
	}
	/**
	 * @return The amount of stamina that the Unit recovers per tick while resting
	 */
	private double getStaminaPerTick() {
		return (double) toughness/100;
	}
	
	public void startFalling() {
		setCurrentActivity(Status.FALLING);
	}
	/**
	 * Return whether this unit is currently falling
	 *         | result == (getCurrentActivity() == Status.FALLING)
	 */
	@Basic @Raw
	public boolean isFalling() {
		return (getCurrentActivity() == Status.FALLING);
	}
	
	/**
	 * Set the isFalling of this unit to the given isFalling.
	 * 
	 * @param  isFalling
	 *         The new value for isFalling.
	 * @post   the isFalling of this new unit is equal to the given isFalling.
	 */
	@Raw
	public void fall() {
		setCurrentActivity(Status.FALLING);
	}
	
	public void fall(double time) {
		double heightDiff = position[2] - newPosition[2];
		int amountOfZLevels = (int) (Math.floor(FALLING_SPEED*time) + Math.floor(position[2]) - Math.floor(position[2]-((FALLING_SPEED*time)%1)));
		if (heightDiff < FALLING_SPEED*time) { 
			position[2] = newPosition[2];
			setCurrentActivity(Status.WAITING);
		}
		else {
			position[2] = position[2] - FALLING_SPEED*time;
		}
		consumeHitpoints(amountOfZLevels*10);
	}
	
	public final static double FALLING_SPEED = 3;
	/**
	 * This method cleans up temporary variables related to the state it is currently in. It should be called when moving from one state to the next.
	 * @effect If the unit is currently resting, the timeSpendResting is set to 0
	 *         | setTimeSpendResting(0)
	 * @effect If the unit is currently working, the progress is set to 0
	 *         | setProgress(0)	 
	 * @effect If the unit is currently attacking, the progress is set to 0
	 *         | setProgress(0)	 
	 */
	private void cleanState() {
		switch(getCurrentActivity()) {
			case RESTING:
				setTimeSpendResting(0);
				break;
			case WORKING:
				setProgress(0);
				this.targetCube = null;
				break;
			case ATTACKING:
				setProgress(0);
				break;
			case MOVING:
				this.walkingSpeed = 0;
				break;
			case FOLLOWING:
				this.walkingSpeed = 0;
				break;
			case WAITING:
				break;
			case FALLING:
				break;
		}
	}
	/**
	 * This method initiates the default behaviour of the Unit. The method chooses one action at random: The unit has a 33% chance to move to a random position 
	 * in the world, a 33% chance to start working and a 33% chance to start resting if its stamina and hitpoints are not completely filled. 
	 * @effect In 33% of the cases where the Unit can still recover hitpoints or stamina and in 50% of the cases where it cannot, the Unit initiates moving to a random position.
	 *         | if ((whichAction) < 1/3) && (!(getHitpoints() == getMaxHitpoints() +/- eps) || !(getStamina() == getMaxStamina() +/- eps))
	 *         |    then moveToRandomPosition()
	 * @effect In 33% of the cases where the Unit can still recover hitpoints or stamina and in 50% of the cases where it cannot, the Unit initiates working.
	 *         | if ((1/3 < whichAction < 2/3) && (!(getHitpoints() == getMaxHitpoints() +/- eps) || !(getStamina() == getMaxStamina() +/- eps)))
	 *         |    then work()
	 * @effect In 33% of the cases where the Unit can still recover hitpoints or stamina the Unit initiates resting.
	 *         | if ((2/3 < whichAction) && (!getHitpoints() == getMaxHitpoints() +/- eps) || !(getStamina() == getMaxStamina() +/- eps))
	 *         |    then rest()
	 */
	private void defaultBehaviour(double time) throws ModelException{
		if (getTask() == null) {
			Task taskToExecute = getFaction().getScheduler().getHighestPriorityTask();
			if (taskToExecute != null){
				try {
					taskToExecute.linkUnit(this);
				} catch (ModelException e) {
					defaultBehaviour(time);
					return;
				}
			}
			else {
				double whichAction = Math.random();
				if (whichAction < (double) 1/4)
					moveToRandomPosition();
				else if (whichAction < (double) 2/4)
					work(selectRandomAdjacentCube());
				else if (whichAction < (double) 3/4)
					attackRandomEnemy();
				else if (Util.fuzzyEquals(getHitpoints(), getMaxHitpoints()) && Util.fuzzyEquals(getStamina(), getMaxStamina())) {
					defaultBehaviour(time);
				}
				else
					rest();
				return;
			}
		}
		try{
			if (getTask().getActivity().isDone()) {
				getTask().terminate();
			}
			else {
				boolean hasRealWork = false;
				while(time>0 && !hasRealWork) {
					hasRealWork = getTask().getActivity().executeStatement();
					time -= 0.001;
				}
			}
		}
		catch(IllegalArgumentException e) {
			returnTask();
		}
		
	}
	
	/**
	 * Replaces the task in each scheduler with a lower priority
	 */
	private void returnTask() {
		getTask().getActivity().Reset();
		Set<Scheduler> schedulers = getTask().getSchedulers();
		getTask().removeSchedulersBinding();
		getTask().setPriority(getTask().getPriority()-1);
		for(Scheduler scheduler : schedulers)
			try {
				scheduler.addTask(getTask());
			} catch (ModelException e1) {
				e1.printStackTrace();
			}
		getTask().loseUnit();
	}
	/**
	 * 
	 * @return A random adjacent cube of this unit
	 */
	private int[] selectRandomAdjacentCube() {
		int[] targetCube = getOccupiedCube().clone();
		Random rand = new Random();
		int randomNum = rand.nextInt(7)+1;
		switch(randomNum) {
			case 1:
				targetCube[0] +=1;
				break;
			case 2:
				targetCube[0] -=1;
				break;
			case 3:
				targetCube[1] +=1;
				break;
			case 4:
				targetCube[1] -=1;
				break;
			case 5:
				targetCube[2] +=1;
				break;
			case 6:
				targetCube[2] -=1;
				break;
			case 7:
				break;
		}
		return targetCube;
	}

	/**
	 * This method initiates moving to a random position
	 * @effect The Unit initiates moving to a random position
	 *        | moveTo(target)
	 */
	private void moveToRandomPosition() throws IllegalArgumentException{
		if (getWorld() == null)
			throw new IllegalArgumentException("Unit doenst belong to a world");
		int[] target = getWorld().selectRandomPosition();
		moveTo(target);
	}

	
	/**
	* @post	The DefaultBehaviour of the unit will be activated
	* 			|this.hasDefaultBehaviour == true
	*/
	public void startDefaultBehaviour() {
		setDefaultBehaviour(true);
	}
	/**
	* @post	The DefaultBehaviour of the unit will be deactivated
	* 			|this.hasDefaultBehaviour == false
	*/
	public void stopDefaultBehaviour() {
		setDefaultBehaviour(false);
	}

	/**
	 * Return the defaultBehaviour of this Unit.
	 */
	@Basic @Raw
	public boolean hasDefaultBehaviour() {
		return this.defaultBehaviour;
	}
	
	
	/**
	 * Set the defaultBehaviour of this Unit to the given defaultBehaviour.
	 * 
	 * @param  defaultBehaviour
	 *         The new defaultBehaviour for this Unit.
	 * @post   The defaultBehaviour of this new Unit is equal to the given
	 *         defaultBehaviour.
	 *       | new.hasDefaultBehaviour() == defaultBehaviour
	 */
	@Raw
	public void setDefaultBehaviour(boolean defaultBehaviour) {
		this.defaultBehaviour = defaultBehaviour;
	}

	/**
	 * Variable registering the defaultBehaviour of this Unit.
	 */
	private boolean defaultBehaviour;
	
	
	
	/**
	 * 
	 * @return Returns the Faction of the Unit
	 */
	@Basic @Raw
	public Faction getFaction(){
		return this.faction;
	}
	
	/**
	 * Checks whether this unit can have the given faction as
	 * its faction
	 * @param 	faction
	 * 			The faction to be checked
	 * @return	True if the faction is null or the faction can have this unit as a unit.
	 * 			|(faction == null || faction.canHaveAsUnit(this))
	 */
	@Raw
	public boolean canHaveAsFaction(@Raw Faction faction){
		return (faction == null || faction.canHaveAsUnit(this));
	}
	
	/**
	 * Sets the faction of the unit to the given faction if it's a valid faction
	 * @param 	faction
	 * 			The new faction of the unit
	 * @pre		The faction must be a valid faction for this unit
	 * 			|canHaveAsFaction(faction)
	 * @post	The faction of this unit is set to the given faction
	 * 			|this.getFaction() == faction
	 */
	@Raw
	private void setFaction(@Raw Faction faction){
		assert canHaveAsFaction(faction);
		this.faction = faction;
	}
	
	
	/**
	 * Changes the faction of this unit
	 * 
	 * @param 	faction
	 * 			The new faction of this unit
	 * @post	The new faction of this unit is set to the given faction
	 * 			|this.getFaction() = faction
	 * @throws 	IllegalArgumentException
	 * 			if the faction is not a valid faction of this unit
	 * 			or if this unit isn't already attached to the given 
	 * 			faction
	 * 
	 */
	void transferFaction (Faction faction) throws IllegalArgumentException{
		if (!canHaveAsFaction(faction) || faction == null)
			throw new IllegalArgumentException("not a valid unit-faction relation");
		if (!faction.unitInFaction(this))
			throw new IllegalArgumentException("not registered in faction yet");
		
		setFaction(faction);

	}
	
	/**
	 * @return	True if this unit has a faction
	 * 			|this.getFaction() != null	
	 */
	@Raw
	public boolean hasFaction(){
		return this.getFaction() != null;
	}
	
	/**
	 * @return	True if this unit has a proper faction.
	 * 			So if the unit has a faction and there is
	 * 			a bidirectional association between the faction
	 * 			and this unit
	 * 			|(hasFaction && getFaction().unitInFaction(this))
	 */
	@Raw
	public boolean hasProperFaction(){
		if (!hasFaction())
			return false;
		if (!getFaction().unitInFaction(this))
			return false;
		return canHaveAsFaction(getFaction());
	}
	
	/**
	 * Variable registering the Faction of the Unit
	 */
	private Faction faction;
	
	
	/**
	 * The item attached to this unit will lose it's attachment
	 * 
	 * @param 	position
	 * 			The position where the item will be dropped
	 * @post	The unit no longer has an item
	 * 			|!this.hasItem
	 * @post	The item will be added back to the units world at
	 * 			the given position
	 * 			|world.getItemsAtPos(position).contains(this.getItem())
	 */
	void dropItem(int[] position) throws ModelException {
		if (getWorld() == null)
			throw new ModelException("Unit doesnt have a world");
		getWorld().placeItem(item, new Position(position));
		this.item.loseUnit();
		setWeight(getWeight(),false);
		
	}
	
	/**
	 * The unit picks an item up
	 * 
	 * @param 	item
	 * 			The item to be picked up
	 * @post	The item is attached to the unit
	 * 			|this.getItem() == item
	 */
	void pickUpItem(Item item) {
		try {
			if (getWorld() == null)
				throw new IllegalArgumentException("Unit has no world");
			item.receiveUnit(this);
			Position positionObject = new Position(getWorld().getOccupiedCube(item.position));
			getWorld().removeItem(item, positionObject);
			setWeight(getWeight(),false);
		}
		catch (IllegalArgumentException e) {
		}
	}
	/**
	 * 
	 * @return Returns the item of the unit
	 */
	@Basic @Raw
	public Item getItem(){
		return this.item;
	}
	
	/**
	 * 
	 * @param 	item
	 * 			The item the unit obtains
	 * @pre		the given item must be a valid item
	 * 			|canHaveAsItem(item)
	 * @post	The item of this unit will be set to the given item
	 * 			|new.getItem() == item
	 */
	@Raw
	private void setItem(Item item){
		assert canHaveAsItem(item);
		this.item = item;
	}
	/**
	 * 
	 * @param item
	 * 			the item the unit picks up
	 */
	@Raw
	void transferItem(@Raw Item item) throws IllegalArgumentException{		
		if (item == null && this.hasItem() && getItem().getUnit()==null){
			setItem(null);
			return;
		}
		if (!canHaveAsItem(item) || item == null)
			throw new IllegalArgumentException("Not a valid item");
		if (hasItem())
			throw new IllegalArgumentException("the unit already has an item");
		if(item.getUnit()!=this)
			throw new IllegalArgumentException("not a valid bidirectional association");
		setItem(item);
	}
	
	
	/**
	 * Checks of the unit can carry the item
	 * @param item
	 * 			item the unit wants to carry
	 * @return true if the item can be carried by the unit and the item already references the
	 * 			the unit
	 * 			|item.canHaveAsUnit(this) && item.getUnit() == this
	 */
	@Raw
	public boolean canHaveAsItem(@Raw Item item){
		return (item == null || (item.canHaveAsUnit(this) && item.getUnit()== this));
	}
	
	/**
	 * 
	 * @return True if the unit has an item
	 */
	@Raw
	public boolean hasItem(){
		return getItem() != null;
	}
	/**
	 * 
	 * @return true if the unit has a proper item attached to it
	 */
	@Raw
	public boolean hasProperItem(){
		return (canHaveAsItem(getItem()) && (getItem()==null || getItem().getUnit()==this));
	}
	
	
	private Item item;
	
	/**
	 * 
	 * @param 	world
	 * 			the world this unit lives in.
	 * @pre		The unit can have this world as a world.
	 * 			|canHaveAsWorld(world)
	 * @post	The world of this unit will be set to the given world
	 * 			|new.getWorld() == world
	 */
	private void setWorld(World world){
		//assert canHaveAsWorld(world);
		this.world = world;
	}
	
	
	/**
	 * 
	 * @return Returns the world of the unit.
	 */
	public World getWorld(){
		return this.world;
	}
	
	/**
	 * Checks if the unit can have the given world as world
	 * @param 	world
	 * 			the world that the unit can belong to
	 * 
	 * @return 	true if the world is null or the world can have this unit as unit
	 * 			|world == null || world.canHaveAsUnit(this)
	 */
	public boolean canHaveAsWorld(World world){
		return 	(getWorld() == null || getWorld().canHaveAsUnit(this));
				
	}
	
	
	/**
	 * Gives the unit a proper world if the unit doesn't have a world yet
	 * @param 	world
	 * 			the world the unit will get
	 * @post	The world of the unit will be set to the given world
	 * 			|new.getWorld() == world
	 * @throws 	IllegalArgumentException
	 * 			The unit can't have the given world as world or already has a world
	 */
	void transferWorld(World world) throws IllegalArgumentException{
		if (world == null)
			throw new IllegalArgumentException("Can't add null as a given world");
		if (!canHaveAsWorld(world))
			throw new IllegalArgumentException("Unit can't have the world as world");
		if (this.getWorld()!=null)
			throw new IllegalArgumentException("The unit already has a world");
		if (!world.hasAsUnit(this))
			throw new IllegalArgumentException("The bidirectional association isn't made");
			
		setWorld(world);
	}
	/**
	 * Checks if the unit has a proper world assigned to it
	 * @return 	true if the unit can have the given world as it's world 
	 * 			and the units world is either null or the world contains the 
	 * 			given unit
	 * 			|canHaveAsWorld(this.getWorld()) && (this.getWorld == null || this.getWorld.hasAsUnit(this))
	 * 			
	 */
	public boolean hasProperWorld(){
		return (canHaveAsWorld(this.getWorld())&&(this.getWorld()==null||this.getWorld().hasAsUnit(this)));
	}
	
	private World world;
	
	/**
	 * 
	 * @return Returns the task of the unit
	 */
	@Basic @Raw
	public Task getTask(){
		return this.task;
	}
	
	/**
	 * 
	 * @param 	task
	 * 			The task the unit is assigned to
	 * @pre		the given task must be a valid task
	 * 			|canHaveAsTask(task)
	 * @post	The task of this unit will be set to the given task
	 * 			|new.getTask() == task
	 */
	@Raw
	private void setTask(Task task){
		assert canHaveAsTask(task);
		this.task = task;
	}
	/**
	 * Sets the task of this unit to the given task
	 * 
	 * @param 	task
	 * 		  	the task the unit will get
	 * @effect	The task of this unit will set to the given task
	 * 			|setTask(task)
	 * @throws	IllegalArgumentException
	 * 			If the unit can't have the given task as a task or the given task is non effective
	 * 			if the given unit already has a task
	 * 			if the task doesn't have this unit as it's unit
	 * 			|((!canHaveAsTask(task) || task == null)||
	 * 			|(hasTask())||
	 * 			|(task.getUnit()!=this))
	 */
	@Raw
	void transferTask(@Raw Task task) throws IllegalArgumentException{	
		if (task == null && this.hasTask() && getTask().getUnit()==null){
			setTask(null);
			return;
		}
		if (!canHaveAsTask(task) || task == null)
			throw new IllegalArgumentException("Not a valid task");
		if (hasTask())
			throw new IllegalArgumentException("the unit already has an task");
		if(task.getUnit()!=this)
			throw new IllegalArgumentException("not a valid bidirectional association");
		setTask(task);
	}
	
	
	/**
	 * Checks if the task can be assigned to the unit
	 * @param task
	 * 			task the unit is assigned to
	 * @return true if the task can be assigned to the unit and the task already references the
	 * 			the unit
	 * 			|task.canHaveAsUnit(this) && task.getUnit() == this
	 */
	@Raw
	public boolean canHaveAsTask(@Raw Task task){
		return (task == null || (task.canHaveAsUnit(this) && task.getUnit()== this));
	}
	
	/**
	 * 
	 * @return True if the unit has an task
	 */
	@Raw
	public boolean hasTask(){
		return getTask() != null;
	}
	/**
	 * 
	 * @return true if the unit has a proper task attached to it
	 */
	@Raw
	public boolean hasProperTask(){
		return (canHaveAsTask(getTask()) && (getTask()==null || getTask().getUnit()==this));
	}
	
	/**
	 * Variable registering the task of the unit
	 */
	private Task task;

	/**
	 * Return the experience points of this Unit.
	 */
	@Basic @Raw
	public int getExperiencePoints() {
		return this.experiencePoints;
	}
	
	/**
	 * Check whether the given experience points is a valid experience points for
	 * any Unit.
	 *  
	 * @param  experiencePoints
	 *         The experience points to check.
	 * @return 
	 *       | result == if experiencePoints is greater than or equal to zero.
	*/
	public static boolean isValidExperiencePoints(int experiencePoints) {
		
		return (experiencePoints >= 0);
	}
	
	/**
	 * Increases the experiencepoints of this unit with the given experiencepoints
	 * 
	 * @param 	experiencePoints
	 * 			|The amount with which the experiencepoints of the unit increase
	 * @post	The new excperiencepoints of the unit is the experiencepoints of this unit
	 * 			plus the given experiencePoints
	 * 			|new.getExperiencePoints() == this.getExperiencePoints + experiencePoints
	 * @post	The unit levels up if the given experiencePoints plus the current experiencepoints 
	 * 			modulo 10 exceeds 10
	 * 			|if (this.getExperiencePoints() % 10 + experiencePoints >= 10)
	 * 			|		timesLevelUp = (this.getExperiencePoints() % 10 + experiencePoints)/10
	 * 			|		this.levelUp(timesLevelUp)
	 */
	private void increaseExperiencePoints(int experiencePoints) {
		if (!isValidExperiencePoints(experiencePoints))
			return;
		if (getExperiencePoints() % 10 + experiencePoints >= 10) {
			int timesToLevel = (int) Math.floor(((double) getExperiencePoints() % 10 + experiencePoints) / 10);
			levelUp(timesToLevel);
		}
		setExperiencePoints(getExperiencePoints()+experiencePoints);
	}
	/**
	 * Set the experience points of this Unit to the given experience points.
	 * 
	 * @param  experiencePoints
	 *         The new experience points for this Unit.
	 * @post   If the given experience points is a valid experience points for any Unit,
	 *         the experience points of this new Unit is equal to the given
	 *         experience points.
	 *       | if (isValidExperiencePoints(experiencePoints))
	 *       |   then new.getExperiencePoints() == experiencePoints
	 */
	@Raw
	private void setExperiencePoints(int experiencePoints) {
		if (!isValidExperiencePoints(experiencePoints))
			this.experiencePoints = 0;
		this.experiencePoints = experiencePoints;
	}
	
	/**
	 * @post	The unit gains 1 level in either strength, agility or thoughness
	 * 			randomly generated
	 * 			|(new.getStrength()==this.getStrength+1 ||
	 * 			|new.getAgility()==this.getAgility+1 ||
	 * 			|new.getThougness==this.getThoughness+1)
	 */
	private void levelUp(){
		double randomNumber = createRandom(0,3);
		
		if (Util.fuzzyLessThanOrEqualTo(randomNumber, 1))
				this.setStrength(this.getStrength()+1, false);
		else if (Util.fuzzyLessThanOrEqualTo(randomNumber, 2))
				this.setAgility(this.getAgility()+1, false);
		else
				this.setToughness(this.getToughness(), false);
		
	}
	
	/**
	 * The unit gains levels equal to the given amount
	 * 
	 * @param 	amount
	 * 			The amount the units total level rises
	 * @effect	The unit uses the method 'levelUp()' amount times
	 * 			|amount*levelUp()
	 */
	private void levelUp(int amount){
		if (amount > 0) {
			levelUp();
			levelUp(amount-1);
		}
	}
	
	/**
	 * 
	 * @param underLimit
	 * 			The underlimit of the random generator
	 * @param upperLimit
	 * 			The upperlimit of the random generator
	 * @return a random number between underlimit and upperlimit
	 */
	public int createRandom(int underLimit, int upperLimit){
		return (int)(Math.random()*(upperLimit-underLimit)+underLimit);
	}
	
	/**
	 * Variable registering the experience points of this Unit.
	 */
	private int experiencePoints;

	/**
	 * Terminate this unit
	 * 
	 * @post	This unit is terminated
	 * 			|new.isTerminated()
	 * @post	The unit no longer references a World
	 * 			|new.getWorld == null
	 * @post	The unit no longer references a Faction
	 * 			|new.getFaction == null
	 * @post	The unit no longer references an Item
	 * 			|new.getItem == null
	 * @throws 	IllegalArgumentException
	 * 			If the faction of this unit still contains this unit
	 * 			|(this.hasFaction() && this.getFaction().unitInFaction(this))
	 * @throws 	IllegalArgumentException
	 * 			If the world of this unit still contains this unit
	 * 			|(this.getWorld()!=null && this.world.hasAsUnit(this))
	 */
	public void terminate() throws ModelException{
		//can only be terminated if the unit is dead??
		try{
			
			if(!isTerminated())
			
				if (hasItem())
					getItem().loseUnit();
				
				if (hasFaction() && getFaction().unitInFaction(this))
					throw new IllegalArgumentException("unit still has a faction");
				
				setFaction(null);
				
				if(getWorld()!=null && getWorld().hasAsUnit(this))
					throw new IllegalArgumentException("unit still has a world");
				
				setWorld(null);
				
				this.isTerminated = true;
				
		}
		catch(IllegalArgumentException e){
			throw new ModelException();
		}
	}
		
	/**
	 * Check whether this unit is terminated
	 */
	public boolean isTerminated(){
		return isTerminated;
	}
	
	/**
	 * Variable registering whether or not this 
	 * unit is terminated
	 */
	private boolean isTerminated = false;
}
	
