package hillbillies.model;


import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;

/**
 * @invar  The position of each Item must be a valid position for any
 *         Item.
 *       | isValidPosition(getPosition())
 */
public abstract class Item {
	
	protected Item(){
	}
	/**
	 * Returns the weight of this item.
	 */
	protected abstract int getWeight();
	
	/**
	 * Sets the weight of this item to the given weight
	 * @param 	weight
	 * 			The weight to be setted
	 * @post	The new weight of the item is equal to the 
	 * 			given weight.
	 */
	protected abstract void setWeight(int weight);
	
	/**
	 * Variable registering the weight of the Item
	 */
	protected int weight;
	
	/**
	 * Return whether this item is currently falling
	 */
	@Basic @Raw
	public boolean isFalling() {
		return this.isFalling;
	}
	
	
	/**
	 * Set the isFalling of this Item to the given isFalling.
	 * 
	 * @param  isFalling
	 *         The new value for isFalling.
	 * @post   the isFalling of this new object_name is equal to the given isFalling.
	 */
	@Raw
	public void setIsFalling(boolean isFalling) {
		this.isFalling = isFalling;
	}
	
	/**
	 * Variable registering whether this item is currently falling
	 */
	private boolean isFalling;

	/**
	 * Advances the time
	 */
	protected void advanceTime(double time) {
		if (isFalling) {
			move(time);
		}
	}
	
	/**
	 * The Item moves for the given time.
	 * 
	 * @param 	time
	 * 			The duration the Item moves.
	 * @post	The Item has travelled a distance time*falling_speed.
	 */
	protected void move(double time) {
		double heightDiff = position[2] - newPosition[2];
		if (heightDiff < FALLING_SPEED*time) { 
			position[2] = newPosition[2];
			setIsFalling(false);
		}
		else 
			position[2] = position[2] - FALLING_SPEED*time;
		
	}
	
	/**
	 * variable registering the falling speed of this Item
	 */
	protected final static double FALLING_SPEED = 3;
	


	
	
	/**
	 * Return the position of this Item.
	 */
	@Basic @Raw
	public double[] getPosition() {
		return this.position;
	}
	
	/**
	 * Check whether the given position is a valid position for
	 * any Item.
	 *  
	 * @param  position
	 *         The position to check.
	 * @return 
	 *       | result == true
	*/
	public static boolean isValidPosition(double[] position) {
		if (position == null)
			return false;
		return true;
	}
	
	/**
	 * Set the position of this Item to the given position.
	 * 
	 * @param  position
	 *         The new position for this Item.
	 * @post   The position of this new Item is equal to
	 *         the given position.
	 *       | new.getPosition() == position
	 * @throws IllegalArgumentException
	 *         The given position is not a valid position for any
	 *         Item.
	 *       | ! isValidPosition(getPosition())
	 */
	@Raw
	void setPosition(double[] position) 
			throws IllegalArgumentException {
		if (! isValidPosition(position))
			throw new IllegalArgumentException();
		this.position = position;
	}
	
	/**
	 * Variable registering the position of this Item.
	 */
	protected double[] position;
	
	/**
	 * Return the newPosition of this Item.
	 */
	@Basic @Raw
	public double[] getnewPosition() {
		return this.newPosition;
	}
	
	
	
	/**
	 * Set the newPosition of this Item to the given newPosition.
	 * 
	 * @param  newPosition
	 *         The new newPosition for this Item.
	 * @post   The newPosition of this new Item is equal to
	 *         the given newPosition.
	 *       | new.getnewPosition() == newPosition
	 */
	@Raw
	public void setnewPosition(double[] newPosition) {
		this.newPosition = newPosition;
	}
	
	/**
	 * Variable registering the newPosition of this Item.
	 */
	private double[]  newPosition;
	



	/**
	 * Return the Unit of this Item.
	 */
	@Basic @Raw
	public Unit getUnit() {
		return this.unit;
	}
	
	/**
	 * Check whether the given Unit is a valid Unit for
	 * any Item.
	 *  
	 * @param  Unit
	 *         The Unit to check.
	 * @return 
	 *       | result == 	true if the Item is terminated and unit is null,
	 *       				or the unit isn't null and the unit isn't terminated.
	*/
	@Raw
	public boolean canHaveAsUnit(@Raw Unit unit) {
		if (isTerminated()){
			return (unit==null);
		}
		else{
			return(unit!=null &&!unit.isTerminated());
		}
			
	}
	
	/**
	 * Set the Unit of this Item to the given Unit.
	 * 
	 * @param  unit
	 *         The new Unit for this Item.
	 * @post   The Unit of this new Item is equal to
	 *         the given Unit.
	 *       | new.getUnit() == unit
	 * @throws IllegalArgumentException
	 *         The given Unit is not a valid Unit for any
	 *         Item.
	 *       | ! isValidUnit(getUnit())
	 */
	@Raw
	private void setUnit(@Raw Unit unit){
		this.unit = unit;
	}
	
	@Raw
	/**
	 * Checks whether this Item has a valid unit attached to it
	 * 
	 * @return 	true if the Item can have the unit as it's unit,
	 * 			and the unit has to have this Item as it's item or is null.
	 */
	public boolean hasProperUnit(){
		return (canHaveAsUnit(getUnit()) && (getUnit() == null || getUnit().getItem()==this ));
	}
	
	/**
	 * Initiates a connection between the given unit and this Item
	 * 
	 * @param 	unit
	 * 			The unit to be connected with.
	 * 
	 * @post	if the unit is a valid unit for this Item and the unit
	 * 			can have this Item as its item then set the unit of this Item
	 * 			to the given unit.
	 * @post	if the given unit is set as this Items unit, then set the Item 
	 * 			of the given unit to this Item.
	 * @throws 	IllegalArgumentException
	 * 			If the unit is not a valid unit for this Item or is null
	 * 			If the unit already has an item but it isn't this Item
	 * 			if the item already has a unit but it isn't the given unit
	 */
	public void receiveUnit(Unit unit) throws IllegalArgumentException{
		if (! this.canHaveAsUnit(unit) || unit == null)
			throw new IllegalArgumentException("Can't have as unit");
		if (unit.hasItem() && unit.getItem()!=this)
			throw new IllegalArgumentException("Unit already has another item");
		if (this.getUnit()!= null && this.getUnit()!= unit)
			throw new IllegalArgumentException("Item already belongs to another unit");
		
		try{
		setUnit(unit);
		unit.transferItem(this);
		}
		catch (IllegalArgumentException e){
			throw new IllegalArgumentException("invalid unit-item connection");
		}
	}
	
	/**
	 * Breaks the connection between this Item and the unit
	 * this Item is attached to.
	 * 
	 * @effect	The unit attached to this Item will no longer be
	 * 			attached to this item
	 * @effect	The unit attached to this Item will break it's association
	 * 			with this Item.
	 */
	public void loseUnit(){
		try{
			Unit formerUnit = getUnit();
			setUnit(null);
			formerUnit.transferItem(null);
		}
		catch(NullPointerException e){
			System.out.println("can't lose unit");
		}
	}
	
	/**
	 * Variable registering the Unit of this Item.
	 */
	protected Unit unit;
	
	/**
	 * Terminates this Item
	 * 
	 * @effect 	This item will lose it's association with it's unit attached 
	 * 			to it.
	 * @effect	This Item is teminated.
	 */
	public void terminate(){
		if (!isTerminated()) {
			loseUnit();
			this.isTerminated = true;
		}
	}
	/**
	 * Check whether this Item is terminated
	 */
	public boolean isTerminated(){
		return this.isTerminated;
	}
	
	/**
	 * Variable registering whether this Item is terminated
	 */
	private boolean isTerminated = false;
	}
