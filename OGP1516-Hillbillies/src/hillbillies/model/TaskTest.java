package hillbillies.model;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import hillbillies.model.expression.E;
import hillbillies.model.expression.Position;
import hillbillies.model.expression.PositionE;
import hillbillies.model.expression.ReadVariable;
import hillbillies.model.expression.This;
import hillbillies.model.statement.MoveTo;
import hillbillies.model.statement.Print;
import hillbillies.model.statement.S;
import ogp.framework.util.ModelException;

public class TaskTest {
	
	private Task task1;
	private Task task2;
	private int[] position = {1,1,1};
	private Position positionExpression;
	private S statement;

	@Before
	public void setUp() throws Exception {
		positionExpression = new Position(position);
		statement = new MoveTo(positionExpression);
		task1 = new Task("task1", 100, statement, position );
		task2 = new Task("task2", -100, statement, position);
	}
	
	@Test
	public void extendedConstructor_legalCase() throws Exception{
		Task myTask = new Task("myTask", 50, statement, position);
		assertEquals(myTask.getName(),"myTask");
		assertEquals(myTask.getPriority(), 50);
		assertEquals(myTask.getActivity(),statement);
		assertEquals(myTask.getLocation(),position);
	}
	
	@Test
	public void extendedConstructor_defaultCase() throws Exception{
		Task myTask = new Task(null,5,statement,null);
		assertEquals(myTask.getName(),"defaultName");
		int[] position = {0,0,0};
		Arrays.equals(position,myTask.getLocation());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void extendedConstructor_nullCase() throws Exception{
		Task myTask = new Task("NullStatement",5,null,position);
	}
	
	@Test
	public void extendedConstructor_extremeValues() throws Exception{
		Task maxValueTask = new Task("myTask",Integer.MAX_VALUE,statement,position);
		assertEquals(maxValueTask.getPriority(), Integer.MAX_VALUE);
		Task minValueTask = new Task("minValueTask",Integer.MIN_VALUE,statement,position);
		assertEquals(minValueTask.getPriority(), Integer.MIN_VALUE);
	}
	
	@Test
	public void setPriority() throws Exception{
		task1.setPriority(20);
		assertEquals(task1.getPriority(),20);
	}
	
	@Test
	public void setActivity() throws Exception{
		E newExpression = new ReadVariable("variable");
		S newStatement = new Print(newExpression);
		task1.setActivity(newStatement);
		assertEquals(task1.getActivity(),newStatement);
	}
	
	@Test(expected=ModelException.class)
	public void setActivity_IllegalArgument() throws Exception{
		task1.setActivity(null);
	}
	
	@Test
	public void isWellFormed_TrueCase() throws Exception{
		assertTrue(task1.isWellFormed());
	}
	
	@Test
	public void isWellFormed_FalseCase() throws Exception{
		E newExpression = new ReadVariable("variable");
		S newStatement = new Print(newExpression);
		task1.setActivity(newStatement);
		assertFalse(task1.isWellFormed());
	}
	
	@Test
	public void isValidName_LegalCase() throws Exception{
		assertTrue(Task.isValidName("ValidName"));
		assertTrue(Task.isValidName("Everything_IS_all0wed"));
	}
	
	@Test
	public void isValidName_FalseCase() throws Exception{
		assertFalse(Task.isValidName(null));
	}
	
	@Test
	public void isValidPriority_LegalCase() throws Exception{
		assertTrue(Task.isValidPriority(5));
		assertTrue(Task.isValidPriority(Integer.MAX_VALUE));
		assertTrue(Task.isValidPriority(-10));
		assertTrue(Task.isValidPriority(Integer.MIN_VALUE));
	}
	
	@Test
	public void isValidPriority_FalseCase() throws Exception{
		//no false priority can be given at the moment
	}
	
	@Test
	public void isValidActivity_LegalCase() throws Exception{
		assertTrue(Task.isValidActivity(statement));
	}
	
	@Test
	public void isValidActivity_falseCase() throws Exception{
		assertFalse(Task.isValidActivity(null));
	}
	
	@Test
	public void canHaveAsLocation_legalCase() throws Exception{
		assertTrue(task1.canHaveAsLocation(position));
		int[] randomPosition = {1,10,5};
		assertTrue(task1.canHaveAsLocation(randomPosition));
	}
	
	@Test
	public void canHaveAsLocation_falseCase() throws Exception{
		assertFalse(task1.canHaveAsLocation(null));
	}
	
	@Test
	public void canHaveAsUnit_legalCase() throws Exception{
		double[] unitPosition = {1.0,1.0,1.0};
		Unit unit0 = new Unit("Unit",unitPosition, 25,50,70,100,false);
		assertTrue(task1.canHaveAsUnit(unit0));
		task1.terminate();
		assertTrue(task1.canHaveAsUnit(null));
	}
	
	@Test
	public void canHaveAsUnit_falseCase() throws Exception{
		double[] unitPosition = {1.0,1.0,1.0};
		Unit unit0 = new Unit("Unit",unitPosition, 25,50,70,100,false);
		unit0.terminate();
		assertFalse(task1.canHaveAsUnit(unit0));
		assertFalse(task1.canHaveAsUnit(null));
	}
	
	@Test
	public void hasProperUnit_TrueCase() throws Exception{
		double[] unitPosition = {1.0,1.0,1.0};
		Unit myUnit = new Unit("Unit",unitPosition, 25,50,70,100,false);
		task1.linkUnit(myUnit);
		assertTrue(task1.hasProperUnit());
	}
	
	@Test
	public void hasProperUnit_FalseCase() throws Exception{
		double[] unitPosition = {1.0,1.0,1.0};
		Unit myUnit = new Unit("Unit",unitPosition, 25,50,70,100,false);
		task1.linkUnit(myUnit);
		assertTrue(task1.hasProperUnit());
		myUnit.terminate();
		assertFalse(task1.hasProperUnit());
	}

	@Test
	public void linkUnit() throws Exception{
		double[] unitPosition = {1.0,1.0,1.0};
		Unit myUnit = new Unit("Unit",unitPosition, 25,50,70,100,false);
		task1.linkUnit(myUnit);
		assertEquals(task1.getUnit(),myUnit);
		assertEquals(myUnit.getTask(),task1);
		task1.loseUnit();
		assertEquals(task1.getUnit(),null);
		assertEquals(myUnit.getTask(),null);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void linkUnit_NullCase() throws Exception{
		task1.linkUnit(null);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void linkUnit_falseCase() throws Exception{
		double[] unitPosition = {1.0,1.0,1.0};
		Unit myUnit = new Unit("Unit",unitPosition, 25,50,70,100,false);
		task1.linkUnit(myUnit);
		assertTrue(task1.hasProperUnit());
		task2.linkUnit(myUnit);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void linkUnit_TaskTerminatedCase() throws Exception{
		double[] unitPosition = {1.0,1.0,1.0};
		Unit myUnit = new Unit("Unit",unitPosition, 25,50,70,100,false);
		task1.terminate();
		task1.linkUnit(myUnit);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void linkUnit_TerminatedCase() throws Exception{
		double[] unitPosition = {1.0,1.0,1.0};
		Unit myUnit = new Unit("Unit",unitPosition, 25,50,70,100,false);
		myUnit.terminate();
		task1.linkUnit(myUnit);
	}
	
	@Test
	public void canHaveAsScheduler_TrueCase() throws Exception{
		double[] unitPosition = {1.0,1.0,1.0};
		Unit myUnit = new Unit("Unit",unitPosition, 25,50,70,100,false);
		Faction faction = new Faction(myUnit);
		Scheduler scheduler = faction.getScheduler();
		task1.canHaveAsScheduler(scheduler);
	}
	
	@Test
	public void canHaveAsScheduler_TaskTerminated() throws Exception{
		double[] unitPosition = {1.0,1.0,1.0};
		Unit myUnit = new Unit("Unit",unitPosition, 25,50,70,100,false);
		Faction faction = new Faction(myUnit);
		Scheduler scheduler = faction.getScheduler();
		task1.terminate();
		assertFalse(task1.canHaveAsScheduler(scheduler));
	}
	
	@Test
	public void canHaveAsScheduler_NullScheduler() throws Exception{
		assertFalse(task1.canHaveAsScheduler(null));
	}
	
	@Test
	public void canHaveAsScheduler_SchedulerTerminated() throws Exception{
		double[] unitPosition = {1.0,1.0,1.0};
		Unit myUnit = new Unit("Unit",unitPosition, 25,50,70,100,false);
		Faction faction = new Faction(myUnit);
		Scheduler scheduler = faction.getScheduler();
		scheduler.terminate();
		assertFalse(task1.canHaveAsScheduler(scheduler));
	}
	
	@Test
	public void schedulerBindings() throws Exception{
		double[] unitPosition = {1.0,1.0,1.0};
		Unit myUnit = new Unit("Unit",unitPosition, 25,50,70,100,false);
		Faction faction = new Faction(myUnit);
		Scheduler scheduler = faction.getScheduler();
		assertEquals(task1.getNbSchedulers(),0);
		scheduler.addTask(task1);
		assertEquals(task1.getNbSchedulers(),1);
		assertTrue(task1.SchedulerInSet(scheduler));
		task1.removeScheduler(scheduler);
		assertFalse(task1.SchedulerInSet(scheduler));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void schedulersBindings_NotController() throws Exception{
		double[] unitPosition = {1.0,1.0,1.0};
		Unit myUnit = new Unit("Unit",unitPosition, 25,50,70,100,false);
		Faction faction = new Faction(myUnit);
		Scheduler scheduler = faction.getScheduler();
		task1.addSchedulerToSet(scheduler);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void schedulerBindings_NullCase() throws Exception{
		task1.addSchedulerToSet(null);
	}
	
	
	@Test(expected=IllegalArgumentException.class)
	public void schedulerBinding_TeminatedCase() throws Exception{
		double[] unitPosition = {1.0,1.0,1.0};
		Unit myUnit = new Unit("Unit",unitPosition, 25,50,70,100,false);
		Faction faction = new Faction(myUnit);
		Scheduler scheduler = faction.getScheduler();
		task1.terminate();
		scheduler.addTask(task1);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void schedulerBinding_SchedulerTerminatedCase() throws Exception{
		double[] unitPosition = {1.0,1.0,1.0};
		Unit myUnit = new Unit("Unit",unitPosition, 25,50,70,100,false);
		Faction faction = new Faction(myUnit);
		Scheduler scheduler = faction.getScheduler();
		scheduler.terminate();
		scheduler.addTask(task1);
	}
	
	@Test(expected=ModelException.class)
	public void schedulerBinding_OverFlow() throws Exception{
		for (int i=0; i<5; i++){
			double[] unitPosition = {1.0,1.0,1.0};
			Unit myUnit = new Unit("Unit",unitPosition, 25,50,70,100,false);
			Faction faction = new Faction(myUnit);
			Scheduler scheduler = faction.getScheduler();
			scheduler.addTask(task1);
		}
		assertEquals(task1.getNbSchedulers(),5);
		double[] unitPosition = {1.0,1.0,1.0};
		Unit myUnit = new Unit("Unit",unitPosition, 25,50,70,100,false);
		Faction faction = new Faction(myUnit);
		Scheduler scheduler = faction.getScheduler();
		scheduler.addTask(task1);
		
	}
	
	@Test
	public void removeSchedulersBinding() throws Exception{
		double[] unitPosition = {1.0,1.0,1.0};
		Unit myUnit1 = new Unit("Unit",unitPosition, 25,50,70,100,false);
		Unit myUnit2 = new Unit("Unit",unitPosition, 25,50,70,100,false);
		Faction faction1 = new Faction(myUnit1);
		Faction faction2 = new Faction(myUnit2);
		Scheduler scheduler1 = faction1.getScheduler();
		Scheduler scheduler2 = faction2.getScheduler();
		scheduler1.addTask(task1);
		scheduler2.addTask(task1);
		assertEquals(task1.getNbSchedulers(),2);
		task1.removeSchedulersBinding();
		assertEquals(task1.getNbSchedulers(),0);
	}
	
	@Test
	public void addVariable() throws Exception{
		E number = new This();
		task1.addVariable("variableName", number);
		assertEquals(task1.getVariable("variableName"),number);
	}
	
	@Test
	public void terminate() throws Exception{
		double[] unitPosition = {1.0,1.0,1.0};
		Unit myUnit1 = new Unit("Unit",unitPosition, 25,50,70,100,false);
		Unit myUnit2 = new Unit("Unit",unitPosition, 25,50,70,100,false);
		Faction faction1 = new Faction(myUnit1);
		Faction faction2 = new Faction(myUnit2);
		Scheduler scheduler1 = faction1.getScheduler();
		Scheduler scheduler2 = faction2.getScheduler();
		scheduler1.addTask(task1);
		scheduler2.addTask(task1);
		task1.linkUnit(myUnit1);
		assertEquals(task1.getUnit(),myUnit1);
		assertEquals(task1.getNbSchedulers(),2);
		task1.terminate();
		assertTrue(task1.isTerminated());
		assertEquals(task1.getUnit(), null);
		assertEquals(task1.getNbSchedulers(),0);
	}
	
	public void notTerminated() throws Exception{
		assertFalse(task1.isTerminated());
	}
	

}
