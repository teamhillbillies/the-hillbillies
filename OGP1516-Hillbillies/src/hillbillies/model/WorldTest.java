package hillbillies.model;

import static org.junit.Assert.*;

import java.util.*;

import org.junit.Before;
import org.junit.Test;

import hillbillies.model.pathfinding.Position;
import hillbillies.part2.facade.Facade;
import hillbillies.part2.listener.DefaultTerrainChangeListener;
import ogp.framework.util.ModelException;

public class WorldTest {
	
	private Unit unit0 , unit1, unit2;
	private double[] position;
	private World world;
	private int nbX;
	private int nbY;
	private int nbZ;
	private double[] nextPosition;
	private double[] lastPosition;
	private int[] itemPosition;
	
	private static final int TYPE_AIR = 0;
	private static final int TYPE_ROCK = 1;
	private static final int TYPE_TREE = 2;
	private static final int TYPE_WORKSHOP = 3;
	private Facade facade;

	@Before
	public void setUp() throws Exception {
		nbX = 10;
		nbY = 20;
		nbZ = 30;
		
		world = new World(new int[nbX][nbY][nbZ], new DefaultTerrainChangeListener());
		
		
		for (int x=0; x<nbX; x++){
			for (int y=0; y<nbY; y++){
				for (int z=0; z<nbZ; z++){
					double[] myPosition = {x,y,z};
					if (world.isValidUnitPosition(myPosition)){
						if (nextPosition == null && position!=null){
							nextPosition = new double[] {x,y,z};
						}
						if (position==null){
							position = new double[] {x,y,z};
							itemPosition = new int[] {x,y,z};
						}
						lastPosition = new double[] {x,y,z};
					}
				}
			}
		}
		
		
		unit0 = new Unit("Unit Low",position,25,25,25,25,false);
		unit1 = new Unit("Unit O'Middle",position, 50,50,50,50,false);
		unit2 = new Unit("UnitHigh",position, 100,100,100,100,false);
		world.addUnit(unit0);
		world.addUnit(unit1);
		world.addUnit(unit2);
		
		
	}
	
	@Test(expected=ModelException.class)
	public void addUnit_NullCase() throws Exception{
		world.addUnit(null);
	}
	
	@Test(expected=ModelException.class)
	public void addUnit_FalseCase() throws Exception{
		
		nbX = 10;
		nbY = 20;
		nbZ = 30;
		
		World world2 = new World(new int[nbX][nbY][nbZ], new DefaultTerrainChangeListener());
		
		Unit myUnit = new Unit("Myunit",position,25,25,25,25, false);
		world2.addUnit(myUnit);
		world.addUnit(myUnit);
		
		
	}
	
	@Test(expected=ModelException.class)
	public void addUnit_WorldFull() throws Exception{
		while (world.getNbUnits()<100){
			Unit myUnit = new Unit("Myunit",position,25,25,25,25, false);
			world.addUnit(myUnit);
		}
		Unit myUnit = new Unit("Myunit",position,25,25,25,25, false);
		world.addUnit(myUnit);
	}

	//BLACKBOX + WHITEBOX.
	@Test
	public void sizeWorld() throws Exception{
		assertEquals(world.getNbCubesX(),nbX);
		assertEquals(world.getNbCubesY(),nbY);
		assertEquals(world.getNbCubesZ(),nbZ);
	}
	
	@Test
	public void getTerrainType() throws Exception{
		int[][][] types = new int[3][3][3];
		types[1][1][0] = TYPE_ROCK;
		types[1][1][1] = TYPE_TREE;
		types[1][1][2] = TYPE_WORKSHOP;
		
		this.facade = new Facade();

		World newWorld = facade.createWorld(types, new DefaultTerrainChangeListener());
		
		assertEquals(newWorld.getTerrainType(new int[] {1,1,0}),TerrainType.ROCK );
		assertEquals(newWorld.getTerrainType(new int[] {1,1,1}),TerrainType.TREE);
		assertEquals(newWorld.getTerrainType(new int[] {1,1,2}),TerrainType.WORKSHOP );
		
	}
	
	@Test
	public void getNbUnits() throws Exception{
		assertEquals(world.getNbUnits(), 3);
	}
	
	@Test
	public void canHaveAsUnit_legalCase() throws Exception{
	
		Unit unit0 = new Unit("Unit Low",position,25,25,25,25,false);
		assertTrue(world.canHaveAsUnit(unit0));
		
	}
	
	@Test
	public void canHaveAsUnit_falseUnits() throws Exception{
		Unit nullUnit = null;
		assertFalse(world.canHaveAsUnit(nullUnit));
		Unit terminatedUnit =new Unit("Unit Low",position,25,25,25,25,false);
		world.addUnit(terminatedUnit);
		world.removeUnit(terminatedUnit);
		assertTrue(terminatedUnit.isTerminated());
		assertFalse(world.canHaveAsUnit(terminatedUnit));
	}
	
	@Test
	public void canHaveAsUnit_worldTerminated() throws Exception{
		Unit unit = new Unit("Unit Low",position,25,25,25,25,false);
		world.terminate();
		assertFalse(world.canHaveAsUnit(unit));
	}
	
	@Test
	public void canHaveAsUnit_worldFull() throws Exception{
		//TODO when constructor unit is changed
	}
	
	@Test
	public void hasAsUnit_trueCase() throws Exception{
		Unit unit = new Unit("Unit Low",position,25,25,25,25,false);
		assertFalse(world.hasAsUnit(unit));
		world.addUnit(unit);
		assertTrue(world.hasAsUnit(unit));
	}
	
	@Test
	public void hasProperUnits_trueCase() throws Exception{
		Unit unit = new Unit("Unit Low",position,25,25,25,25,false);
		world.addUnit(unit);
		assertTrue(world.hasProperUnits());
	}
	
	@Test
	public void isValidNbUnits_legalCase() throws Exception{
		assertTrue(World.isValidNbUnits(10));
		assertTrue(World.isValidNbUnits(100));
		assertTrue(World.isValidNbFactions(0));
	}
	
	@Test
	public void isValidNbUnits_extremeValues() throws Exception{
		assertFalse(World.isValidNbUnits(Integer.MAX_VALUE));
	}
	
	@Test
	public void isValidNbUnits_falseCase() throws Exception{
		assertFalse(World.isValidNbUnits(101));
	}
	
	@Test
	public void createUnit() throws Exception{
		Unit myUnit = world.createUnit();
		assertFalse(myUnit==null);
	}
	
	@Test
	public void addUnit() throws Exception{
		Unit myUnit = new Unit("Unit Low",position,25,25,25,25,false);
		world.addUnit(myUnit);
		assertTrue(world.hasAsUnit(myUnit));
		assertTrue(myUnit.getWorld()==world);
	}
	
	@Test
	public void removeUnit() throws Exception{
		Unit myUnit = new Unit("Unit Low",position,25,25,25,25,false);
		world.addUnit(myUnit);
		world.removeUnit(myUnit);
		assertTrue(myUnit.isTerminated());
		assertFalse(world.hasAsUnit(myUnit));
	}
	
	@Test
	public void isValidItemPosition() throws Exception{
		int[][][] types = new int[3][3][3];
		types[1][1][0] = TYPE_ROCK;
		types[1][1][1] = TYPE_TREE;
		types[1][1][2] = TYPE_WORKSHOP;
		
		this.facade = new Facade();

		World newWorld = facade.createWorld(types, new DefaultTerrainChangeListener());
		
		int[] positionWorkShop = {1,1,2};
		int[] positionRock = {1,1,0};
		int[] positionTree = {1,1,1};
		
		assertTrue(newWorld.isValidItemPosition(positionWorkShop));
		assertFalse(newWorld.isValidItemPosition(positionRock));
		assertFalse(newWorld.isValidItemPosition(positionTree));
	}
	
	@Test
	public void isValidUnitPosition() throws Exception{
		int[][][] types = new int[3][3][3];
		types[1][1][0] = TYPE_ROCK;
		types[1][1][1] = TYPE_TREE;
		types[1][1][2] = TYPE_WORKSHOP;
		
		this.facade = new Facade();

		World newWorld = facade.createWorld(types, new DefaultTerrainChangeListener());
		
		double[] positionWorkShop = {1,1,2};
		double[] positionRock = {1,1,0};
		double[] positionTree = {1,1,1};
		double[] invalidPosition = {Double.NaN,1,1};
		
		assertTrue(newWorld.isValidUnitPosition(positionWorkShop));
		assertFalse(newWorld.isValidUnitPosition(positionRock));
		assertFalse(newWorld.isValidUnitPosition(positionTree));
		assertFalse(newWorld.isValidUnitPosition(invalidPosition));
	}
	
	@Test
	public void isValidUnitCube() throws Exception{
		//TODO
	}
	
	@Test
	public void getNbFactions() throws Exception{
		assertEquals(world.getNbFactions(),3);
		for (int i=0; i<3; i++){
			Unit myUnit = new Unit("Unit Low",position,25,25,25,25,false);
			world.addUnit(myUnit);
		}
		assertEquals(world.getNbFactions(),5);
	}
	
	@Test
	public void canHaveAsFaction_legalCase() throws Exception{
		assertTrue(world.canHaveAsFaction(unit0.getFaction()));
	}
	
	@Test
	public void canHaveAsFaction_falseCase() throws Exception{
		assertFalse(world.canHaveAsFaction(null));
	}
	
	@Test
	public void getFactionAt() throws Exception{
		assertEquals(world.getFactionAt(0),unit0.getFaction());
		assertEquals(world.getFactionAt(2),unit2.getFaction());
		Unit fillUnit = new Unit("Unit Low",position,25,25,25,25,false);
		world.addUnit(fillUnit);
		Unit lastUnit = new Unit("Unit Low",position,25,25,25,25,false);
		world.addUnit(lastUnit);
		assertEquals(world.getFactionAt(4),lastUnit.getFaction());
	}
	
	@Test
	public void canHaveAsFactionAt() throws Exception{
		assertFalse(world.canHaveAsFactionAt(unit0.getFaction(), 2));
		assertFalse(world.canHaveAsFactionAt(null, 2));
		assertFalse(world.canHaveAsFactionAt(unit0.getFaction(), 50));
	}
	
	@Test
	public void hasAsFaction() throws Exception{
		assertTrue(world.hasAsFaction(unit0.getFaction()));
		assertFalse(world.hasAsFaction(null));
	}
	
	@Test
	public void hasProperFactions() throws Exception{
		assertTrue(world.hasProperFactions());
	}
	
	@Test
	public void canHaveAsItem() throws Exception{
		Item someItem = new Boulder(itemPosition);
		assertTrue(world.canHaveAsItem(someItem));
		Item nullItem = null;
		assertFalse(world.canHaveAsItem(nullItem));
		someItem.terminate();
		assertFalse(world.canHaveAsItem(someItem));
	}
	
	@Test
	public void getNbItems() throws Exception{
		assertEquals(world.getNbItems(),0);
		Item someItem = new Boulder(itemPosition);
		Position position = new Position(itemPosition[0],itemPosition[1],itemPosition[2]); 
		world.addItem(someItem, position);
		assertEquals(world.getNbItems(),1);
	}
	
	@Test
	public void hasAsItem() throws Exception{
		Item someItem = new Boulder(itemPosition);
		Position position = new Position(itemPosition[0],itemPosition[1],itemPosition[2]); 
		world.addItem(someItem, position);
		assertTrue(world.HasAsItem(someItem));
		
		Item anotherItem = new Boulder(itemPosition);
		assertFalse(world.HasAsItem(anotherItem));
		
		assertFalse(world.HasAsItem(null));
	}
	
	@Test
	public void addItem() throws Exception{
		Item someItem = new Boulder(itemPosition);
		Position position = new Position(itemPosition[0],itemPosition[1],itemPosition[2]); 
		world.addItem(someItem, position);
		assertTrue(world.HasAsItem(someItem));
	}
	
	@Test
	public void removeItem() throws Exception{
		Item someItem = new Boulder(itemPosition);
		Position position = new Position(itemPosition[0],itemPosition[1],itemPosition[2]); 
		world.addItem(someItem, position);
		assertTrue(world.HasAsItem(someItem));
		world.removeItem(someItem, position);
		assertFalse(world.HasAsItem(someItem));
	}
	
	@Test
	public void hasProperItems() throws Exception{
		Item someItem = new Boulder(itemPosition);
		Position position = new Position(itemPosition[0],itemPosition[1],itemPosition[2]); 
		world.addItem(someItem, position);
		assertTrue(world.hasProperItems());
	}
	
	@Test
	public void getItemsAtPos() throws Exception{
		Item someItem = new Boulder(itemPosition);
		Position position = new Position(itemPosition[0],itemPosition[1],itemPosition[2]); 
		world.addItem(someItem, position);
		List<Item> someList = new ArrayList<Item>();
		someList.add(someItem);
		assertEquals(world.getItemsAtPos(itemPosition),someList);
	}
	
	@Test
	public void WorldTerminate() throws Exception{
		world.terminate();
		assertTrue(world.isTerminated());
		assertEquals(world.getNbUnits(),0);
		assertEquals(world.getNbFactions(),0);
		assertEquals(world.getNbItems(),0);
		assertTrue(unit0.isTerminated());
	}
	
	
}

