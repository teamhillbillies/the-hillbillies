package hillbillies.model;

import ogp.framework.util.ModelException;

/**
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 * @invar  	The position of the Log must be a valid position for this Log.
 * @invar 	The weight of this Log must be a valid weight for this Log.
 */
public class Log extends Item {
	
	/**
	 * Initialize this new Log with given position and weight.
	 *
	 * @param  position
	 *         The position for this new Log.
	 * @effect The position of this new Log is set to
	 *         the given position.
	 *       | this.setPosition(position)
	 */
	public Log(int[] position) throws ModelException {
		setWeight((int)(Math.random()*(MAX_WEIGHT-MIN_WEIGHT)+MIN_WEIGHT));
		double[] truePosition = {0.5,0.5,0.5};
		for (int i=0;i<position.length;i++) {
			truePosition[i] += position[i];
		}
		try{
			this.setPosition(truePosition);
			this.setnewPosition(truePosition);
		}
		catch (IllegalArgumentException e){
			throw new ModelException();
		}
	}
	
	/**
	 * Variable registering the minimum weight of a log
	 */
	private final static int MIN_WEIGHT = 10;
	
	/**
	 * Variable registering the maximum weight of a log
	 */
	private final static int MAX_WEIGHT = 50;
	
	/**
	 * Returns the weight of this log
	 */
	@Override
	protected int getWeight() {
		return this.weight;
	}
	
	/**
	 * 
	 * @param 	weight
	 * 			The weight to be checked
	 * @return 	Return true if the given weight is between the minimum and maximum weight of the log
	 */
	private static boolean isValidWeight(int weight) {
		return (MIN_WEIGHT <= weight && weight <= MAX_WEIGHT);
	}
	
	/**
	 * @param	weight
	 * 			The new weight of the log
	 * @post	If the given weight is a valid weight for the log then the given weight
	 * 			of this log is equal to the given weight
	 * @throws	IllegalArgumentException
	 * 			The given weight is not a valid weight for this log.
	 */
	protected void setWeight(int weight) throws IllegalArgumentException {
		if (!isValidWeight(weight)) {
			throw new IllegalArgumentException("this is not a valid log weight");
		}
		this.weight = weight;

	}

}
