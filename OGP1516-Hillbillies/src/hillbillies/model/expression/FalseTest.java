package hillbillies.model.expression;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class FalseTest {
	private static False falseExpression, falseExpression2;

	@Before
	public void setUp() throws Exception {
		falseExpression = new False();
		falseExpression2 = new False();
	}

	@Test
	public void extendedConstructor() throws Exception{
		False myFalse = new False();
		assertEquals(myFalse.getValue(),false);
	}
	
	@Test
	public void isMutable()throws Exception{
		assertFalse(falseExpression.isMutable());
	}
	
	@Test
	public void Equals_EqualExpression()throws Exception{
		assertTrue(falseExpression.equals(falseExpression2));
	}
	
	@Test
	public void Equals_NonEffective()throws Exception{
		assertFalse(falseExpression.equals(null));
	}
	
	@Test
	public void Equals_SameExpression()throws Exception{
		assertTrue(falseExpression.equals(falseExpression));
	}
	
	@Test
	public void Equals_DifferentType() throws Exception{
		assertFalse(falseExpression.equals(new True()));
	}
	
	@Test
	public void toStringFalse()throws Exception{
		assertEquals(falseExpression.toString(),"false");
	}
	

}
