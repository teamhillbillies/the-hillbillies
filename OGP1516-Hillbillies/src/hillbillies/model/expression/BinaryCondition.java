package hillbillies.model.expression;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Model;
import be.kuleuven.cs.som.annotate.Raw;

/**
 *  A class of binary arithmetic conditions.
 *   A binary condition involves a single operator applied
 *   to a left-hand condition and a right-hand condition.
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 *
 */
public abstract class BinaryCondition extends ComposedBooleanE {
	
	/**
	 * 
	 * @param 	left
	 * 			The left condition for this binary condition
	 * @param 	right
	 * 			The right condition for this binary condition
	 * @throws 	IllegalArgumentException
	 * 			This new binary condition can not have the given
	 * 			given left condition or the given right condition as
	 * 			it's condition
	 */
	@Model
	protected BinaryCondition(BooleanE left, BooleanE right) throws IllegalArgumentException{
		if (!canHaveAsOperand(left))
			throw new IllegalArgumentException("can't have as leftoperand");
		if (!canHaveAsOperand(right))
			throw new IllegalArgumentException("can't have as rightoperand");
		setOperandAt(1,left);
		setOperandAt(2,right);
	}
	
	/**
	 * 
	 * @param 	left
	 * 			The left condition for this binary condition
	 * @param 	right
	 * 			The right condition for this binary condition
	 * @throws 	IllegalArgumentException
	 * 			This new binary condition can not have the given
	 * 			given left condition or the given right condition as
	 * 			it's condition
	 */
	@Model
	protected BinaryCondition(E left, E right) throws IllegalArgumentException{
		if (!canHaveAsOperand(left))
			throw new IllegalArgumentException("can't have as leftoperand");
		if (!canHaveAsOperand(right))
			throw new IllegalArgumentException("can't have as rightoperand");
		setOperandAt(1,left);
		setOperandAt(2,right);
	}
	
	/**
	 * Returns the number of conditions, operands
	 * 
	 * @return always returns 2
	 */
	@Override
	@Basic
	public final int getNbOperands() {
		// TODO Auto-generated method stub
		return 2;
	}
	
	/**
	 * Checks whether this binary condition can have the
	 * given number as number of operands
	 * 
	 * @param	number
	 * 			the number to be checked
	 * @return 	true if the given number is equal to 2
	 */
	public boolean canHaveAsNbOperands(int number){
		return number == 2;
	}

	/**
	 * Returns the operand at the given index
	 * 
	 * @return	if the index is 1 return the left operand, if the index is
	 * 			2 return the right operand
	 * @throws	IllegalArgumentException
	 * 			The given index is not equal to 1 or 2
	 */
	@Override
	@Raw
	public final E getOperandAt(int index) throws IllegalArgumentException {
		if ((index!=1) && (index!=2))
			throw new IllegalArgumentException("Illegal index");
		if (index == 1)
			return getLeftOperand();
		return getRightOperand();
	}
	
	
	/**
	 * Set the operand for this binary expression at the given
	 * index to the given operand.
	 */
	@Override
	@Raw
	protected void setOperandAt(int index, E operand) {
		if (index == 1)
			this.leftOperand = operand;
		else
			this.rightOperand = operand;
		}
	
	/**
	 * Returns the left operand of this binary condition
	 */
	@Basic
	public E getLeftOperand(){
		return this.leftOperand;
	}
	
	/**
	 * Variable referencing the left operand of this binary condition
	 */
	private E leftOperand;
	
	/**
	 * Returns the right operand of this binary condition
	 */
	@Basic
	public E getRightOperand(){
		return this.rightOperand;
	}
	
	/**
	 * Variable referencing the right operand of this binary condition
	 */
	private E rightOperand;
	
	/**
	 * Return the textual representation of this binary condition
	 * 
	 * @return	If both operands are of the class IsCondition then the
	 * 			textual representation is the textual representation of the
	 * 			left operand followed by the symbol referencing the operator
	 * 			followed by the textual representation of the right operand
	 * @return	If the left operand of this binary condition is of the class IsCondition
	 *         	and the right operand of this binary condition is a composed condition
	 *         	but not an IsCondition, the textual representation of the left operand 
	 *         	of this binary condition, followed by the symbol representing the operator
	 *         	of this binary condition followed by the textual representation
	 *         	of the right operand of this binary condition in parenthesis.
	 * @return 	If the left operand of this binary condition is a composed condition
	 * 			but not an IsCondition and the right operand of this binary condition 
	 * 			is an IsCondition, the textual representation of the left operand of this binary
	 *         	condition in parenthesis, followed by the symbol representing the operator
	 *         	of this binary condition followed by the textual representation
	 *         	of the right operand of this binary condition.
	 *@return 	If both operands of this binary condition are composed conditions but 
	 *			not part of the class IsCondition, the textual representation of the 
	 *			left operand of this binary condition in parenthesis, followed by the symbol 
	 *			representing the operator of this binary condition followed by the textual representation
	 *         	of the right operand of this binary condition in parenthesis.
	 *@throws	Error
	 *			If it's none of the above situations     
	 */
	@Override
	public String toString() {
		getLeftOperand().transferStatement(getStatement());
		getRightOperand().transferStatement(getStatement());
		String result;
		if (getLeftOperand() instanceof IsCondition)
			result = getLeftOperand().toString();
		else if (getLeftOperand() instanceof ComposedBooleanE)
			result = "(" + getLeftOperand().toString() + ")";
		else
			throw new Error("Unknown expression type!");
		result += this.getOperatorSymbol();
		if (getLeftOperand() instanceof IsCondition)
			result += getRightOperand().toString();
		else if (getRightOperand() instanceof ComposedBooleanE)
			result += "(" + getRightOperand().toString() + ")";
		else
			throw new Error("Unknown expression type!");
		return result;
	}
	
	/**
	 * Returns the textual representation of the operatorsymbol of this class
	 */
	public abstract String getOperatorSymbol();

}
