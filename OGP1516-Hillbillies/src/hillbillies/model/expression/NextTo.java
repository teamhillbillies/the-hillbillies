package hillbillies.model.expression;

import hillbillies.model.Unit;

/**
 * A class that defines the next position of the given position
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 *
 */
public class NextTo extends ComposedPositionE {
	
	/**
	 * Initialize this NextTo
	 * @param	position
	 * 			The position where the next-to position will be calculated of
	 * @post	The value of this NextTo is equal to 
	 * 			the position next to the given position
	 */
	public NextTo(PositionE position){
		super(position);
	}
	
	public NextTo(E position){
		super(position);
	}
	
	/**
	 * Finds the next position in the world starting from the given position
	 * @param 	startingPosition
	 * 			the position where the search starts
	 * @return	the next position to the given starting position
	 * @throws 	IllegalArgumentException
	 * 			if no position is found
	 */
	public int[] findNextPosition(PositionE finalPosition) throws IllegalArgumentException {
		int[] targetPosition = finalPosition.getValue();
		int[] goodPosition;
		for(int i = -1;i<=1;i++) {
			for(int j = -1;j<=1;j++) {
				for(int k = -1;k<=1;k++) {
					goodPosition = new int[]{targetPosition[0]+i,targetPosition[1]+j,targetPosition[2]+k};
					if (getStatement().getTask().getUnit().getWorld().isValidUnitCube(goodPosition)) {
						try {
							getStatement().getTask().getUnit().getWorld().findPath(getUnitOccupiedCube(), goodPosition);
							return goodPosition;
						}
						catch(IllegalArgumentException e) {
						}
					}
				}
			}
		}
		throw new IllegalArgumentException("no valid position could be found");
			
	}
	
	
	/**
	 * Returns the position of next cube in the world
	 */
	@Override
	public int[] getValue() throws IllegalArgumentException{
		getOperandAt(1).transferStatement(getStatement());
		if (getOperandAt(1) instanceof ReadVariable) {
			return (int[]) getOperandAt(1).getValue();
		}
		int[] nextPosition = findNextPosition((PositionE) getOperandAt(1));
		return nextPosition;
	}
	
	/**
	 * Returns a textual representation this class
	 * 
	 * @return	string "next_to" + the textual representation of the operand
	 */
	@Override
	public String toString() {
		getOperandAt(1).transferStatement(getStatement());
		return "next_to " + getOperandAt(1).toString();
	}
	
	/**
	 * Returns a clone of this expression
	 */
	@Override
	public E clone() {
		return new NextTo((PositionE) getOperandAt(1).clone());
	}



}
