package hillbillies.model.expression;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import hillbillies.model.*;
import hillbillies.model.statement.ExpressionStatement;
import hillbillies.model.statement.MoveTo;
import hillbillies.model.statement.S;
import hillbillies.part2.facade.Facade;
import hillbillies.part2.listener.DefaultTerrainChangeListener;

public class BoulderExpressionTest extends BoulderExpression {
	
	private World world;
	private int nbX;
	private int nbY;
	private int nbZ;
	
	private static final int TYPE_AIR = 0;
	private static final int TYPE_ROCK = 1;
	private static final int TYPE_TREE = 2;
	private static final int TYPE_WORKSHOP = 3;
	private Facade facade;
	private BoulderExpression boulderExpression;
	


	@Before
	public void setUp() throws Exception {
		
		nbX = 10;
		nbY = 20;
		nbZ = 30;
		
		world = new World(new int[nbX][nbY][nbZ], new DefaultTerrainChangeListener());
		
		
		boulderExpression = new BoulderExpression();
	}
	
	@Test
	public void extendedConstructor() throws Exception{
		BoulderExpression boulderExpression = new BoulderExpression();
	}
	
	@Test
	public void geValue() throws Exception {
		int[] position = {0,0,0};
		PositionE positionExpression = new Position(position);
		S statement = new MoveTo(positionExpression);
		Task task1 = new Task("task1", 100, statement, position );
		double[] unitPosition = {0,0,0};
		Unit unit0 = new Unit("UnitHigh",unitPosition, 100,100,100,100,false);
		task1.linkUnit(unit0);
		world.addUnit(unit0);
		hillbillies.model.pathfinding.Position position2 = new hillbillies.model.pathfinding.Position(position);
		Boulder boulder = new Boulder(position);
		world.addItem(boulder, position2);
		boulderExpression.transferStatement((ExpressionStatement) statement);
		Arrays.equals(boulderExpression.getValue(),position);
	}
	
	@Test
	public void equals_SameExpression() throws Exception{
		int[] position = {0,0,0};
		PositionE positionExpression = new Position(position);
		S statement = new MoveTo(positionExpression);
		Task task1 = new Task("task1", 100, statement, position );
		double[] unitPosition = {0,0,0};
		Unit unit0 = new Unit("UnitHigh",unitPosition, 100,100,100,100,false);
		task1.linkUnit(unit0);
		world.addUnit(unit0);
		hillbillies.model.pathfinding.Position position2 = new hillbillies.model.pathfinding.Position(position);
		Boulder boulder = new Boulder(position);
		world.addItem(boulder, position2);
		boulderExpression.transferStatement((ExpressionStatement) statement);
		assertTrue(boulderExpression.equals(boulderExpression));
	}
	
	@Test
	public void equals_DifferentType() throws Exception{
		int[] position = {0,0,0};
		PositionE positionExpression = new Position(position);
		S statement = new MoveTo(positionExpression);
		Task task1 = new Task("task1", 100, statement, position );
		double[] unitPosition = {0,0,0};
		Unit unit0 = new Unit("UnitHigh",unitPosition, 100,100,100,100,false);
		task1.linkUnit(unit0);
		world.addUnit(unit0);
		hillbillies.model.pathfinding.Position position2 = new hillbillies.model.pathfinding.Position(position);
		Boulder boulder = new Boulder(position);
		world.addItem(boulder, position2);
		boulderExpression.transferStatement((ExpressionStatement) statement);
		assertFalse(boulderExpression.equals(new False()));
	}
	
	@Test
	public void toStringBoulderExpression() throws Exception{
		assertEquals(boulderExpression.toString(),"boulder");
	}
	
	@Test
	public void cloneBoulderExpression() throws Exception{
		int[] position = {0,0,0};
		PositionE positionExpression = new Position(position);
		S statement = new MoveTo(positionExpression);
		Task task1 = new Task("task1", 100, statement, position );
		double[] unitPosition = {0,0,0};
		Unit unit0 = new Unit("UnitHigh",unitPosition, 100,100,100,100,false);
		task1.linkUnit(unit0);
		world.addUnit(unit0);
		hillbillies.model.pathfinding.Position position2 = new hillbillies.model.pathfinding.Position(position);
		Boulder boulder = new Boulder(position);
		world.addItem(boulder, position2);
		boulderExpression.transferStatement((ExpressionStatement) statement);
		boulderExpression.clone();
	}

}
