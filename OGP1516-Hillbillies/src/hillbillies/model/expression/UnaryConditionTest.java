package hillbillies.model.expression;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class UnaryConditionTest {
	
	private static UnaryCondition someUnaryCondition;
	private static ComposedBooleanE isSolid;

	@Before
	public void setUp() throws Exception {
		int[] position = {0,0,0};
		PositionE positionE = new Position(position);
		isSolid = new IsSolid(positionE);
		someUnaryCondition = new NotCondition(isSolid);
		
	}


	@Test
	public void getNbOperands() throws Exception {
		assertEquals(1, someUnaryCondition.getNbOperands());
	}
	
	@Test
	public void canHaveAsNbOperands_TrueCase() throws Exception {
		assertTrue(someUnaryCondition.canHaveAsNbOperands(1));
	}
	
	@Test
	public void canHaveAsNbOperands_FalseCase() throws Exception {
		assertFalse(someUnaryCondition.canHaveAsNbOperands(2));
	}
	
	@Test
	public void getOperandAt() throws Exception{
		assertEquals(someUnaryCondition.getOperandAt(1),isSolid);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void getOperandAt_IndexTooHigh() throws Exception{
		someUnaryCondition.getOperandAt(2);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void getOperandAt_IndexTooLow() throws Exception{
		someUnaryCondition.getOperandAt(0);
	}
	
	@Test
	public void toStringUnaryCondition() throws Exception{
		//TODO
	}

}
