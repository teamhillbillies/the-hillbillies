package hillbillies.model.expression;

import java.util.Arrays;

import hillbillies.model.TerrainType;

/**
 * Class consisting is-condition that check if the given position is passable
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 *
 */
public class IsPassable extends IsCondition {
	
	/**
	 * Initializes this is-condition with the position.
	 * 
	 * @param 	position
	 * 			The position to be checked
	 * @effect	This new IsPassable is initialized as an is-condition
	 *         	with the given position
	 */
	public IsPassable(PositionE position) throws IllegalArgumentException{
		super(position);
	}
	
	public IsPassable(E position) throws IllegalArgumentException{
		super(position);
	}

	/**
	 * Returns a textual representation of the IsPassable
	 * 
	 * @return 	"isPassable followed by the position as an array of int transformed
	 * 			 to a string between parenthesis
	 */
	@Override
	public String toString() {
		getOperandAt(1).transferStatement(getStatement());
		return "isPassable( " + Arrays.toString((int [])getOperandAt(1).getValue()) + " )";
	}
	
	/**
	 * Returns the value of this condition
	 * 
	 * @return	true if the given position is passable
	 */
	@Override
	public Boolean getValue() {
		//beter een isSolid maken in world
		//We hebben een isPassable als deel van de klasse van TerrainType :)
		getOperandAt(1).transferStatement(getStatement());
		return (getStatement().getTask().getUnit().getWorld().getTerrainType((int [])getOperandAt(1).getValue()).isPassable());
	}
	
	/**
	 * Returns a clone of this expression
	 */
	@Override
	public E clone() {
		return new IsPassable((PositionE) getOperandAt(1).clone());
	}
}
