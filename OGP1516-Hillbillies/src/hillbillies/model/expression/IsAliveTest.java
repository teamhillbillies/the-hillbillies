package hillbillies.model.expression;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import hillbillies.model.Boulder;
import hillbillies.model.Task;
import hillbillies.model.Unit;
import hillbillies.model.statement.ExpressionStatement;
import hillbillies.model.statement.Follow;
import hillbillies.model.statement.S;

public class IsAliveTest {

	private static UnitE unitE1, unitE2;
	private static S statement1, statement2;
	private static Task task1, task2;
	private static Unit unit1, unit2;
	private static IsAlive trueIsAlive, falseIsAlive;

	@Before
	public void setUp() throws Exception {
		unitE1 = new This();
		unitE2 = new This();
		statement1 = new Follow(unitE1);
		statement2 = new Follow(unitE2);
		int[] position = {0,0,0};
		task1 = new Task("TaskTest1",10,statement1, position);
		task2 = new Task("TaskTest2",10,statement2, position);
		double[] unitPosition = {0,0,0};
		unit1 = new Unit("Unit1",unitPosition, 100,100,100,100,false);
		unit2 = new Unit("Unit2",unitPosition, 100,100,100,100,false);
		task1.linkUnit(unit1);
		task2.linkUnit(unit2);
		unit2.consumeHitpoints(1000);
		trueIsAlive = new IsAlive(unitE1);
		falseIsAlive = new IsAlive(unitE2);
		trueIsAlive.transferStatement((ExpressionStatement) statement1);
		falseIsAlive.transferStatement((ExpressionStatement) statement2);
	}

	@Test
	public void extendedConstructor() throws Exception {
		IsAlive myIsAlive = new IsAlive(unitE1);
	}
	
	@Test
	public void getValue() throws Exception{
		assertTrue(trueIsAlive.getValue());
		assertFalse(falseIsAlive.getValue());
	}
	
	@Test
	public void toStringIsAlive() throws Exception{
		assertEquals(trueIsAlive.toString(),"IsAlive( Unit1 )");
	}
	
	@Test
	public void cloneIsAlive() throws Exception{
		trueIsAlive.clone();
	}

}
