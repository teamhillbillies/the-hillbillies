package hillbillies.model.expression;

import java.util.Iterator;
import java.util.Set;
import java.util.stream.Collectors;

import hillbillies.model.Faction;
import hillbillies.model.Unit;

/**
 * Class that defines the closest unit with a different faction with the starting position: the occupied cube
 * of the unit that's executing the statement where this expression points to
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 *
 */
public class Enemy extends BasicUnitE {
	
	/**
	 * Initialize this Enemy
	 * @effect	The value of this Enemy is equal to 
	 * 			the nearest unit with a different faction from the unit
	 * 			occupying the task
	 */
	public Enemy(){
	}
	
	/**
	 * Finds the nearest unit in the world with a different faction starting from the given position
	 * @param 	startingPosition
	 * 			the position where the search starts
	 * @return	the closest unit with a different faction to the given starting position
	 * @throws 	IllegalArgumentException
	 * 			if no unit is found
	 */
	public Unit findNearestEnemy(int[] startingPosition){
		Set<Unit> allUnits = getStatement().getTask().getUnit().getWorld().getAllUnits();
		Faction unitFaction = getStatement().getTask().getUnit().getFaction();
		Set<Unit> enemyUnits = allUnits.stream().filter(unit -> unit.getFaction() != unitFaction).collect(Collectors.toSet());
		double smallestDistance = Double.MAX_VALUE;
		Unit nearestUnit = null;
		Iterator<Unit> iterator = enemyUnits.iterator();
		while(iterator.hasNext()) {
			Unit unit = iterator.next();
			int[] unitLocation = unit.getOccupiedCube();
			int xDiff = unitLocation[0]-startingPosition[0];
			int yDiff = unitLocation[1]-startingPosition[1];
			int zDiff = unitLocation[2]-startingPosition[2];
			double distance = Math.pow(xDiff, 2) + Math.pow(yDiff, 2) + Math.pow(zDiff, 2);
			if (distance < smallestDistance) {
				try {
					getStatement().getTask().getUnit().getWorld().findPath(getUnitOccupiedCube(), unitLocation);
					smallestDistance = distance;
					nearestUnit = unit;
				}
				catch(IllegalArgumentException e) {
				}
			}
		}
		if (nearestUnit == null) {
			throw new IllegalArgumentException();
		}
		return nearestUnit;
	}
	
	/**
	 * Returns the occupied cube of the nearest unit with a different faction
	 */
	public Unit getValue(){
		return findNearestEnemy(getUnitOccupiedCube());
	}
	
	/**
	 * Returns a textual representation of this class
	 * 
	 * @return	always returns "enemy"
	 */
	public String toString(){
		return "enemy";
	}
	
	/**
	 * Returns a clone of the given expression
	 */
	@Override
	public E clone() {
		return new Enemy();
	}

}
