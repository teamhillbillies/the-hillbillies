package hillbillies.model.expression;

import java.util.Iterator;

import java.util.Set;

import hillbillies.model.Unit;
/**
 * Class that defines the closest unit with the same faction with the starting position: the occupied cube
 * of the unit that's executing the statement where this expression points to
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 *
 */
public class Friend extends BasicUnitE {
	
	/**
	 * Initialize this Boulder
	 * @effect	The value of this Friend is equal to 
	 * 			the nearest unit with the same faction from the unit
	 * 			occupying the task
	 */
	public Friend(){
	}
	
	/**
	 * Finds the nearest unit in the world with the same faction starting from the given position
	 * @param 	startingPosition
	 * 			the position where the search starts
	 * @return	the closest unit with the same faction to the given starting position
	 * @throws 	IllegalArgumentException
	 * 			if no unit is found
	 */
	public Unit findNearestFriend(int[] startingPosition){
		Set<Unit> allUnitsInFaction = getStatement().getTask().getUnit().getFaction().getUnitsInFaction();
		double smallestDistance = Double.MAX_VALUE;
		Unit nearestUnit = null;
		Iterator<Unit> iterator = allUnitsInFaction.iterator();
		while(iterator.hasNext()) {
			Unit unit = iterator.next();
			int[] unitLocation = unit.getOccupiedCube();
			int xDiff = unitLocation[0]-startingPosition[0];
			int yDiff = unitLocation[1]-startingPosition[1];
			int zDiff = unitLocation[2]-startingPosition[2];
			double distance = Math.pow(xDiff, 2) + Math.pow(yDiff, 2) + Math.pow(zDiff, 2);
			if (distance < smallestDistance && unit != getStatement().getTask().getUnit()) {
				try {
					getStatement().getTask().getUnit().getWorld().findPath(getUnitOccupiedCube(), unitLocation);
					smallestDistance = distance;
					nearestUnit = unit;
				}
				catch(IllegalArgumentException e) {
				}
			}
		}
		if (nearestUnit == null) {
			throw new IllegalArgumentException();
		}
		return nearestUnit;
	}
	
	/**
	 * Returns the occupied cube of the nearest unit with the same faction
	 */
	public Unit getValue(){
		return findNearestFriend(getUnitOccupiedCube());
	}
	
	/**
	 * Returns a textual representation of this class
	 * 
	 * @return	always returns "friend"
	 */
	public String toString(){
		return "friend";
	}
	
	/**
	 * Returns a clone of this expression
	 */
	@Override
	public E clone() {
		return new Friend();
	}

}
