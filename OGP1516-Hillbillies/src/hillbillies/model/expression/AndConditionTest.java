package hillbillies.model.expression;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import hillbillies.model.Task;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.model.statement.ExpressionStatement;
import hillbillies.model.statement.MoveTo;
import hillbillies.model.statement.S;
import hillbillies.part2.facade.Facade;
import hillbillies.part2.listener.DefaultTerrainChangeListener;

public class AndConditionTest {
	
	private static final int TYPE_AIR = 0;
	private static final int TYPE_ROCK = 1;
	private static final int TYPE_TREE = 2;
	private static final int TYPE_WORKSHOP = 3;
	private Facade facade;
	private World world;
	private static PositionE solidPosition,passablePosition;
	private static IsSolid trueIsPassable, falseIsPassable;
	private static S statement;
	private Unit unit0;
	private Task task1;
	private static AndCondition trueAndCondition, falseAndCondition, composedFalseAndCondition, composedTrueAndCondition;

	@Before
	public void setUp() throws Exception {
		
		int[][][] types = new int[3][3][3];
		types[1][1][0] = TYPE_AIR;
		types[1][1][1] = TYPE_TREE;
		types[1][1][2] = TYPE_WORKSHOP;
		
		this.facade = new Facade();
		
		double[] unitPosition = {1,1,1};
		unit0 = new Unit("Unit Low0",unitPosition,25,25,25,25,false);

		world = facade.createWorld(types, new DefaultTerrainChangeListener());
		int[] position1 = {1,1,1};
		solidPosition = new Position(position1);
		falseIsPassable = new IsSolid(solidPosition);
		
		world.addUnit(unit0);
		
		statement = new MoveTo(solidPosition);
		
		task1 = new Task("TaskTest1",10,statement, position1);
		task1.linkUnit(unit0);
		
		int[] position2 = {1,1,0};
		passablePosition = new Position(position2);
		trueIsPassable = new IsSolid(passablePosition);

		trueAndCondition = new AndCondition(falseIsPassable,falseIsPassable);
		falseAndCondition = new AndCondition(trueIsPassable, falseIsPassable);
		composedFalseAndCondition = new AndCondition(falseAndCondition, trueAndCondition);
		composedTrueAndCondition = new AndCondition(trueAndCondition, trueAndCondition);
		
		composedTrueAndCondition.transferStatement((ExpressionStatement) statement);
		composedFalseAndCondition.transferStatement((ExpressionStatement) statement);
		
	}


	@Test
	public void extendedConstructor() throws Exception {
		AndCondition myAndCondition = new AndCondition(trueAndCondition,falseAndCondition);
		assertEquals(myAndCondition.getLeftOperand(),trueAndCondition);
		assertEquals(myAndCondition.getRightOperand(),falseAndCondition);
		assertEquals(myAndCondition.getOperatorSymbol(),"&&");
	}
	
	@Test
	public void getValue_NormalAndCondition() throws Exception{
		trueAndCondition.transferStatement((ExpressionStatement) statement);
		falseAndCondition.transferStatement((ExpressionStatement) statement);
		
		assertTrue(trueAndCondition.getValue());
		assertFalse(falseAndCondition.getValue());
	}
	
	@Test
	public void getValue_ComposedAndCondition() throws Exception{
		assertTrue(composedTrueAndCondition.getValue());
		assertFalse(composedFalseAndCondition.getValue());
	}
	
	@Test
	public void toStringNormalAndCondition() throws Exception{
		falseAndCondition.transferStatement((ExpressionStatement) statement);
		
		int[] position1 = {1,1,1};
		int[] position2 = {1,1,0};
		
		assertEquals(falseAndCondition.toString(), "isSolid( " + Arrays.toString(position2) + " )&&"
				+ "isSolid( " + Arrays.toString(position1) + " )");
	}
	
	@Test
	public void toStringComposedAndCondition() throws Exception{
		int[] position1 = {1,1,1};
		int[] position2 = {1,1,0};
		
		assertEquals(composedFalseAndCondition.toString(), "(isSolid( " + Arrays.toString(position2) + " )&&"
				+ "isSolid( " + Arrays.toString(position1) + " ))&&(isSolid( " + Arrays.toString(position1) + " )&&"
				+ "isSolid( " + Arrays.toString(position1) + " ))");
	}
	
	@Test
	public void cloneAndCondition() throws Exception{
		AndCondition cloneAndCondition = new AndCondition(trueAndCondition,falseAndCondition);
		cloneAndCondition = (AndCondition) falseAndCondition.clone();
		cloneAndCondition.transferStatement((ExpressionStatement) statement);
		assertEquals(cloneAndCondition.getLeftOperand(),falseAndCondition);
		assertEquals(cloneAndCondition.getRightOperand(),trueAndCondition);
	}
	
	

}
