package hillbillies.model.expression;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import hillbillies.model.Boulder;
import hillbillies.model.Task;
import hillbillies.model.Unit;
import hillbillies.model.statement.ExpressionStatement;
import hillbillies.model.statement.Follow;
import hillbillies.model.statement.S;

public class CarriesItemTest {
	
	private static UnitE unitE1, unitE2;
	private static S statement1, statement2;
	private static Task task1, task2;
	private static Unit unit1, unit2;
	private static CarriesItem trueCarriesItem, falseCarriesItem;

	@Before
	public void setUp() throws Exception {
		unitE1 = new This();
		unitE2 = new This();
		statement1 = new Follow(unitE1);
		statement2 = new Follow(unitE2);
		int[] position = {0,0,0};
		task1 = new Task("TaskTest1",10,statement1, position);
		task2 = new Task("TaskTest2",10,statement2, position);
		double[] unitPosition = {0,0,0};
		unit1 = new Unit("Unit1",unitPosition, 100,100,100,100,false);
		unit2 = new Unit("Unit2",unitPosition, 100,100,100,100,false);
		task1.linkUnit(unit1);
		task2.linkUnit(unit2);
		Boulder boulder = new Boulder(position);
		boulder.receiveUnit(unit1);
		trueCarriesItem = new CarriesItem(unitE1);
		falseCarriesItem = new CarriesItem(unitE2);
		trueCarriesItem.transferStatement((ExpressionStatement) statement1);
		falseCarriesItem.transferStatement((ExpressionStatement) statement2);
		
	}

	@Test
	public void extendedConstructor() throws Exception{
		CarriesItem myCarriesItem = new CarriesItem(unitE1);
	}
	
	@Test
	public void getValue() throws Exception{
		assertTrue(trueCarriesItem.getValue());
		assertFalse(falseCarriesItem.getValue());
	}
	
	@Test
	public void cloneCarriesItem() throws Exception{
		trueCarriesItem.clone();
	}
	
	@Test
	public void toStringCarriesItem() throws Exception{
		assertEquals(trueCarriesItem.toString(), "carriesItem( Unit1 )");
	}
	
	@Test
	public void bindings() throws Exception{
		assertEquals(trueCarriesItem.getStatement().getTask().getUnit(),unit1);
		trueCarriesItem.getValue();
		assertEquals(unitE1.getStatement().getTask(),task1);
	}

}
