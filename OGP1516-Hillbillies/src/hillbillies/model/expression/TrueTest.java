package hillbillies.model.expression;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TrueTest {
	private static True trueExpression, trueExpression2;

	@Before
	public void setUp() throws Exception {
		trueExpression = new True();
		trueExpression2 = new True();
	}

	@Test
	public void extendedConstructor() throws Exception{
		True myTrue = new True();
		assertEquals(myTrue.getValue(),true);
	}
	
	@Test
	public void isMutable()throws Exception{
		assertFalse(trueExpression.isMutable());
	}
	
	@Test
	public void Equals_EqualExpression()throws Exception{
		assertTrue(trueExpression.equals(trueExpression2));
	}
	
	@Test
	public void Equals_NonEffective()throws Exception{
		assertFalse(trueExpression.equals(null));
	}
	
	@Test
	public void Equals_SameExpression()throws Exception{
		assertTrue(trueExpression.equals(trueExpression));
	}
	
	@Test
	public void Equals_DifferentType() throws Exception{
		assertFalse(trueExpression.equals(new False()));
	}
	
	@Test
	public void toStringFalse()throws Exception{
		assertEquals(trueExpression.toString(),"true");
	}

}
