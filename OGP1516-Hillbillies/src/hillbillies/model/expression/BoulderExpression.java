package hillbillies.model.expression;

import java.util.Iterator;
import java.util.Set;

import hillbillies.model.Boulder;
import hillbillies.model.Item;

/**
 * Class that defines the position of the closest boulder with the starting position the occupied cube
 * of the unit that's executing the statement where this expression points to
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 *
 */
public class BoulderExpression extends BasicPositionE {
	
	/**
	 * Initialize this Boulder
	 * @post	The value of this Boulder is equal to 
	 * 			the position of the nearest boulder from the unit
	 * 			occupying the task
	 */
	public BoulderExpression(){
	}
	
	/**
	 * Finds the nearest boulder in the world starting from the given position
	 * @param 	startingPosition
	 * 			the position where the search starts
	 * @return	the closest boulder to the given starting position
	 * @throws 	IllegalArgumentException
	 * 			if no boulder is found
	 */
	public Boulder findNearestBoulder(int[] startingPosition) throws IllegalArgumentException{
		Set<Item> allItems = getStatement().getTask().getUnit().getWorld().getAllItems();
		double smallestDistance = Double.MAX_VALUE;
		Boulder nearestBoulder = null;
		Iterator<Item> iterator = allItems.iterator();
		while(iterator.hasNext()) {
			Item item = iterator.next();
			if (item instanceof Boulder) {
				double[] itemLocation = item.getPosition();
				double xDiff = itemLocation[0]-startingPosition[0]-0.5;
				double yDiff = itemLocation[1]-startingPosition[1]-0.5;
				double zDiff = itemLocation[2]-startingPosition[2]-0.5;
				double distance = Math.pow(xDiff, 2) + Math.pow(yDiff, 2) + Math.pow(zDiff, 2);
				if (distance < smallestDistance) {
					try {
						getStatement().getTask().getUnit().getWorld().findPath(getUnitOccupiedCube(), getStatement().getTask().getUnit().getWorld().getOccupiedCube(itemLocation));
						smallestDistance = distance;
						nearestBoulder = (Boulder) item;
					}
					catch(IllegalArgumentException e) {
					}
				}
			}
		}
		if (nearestBoulder == null) {
			throw new IllegalArgumentException();
		}
		return nearestBoulder;
	}
	
	
	/**
	 * Returns the position of the nearest boulder
	 * @return	the boulder closest to the position of the unit doing the linked statement
	 * @throws	IllegalArgumentException
	 * 			if no boulder is found
	 */
	@Override
	public int[] getValue() throws IllegalArgumentException{
		double[] truePosition = findNearestBoulder(getUnitOccupiedCube()).getPosition();
		return new int[]{(int) Math.floor(truePosition[0]),(int) Math.floor(truePosition[1]),(int) Math.floor(truePosition[2])};
	}
	
	/**
	 * Returns a textual representation of the nearest boulder
	 * 
	 * @return	string "boulder"
	 */
	@Override
	public String toString() {
		return "boulder";
	}
	
	/**
	 * Returns a clone of the this expression
	 */
	@Override
	public E clone() {
		return new BoulderExpression();
	}
	
	

}
