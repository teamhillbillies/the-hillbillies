package hillbillies.model.expression;

/**
 * Class representing the not condition
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 *
 */

/**
 * A class of unary conditions, representing the not-condition of
 * a given operand.
 * 
 * @version  2.0
 * @author   Eric Steegmans
 */
public class NotCondition extends UnaryCondition {
	
	/**
	 * Initialize this new not-condition with given operand.
	 *
	 * @param  operand
	 *         The operand for this new not-condition.
	 * @effect This new not-condition is initialized as a unary condition
	 *         with the given operand as its operand.
	 *       | super(operand)
	 */
	public NotCondition(BooleanE operand) throws IllegalArgumentException{
		super(operand);
	}
	
	public NotCondition(E operand) throws IllegalArgumentException{
		super(operand);
	}
	
	/**
	 * Return the symbol representing the operator of this not-condition
	 * 
	 * @return The string "!"
	 */
	@Override
	public String getOperatorSymbol() {
		return "!";
	}
	
	/**
	 * Return the value of this not-condition.
	 *
	 * @return 	if the operand of the not-condition returns true then return false
	 * 			if the operand of the not-condition returns false then return true
	 */
	@Override
	public Boolean getValue() {
		getOperandAt(1).transferStatement(getStatement());
		return !(Boolean)getOperand().getValue();
	}
	
	/**
	 * Returns a clone of this expression
	 */
	@Override
	public E clone() {
		NotCondition notConditionClone = new NotCondition((BooleanE) getOperandAt(1).clone());
		return notConditionClone;
	}

}
