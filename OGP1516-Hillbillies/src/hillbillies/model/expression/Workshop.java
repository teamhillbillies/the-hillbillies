package hillbillies.model.expression;

import java.util.Iterator;
import java.util.Set;

import hillbillies.model.Unit;
import hillbillies.model.pathfinding.Position;

/**
 * Class that defines the position of the closest workshop with the starting position the occupied cube
 * of the unit that's executing the statement where this expression points to
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 *
 */
public class Workshop extends BasicPositionE {
	
	/**
	 * Initialize this Workshop
	 * @post	The value of this Workshop is equal to 
	 * 			the position of the nearest workshop from the unit
	 * 			occupying the task
	 */
	public Workshop(){
	}
	
	/**
	 * Finds the workshop closest to the given starting position
	 * 
	 * @param 	startingPosition
	 * 			The position where the search starts
	 * @return	the workshop closest to the given starting position
	 * @throws 	IllegalArgumentException
	 * 			if no workshop is found
	 * 			
	 */
	public int[] findNearestWorkshop(int[] startingPosition) throws IllegalArgumentException{
		Set<hillbillies.model.pathfinding.Position> allWorkshops = getStatement().getTask().getUnit().getWorld().getAllWorkshops();
		double smallestDistance = Double.MAX_VALUE;
		Position nearestPosition = null;
		Iterator<Position> iterator = allWorkshops.iterator();
		while(iterator.hasNext()) {
			Position position = iterator.next();
			int[] positionLocation = new int[]{position.getX(),position.getY(),position.getZ()};
			int xDiff = positionLocation[0]-startingPosition[0];
			int yDiff = positionLocation[1]-startingPosition[1];
			int zDiff = positionLocation[2]-startingPosition[2];
			double distance = Math.pow(xDiff, 2) + Math.pow(yDiff, 2) + Math.pow(zDiff, 2);
			if (distance < smallestDistance) {
				try {
					getStatement().getTask().getUnit().getWorld().findPath(getUnitOccupiedCube(), positionLocation);
					smallestDistance = distance;
					nearestPosition = position;
				}
				catch(IllegalArgumentException e) {
				}
			}
		}
		if (nearestPosition == null) {
			throw new IllegalArgumentException();
		}
		return new int[]{nearestPosition.getX(),nearestPosition.getY(),nearestPosition.getZ()};
	}
	

	
	/**
	 * Returns the position of the nearest workshop
	 */
	@Override
	public int[] getValue(){
		return findNearestWorkshop(getUnitOccupiedCube());
	}
	
	/**
	 * Returns a textual representation of the nearest workshop
	 * 
	 * @return	string "workshop"
	 */
	@Override
	public String toString() {
		return "workshop";
	}
	
	/**
	 * Returns a clone of this expression
	 */
	@Override
	public E clone() {
		return new Workshop();
	}

}
