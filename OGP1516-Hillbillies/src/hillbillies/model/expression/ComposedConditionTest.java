package hillbillies.model.expression;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ComposedConditionTest {
	
	private static ComposedBooleanE someComposedCondition, immutableComposedCondition, expressionWithManyOperands,
	identicalCondition;
	private static PositionE subExpression, subExpression2;

	@Before
	public void setUp() throws Exception {
		int[] position = {1,1,1};
		subExpression = new Position(position);
		subExpression2 = new Position(position);
		someComposedCondition = new IsSolid(subExpression);
		identicalCondition = new IsSolid(subExpression2);
		immutableComposedCondition = new IsPassable(subExpression);
		expressionWithManyOperands = new AndCondition(someComposedCondition, immutableComposedCondition);
		
		
	}

	@Test
	public void equals_SameExpression() {
		assertTrue(someComposedCondition.equals(someComposedCondition));
		
	}
	
	@Test
	public void equals_DifferentExpression(){
		assertFalse(someComposedCondition.equals(immutableComposedCondition));
	}
	
	@Test
	public void equals_IdenticalExpression(){
		assertTrue(someComposedCondition.equals(identicalCondition));
	}
	
	@Test
	public void equals_ExpressionWithManyOperands(){
		assertFalse(someComposedCondition.equals(expressionWithManyOperands));
	}
	
	@Test
	public void equals_NonEffective(){
		assertFalse(someComposedCondition.equals(null));
	}

	@Test
	public void equals_DifferentKindExpression(){
		assertFalse(someComposedCondition.equals(subExpression));
	}
	
	@Test
	public void isMutable_mutableExpression(){
		//TODO
		//there isn't a mutable expression at the moment
	}
	
	@Test
	public void isImmutable_ImmutableExpression(){
		assertFalse(immutableComposedCondition.isMutable());
	}
	
	@Test
	public void canHaveAsNbOperands(){
		assertFalse(someComposedCondition.canHaveAsNbOperands(0));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void getOperandAt_NonPositiveIndex(){
		someComposedCondition.getOperandAt(0);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void getOperandAt_IndexTooHigh(){
		someComposedCondition.getOperandAt(someComposedCondition.getNbOperands()+1);
	}
	
	@Test
	public void canHaveAsOperand_TrueCase(){
		assertTrue(someComposedCondition.canHaveAsOperand(subExpression));
	}
	
	@Test
	public void canHaveAsOperand_FalseCase(){
		assertFalse(someComposedCondition.canHaveAsOperand(null));
	}
	
	@Test
	public void canHaveAsOperand_CyclicOperand(){
		assertFalse(someComposedCondition.canHaveAsOperand(expressionWithManyOperands));
	}
	
	@Test
	public void hasAsSubExpression_SameExpression(){
		assertTrue(someComposedCondition.hasAsSubExpression(someComposedCondition));
	}
	
	@Test
	public void hasAsSubExpression_SubExpression(){
		assertEquals(someComposedCondition.getOperandAt(1),subExpression);
		assertTrue(someComposedCondition.getOperandAt(1).hasAsSubExpression(subExpression));
		assertTrue(expressionWithManyOperands.hasAsSubExpression(someComposedCondition));
		
	}
	
	@Test
	public void hasAsSubExpression_FalseCase(){
		assertFalse(someComposedCondition.hasAsSubExpression(immutableComposedCondition));
	}
	
	@Test
	public void hasAsSubExpression_NullCase(){
		assertFalse(someComposedCondition.hasAsSubExpression(null));
	}
	
	

}
