package hillbillies.model.expression;

/**
 * A class that defines the position of the oocupied cube of the unit executing the
 * statement where this expression points to
 * 

 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 *
 */
public class ThisPosition extends BasicPositionE {
	
	/**
	 * Initialize this ThisPosition
	 * @post	The value of this ThisPosition is equal to 
	 * 			the position of the unit occupying the task
	 */
	public ThisPosition(){
	}
	
	/**
	 * 
	 * Returns the position of the unit excecuting the statement that this
	 * expression is pointing to
	 */	 
	public int[] getValue() {
		return getStatement().getTask().getUnit().getOccupiedCube();
	}
	
	/**
	 * Returns the textual representation of this class
	 * 
	 * @return	"thisPosition"
	 */
	@Override
	public String toString() {
		return "thisPosition";
	}
	
	/**
	 * Returns the clone of this expression
	 */
	@Override
	public E clone() {
		return new ThisPosition();
	}
}
