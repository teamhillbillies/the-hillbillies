package hillbillies.model.expression;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ReadVariableTest {
	
	private static ReadVariable variable1, variable2;

	@Before
	public void setUp() throws Exception {
		variable1 = new ReadVariable("variable1");
		variable2 = new ReadVariable("variable2");
	}

	@Test
	public void extendedConstructor() throws Exception {
		ReadVariable myVariable = new ReadVariable("myVariable");
		assertEquals(myVariable.getStringVariable(),"myVariable");
	}
	
	@Test
	public void getValue() throws Exception{
		//TODO
	}
	
	@Test
	public void equals_SameExpression() throws Exception{
		assertTrue(variable1.equals(variable1));
	}
	
	@Test
	public void equals_DifferentExpression() throws Exception{
		assertFalse(variable1.equals(variable2));
	}
	
	@Test
	public void equals_NonEffective() throws Exception{
		assertFalse(variable1.equals(null));
	}
	
	@Test
	public void equals_DifferentType() throws Exception{
		assertFalse(variable1.equals(new False()));
	}
	
	@Test
	public void toStringReadVariable() throws Exception{
		//TODO
	}

}
