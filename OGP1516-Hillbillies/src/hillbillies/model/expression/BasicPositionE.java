package hillbillies.model.expression;

/**
 * A class of basic position expressions, these do not contain any additional subexpressions
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 */
public abstract class BasicPositionE extends PositionE {

	/**
	 * @return	false, since basic position expressions do not contain subexpressions
	 */
	@Override
	public boolean hasAsSubExpression(E expression) {
		return false;
	}

}
