package hillbillies.model.expression;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import hillbillies.model.Task;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.model.statement.ExpressionStatement;
import hillbillies.model.statement.MoveTo;
import hillbillies.model.statement.S;
import hillbillies.part2.facade.Facade;
import hillbillies.part2.listener.DefaultTerrainChangeListener;

public class IsPassableTest {

	private static final int TYPE_AIR = 0;
	private static final int TYPE_ROCK = 1;
	private static final int TYPE_TREE = 2;
	private static final int TYPE_WORKSHOP = 3;
	private Facade facade;
	private World world;
	private static PositionE solidPosition,passablePosition;
	private static IsPassable trueIsPassable, falseIsPassable;
	private static S statement;
	private Unit unit0;
	private Task task1;

	@Before
	public void setUp() throws Exception {
		
		int[][][] types = new int[3][3][3];
		types[1][1][0] = TYPE_AIR;
		types[1][1][1] = TYPE_TREE;
		types[1][1][2] = TYPE_WORKSHOP;
		
		this.facade = new Facade();
		
		double[] unitPosition = {1,1,1};
		unit0 = new Unit("Unit Low0",unitPosition,25,25,25,25,false);

		world = facade.createWorld(types, new DefaultTerrainChangeListener());
		int[] position1 = {1,1,1};
		solidPosition = new Position(position1);
		falseIsPassable = new IsPassable(solidPosition);
		
		world.addUnit(unit0);
		
		statement = new MoveTo(solidPosition);
		
		task1 = new Task("TaskTest1",10,statement, position1);
		task1.linkUnit(unit0);
		
		int[] position2 = {1,1,0};
		passablePosition = new Position(position2);
		trueIsPassable = new IsPassable(passablePosition);
		
		trueIsPassable.transferStatement((ExpressionStatement) statement);
		falseIsPassable.transferStatement((ExpressionStatement) statement);
		
		
	}

	@Test
	public void extendedConstructor() throws Exception{
		IsPassable myIsPassable = new IsPassable(solidPosition);
	}
	
	@Test
	public void getValue() throws Exception{
		assertTrue(trueIsPassable.getValue());
		assertFalse(falseIsPassable.getValue());
	}
	
	@Test
	public void toStringIsPassable() throws Exception{
		int[] position = {1,1,0};
		assertEquals(trueIsPassable.toString(),"isPassable( " + Arrays.toString(position) + " )");
	}
	
	@Test
	public void cloneIsPassable() throws Exception{
		trueIsPassable.clone();
	}

}
