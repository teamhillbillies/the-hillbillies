package hillbillies.model.expression;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import hillbillies.model.Task;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.model.statement.ExpressionStatement;
import hillbillies.model.statement.MoveTo;
import hillbillies.model.statement.S;
import hillbillies.part2.facade.Facade;
import hillbillies.part2.listener.DefaultTerrainChangeListener;

public class NotConditionTest {

	private static final int TYPE_AIR = 0;
	private static final int TYPE_ROCK = 1;
	private static final int TYPE_TREE = 2;
	private static final int TYPE_WORKSHOP = 3;
	private Facade facade;
	private World world;
	private static PositionE solidPosition,passablePosition;
	private static IsPassable trueIsPassable, falseIsPassable;
	private static S statement;
	private Unit unit0;
	private Task task1;
	private NotCondition trueNotCondition, falseNotCondition;

	@Before
	public void setUp() throws Exception {
		
		int[][][] types = new int[3][3][3];
		types[1][1][0] = TYPE_AIR;
		types[1][1][1] = TYPE_TREE;
		types[1][1][2] = TYPE_WORKSHOP;
		
		this.facade = new Facade();
		
		double[] unitPosition = {1,1,1};
		unit0 = new Unit("Unit Low0",unitPosition,25,25,25,25,false);

		world = facade.createWorld(types, new DefaultTerrainChangeListener());
		int[] position1 = {1,1,1};
		solidPosition = new Position(position1);
		falseIsPassable = new IsPassable(solidPosition);
		
		world.addUnit(unit0);
		
		statement = new MoveTo(solidPosition);
		
		task1 = new Task("TaskTest1",10,statement, position1);
		task1.linkUnit(unit0);
		
		int[] position2 = {1,1,0};
		passablePosition = new Position(position2);
		trueIsPassable = new IsPassable(passablePosition);
		
		falseNotCondition = new NotCondition(trueIsPassable);
		trueNotCondition = new NotCondition(falseIsPassable);
		
		falseNotCondition.transferStatement((ExpressionStatement) statement);
		trueNotCondition.transferStatement((ExpressionStatement) statement);
		
		
	}

	@Test
	public void extendedConstructor() throws Exception{
		NotCondition myNotCondition = new NotCondition(trueIsPassable);
		assertEquals(myNotCondition.getOperand(),trueIsPassable);
	}
	
	@Test
	public void getValue() throws Exception{
		assertTrue(trueNotCondition.getValue());
		assertFalse(falseNotCondition.getValue());
	}
	
	@Test
	public void toStringNotCondition() throws Exception{
		trueIsPassable.transferStatement((ExpressionStatement) statement);
		assertEquals(trueNotCondition.toString(),"!" + falseIsPassable.toString());
	}
	
	@Test
	public void cloneNotCondition() throws Exception{
		trueNotCondition.clone();
	}

}
