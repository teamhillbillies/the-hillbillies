package hillbillies.model.expression;

import hillbillies.model.Unit;

/**
 * Class consisting is-condition that check if the given unit is dead
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 *
 */
public class IsAlive extends IsCondition {
	
	/**
	 * Initializes this is-condition with the given unit.
	 * 
	 * @param 	unit
	 * 			The unit to be checked
	 * @effect	This new IsDead is initialized as an is-condition
	 *         	with the given unit
	 */
	public IsAlive(UnitE unit) throws IllegalArgumentException{
		super(unit);
	}
	
	public IsAlive(E unit) throws IllegalArgumentException{
		super(unit);
	}
	
	/**
	 * Returns a textual representation of the IsDead-condition
	 * 
	 * @return "IsDead followed by the name of the unit to be checked between parenthesis
	 */
	@Override
	public String toString() {
		getOperandAt(1).transferStatement(getStatement());
		return "isAlive( " + ((Unit)getOperandAt(1).getValue()).getName() + " )";
	}
	
	/**
	 * Returns the value of this condition
	 * 
	 * @return	true if the given unit is dead.
	 */
	@Override
	public Boolean getValue() {
		// TODO Auto-generated method stub
		getOperandAt(1).transferStatement(getStatement());
		return !((Unit)getOperandAt(1).getValue()).isDead();
	}

	/**
	 * Returns a clone of this expression
	 */
	@Override
	public E clone() {
		return new IsAlive((UnitE) getOperandAt(1).clone());
	}
}
