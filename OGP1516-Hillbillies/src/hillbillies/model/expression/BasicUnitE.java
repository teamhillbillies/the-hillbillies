package hillbillies.model.expression;

/**
 * A class of basic unit expressions, these do not contain any additional subexpressions
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 */
public abstract class BasicUnitE extends UnitE {

	/**
	 * @return	false, since basic unit expressions do not contain subexpressions
	 */
	@Override
	public boolean hasAsSubExpression(E expression) {
		return false;
	}

}