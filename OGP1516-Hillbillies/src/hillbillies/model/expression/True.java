package hillbillies.model.expression;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;

/**
 * A class that defines true
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 */
public class True extends BasicBooleanE {
	
	/**
	 * Returns the value of this expression which is always false
	 */
	@Override
	@Basic @Immutable
	public Boolean getValue() {
		return true;
	}

	/**
	 * checks whether the state of this can be changed
	 * 
	 * @return returns always false
	 */
	@Override
	public boolean isMutable() {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * Check whether this is equal to the given object
	 * 
	 * @return 	true if and only if the given object is instance of E
	 * 			and it's value is equal to the value of this
	 */
	@Override
	public boolean equals(Object other) {
		// TODO Auto-generated method stub
		return ((other instanceof E) &&(((E) other).getValue() == this.getValue()));
	}

	/**
	 * Returns a textual representation of this
	 * 
	 * @return "true"
	 */
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "true";
	}
	
	/**
	 * Returns a clone of this expression
	 */
	@Override
	public E clone() {
		return new True();
	}
}
