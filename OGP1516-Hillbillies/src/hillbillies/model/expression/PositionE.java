package hillbillies.model.expression;

import java.util.Arrays;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
/**
 * A class that defines a position
 * 
 * @note 	the given position must be a valid position 
 * 			of the world in which it is connected with
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0.
 *
 */
public abstract class PositionE extends E {
	
	

	/**
	 * Returns the value of this PositionE
	 */
	@Override
	@Basic @Immutable
	public abstract int[] getValue();
	
	/**
	 * Returns whether this PositionE is mutable
	 * 
	 * @return	returns always false
	 */
	@Override
	public boolean isMutable() {
		return false;
	}

	/**
	 * Returns whether the given object is equal to this PositionE
	 * 
	 * @return	true only if the given object is instance of PositionE and
	 * 			the value of this is equal to the value of the given object
	 * 
	 */
	@Override
	public boolean equals(Object other) {
		return (other instanceof PositionE) && (Arrays.equals(((PositionE)other).getValue(), this.getValue()));
	}
	

}
