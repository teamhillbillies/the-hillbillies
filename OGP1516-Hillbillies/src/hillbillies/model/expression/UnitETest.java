package hillbillies.model.expression;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import hillbillies.model.Task;
import hillbillies.model.Unit;
import hillbillies.model.statement.*;

public class UnitETest {
	
	private static UnitE unitE1, unitE2;
	private static S statement1, statement2;
	private static Task task1, task2;
	private static Unit unit1, unit2;

	@Before
	public void setUp() throws Exception {
		unitE1 = new This();
		unitE2 = new This();
		statement1 = new Follow(unitE1);
		statement2 = new Follow(unitE2);
		int[] position = {0,0,0};
		task1 = new Task("TaskTest1",10,statement1, position);
		task2 = new Task("TaskTest2",10,statement2, position);
		double[] unitPosition = {0,0,0};
		unit1 = new Unit("Unit1",unitPosition, 100,100,100,100,false);
		unit2 = new Unit("Unit2",unitPosition, 100,100,100,100,false);
		task1.linkUnit(unit1);
		task2.linkUnit(unit2);
	}


	@Test
	public void getValue() throws Exception{
		unitE1.transferStatement((ExpressionStatement)statement1);
		assertEquals(unitE1.getValue(),unit1);
	}
	
	 @Test
	 public void isMutable() throws Exception{
		 assertFalse(unitE1.isMutable());
	 }
	 
	 @Test
	 public void equals_DifferentExpression() throws Exception{
		 unitE1.transferStatement((ExpressionStatement)statement1);
		 unitE2.transferStatement((ExpressionStatement)statement2);
		 assertFalse(unitE1.equals(unitE2));
	 }
	 
	 @Test
	 public void equals_SameExpression() throws Exception{
		 unitE1.transferStatement((ExpressionStatement)statement1);
		 assertTrue(unitE1.equals(unitE1));
	 }
	 
	 @Test
	 public void equals_DifferentType() throws Exception{
		 unitE1.transferStatement((ExpressionStatement)statement1);
		 assertFalse(unitE1.equals(new False()));
	 }
	 
	 @Test
	 public void equals_NonEffective() throws Exception{
		 unitE1.transferStatement((ExpressionStatement)statement1);
		 assertFalse(unitE1.equals(null));
	 }

}
