package hillbillies.model.expression;

/**
 * A class of basic expressions, it's value can be obtained without
 * computations
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 */
public abstract class BasicBooleanE extends BooleanE {

	/**
	 * @return	false
	 */
	@Override
	public boolean hasAsSubExpression(E expression) {
		return false;
	}

}
