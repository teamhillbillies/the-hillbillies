package hillbillies.model.expression;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import hillbillies.model.Task;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.model.statement.ExpressionStatement;
import hillbillies.model.statement.MoveTo;
import hillbillies.model.statement.S;
import hillbillies.part2.listener.DefaultTerrainChangeListener;

public class ThisTest {
	
	private World world;
	private int nbX;
	private int nbY;
	private int nbZ;
	private Unit unit0, unit1, unit2, unit3 ,unit4, unit5;
	private static This someThis;

	@Before
	public void setUp() throws Exception {
		nbX = 10;
		nbY = 20;
		nbZ = 30;
		
		world = new World(new int[nbX][nbY][nbZ], new DefaultTerrainChangeListener());
		double[] unitPosition = {0,0,0};
		unit0 = new Unit("Unit Low0",unitPosition,25,25,25,25,false);
		world.addUnit(unit0);
		int[] position = {0,0,0};
		PositionE positionExpression = new Position(position);
		S statement = new MoveTo(positionExpression);
		Task task1 = new Task("task1", 100, statement, position );
		task1.linkUnit(unit0);
		someThis = new This();
		someThis.transferStatement((ExpressionStatement) statement);	
	}

	@Test
	public void extendedConstructor() throws Exception {
		This myThis = new This();
	}
	
	@Test
	public void getValue() throws Exception {
		assertEquals(someThis.getValue(),unit0);
	}
	
	@Test
	public void toStringThis() throws Exception {
		assertEquals(someThis.toString(),"this");
	}
	
	@Test
	public void thisClone() throws Exception {
		someThis.clone();
	}

}
