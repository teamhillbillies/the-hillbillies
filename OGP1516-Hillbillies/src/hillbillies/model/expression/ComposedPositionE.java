package hillbillies.model.expression;

import be.kuleuven.cs.som.annotate.Model;

public abstract class ComposedPositionE extends PositionE {


	/**
	 * Initialize this new unary condition with given operand.
	 *
	 * @param  operand2
	 *         The operand for this new unary condition.
	 * @post   The operand for this new unary condition is the
	 *         same as the given operand.
	 * @throws IllegalArgumentExeption
	 *         This new unary condition cannot have the given
	 *         operand as its operand.
	 */
	@Model
	protected ComposedPositionE(E operand2) throws IllegalArgumentException {
		if (!canHaveAsOperand(operand2))
			throw new IllegalArgumentException("can't initialize UnaryCondition");
		setOperandAt(1,operand2);
	}
	
	/**
	 * Return the number of operands involved in this is-condition.
	 *
	 * @return Always returns 1
	 */
	public int getNbOperands() {
		return 1;
	}
	
	/**
	 * Check whether this unary condition can have the given
	 * number as its number of operands.
	 *
	 * @return True if and only if the given number is 1.
	 */
	public boolean canHaveAsNbOperands(int number){
		return number == 1;
	}
	
	/**
	 * Check whether this composed position expression can have the given
	 * expressions as one of its operands.
	 *
	 * @param  expression
	 *         The expression to check.
	 * @return True if and only if the given expression is effective,
	 *         and if that expression does not have this composed
	 *         position expression as a subexpression.
	 */
	public boolean canHaveAsOperand(E expression){
		return ((expression != null) && (!expression.hasAsSubExpression(this)));
	}
	
	/**
	 * Check whether this composed position expression has the given expression
	 * as one of its subexpressions.
	 *
	 * @return True if and only if the given expression is the same
	 *         expression as this composed position expression, or if the given
	 *         expression is a subexpression of one of the operands
	 *         of this composed position expression.
	 */
	@Override
	public boolean hasAsSubExpression(E expression) {
		if (expression == this){
			return true;
		}
		
		for (int ind=1; ind<getNbOperands(); ind++){
			if (getOperandAt(ind).hasAsSubExpression(expression))
				return true;
		}
		return false;
	}
	
	/**
	 * Return the operand of this unary condition at the given index.
	 * 
	 * @return The one and only operand of this unary condition.
	 */
	public E getOperandAt(int index) throws IllegalArgumentException {
		if (index!=1)
			throw new IllegalArgumentException("not a valid index");
		return getOperand();
	}
	
	/**
	 * Return the only operand of this unary condition.
	 * 
	 * @return the one and only operand of this unary condition
	 */
	public E getOperand(){
		return this.operand;
		
	}
	
	/**
	 * Set the operand for this unary condition at the given
	 * index to the given operand.
	 */
	protected void setOperandAt(int index, E operand) {
		this.operand = operand;

	}
	
	/**
	 * Variable referencing the operand of this unary condition.
	 */
	private E operand;


}
