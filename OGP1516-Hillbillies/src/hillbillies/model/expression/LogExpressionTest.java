package hillbillies.model.expression;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import hillbillies.model.Boulder;
import hillbillies.model.Log;
import hillbillies.model.Task;
import hillbillies.model.TerrainType;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.model.statement.ExpressionStatement;
import hillbillies.model.statement.MoveTo;
import hillbillies.model.statement.S;
import hillbillies.part2.facade.Facade;
import hillbillies.part2.listener.DefaultTerrainChangeListener;

public class LogExpressionTest {

	private World world;
	private int nbX;
	private int nbY;
	private int nbZ;
	
	private static final int TYPE_AIR = 0;
	private static final int TYPE_ROCK = 1;
	private static final int TYPE_TREE = 2;
	private static final int TYPE_WORKSHOP = 3;
	private Facade facade;
	private LogExpression logExpression;
	


	@Before
	public void setUp() throws Exception {
		
		nbX = 10;
		nbY = 20;
		nbZ = 30;
		
		world = new World(new int[nbX][nbY][nbZ], new DefaultTerrainChangeListener());
		
		logExpression = new LogExpression();
	}
	
	@Test
	public void extendedConstructor() throws Exception{
		LogExpression logExpression = new LogExpression();
	}
	
	@Test
	public void geValue() throws Exception {
		int[] position = {0,0,0};
		PositionE positionExpression = new Position(position);
		S statement = new MoveTo(positionExpression);
		Task task1 = new Task("task1", 100, statement, position );
		double[] unitPosition = {0,0,0};
		Unit unit0 = new Unit("UnitHigh",unitPosition, 100,100,100,100,false);
		task1.linkUnit(unit0);
		world.addUnit(unit0);
		hillbillies.model.pathfinding.Position position2 = new hillbillies.model.pathfinding.Position(position);
		Log log = new Log(position);
		world.addItem(log, position2);
		logExpression.transferStatement((ExpressionStatement) statement);
		Arrays.equals(logExpression.getValue(),position);
	}
	
	@Test
	public void equals_SameExpression() throws Exception{
		int[] position = {0,0,0};
		PositionE positionExpression = new Position(position);
		S statement = new MoveTo(positionExpression);
		Task task1 = new Task("task1", 100, statement, position );
		double[] unitPosition = {0,0,0};
		Unit unit0 = new Unit("UnitHigh",unitPosition, 100,100,100,100,false);
		task1.linkUnit(unit0);
		world.addUnit(unit0);
		hillbillies.model.pathfinding.Position position2 = new hillbillies.model.pathfinding.Position(position);
		Log log = new Log(position);
		world.addItem(log, position2);
		logExpression.transferStatement((ExpressionStatement) statement);
		assertTrue(logExpression.equals(logExpression));
	}
	
	@Test
	public void equals_DifferentType() throws Exception{
		int[] position = {0,0,0};
		PositionE positionExpression = new Position(position);
		S statement = new MoveTo(positionExpression);
		Task task1 = new Task("task1", 100, statement, position );
		double[] unitPosition = {0,0,0};
		Unit unit0 = new Unit("UnitHigh",unitPosition, 100,100,100,100,false);
		task1.linkUnit(unit0);
		world.addUnit(unit0);
		hillbillies.model.pathfinding.Position position2 = new hillbillies.model.pathfinding.Position(position);
		Log log = new Log(position);
		world.addItem(log, position2);
		logExpression.transferStatement((ExpressionStatement) statement);
		assertFalse(logExpression.equals(new False()));
	}
	
	
	@Test
	public void toStringBoulderExpression() throws Exception{
		assertEquals(logExpression.toString(),"log");
	}
	
	@Test
	public void cloneLogExpression() throws Exception{
		int[] position = {0,0,0};
		PositionE positionExpression = new Position(position);
		S statement = new MoveTo(positionExpression);
		Task task1 = new Task("task1", 100, statement, position );
		double[] unitPosition = {0,0,0};
		Unit unit0 = new Unit("UnitHigh",unitPosition, 100,100,100,100,false);
		task1.linkUnit(unit0);
		world.addUnit(unit0);
		hillbillies.model.pathfinding.Position position2 = new hillbillies.model.pathfinding.Position(position);
		Log log = new Log(position);
		world.addItem(log, position2);
		logExpression.transferStatement((ExpressionStatement) statement);
		logExpression.clone();
	}
}
