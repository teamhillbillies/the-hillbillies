package hillbillies.model.expression;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import hillbillies.model.Task;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.model.statement.ExpressionStatement;
import hillbillies.model.statement.Follow;
import hillbillies.model.statement.S;
import hillbillies.part2.listener.DefaultTerrainChangeListener;

public class IsEnemyTest {

	private World world;
	private int nbX;
	private int nbY;
	private int nbZ;
	private Unit unit0, unit1, unit2, unit3 ,unit4, unit5;

	private static UnitE unitE3, unitE4, unitE5;
	private static S statement1, statement2, statement3;
	private static Task task1, task2;
	private static IsEnemy trueIsEnemy, falseIsEnemy;

	@Before
	public void setUp() throws Exception {
		nbX = 10;
		nbY = 20;
		nbZ = 30;
		
		world = new World(new int[nbX][nbY][nbZ], new DefaultTerrainChangeListener());
		double[] unitPosition = {0,0,0};
		unit0 = new Unit("Unit Low0",unitPosition,25,25,25,25,false);
		unit1 = new Unit("Unit Low1",unitPosition,25,25,25,25,false);
		unit2 = new Unit("Unit Low2",unitPosition,25,25,25,25,false);
		unit3 = new Unit("Unit Low3",unitPosition,25,25,25,25,false);
		unit4 = new Unit("Unit Low4",unitPosition,25,25,25,25,false);
		double[] newPosition = {0,0,0};
		unit5 = new Unit("Unit Low4",newPosition,25,25,25,25,false);
		
		world.addUnit(unit0);
		world.addUnit(unit1);
		world.addUnit(unit2);
		world.addUnit(unit3);
		world.addUnit(unit4);
		world.addUnit(unit5);
		
		unitE3 = new This();
		unitE4 = new This();
		unitE5 = new Enemy();
		
		statement1 = new Follow(unitE4);
		statement2 = new Follow(unitE5);
		int[] position = {0,0,0};
		task1 = new Task("TaskTest1",10,statement1, position);
		task2 = new Task("TaskTest2",10,statement2, position);
		task1.linkUnit(unit4);
		task2.linkUnit(unit5);
		unit2.consumeHitpoints(1000);
		falseIsEnemy = new IsEnemy(unitE4);
		trueIsEnemy = new IsEnemy(unitE5);
		falseIsEnemy.transferStatement((ExpressionStatement) statement1);
		trueIsEnemy.transferStatement((ExpressionStatement) statement2);
	}

	@Test
	public void extendedConstructor() throws Exception {
		IsEnemy myIsEnemy = new IsEnemy(unitE3);
	}
	
	@Test
	public void getValue() throws Exception{
		assertFalse(falseIsEnemy.getValue());
		assertTrue(trueIsEnemy.getValue());
	}
	
	@Test
	public void toStringIsFriend() throws Exception{
		assertEquals(falseIsEnemy.toString(),"isEnemy( Unit Low4 )");
	}
	
	@Test
	public void cloneIsFriend() throws Exception{
		falseIsEnemy.clone();
	}

}
