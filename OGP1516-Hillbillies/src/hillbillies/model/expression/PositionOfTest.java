package hillbillies.model.expression;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import hillbillies.model.Task;
import hillbillies.model.Unit;
import hillbillies.model.statement.ExpressionStatement;
import hillbillies.model.statement.Follow;
import hillbillies.model.statement.S;

public class PositionOfTest {
	private static UnitE unitE1, unitE2;
	private static S statement1, statement2;
	private static Task task1, task2;
	private static Unit unit1, unit2;
	private static PositionOf positionOf1, positionOf2;

	@Before
	public void setUp() throws Exception {
		unitE1 = new This();
		unitE2 = new This();
		statement1 = new Follow(unitE1);
		statement2 = new Follow(unitE2);
		int[] position1 = {0,0,0};
		int[] position2 = {1,1,1};
		task1 = new Task("TaskTest1",10,statement1, position1);
		task2 = new Task("TaskTest2",10,statement2, position2);
		double[] unitPosition1 = {0,0,0};
		double[] unitPosition2 = {2,2,2};
		unit1 = new Unit("Unit1",unitPosition1, 100,100,100,100,false);
		unit2 = new Unit("Unit2",unitPosition2, 100,100,100,100,false);
		task1.linkUnit(unit1);
		task2.linkUnit(unit2);
		positionOf1 = new PositionOf(unitE1);
		positionOf2 = new PositionOf(unitE2);
		positionOf1.transferStatement((ExpressionStatement) statement1);
		positionOf2.transferStatement((ExpressionStatement) statement2);
	}

	@Test
	public void extendedConstructor() throws Exception {
		PositionOf myPositionOf = new PositionOf(unitE2);
	}
	
	@Test
	public void getValue() throws Exception{
		int[] position = {0,0,0};
		int[] position2 = {2,2,2};
		assertTrue(Arrays.equals(positionOf1.getValue(), position));
		assertTrue(Arrays.equals(positionOf2.getValue(), position2));
	}
	
	@Test
	public void toStringPositionOf() throws Exception{
		assertEquals(positionOf1.toString(),"position_of " + unitE1.toString());
	}
	
	@Test
	public void clonePositionOf() throws Exception{
		positionOf1.clone();
	}
	

}
