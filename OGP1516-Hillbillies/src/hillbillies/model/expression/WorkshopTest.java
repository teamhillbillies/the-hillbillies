package hillbillies.model.expression;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import hillbillies.model.Task;
import hillbillies.model.TerrainType;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.model.statement.ExpressionStatement;
import hillbillies.model.statement.MoveTo;
import hillbillies.model.statement.S;
import hillbillies.part2.facade.Facade;
import hillbillies.part2.listener.DefaultTerrainChangeListener;

public class WorkshopTest {
	
	private Workshop someWorkShopExpression;
	private World world;

	
	private static final int TYPE_AIR = 0;
	private static final int TYPE_ROCK = 1;
	private static final int TYPE_TREE = 2;
	private static final int TYPE_WORKSHOP = 3;
	private Facade facade;

	@Before
	public void setUp() throws Exception {
		
		int[][][] types = new int[3][3][3];
		types[1][1][0] = TYPE_ROCK;
		types[1][1][1] = TYPE_TREE;
		types[1][1][2] = TYPE_WORKSHOP;
		
		this.facade = new Facade();

		world = facade.createWorld(types, new DefaultTerrainChangeListener());
		int[] position = {1,1,2};
		PositionE positionExpression = new Position(position);
		S statement = new MoveTo(positionExpression);
		Task task1 = new Task("task1", 100, statement, position );
		double[] unitPosition = {1,1,2};
		Unit unit0 = new Unit("UnitHigh",unitPosition, 100,100,100,100,false);
		task1.linkUnit(unit0);
		world.addUnit(unit0);
		someWorkShopExpression = new Workshop();
		someWorkShopExpression.transferStatement((ExpressionStatement) statement);
	}


	@Test
	public void extendedConstructor() throws Exception{
		Workshop myWorkshopExpression = new Workshop();
	}
	
	@Test
	public void getValue() throws Exception{
		int[] newPosition = {1,1,2};
		Arrays.equals(newPosition, someWorkShopExpression.getValue());
	}
	
	@Test
	public void toStringWorkShop() throws Exception{
		assertEquals(someWorkShopExpression.toString(),"workshop");
	}
	
	@Test
	public void workshopClone() throws Exception{
		someWorkShopExpression.clone();
	}
	

}
