package hillbillies.model.expression;

import java.util.Iterator;
import java.util.Set;

import hillbillies.model.Unit;

/**
 * Class that defines the closest unit with the starting position: the occupied cube
 * of the unit that's executing the statement where this expression points to
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 *
 */
public class Any extends BasicUnitE {
	
	/**
	 * Initialize this Any
	 * @effect	The value of this Any is equal to 
	 * 			the nearest unit from the unit
	 * 			occupying the task
	 */
	public Any(){
	}
	
	/**
	 * Finds the nearest unit in the world starting from the given position
	 * @param 	startingPosition
	 * 			the position where the search starts
	 * @return	the closest unit to the given starting position
	 * @throws 	IllegalArgumentException
	 * 			if no unit is found
	 */
	public Unit findNearestUnit(int[] startingPosition) throws IllegalArgumentException{
		Set<Unit> allUnits = getStatement().getTask().getUnit().getWorld().getAllUnits();
		double smallestDistance = Double.MAX_VALUE;
		Unit nearestUnit = null;
		Iterator<Unit> iterator = allUnits.iterator();
		while(iterator.hasNext()) {
			Unit unit = iterator.next();
			int[] unitLocation = unit.getOccupiedCube();
			int xDiff = unitLocation[0]-startingPosition[0];
			int yDiff = unitLocation[1]-startingPosition[1];
			int zDiff = unitLocation[2]-startingPosition[2];
			double distance = Math.pow(xDiff, 2) + Math.pow(yDiff, 2) + Math.pow(zDiff, 2);
			if (distance < smallestDistance && unit!=getStatement().getTask().getUnit()) {
				try {
					getStatement().getTask().getUnit().getWorld().findPath(getUnitOccupiedCube(), unitLocation);
					smallestDistance = distance;
					nearestUnit = unit;
				}
				catch(IllegalArgumentException e) {
				}
			}
		}
		if (nearestUnit == null) {
			throw new IllegalArgumentException();
		}
		return nearestUnit;
		
	}
	
	/**
	 * Returns the occupied cube of the nearest unit
	 */
	public Unit getValue() throws IllegalArgumentException{
		return findNearestUnit(getUnitOccupiedCube());
	}
	
	/**
	 * Returns a textual representation of this class
	 * 
	 * @return	always returns "any"
	 */
	public String toString(){
		return "any";
	}
	
	/**
	 * Returns a clone of this expression
	 */
	@Override
	public E clone() {
		return new Any();
	}

}
