package hillbillies.model.expression;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import hillbillies.model.Task;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.model.statement.ExpressionStatement;
import hillbillies.model.statement.MoveTo;
import hillbillies.model.statement.S;
import hillbillies.part2.listener.DefaultTerrainChangeListener;

public class FriendTest {

	private World world;
	private int nbX;
	private int nbY;
	private int nbZ;
	private Unit unit0, unit1, unit2, unit3 ,unit4, unit5;
	private static Friend friend;
	

	@Before
	public void setUp() throws Exception {
		nbX = 10;
		nbY = 20;
		nbZ = 30;
		
		world = new World(new int[nbX][nbY][nbZ], new DefaultTerrainChangeListener());
		double[] unitPosition = {0,0,0};
		unit0 = new Unit("Unit Low0",unitPosition,25,25,25,25,false);
		unit1 = new Unit("Unit Low1",unitPosition,25,25,25,25,false);
		unit2 = new Unit("Unit Low2",unitPosition,25,25,25,25,false);
		unit3 = new Unit("Unit Low3",unitPosition,25,25,25,25,false);
		unit4 = new Unit("Unit Low4",unitPosition,25,25,25,25,false);
		double[] newPosition = {0,0,0};
		unit5 = new Unit("Unit Low4",newPosition,25,25,25,25,false);
		
		world.addUnit(unit0);
		world.addUnit(unit1);
		world.addUnit(unit2);
		world.addUnit(unit3);
		world.addUnit(unit4);
		world.addUnit(unit5);
		int[] position = {0,0,0};
		PositionE positionExpression = new Position(position);
		S statement = new MoveTo(positionExpression);
		Task task1 = new Task("task1", 100, statement, position );
		task1.linkUnit(unit4);
		friend = new Friend();
		friend.transferStatement((ExpressionStatement) statement);	
	}

	@Test
	public void extendedConstructor() throws Exception {
		Friend myFriend = new Friend();
	}
	
	@Test
	public void getValue() throws Exception{
		assertEquals(friend.getValue(),unit5);
	}
	
	@Test
	public void toStringFriend() throws Exception{
		assertEquals(friend.toString(),"friend");
	}
	
	@Test
	public void cloneFriend() throws Exception{
		friend.clone();
	}
	
	@Test
	public void sameFaction() throws Exception{
		assertEquals(unit4.getFaction(),unit5.getFaction());
	}

}
