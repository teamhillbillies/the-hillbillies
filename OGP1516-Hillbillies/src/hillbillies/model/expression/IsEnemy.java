package hillbillies.model.expression;

import hillbillies.model.Unit;

/**
 * Class consisting is-condition that check if the given unit is an enemy
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 *
 */
public class IsEnemy extends IsCondition {
	
	/**
	 * Initializes this is-condition with the given unit.
	 * 
	 * @param 	unit
	 * 			The unit to be checked
	 * @effect	This new IsEnemy is initialized as an is-condition
	 *         	with the given unit
	 */
	public IsEnemy(UnitE unit) throws IllegalArgumentException{
		super(unit);
	}
	
	public IsEnemy(E unit) throws IllegalArgumentException{
		super(unit);
	}
	
	/**
	 * Returns a textual representation of the IsEnemy
	 * 
	 * @return "isEnemy followed by the name of the unit to be checked between parenthesis
	 */
	@Override
	public String toString() {
		getOperandAt(1).transferStatement(getStatement());
		return "isEnemy( " + ((Unit)getOperandAt(1).getValue()).getName() + " )";
	}
	
	/**
	 * Returns the value of this condition
	 * 
	 * @return	true if the given unit is an enemy
	 */
	@Override
	public Boolean getValue() {
		// TODO Auto-generated method stub
		getOperandAt(1).transferStatement(getStatement());
		return (this.getStatement().getTask().getUnit().getFaction() != ((Unit)getOperandAt(1).getValue()).getFaction());
	}

	/**
	 * Returns a clone of this expression
	 */
	@Override
	public E clone() {
		return new IsEnemy((UnitE) getOperandAt(1).clone());
	}
}
