package hillbillies.model.expression;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import hillbillies.model.Boulder;
import hillbillies.model.Task;
import hillbillies.model.TerrainType;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.model.statement.ExpressionStatement;
import hillbillies.model.statement.MoveTo;
import hillbillies.model.statement.S;
import hillbillies.part2.facade.Facade;
import hillbillies.part2.listener.DefaultTerrainChangeListener;

public class HereTest {
	
	private World world;
	private int nbX;
	private int nbY;
	private int nbZ;
	
	private static final int TYPE_AIR = 0;
	private static final int TYPE_ROCK = 1;
	private static final int TYPE_TREE = 2;
	private static final int TYPE_WORKSHOP = 3;
	private Facade facade;
	private Here here;

	@Before
	public void setUp() throws Exception {
			
		nbX = 10;
		nbY = 20;
		nbZ = 30;
		
		world = new World(new int[nbX][nbY][nbZ], new DefaultTerrainChangeListener());
		
		here = new Here();
	}

	@Test
	public void extendedConstructor() throws Exception{
		Here hereExpression = new Here();
	}
	
	@Test
	public void getValue() throws Exception{
		int[] position = {0,0,0};
		PositionE positionExpression = new Position(position);
		S statement = new MoveTo(positionExpression);
		Task task1 = new Task("task1", 100, statement, position );
		double[] unitPosition = {0,0,0};
		Unit unit0 = new Unit("UnitHigh",unitPosition, 100,100,100,100,false);
		task1.linkUnit(unit0);
		world.addUnit(unit0);
		here.transferStatement((ExpressionStatement) statement);
		Arrays.equals(here.getValue(),position);
	}
	
	@Test
	public void equals_SameExpression() throws Exception{
		int[] position = {0,0,0};
		PositionE positionExpression = new Position(position);
		S statement = new MoveTo(positionExpression);
		Task task1 = new Task("task1", 100, statement, position );
		double[] unitPosition = {0,0,0};
		Unit unit0 = new Unit("UnitHigh",unitPosition, 100,100,100,100,false);
		task1.linkUnit(unit0);
		world.addUnit(unit0);
		here.transferStatement((ExpressionStatement) statement);
		assertTrue(here.equals(here));
	}
	
	@Test
	public void equals_DifferentExpression() throws Exception{
		int[] position = {0,0,0};
		PositionE positionExpression = new Position(position);
		S statement = new MoveTo(positionExpression);
		Task task1 = new Task("task1", 100, statement, position );
		double[] unitPosition = {0,0,0};
		Unit unit0 = new Unit("UnitHigh",unitPosition, 100,100,100,100,false);
		task1.linkUnit(unit0);
		world.addUnit(unit0);
		here.transferStatement((ExpressionStatement) statement);
		assertFalse(here.equals(new False()));
	}
	
	@Test
	public void toStringHere() throws Exception{
		int[] position = {0,0,0};
		PositionE positionExpression = new Position(position);
		S statement = new MoveTo(positionExpression);
		Task task1 = new Task("task1", 100, statement, position );
		double[] unitPosition = {0,0,0};
		Unit unit0 = new Unit("UnitHigh",unitPosition, 100,100,100,100,false);
		task1.linkUnit(unit0);
		world.addUnit(unit0);
		here.transferStatement((ExpressionStatement) statement);
		assertEquals(here.toString(),"here");
	}

}
