package hillbillies.model.expression;

import be.kuleuven.cs.som.annotate.Raw;

/**
 * A class of single is-conditions, with no operator
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 *
 */
public abstract class IsCondition extends ComposedBooleanE {
	
	/**
	 * Initialize this new is-condition with given operand.
	 *
	 * @param  operand
	 *         The operand for this new is-condition.
	 * @post   The operand for this new is-condition is the
	 *         same as the given operand.
	 * @throws IllegalArgumentExeption
	 *         This new is-condition cannot have the given
	 *         operand as its operand.
	 */
	protected IsCondition (E operand) throws IllegalArgumentException{
		if (!canHaveAsOperand(operand))
			throw new IllegalArgumentException("Illegal Operand");
		setOperandAt(1, operand);
	}
	
	/**
	 * Return the number of operands involved in this is-condition.
	 *
	 * @return Always returns 1
	 */
	@Override
	public int getNbOperands() {
		return 1;
	}
	
	/**
	 * Check whether this is-condition can have the given
	 * number as its number of operands.
	 *
	 * @return True if and only if the given number is 1.
	 */
	@Override
	@Raw
	public boolean canHaveAsNbOperands(int number){
		//must be an override
		return number == 1;
	}
	
	
	/**
	 * Return the operand of this is-condition at the given index.
	 * 
	 * @return The one and only operand of this is-condition.
	 */
	@Override
	public E getOperandAt(int index) throws IllegalArgumentException {
		if (index!=1)
			throw new IllegalArgumentException("Illegal index");
		return this.operand;
	}
	
	/**
	 * Set the operand for this is-condition at the given
	 * index to the given operand.
	 */
	@Override
	protected void setOperandAt(int index, E operand) {
		this.operand = operand;

	}
	
	/**
	 * Variable referencing the operand of this is-condition
	 */
	private E operand;
	
	/**
	 * Return a textual representation of this is-condition
	 */
	@Override
	public abstract String toString();

}
