package hillbillies.model.expression;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import hillbillies.model.Unit;

/**
 * A class that defines a unit
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 */
public abstract class UnitE extends E {
	
	
	/**
	 * Returns the value of this UnitE
	 */
	@Override
	@Basic @Immutable
	public abstract Unit getValue();

	/**
	 * Return whether the state of this is mutable
	 * 
	 * @return	always returns false
	 */
	@Override
	public boolean isMutable() {
		return false;
	}

	/**
	 * Returns whether the given object references the same unit as this
	 * 
	 * @return	
	 */
	@Override
	public boolean equals(Object other) {
		if (other instanceof Unit)
			return (this.getValue()==other);
		else
			if (other == this)
				return true;
			else if ((other instanceof UnitE) && (((UnitE) other).getValue()==this.getValue()))
				return true;
			return false;
	}

}
