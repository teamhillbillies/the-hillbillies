package hillbillies.model.expression;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import hillbillies.model.statement.ExpressionStatement;
import hillbillies.model.statement.MoveTo;
import hillbillies.model.statement.S;

public class ETest {
	
	private static E someE, expressionFalse, expressionTrue, expressionPosition, expressionImmutable;

	@Before
	public void setUp() throws Exception {
		int[] position = {0,0,0};
		expressionPosition = new Position(position);
		someE = new IsSolid((PositionE)expressionPosition);
		expressionTrue = new True();
		expressionFalse = new False();
		
		
	}

	@Test
	public void hasAsSubExpression_SameExpression() throws Exception {
		assertTrue(someE.hasAsSubExpression(someE));
	}
	
	@Test
	public void hasAsSubExpression_NonEffectiveExpression() throws Exception {
		assertFalse(someE.hasAsSubExpression(null));
	}
	
	@Test
	public void equals_NonEffectiveExpression() throws Exception {
		assertFalse(someE.equals(null));
	}
	
	@Test
	public void equals_SameExpression() throws Exception {
		assertTrue(someE.equals(someE));
	}
	
	@Test
	public void equals_differentExpressions() throws Exception{
		assertFalse(expressionFalse.equals(expressionTrue));
	}
	
	@Test
	public void isIdenticalTo_EqualExpressions() throws Exception{
		assertTrue(expressionTrue.isIdenticalTo(new True()));
	}
	
	@Test
	public void isIdenticalTo_DifferentExpression() throws Exception{
		assertFalse(someE.isIdenticalTo(expressionPosition));
	}
	
	@Test
	public void isIdenticalTo_EqualExpression() throws Exception{
		assertTrue(someE.isIdenticalTo(someE));
	}
	
	@Test
	public void clone_MutableExpression() throws Exception{
		//TODO
		//mutable expression doesn't exist in our expression
	}
	
	@Test
	public void clone_ImmutableExpression() throws Exception{
		E clone = expressionImmutable.clone();
		assertNotNull(clone);
		assertEquals(clone, expressionImmutable);
		assertSame(clone, expressionImmutable);
	}
	
	@Test
	public void hashCode_SingleCase() {
		E cloneSomeExpression = someE.clone();
		assertEquals(someE.hashCode(), cloneSomeExpression.hashCode());
	}
	
	@Test
	public void toString_SingleCase() {
		String result = someE.toString();
		assertNotNull(result);
		assertTrue(result.length() > 0);
	}
	
	@Test
	public void transferStatement() throws Exception{
		int[] position = {0,0,0};
		PositionE positionExpression = new Position(position);
		ExpressionStatement statement = new MoveTo(positionExpression);
		someE.transferStatement(statement);
		assertEquals(someE.getStatement(),statement);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void transferStatement_NullCase(){
		someE.transferStatement(null);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void getUnitOccupiedCube_NullPointer(){
		someE.getUnitOccupiedCube();
	}
	
	@Test
	public void getUnitOccupiedCube(){
		//TODO
	}
	
	
	

}
