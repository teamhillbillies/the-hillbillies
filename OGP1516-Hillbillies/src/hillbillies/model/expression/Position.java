package hillbillies.model.expression;

import java.util.Arrays;

/**
 * Class that defines the position of the given position
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 *
 */
public class Position extends BasicPositionE {
	
	/**
	 * Initialize this position
	 * @post	The value of this position is equal to the given position
	 */
	public Position(int[] position){
		this.position = position;
	}
	
	/**
	 *	Variable containing the position
	 *
	 *@invar	The position must be a valid position in the 
	 *			world which it is connected with
	 */
	private final int[] position;
	
	/**
	 * Returns the position
	 */
	@Override
	public int[] getValue(){
		return this.position;
	}
	
	/**
	 * Returns a textual representation of this position
	 * 
	 * @return	The textual representation of the value of this position
	 * 			as defined by the predefined class Array casted to string.
	 */
	@Override
	public String toString() {
		return Arrays.toString(getValue());
	}
	
	/**
	 * Returns a clone of this expression
	 */
	@Override
	public E clone() {
		return new Position(position.clone());
	}

}
