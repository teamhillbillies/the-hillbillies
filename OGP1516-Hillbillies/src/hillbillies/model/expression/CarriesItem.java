package hillbillies.model.expression;

import hillbillies.model.Unit;

/**
 * Class consisting is-condition that check if the given unit carries an item
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 *
 */
public class CarriesItem extends IsCondition {
	
	/**
	 * Initializes this is-condition with the given unit.
	 * 
	 * @param 	unit
	 * 			The unit to be checked
	 * @effect	This new CarriesItem is initialized as an is-condition
	 *         	with the given unit
	 */
	public CarriesItem(UnitE unit) throws IllegalArgumentException{
		super(unit);
	}
	
	public CarriesItem(E unit) throws IllegalArgumentException{
		super(unit);
	}
	
	/**
	 * Returns a textual representation of the CarriesItem
	 * 
	 * @return "carriesItem followed by the name of the unit to be checked between parenthesis
	 */
	@Override
	public String toString() {
		getOperandAt(1).transferStatement(getStatement());
		return "carriesItem( " + ((Unit)getOperandAt(1).getValue()).getName() + " )";
	}

	/**
	 * Returns the value of this condition
	 * 
	 * @return	true if the given unit carries an item
	 */
	@Override
	public Boolean getValue() {
		// TODO Auto-generated method stub
		getOperandAt(1).transferStatement(getStatement());
		//System.out.println(getOperandAt(1).getStatement());
		return ((Unit)getOperandAt(1).getValue()).getItem()!= null;
	}
	
	/**
	 * Returns a clone of this expression
	 */
	@Override
	public E clone() {
		return new CarriesItem((UnitE) getOperandAt(1).clone());
	}

}
