package hillbillies.model.expression;

/**
 * A class defining the expression that reads a variable
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 */
public class ReadVariable extends E {
	
	/**
	 * Initializes this ReadVariable with the given variable name
	 * @param 	stringVariable
	 * 			The name of the variable that want to be read
	 * @post	The string variable is set to the given stringVariable
	 */
	public ReadVariable(String stringVariable){
		this.stringVariable = stringVariable;
	}
	
	/**
	 * Variable referencing the variable to be read in the form of a string
	 */
	private final String stringVariable;
	
	/**
	 * Returns the string variable of this ReadVariable
	 */
	public String getStringVariable(){
		return stringVariable;
	}
	
	/**
	 * Returns the value of the variable that wants to be read
	 */
	@Override
	public Object getValue() throws IllegalArgumentException{
		return this.getStatement().getTask().getVariable(getStringVariable()).getValue();
	}
	
	/**
	 * Returns whether this ReadVariable is mutable
	 * 
	 * @return	always returns false
	 */
	@Override
	public boolean isMutable() {
		return false;
	}
	
	/**
	 * Checks whether the given object is equals to this expression
	 * 
	 * @param	other
	 * 			The object to be checked
	 * @return	true if the given object is equal to this expression
	 */
	@Override
	public boolean equals(Object other) {
		return other == this;
	}
	
	/**
	 * Returns a textual representation of this ReadVariable
	 * 
	 * @return	the textual representation of the object this expression reads
	 */
	@Override
	public String toString() {
		return getValue().toString();
	}
	
	/**
	 * Returns a clone of this expression
	 */
	@Override
	public E clone() {
		return new ReadVariable(stringVariable);
	}

	@Override
	public boolean hasAsSubExpression(E expression) {
		return false;
	}

}
