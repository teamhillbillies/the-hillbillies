package hillbillies.model.expression;

/**
 * A class of binary conditions, representing the or operator of
 * the operand at the left-hand side with the operand at the right
 * hand side.
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 *
 */
public class OrCondition extends BinaryCondition {
	
	/**
	 * 
	 * @param 	left
	 * 			The left operand of this or-condition
	 * @param 	right
	 * 			The right operand of this or-condition
	 * @effect 	This or-condition is initialized as a binary condition
	 */
	public OrCondition(BooleanE left, BooleanE right) throws IllegalArgumentException{
		super(left,right);
	}
	
	/**
	 * 
	 * @param 	left
	 * 			The left operand of this or-condition
	 * @param 	right
	 * 			The right operand of this or-condition
	 * @effect 	This or-condition is initialized as a binary condition
	 */
	public OrCondition(E left, E right) throws IllegalArgumentException{
		super(left,right);
	}
	
	/**
	 * Returning the symbol representing the operator of this or-condition
	 * 
	 * @return	The string "||"
	 */
	@Override
	public String getOperatorSymbol() {
		return "||";
	}
	
	/**
	 * Return the value of this or-condition
	 * 
	 * @return true if the left operand or the right operand are true
	 */
	@Override
	public Boolean getValue() {
		getOperandAt(1).transferStatement(getStatement());
		getOperandAt(2).transferStatement(getStatement());
		return ((Boolean)getLeftOperand().getValue() || (Boolean)getRightOperand().getValue());
	}
	
	/**
	 * Returns a clone of this expression
	 */
	@Override
	public E clone() {
		OrCondition orConditionClone = new OrCondition((BooleanE) getOperandAt(1).clone(),(BooleanE)getOperandAt(2).clone());
		return orConditionClone;
	}

}
