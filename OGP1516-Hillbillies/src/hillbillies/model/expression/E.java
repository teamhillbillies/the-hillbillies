package hillbillies.model.expression;

import hillbillies.model.Unit;
import hillbillies.model.statement.ExpressionStatement;
import hillbillies.model.statement.S;


/**
 * A class of expressions with Units, Positions, Values as it's basic inputs.
 * With these the basic expressions can be formed and the basic expressions are the
 * smallest elements of the composed expressions.
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 *
 * @invar This expression must have a proper statement
 */
public abstract class E implements Cloneable {
	
	/**
	 * 
	 * @param 	expression
	 * 			The expression to be checked
	 * @return	true if the given expression is the same expression
	 * 			as this expression
	 * 			|if (expression == this)
	 * 			|	result == true
	 * @return	false if the given expression isn't effective
	 * 			|if (expression == null)
	 * 				result == false
	 */
	public abstract boolean hasAsSubExpression(E expression);
	
	
	/**
	 * 
	 * @return	the value of this expression
	 */
	public abstract Object getValue();
	
	/**
	 * check whether the state of this expression can be changed
	 * 
	 */
	public abstract boolean isMutable();
	
	/**
	 * @return 	true if the given object is the same as this expression
	 * @return 	if this expression is mutable, true if and only if this
	 *         	expression and the given object are the same.
	 * @return	false if the given object is not effective
	 * @return	false if the given object and this expression do not belong
	 * 			to the same concrete class
	 * @return 	false if the value of the given object does not match
	 * 			the value of this expression	
	 */
	@Override
	public abstract boolean equals(Object other);
	
	/**
	 * 
	 * @param 	other
	 * 			the expression to compare with
	 * @return	true if the given expression is equal to this expression
	 * @return 	false if the given expression is not effective
	 * @return	false if the given expression is from a different concrete
	 * 			class than this expression
	 */
	public boolean isIdenticalTo(E other){
		if (other == null){
			return false;
		}
		
		if (this.getClass() != other.getClass()){
			return false;
		}
		
		if (this.equals(other)){
			return true;
		}
		return false;
			
	}
	
	/**
	 * Return a clone of this expression
	 * @return 
	 * 
	 * @return	the returning expression is identical to this expression
	 * @return 	the returning expression is the same as this expression
	 * 			if the expression is immutable
	 */
	@Override
	public abstract E clone();
	
	/**
	 * Return the hashCode of this expression
	 */
	@Override
	public int hashCode(){
		return super.hashCode();
	}
	
	/**
	 * Return the textual representation of this expression
	 * 
	 * @return 	the resulting string is non-empty
	 */
	@Override
	public abstract String toString();
	
	/**
	 * Returns the statement of this expression
	 */
	public ExpressionStatement getStatement(){
		return this.statement;
	}
	
	/**
	 * Checks whether this expression can have the given statement as it's statement
	 * 
	 * @param 	statement
	 * 			The statement to be checked
	 * @return	true if the given statement is effective
	 */
	public boolean canHaveAsStatement(ExpressionStatement statement){
		if (statement == null)
			return false;
		return true;
	}
	
	/**
	 * Sets the statement of this expression to the given statement
	 * 
	 * @param 	statement
	 * 			The new statement of this expression
	 * @pre		The given statement must be a valid statement for this expression
	 * @post	The statement of this statement is set to the given statement
	 */
	private void setStatement(ExpressionStatement statement){
		assert canHaveAsStatement(statement);
		this.statement = statement;
	}
	
	/**
	 * Checks whether this expression has a valid statement
	 * 
	 * @return	false if this expression can't have it's current 
	 * 			statement as it's statement
	 * @return 	false if the statement of this expression doesn't have 
	 * 			this expression as it's expression
	 * @return	otherwise return true
	 */
	public boolean hasProperStatement(){
		if (!canHaveAsStatement(this.getStatement()))
			return false;
		if (this.getStatement().getExpression()!=this)
			return false;
		return true;
	}
	
	/**
	 * Appoints the given statement to this expression
	 * 
	 * @param 	statement
	 * 			the new statement of this expression
	 * @post 	the new statement of this expression is the
	 * 			given statement
	 * @throws 	IllegalArgumentException
	 * 			if this expression can't have the given statement as it's statement
	 * 			if the given statement points to another expression
	 */
	public void transferStatement(ExpressionStatement statement) throws IllegalArgumentException{
		if (!canHaveAsStatement(statement))
			throw new IllegalArgumentException("cannot have this statement as a statement");
		//if (statement.getExpression()!=this)
		//	throw new IllegalArgumentException("The statement doesnt have this expression as expression");
		setStatement(statement);
		
	}

	/**
	 * Variable referencing the expression statement this expression points to
	 */
	private ExpressionStatement statement;
	
	/**
	 * Gets the position of the cube occupied by the unit linked to the statement
	 */
	public int[] getUnitOccupiedCube() throws IllegalArgumentException{
		try{
		return this.getStatement().getTask().getUnit().getOccupiedCube();
		}
		catch(NullPointerException e){
			throw new IllegalArgumentException("nulpointerException");
		}
		
		
		
	}
	

	
//	public abstract void terminate();
//	
//
//	public boolean isTerminated(){
//		return isTerminated;
//	}
//	
//
//	protected boolean isTerminated = false;

}
