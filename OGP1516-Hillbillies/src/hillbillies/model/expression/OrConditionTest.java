package hillbillies.model.expression;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import hillbillies.model.Task;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.model.statement.ExpressionStatement;
import hillbillies.model.statement.MoveTo;
import hillbillies.model.statement.S;
import hillbillies.part2.facade.Facade;
import hillbillies.part2.listener.DefaultTerrainChangeListener;

public class OrConditionTest {

		
	private static final int TYPE_AIR = 0;
	private static final int TYPE_ROCK = 1;
	private static final int TYPE_TREE = 2;
	private static final int TYPE_WORKSHOP = 3;
	private Facade facade;
	private World world;
	private static PositionE solidPosition,passablePosition;
	private static IsSolid trueIsPassable, falseIsPassable;
	private static S statement;
	private Unit unit0;
	private Task task1;
	private static OrCondition trueOrCondition, falseOrCondition, composedFalseOrCondition, composedTrueOrCondition;

	@Before
	public void setUp() throws Exception {
		
		int[][][] types = new int[3][3][3];
		types[1][1][0] = TYPE_AIR;
		types[1][1][1] = TYPE_TREE;
		types[1][1][2] = TYPE_WORKSHOP;
		
		this.facade = new Facade();
		
		double[] unitPosition = {1,1,1};
		unit0 = new Unit("Unit Low0",unitPosition,25,25,25,25,false);

		world = facade.createWorld(types, new DefaultTerrainChangeListener());
		int[] position1 = {1,1,1};
		solidPosition = new Position(position1);
		falseIsPassable = new IsSolid(solidPosition);
		
		world.addUnit(unit0);
		
		statement = new MoveTo(solidPosition);
		
		task1 = new Task("TaskTest1",10,statement, position1);
		task1.linkUnit(unit0);
		
		int[] position2 = {1,1,0};
		passablePosition = new Position(position2);
		trueIsPassable = new IsSolid(passablePosition);

		trueOrCondition = new OrCondition(trueIsPassable,falseIsPassable);
		falseOrCondition = new OrCondition(trueIsPassable, trueIsPassable);
		composedFalseOrCondition = new OrCondition(falseOrCondition, falseOrCondition);
		composedTrueOrCondition = new OrCondition(trueOrCondition, falseOrCondition);
		
		composedTrueOrCondition.transferStatement((ExpressionStatement) statement);
		composedFalseOrCondition.transferStatement((ExpressionStatement) statement);
		
	}


	@Test
	public void extendedConstructor() throws Exception {
		OrCondition myOrCondition = new OrCondition(trueOrCondition,falseOrCondition);
		assertEquals(myOrCondition.getLeftOperand(),trueOrCondition);
		assertEquals(myOrCondition.getRightOperand(),falseOrCondition);
		assertEquals(myOrCondition.getOperatorSymbol(),"||");
	}
	
	@Test
	public void getValue_NormalOrCondition() throws Exception{
		trueOrCondition.transferStatement((ExpressionStatement) statement);
		falseOrCondition.transferStatement((ExpressionStatement) statement);
		
		assertTrue(trueOrCondition.getValue());
		assertFalse(falseOrCondition.getValue());
	}
	
	@Test
	public void getValue_ComposedOrCondition() throws Exception{
		assertTrue(composedTrueOrCondition.getValue());
		assertFalse(composedFalseOrCondition.getValue());
	}
	
	@Test
	public void toStringNormalOrCondition() throws Exception{
		falseOrCondition.transferStatement((ExpressionStatement) statement);
		
		int[] position1 = {1,1,1};
		int[] position2 = {1,1,0};
		
		assertEquals(falseOrCondition.toString(), "isSolid( " + Arrays.toString(position2) + " )||"
				+ "isSolid( " + Arrays.toString(position2) + " )");
	}
	
	@Test
	public void toStringComposedOrCondition() throws Exception{
		int[] position1 = {1,1,1};
		int[] position2 = {1,1,0};
		
		assertEquals(composedFalseOrCondition.toString(), "(isSolid( " + Arrays.toString(position2) + " )||"
				+ "isSolid( " + Arrays.toString(position2) + " ))||(isSolid( " + Arrays.toString(position2) + " )||"
				+ "isSolid( " + Arrays.toString(position2) + " ))");
	}
	
	@Test
	public void cloneOrCondition() throws Exception{
		OrCondition cloneOrCondition = new OrCondition(trueOrCondition,falseOrCondition);
		cloneOrCondition = (OrCondition) falseOrCondition.clone();
		cloneOrCondition.transferStatement((ExpressionStatement) statement);
		assertEquals(cloneOrCondition.getLeftOperand(),falseOrCondition);
		assertEquals(cloneOrCondition.getRightOperand(),trueOrCondition);
	}
		

}
