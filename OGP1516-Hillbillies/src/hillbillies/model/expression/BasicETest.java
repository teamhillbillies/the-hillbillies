package hillbillies.model.expression;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class BasicETest {
	
	private static BasicBooleanE falseExpression, trueExpression;

	@Before
	public void setUp() throws Exception {
		falseExpression = new False();
		trueExpression = new True();
	}

	@Test
	public void hasAsSubExpression_SameExpression() throws Exception{
		assertTrue(falseExpression.hasAsSubExpression(falseExpression));
	}
	
	@Test
	public void hasAsSubExpression_OtherExpression() throws Exception{
		assertFalse(falseExpression.hasAsSubExpression(trueExpression));
	}

}
