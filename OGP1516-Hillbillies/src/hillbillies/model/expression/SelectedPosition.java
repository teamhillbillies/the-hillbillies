package hillbillies.model.expression;

/**
 * A class that defines the selected position
 * 
 * @note 	the given position must be a valid position 
 * 			of the world in which it is connected with
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0.
 *
 */
public class SelectedPosition extends BasicPositionE {
	
	/**
	 * Returns the position of the selected cube
	 */
	@Override
	public int[] getValue() {
		return getStatement().getTask().getLocation();
	}
	
	/**
	 * Returns a textual representation of the selected cube
	 * 
	 * @return	String "selected"
	 */
	@Override
	public String toString() {
		return "selected";
	}
	
	/**
	 * Returns a clone of this expression
	 */
	@Override
	public E clone() {
		return new SelectedPosition();
	}
}
