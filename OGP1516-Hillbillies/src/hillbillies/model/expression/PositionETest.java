package hillbillies.model.expression;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PositionETest {
	
	private static PositionE positionExpression1, positionExpression2;

	@Before
	public void setUp() throws Exception {
		int[] position_1 = {1,1,1};
		int[] position_2 = {2,2,2};
		positionExpression1 = new Position(position_1);
		positionExpression2 = new Position(position_2);
	}


	@Test
	public void equals_SameExpression() throws Exception{
		assertTrue(positionExpression1.equals(positionExpression1));
	}
	
	@Test
	public void equals_DifferentExpression() throws Exception{
		assertFalse(positionExpression1.equals(positionExpression2));
	}
	
	@Test
	public void equals_DifferentType() throws Exception{
		assertFalse(positionExpression1.equals(new False()));
	}
	
	@Test
	public void equals_NonEffective() throws Exception{
		assertFalse(positionExpression1.equals(null));
	}
	
	@Test
	public void isMutable() throws Exception{
		assertFalse(positionExpression1.isMutable());
	}
	
	@Test
	public void isValue() throws Exception{
		int[] position = {1,1,1};
		Arrays.equals(positionExpression1.getValue(),position);
	}

}
