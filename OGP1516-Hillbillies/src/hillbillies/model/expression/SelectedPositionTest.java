package hillbillies.model.expression;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import hillbillies.model.Task;
import hillbillies.model.TerrainType;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.model.statement.ExpressionStatement;
import hillbillies.model.statement.MoveTo;
import hillbillies.model.statement.S;
import hillbillies.part2.facade.Facade;
import hillbillies.part2.listener.DefaultTerrainChangeListener;

public class SelectedPositionTest {
	private static SelectedPosition someSelectedPosition;
	private static S statement;
	private static Task task1;
	private static Unit unit0;
	private World world;
	private int nbX;
	private int nbY;
	private int nbZ;
	
	private static final int TYPE_AIR = 0;
	private static final int TYPE_ROCK = 1;
	private static final int TYPE_TREE = 2;
	private static final int TYPE_WORKSHOP = 3;
	private Facade facade;

	@Before
	public void setUp() throws Exception {
		nbX = 10;
		nbY = 20;
		nbZ = 30;
		
		world = new World(new int[nbX][nbY][nbZ], new DefaultTerrainChangeListener());
		
		int[] position = {0,0,0};
		PositionE positionExpression = new Position(position);
		S statement = new MoveTo(positionExpression);
		Task task1 = new Task("task1", 100, statement, position );
		double[] unitPosition = {0,0,0};
		Unit unit0 = new Unit("UnitHigh",unitPosition, 100,100,100,100,false);
		task1.linkUnit(unit0);
		world.addUnit(unit0);
		someSelectedPosition = new SelectedPosition();
		someSelectedPosition.transferStatement((ExpressionStatement) statement);
	}

	@Test
	public void extendedConstructor() throws Exception{
		SelectedPosition mySelectedPosition = new SelectedPosition();
	}
	
	@Test
	public void getValue() throws Exception{
		int[] position = {0,0,0};
		Arrays.equals(someSelectedPosition.getValue(),position);
	}
	
	@Test
	public void toStringSelectedPosition() throws Exception{
		assertEquals(someSelectedPosition.toString(), "selected");
	}
	
	@Test
	public void equals_SameExpression() throws Exception{
		assertTrue(someSelectedPosition.equals(someSelectedPosition));
	}
	
	@Test
	public void equals_DifferentType() throws Exception{
		assertFalse(someSelectedPosition.equals(new False()));
	}
	
	@Test
	public void equals_NonEffective() throws Exception{
		assertFalse(someSelectedPosition.equals(null));
	}
	
	@Test
	public void cloneSelectedPosition() throws Exception{
		someSelectedPosition.clone();
	}

}
