package hillbillies.model.expression;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

public class PositionTest {
	
	private static Position somePosition, samePosition, differentPosition;

	@Before
	public void setUp() throws Exception {
		int[] position =  {0,0,0};
		somePosition = new Position(position);
		samePosition = new Position(position);
		int[] otherPosition = {1,1,1};
		differentPosition = new Position(otherPosition);
	}


	@Test
	public void extendedConstructor() throws Exception{
		int[] myPosition = {2,2,2};
		Position myPositionExpression = new Position(myPosition);
	}
	
	@Test
	public void getValue() throws Exception{
		int[] position = {0,0,0};
		Arrays.equals(somePosition.getValue(),position);
	}
	
	@Test
	public void equals_samePosition() throws Exception{
		assertTrue(somePosition.equals(samePosition));
	}
	
	@Test
	public void equals_differentPosition() throws Exception{
		assertFalse(somePosition.equals(differentPosition));
	}
	
	@Test
	public void equals_differentType() throws Exception{
		assertFalse(somePosition.equals(new False()));
	}
	
	@Test
	public void equals_NonEffective() throws Exception{
		assertFalse(somePosition.equals(null));
	}
	
	@Test
	public void equals_Clone() throws Exception{
		assertTrue(somePosition.equals(somePosition.clone()));
	}
	
	@Test
	public void toStringPosition() throws Exception{
		int[] position = {0,0,0};
		assertEquals(somePosition.toString(),Arrays.toString(position));
	}

}
