package hillbillies.model.expression;

import be.kuleuven.cs.som.annotate.Raw;

/**
 * 	A class of composed arithmetic conditions.
 *  A composed condition involves a single operator applied
 *  to a series of conditions.
 * 
 * @invar    Each composed condition can have its number of
 *           operands as number of operands.
 * @invar    Each composed condition can have each of its
 *           operands as operand.
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 */
public abstract class ComposedBooleanE extends BooleanE {
	
	/**
	 * Return the number of operands involved in this composed
	 * condition.
	 */
	public abstract int getNbOperands();
	
	/**
	 * Check whether this composed condition can have the given
	 * number of operands as its number of operands.
	 *
	 * @param  nbOperands
	 *         The number of operands to check.
	 */
	@Raw
	public abstract boolean canHaveAsNbOperands(int nbOperands);
	
	/**
	 * Return the operand of this composed condition at the given index.
	 *
	 * @param  index
	 *         The index of the requested operand.
	 * @throws IndexOutOfBoundsException
	 *         The given index is not positive or exceeds the
	 *         number of operands for this composed expression.
	 *       | (index < 1) || (index > getNbOperands())
	 */
	public abstract E getOperandAt(int index) 
			throws IllegalArgumentException;
	
	/**
	 * Check whether this composed condition can have the given
	 * expressions as one of its operands.
	 *
	 * @param  expression
	 *         The expression to check.
	 * @return True if and only if the given expression is effective,
	 *         and if that expression does not have this composed
	 *         condition as a subexpression.
	 */
	public boolean canHaveAsOperand(E expression){
		return ((expression != null) && (!expression.hasAsSubExpression(this)));
	}
	
	/**
	 * Set the operand for this composed condition at the given
	 * index to the given condition.
	 * 
	 * @param 	index
	 * 			The index at which the condition must be registered
	 * @param 	condition
	 * 			The condition to be registered
	 * @pre		The given index must be a valid index so must be 
	 * 			positive and less or equal than the number of maximum
	 * 			operands allowed
	 * @post	The condition at the given index is the same as the 
	 * 			given condition 
	 * 			
	 * 			
	 */
	protected abstract void setOperandAt(int index, E condition);

	
	/**
	 * Check whether this composed condition has the given expression
	 * as one of its subexpressions.
	 *
	 * @return True if and only if the given expression is the same
	 *         expression as this composed condition, or if the given
	 *         expression is a subexpression of one of the operands
	 *         of this composed expression.
	 */
	@Override
	public boolean hasAsSubExpression(E expression) {
		if (expression == this){
			return true;
		}
		
		for (int ind=1; ind<getNbOperands(); ind++){
			if (getOperandAt(ind).hasAsSubExpression(expression))
				return true;
		}
		return false;
	}
	
	/**
	 * Returns the value of the given composed condition
	 */
	//@Override
	//public abstract Boolean getValue();
	
	/**
	 *  Check whether the state of this composed condition
	 *  can be changed.
	 *  
	 *  @return	true if atleast one of it's subexpressions is
	 *  		mutable 
	 */
	@Override
	public boolean isMutable() {
		for (int ind=1; ind<=getNbOperands(); ind++){
			if (getOperandAt(ind).isMutable())
				return true;
		}
		return false;
	}
	
	/**
	 * Check whether this composed condition is equal to 
	 * the given object.
	 *
	 * @return If this composed condition is mutable or the other object
	 *         is a mutable composed condition, true if and only if this
	 *         composed condition is the same as the given object.
	 * @return If this composed condition is immutable and the other object
	 *         is an immutable composed condition, true if and only if both
	 *         composed conditions belong to the same class, if they have the
	 *         same number of operands, and if all corresponding operands of
	 *         both composed expressions are equal.
	 */
	@Override
	public boolean equals(Object other) {
		if ((other == null) || (this.getClass() != other.getClass()))
			return false;
		ComposedBooleanE otherCond = (ComposedBooleanE) other;
		if (this.isMutable() || otherCond.isMutable())
			return this == other;
		if (getNbOperands() != otherCond.getNbOperands())
			return false;
		for (int ind=1; ind<=getNbOperands(); ind++)
			if(!getOperandAt(ind).equals(otherCond.getOperandAt(ind))){
				return false;
		}
		return true;
	}
	
	/**
	 * Check whether this composed expression is identical to 
	 * the given expression.
	 *
	 * @return If the other object is a composed condition, true if and only if both
	 *         composed conditions have the same number of operands, and if
	 *         all corresponding operands of both composed expressions are identical
	 *         to each other.
	 */
	@Override
	public boolean isIdenticalTo(E other){
		if ((other==null) || (this.getClass()!=other.getClass()))
			return false;
		ComposedBooleanE otherCond = (ComposedBooleanE) other;
		if (getNbOperands() != otherCond.getNbOperands())
			return false;
		for (int ind = 1; ind <= getNbOperands(); ind++)
			if (!getOperandAt(ind).isIdenticalTo(otherCond.getOperandAt(ind)))
				return false;
		return true;
	}

}
