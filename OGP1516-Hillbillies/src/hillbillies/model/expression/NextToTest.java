package hillbillies.model.expression;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import hillbillies.model.Task;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.model.statement.ExpressionStatement;
import hillbillies.model.statement.MoveTo;
import hillbillies.model.statement.S;
import hillbillies.part2.facade.Facade;
import hillbillies.part2.listener.DefaultTerrainChangeListener;

public class NextToTest {
	private static final int TYPE_AIR = 0;
	private static final int TYPE_ROCK = 1;
	private static final int TYPE_TREE = 2;
	private static final int TYPE_WORKSHOP = 3;
	private static PositionE somePosition;
	private static IsSolid trueIsPassable, falseIsPassable;
	private static S statement;
	private static NextTo nextTo;
	private Facade facade;
	private World world;
	private Unit unit0;
	private Task task1;


	@Before
	public void setUp() throws Exception {
		
		int[][][] types = new int[3][3][3];
		types[1][1][0] = TYPE_AIR;
		types[1][1][1] = TYPE_TREE;
		types[1][1][2] = TYPE_WORKSHOP;
		
		this.facade = new Facade();
		
		int[] position = {0,0,0};
		
		somePosition = new Position(position);
		statement = new MoveTo(somePosition);
		nextTo = new NextTo(somePosition);
		nextTo.transferStatement((ExpressionStatement) statement);
		
		double[] unitPosition = {1,1,1};
		unit0 = new Unit("Unit Low0",unitPosition,25,25,25,25,false);

		world = facade.createWorld(types, new DefaultTerrainChangeListener());
		
		world.addUnit(unit0);
		
		task1 = new Task("TaskTest1",10,statement, position);
		task1.linkUnit(unit0);
		
	}

	@Test
	public void extendedConstructor() throws Exception {
		NextTo myNextTo = new NextTo(somePosition);
	}
	
	@Test
	public void getValue() throws Exception{
		//Geeft {0,0,0} ipv de oplossing
		
		int[] position1 = {0,0,1};
		int[] position2 = {0,1,0};
		int[] position3 = {1,0,0};
		
		assertTrue((Arrays.equals(nextTo.getValue(), position1))||(Arrays.equals(nextTo.getValue(), position2))
				||(Arrays.equals(nextTo.getValue(), position3)));
	}
	
	@Test
	public void toStringNextTo() throws Exception{
		assertEquals(nextTo.toString(),"next_to " + somePosition.toString());
	}
	
	@Test
	public void cloneNextTo() throws Exception{
		nextTo.clone();
	}

}
