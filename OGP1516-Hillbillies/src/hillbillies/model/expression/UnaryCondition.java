package hillbillies.model.expression;

import be.kuleuven.cs.som.annotate.Model;

/**
 *   A class of unary arithmetic expressions.
 *   A unary expression involves a single operator applied
 *   to a single operand.
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 *
 */
public abstract class UnaryCondition extends ComposedBooleanE {
	
	/**
	 * Initialize this new unary condition with given operand.
	 *
	 * @param  operand2
	 *         The operand for this new unary condition.
	 * @post   The operand for this new unary condition is the
	 *         same as the given operand.
	 * @throws IllegalArgumentExeption
	 *         This new unary condition cannot have the given
	 *         operand as its operand.
	 */
	@Model
	protected UnaryCondition(E operand2) throws IllegalArgumentException {
		if (!canHaveAsOperand(operand2))
			throw new IllegalArgumentException("can't initialize UnaryCondition");
		setOperandAt(1,operand2);
	}
	
	/**
	 * Return the number of operands involved in this is-condition.
	 *
	 * @return Always returns 1
	 */
	@Override
	public int getNbOperands() {
		return 1;
	}
	
	/**
	 * Check whether this unary condition can have the given
	 * number as its number of operands.
	 *
	 * @return True if and only if the given number is 1.
	 */
	public boolean canHaveAsNbOperands(int number){
		return number == 1;
	}
	
	
	/**
	 * Return the operand of this unary condition at the given index.
	 * 
	 * @return The one and only operand of this unary condition.
	 */
	@Override
	public E getOperandAt(int index) throws IllegalArgumentException {
		if (index!=1)
			throw new IllegalArgumentException("not a valid index");
		return getOperand();
	}
	
	/**
	 * Return the only operand of this unary condition.
	 * 
	 * @return the one and only operand of this unary condition
	 */
	public E getOperand(){
		return this.operand;
		
	}
	
	/**
	 * Set the operand for this unary condition at the given
	 * index to the given operand.
	 */
	@Override
	protected void setOperandAt(int index, E operand) {
		this.operand = operand;

	}
	
	/**
	 * Variable referencing the operand of this unary condition.
	 */
	private E operand;

	/**
	 * Returns a textual representation of this class
	 * 
	 * @return	if the given operand is an isCondition return
	 * 			the operatorsymbol followed by the textual representation
	 * 			of the operand
	 * @return	if the given operan is a composedCondition return
	 * 			the operatorsymbol followed by the textual representation
	 * 			of the operand between parenthesis
	 * @throw	otherwise throw an error
	 */
	@Override
	public String toString() {
		
		getOperandAt(1).transferStatement(getStatement());
		if (getOperand() instanceof IsCondition)
			return getOperatorSymbol() + getOperand().toString();
		if (getOperand() instanceof ComposedBooleanE)
			return getOperatorSymbol() + "(" + getOperand().toString() + ")";
		throw new Error("unknown type");
	}
	
	/**
	 * Returns the operator symbol of this unary condition
	 * 
	 * @return	the operator symbol of this unary condition
	 */
	public abstract String getOperatorSymbol();

}
