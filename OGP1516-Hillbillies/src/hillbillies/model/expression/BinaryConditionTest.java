package hillbillies.model.expression;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class BinaryConditionTest {
	private static BinaryCondition someBinaryCondition, sameAndCondition, orCondition, symmetricAndCondition;
	private static ComposedBooleanE isSolid, isPassable;

	@Before
	public void setUp() throws Exception {
		int[] position = {0,0,0};
		PositionE positionE = new Position(position);
		isSolid = new IsSolid(positionE);
		int[] position2 = {0,0,0};
		PositionE positionE2 = new Position(position2);
		isPassable = new IsPassable(positionE2);
		someBinaryCondition = new AndCondition(isSolid,isPassable);
		sameAndCondition = new AndCondition(isSolid, isPassable);
		orCondition = new OrCondition(isSolid,isPassable);
		symmetricAndCondition = new AndCondition(isPassable, isSolid);
	}

	@Test
	public void extendedConstructor() throws Exception {
		BinaryCondition myAndCondition = new AndCondition(isSolid,isPassable);
		assertEquals(myAndCondition.getNbOperands(),2);
		assertEquals(myAndCondition.getOperandAt(1),isSolid);
		assertEquals(myAndCondition.getOperandAt(2),isPassable);
		assertEquals(myAndCondition.getLeftOperand(),isSolid);
		assertEquals(myAndCondition.getRightOperand(),isPassable);
	}
	
	@Test
	public void canHaveAsNbOperands() throws Exception{
		assertTrue(someBinaryCondition.canHaveAsNbOperands(2));
	}
	
	@Test
	public void canHaveAsNbOperands_FalseCase() throws Exception{
		assertTrue(someBinaryCondition.canHaveAsNbOperands(3));
	}
	
	@Test
	public void getOperandAt() throws Exception{
		assertEquals(someBinaryCondition.getOperandAt(1),isSolid);
		assertEquals(someBinaryCondition.getOperandAt(2),isPassable);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void getOperandAt_IndexTooLow() throws Exception{
		someBinaryCondition.getOperandAt(0);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void getOperandAt_IndexTooHigh() throws Exception{
		someBinaryCondition.getOperandAt(3);
	}
	
	@Test
	public void toStringBinaryCondition() throws Exception{
		//TODO
	}

}
