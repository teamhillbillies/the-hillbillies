package hillbillies.model.expression;

import hillbillies.model.Unit;

/**
 * Class that defines the unit that is executing the statement this expression points to
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 *
 */
public class This extends BasicUnitE {
	
	/**
	 * Initializes this This
	 * @effect	the value of this This is the unit executing the statement this expression
	 * 			points to
	 */
	public This(){
	}
	
	/**
	 * Returns the unit executing the statement this expression points to
	 */
	public Unit getValue(){
		return getStatement().getTask().getUnit();
	}
	
	/**
	 * Returns a textual representation of this class
	 * 
	 * @return	always returns "this"
	 */
	public String toString(){
		return "this";
	}
	
	/**
	 * Returns a clone of this expression
	 */
	@Override
	public E clone() {
		return new This();
	}
}
