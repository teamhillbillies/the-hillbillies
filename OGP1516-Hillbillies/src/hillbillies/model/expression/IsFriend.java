package hillbillies.model.expression;

import hillbillies.model.Unit;

/**
 * Class consisting is-condition that check if the given unit is an enemy
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 *
 */
public class IsFriend extends IsCondition {
	
	/**
	 * Initializes this is-condition with the given unit.
	 * 
	 * @param 	unit
	 * 			The unit to be checked
	 * @effect	This new IsFriend is initialized as an is-condition
	 *         	with the given unit
	 */
	public IsFriend(UnitE unit) throws IllegalArgumentException{
		super(unit);
	}
	
	public IsFriend(E unit) throws IllegalArgumentException{
		super(unit);
	}
	
	/**
	 * Returns a textual representation of the IsEnemy
	 * 
	 * @return "isFriend followed by the name of the unit to be checked between parenthesis
	 */
	@Override
	public String toString() {
		getOperandAt(1).transferStatement(getStatement());
		return "isFriend( " + ((Unit)getOperandAt(1).getValue()).getName() + " )";
	}
	
	/**
	 * Returns the value of this condition
	 * 
	 * @return	true if the given unit is a friend
	 */
	@Override
	public Boolean getValue() {
		// TODO Auto-generated method stub
		getOperandAt(1).transferStatement(getStatement());
		return (this.getStatement().getTask().getUnit().getFaction() == ((Unit)getOperandAt(1).getValue()).getFaction());
	}
	
	/**
	 * Returns a clone of this expression
	 */
	@Override
	public E clone() {
		return new IsFriend((UnitE) getOperandAt(1).clone());
	}

}
