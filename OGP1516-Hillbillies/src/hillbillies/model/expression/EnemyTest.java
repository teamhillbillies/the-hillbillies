package hillbillies.model.expression;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import hillbillies.model.Task;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.model.statement.ExpressionStatement;
import hillbillies.model.statement.MoveTo;
import hillbillies.model.statement.S;
import hillbillies.part2.listener.DefaultTerrainChangeListener;

public class EnemyTest {

	private World world;
	private int nbX;
	private int nbY;
	private int nbZ;
	private Unit unit0, unit1;
	private static Enemy enemy;
	

	@Before
	public void setUp() throws Exception {
		nbX = 10;
		nbY = 20;
		nbZ = 30;
		
		world = new World(new int[nbX][nbY][nbZ], new DefaultTerrainChangeListener());
		double[] unit0Position = {0,0,0};
		unit0 = new Unit("Unit Low",unit0Position,25,25,25,25,false);
		double[] unit1Position = {1,1,1};
		unit1 = new Unit("Unit Low",unit0Position,25,25,25,25,false);
		world.addUnit(unit0);
		world.addUnit(unit1);
		int[] position = {0,0,0};
		PositionE positionExpression = new Position(position);
		S statement = new MoveTo(positionExpression);
		Task task1 = new Task("task1", 100, statement, position );
		double[] unitPosition = {0,0,0};
		task1.linkUnit(unit0);
		enemy = new Enemy();
		enemy.transferStatement((ExpressionStatement) statement);
	}

	@Test
	public void extendedConstructor() throws Exception{
		Enemy myEnemy = new Enemy();
	}
	
	@Test
	public void getValue() throws Exception{
		assertEquals(enemy.getValue(),unit1);
	}
	
	@Test
	public void toStringEnemy() throws Exception{
		assertEquals(enemy.toString(),"enemy");
	}
	
	@Test
	public void enemyClone() throws Exception{
		enemy.clone();
	}

}
