package hillbillies.model.expression;

import java.util.Arrays;

import hillbillies.model.TerrainType;

/**
 * Class consisting is-condition that check if the given position is solid
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 *
 */
public class IsSolid extends IsCondition {
	
	/**
	 * Initializes this is-condition with the position.
	 * 
	 * @param 	position
	 * 			The position to be checked
	 * @effect	This new IsPassable is initialized as an is-condition
	 *         	with the given position
	 */
	public IsSolid(PositionE position) throws IllegalArgumentException{
		super(position);
	}
	
	public IsSolid(E position) throws IllegalArgumentException{
		super(position);
	}
	
	/**
	 * Returns a textual representation of the IsPassable
	 * 
	 * @return 	"isSolid followed by the position as an array of int transformed
	 * 			 to a string between parenthesis
	 */
	@Override
	public String toString() {
		getOperandAt(1).transferStatement(getStatement());
		return "isSolid( " + Arrays.toString((int [])getOperandAt(1).getValue()) + " )";
	}
	
	/**
	 * Returns the value of this condition
	 * 
	 * @return	true if the given position is solid
	 */
	@Override
	public Boolean getValue() {
		getOperandAt(1).transferStatement(getStatement());
		return (!getStatement().getTask().getUnit().getWorld().getTerrainType((int [])getOperandAt(1).getValue()).isPassable());
	}
	
	/**
	 * Returns a clone of this expression
	 */
	@Override
	public E clone() {
		return new IsSolid((PositionE) getOperandAt(1).clone());
	}

}
