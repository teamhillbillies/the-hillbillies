package hillbillies.model.expression;

/**
 * A class of binary conditions, representing the and operator of
 * the operand at the left-hand side with the operand at the right
 * hand side.
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 *
 */
public class AndCondition extends BinaryCondition {
	
	/**
	 * 
	 * @param 	left
	 * 			The left operand of this and-condition
	 * @param 	right
	 * 			The right operand of this and-condition
	 * @effect 	This and-condition is initialized as a binary condition
	 */
	public AndCondition(BooleanE left, BooleanE right) throws IllegalArgumentException{
		super(left, right);
	}
	
	/**
	 * 
	 * @param 	left
	 * 			The left operand of this and-condition
	 * @param 	right
	 * 			The right operand of this and-condition
	 * @effect 	This and-condition is initialized as a binary condition
	 */
	public AndCondition(E left, E right) throws IllegalArgumentException{
		super(left, right);
	}
	
	/**
	 * Return the value of this and-condition
	 * 
	 * @return true if the left operand and the right operand are true
	 */
	@Override
	public Boolean getValue(){
		getOperandAt(1).transferStatement(getStatement());
		getOperandAt(2).transferStatement(getStatement());
		return ((Boolean)getLeftOperand().getValue() && (Boolean)getRightOperand().getValue());
	}
	
	/**
	 * Returning the symbol representing the operator of this and-condition
	 * 
	 * @return	The string "&&"
	 */
	@Override
	public String getOperatorSymbol() {
		return "&&";
	}
	
	/**
	 * Returns a clone of this expression
	 */
	@Override
	public E clone() {
		AndCondition andConditionClone = new AndCondition((BooleanE)getOperandAt(1).clone(),(BooleanE)getOperandAt(2).clone());
		return andConditionClone;
	}

}
