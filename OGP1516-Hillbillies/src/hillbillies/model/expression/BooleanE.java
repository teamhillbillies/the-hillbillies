package hillbillies.model.expression;

import java.util.Arrays;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
/**
 * A class that defines a boolean
 * 
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0.
 *
 */
public abstract class BooleanE extends E {
	
	

	/**
	 * Returns the value of this BooleanE
	 */
	@Override
	@Basic @Immutable
	public abstract Boolean getValue();
	
	/**
	 * Returns whether this BooleanE is mutable
	 * 
	 * @return	returns always false
	 */
	@Override
	public boolean isMutable() {
		return false;
	}

	/**
	 * Returns whether the given object is equal to this BooleanE
	 * 
	 * @return	true only if the given object is instance of BooleanE and
	 * 			the value of this is equal to the value of the given object
	 * 
	 */
	@Override
	public boolean equals(Object other) {
		return (other instanceof BooleanE) && (this.getValue()==((BooleanE)other).getValue());
	}
	

}