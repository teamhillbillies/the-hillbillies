package hillbillies.model.expression;

import java.util.Iterator;
import java.util.Set;

import hillbillies.model.Log;
import hillbillies.model.Item;

/**
 * Class that defines the position of the closest log with the starting position the occupied cube
 * of the unit that's executing the statement where this expression points to
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 *
 */
public class LogExpression extends BasicPositionE {
	
	/**
	 * Initialize this Log
	 * @post	The value of this Log is equal to 
	 * 			the position of the nearest log from the unit
	 * 			occupying the task
	 */
	public LogExpression(){
	}
	
	/**
	 * Finds the log closest to the given starting position
	 * 
	 * @param 	startingPosition
	 * 			The position where the search starts
	 * @return	the log closest to the given starting position
	 * @throws 	IllegalArgumentException
	 * 			if no log is found
	 * 			
	 */
	public Log findNearestLog(int[] startingPosition) throws IllegalArgumentException{
		Set<Item> allItems = getStatement().getTask().getUnit().getWorld().getAllItems();
		double smallestDistance = Double.MAX_VALUE;
		Log nearestLog = null;
		Iterator<Item> iterator = allItems.iterator();
		while(iterator.hasNext()) {
			Item item = iterator.next();
			if (item instanceof Log) {
				double[] itemLocation = item.getPosition();
				double xDiff = itemLocation[0]-startingPosition[0]-0.5;
				double yDiff = itemLocation[1]-startingPosition[1]-0.5;
				double zDiff = itemLocation[2]-startingPosition[2]-0.5;
				double distance = Math.pow(xDiff, 2) + Math.pow(yDiff, 2) + Math.pow(zDiff, 2);
				if (distance < smallestDistance) {
					try {
						getStatement().getTask().getUnit().getWorld().findPath(getUnitOccupiedCube(), getStatement().getTask().getUnit().getWorld().getOccupiedCube(itemLocation));
						smallestDistance = distance;
						nearestLog = (Log) item;
					}
					catch(IllegalArgumentException e) {
					}
				}
			}
		}
		if (nearestLog == null) {
			throw new IllegalArgumentException();
		}
		return nearestLog;
	}
	

	
	/**
	 * Returns the position of the nearest log
	 * @returns	the position of the nearest log
	 * @throws 	IllegalArgumentExeption
	 * 			if no log is found
	 */
	@Override
	public int[] getValue() throws IllegalArgumentException{
		double[] truePosition = findNearestLog(getUnitOccupiedCube()).getPosition();
		return new int[]{(int) Math.floor(truePosition[0]),(int) Math.floor(truePosition[1]),(int) Math.floor(truePosition[2])};
	}
	
	/**
	 * Returns a textual representation of the nearest log
	 * 
	 * @return	string "log"
	 */
	@Override
	public String toString() {
		return "log";
	}
	
	/**
	 * Returns a clone of this expression
	 */
	@Override
	public E clone() {
		return new LogExpression();
	}

}
