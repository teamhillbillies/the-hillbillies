package hillbillies.model.expression;

import hillbillies.model.Unit;

/**
 * A class that defines the position of the given unit
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 *
 */
public class PositionOf extends ComposedPositionE {
	
	/**
	 * Initializes this class with the given unit expression
	 * 
	 * @param 	unit
	 * 			an expression that references a unit
	 * @post	The operand of this class is set to the given unit expression
	 * 
	 */
	public PositionOf(UnitE unit){
		super(unit);
	}
	
	public PositionOf(E unit){
		super(unit);
	}
	
	/**
	 * Finds the position of the unit referenced by the operand which is a UnitE
	 * @param 	unit
	 * 			the unit expression
	 * @return	the position of the unit
	 */
	public int[] findPosition(UnitE unit){
		return unit.getUnitOccupiedCube();
	}
	
	/**
	 * Returns the position of the unit referenced by the operand which is a UnitE
	 */
	@Override
	public int[] getValue() throws IllegalArgumentException{
		getOperandAt(1).transferStatement(getStatement());
		if (getOperandAt(1) instanceof ReadVariable) {
			return ((Unit)getOperandAt(1).getValue()).getOccupiedCube();
		}
		else
			return findPosition((UnitE) getOperandAt(1));
	}
	
	/**
	 * Returns a textual representation of this class
	 * 
	 * @return	"position_of" + the textual representation of the operand
	 * 			of this class
	 */
	public String toString() {
		getOperandAt(1).transferStatement(getStatement());
		return "position_of " + getOperandAt(1).toString();
	}
	
	/**
	 * Returns a clone of this expression
	 */
	@Override
	public E clone() {
		return new PositionOf((UnitE) getOperandAt(1).clone());
	}


}
