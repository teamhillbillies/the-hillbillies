package hillbillies.model.expression;

/**
 * A class that defines the position of the oocupied cube of the unit executing the
 * statement where this expression points to
 * 

 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 *
 */
public class Here extends BasicPositionE {
	/**
	 * Initialize this Here
	 * @post	The value of this Here is equal to 
	 * 			the position of the unit occupying the task
	 */
	public Here(){
	}
	
	/**
	 *Variable containing the position of the unit
	 *
	 *@invar	The position must be a valid position in the 
	 *			world which it is connected with
	 */
	//private final int[] position;
	
	/**
	 * Returns the position of the unit
	 */
	@Override
	public int[] getValue(){
		return getStatement().getTask().getUnit().getOccupiedCube();
	}
	
	/**
	 * Returns a textual representation of the position of the unit
	 * 
	 * @return	string "here"
	 */
	@Override
	public String toString() {
		return "here";
	}

	/**
	 * Returns a clone of this expression
	 */
	@Override
	public E clone() {
		return new Here();
	}
}
