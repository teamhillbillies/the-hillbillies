package hillbillies.model;

import java.util.ArrayList;

import java.util.List;

import hillbillies.model.expression.*;
import hillbillies.model.statement.*;
import hillbillies.part3.programs.ITaskFactory;
import hillbillies.part3.programs.SourceLocation;
import ogp.framework.util.ModelException;

public class TaskFactory implements ITaskFactory<E,S,Task> {
	
	
	@Override
	public List<Task> createTasks(String name, int priority, S activity, List<int[]> selectedCubes) {
		List<Task> list = new ArrayList<Task>();
		if (selectedCubes.isEmpty()) {
			try {
				list.add(new Task(name,priority,activity,null));
			} catch (ModelException e) {
			}
			return list;
		}
		for(int i=0;i<selectedCubes.size();i++) {
			try {
				list.add(new Task(name,priority,activity.clone(),selectedCubes.get(i)));
			} catch (ModelException e) {
			}
		}
		return list;
	}
	
	@Override
	public S createAssignment(String variableName, E value, SourceLocation sourceLocation) {
		S assignment = new Assign(value, variableName);
		return assignment;
	}
	
	@Override
	public S createWhile(E condition, S body, SourceLocation sourceLocation) {
		S whileStatement = new WhileStatement(condition, body);
		return whileStatement;
	}
	
	@Override
	public S createIf(E condition, S ifBody, S elseBody, SourceLocation sourceLocation) {
		S ifStatement = new IfStatement(condition,ifBody,elseBody);
		return ifStatement;
	}
	
	@Override
	public S createBreak(SourceLocation sourceLocation) {
		S breakS = new Break();
		return breakS;
	}
	
	@Override
	public S createPrint(E value, SourceLocation sourceLocation) {
		S print = new Print(value);
		return print;
	}
	
	@Override
	public S createSequence(List<S> statements, SourceLocation sourceLocation) {
		S sequence = new Sequence(statements);
		return sequence;
	}
	
	@Override
	public S createMoveTo(E position, SourceLocation sourceLocation) {
		S moveTo = new MoveTo(position);
		return moveTo;
	}
	
	@Override
	public S createWork(E position, SourceLocation sourceLocation) {
		S work;
		if (position instanceof ReadVariable) {
			work = new Work(position);
		}
		else
			work = new Work((PositionE)position);
		return work;
	}
	
	@Override
	public S createFollow(E unit, SourceLocation sourceLocation) {
		S follow;
		if (unit instanceof ReadVariable) {
			follow = new Follow(unit);
		}
		else
			follow = new Follow((UnitE)unit);
		return follow;
	}
	
	@Override
	public S createAttack(E unit, SourceLocation sourceLocation) {
		S attack;
		if (unit instanceof ReadVariable) {
			attack = new Attack(unit);
		}
		else
			attack = new Attack((UnitE)unit);
		return attack;
	}
	
	@Override
	public E createReadVariable(String variableName, SourceLocation sourceLocation) {
		E readVariable = new ReadVariable(variableName);
		return readVariable;
	}
	
	@Override
	public E createIsSolid(E position, SourceLocation sourceLocation) {
		E isSolid;
		if (position instanceof ReadVariable) {
			isSolid = new IsSolid(position);
		}
		else
			isSolid = new IsSolid((PositionE) position);
		return isSolid;
	}
	
	@Override
	public E createIsPassable(E position, SourceLocation sourceLocation) {
		E isPassable;
		if (position instanceof ReadVariable) {
			isPassable = new IsPassable(position);
		}
		else
			isPassable = new IsPassable((PositionE) position);
		return isPassable;
	}
	
	@Override
	public E createIsFriend (E unit, SourceLocation sourceLocation) {
		E isFriend;
		if (unit instanceof ReadVariable) {
			isFriend = new IsFriend(unit);
		}
		else
			isFriend = new IsFriend((UnitE) unit);
		return isFriend;
	}
	
	@Override
	public E createIsEnemy(E unit, SourceLocation sourceLocation) {
		E isEnemy;
		if (unit instanceof ReadVariable) {
			isEnemy = new IsEnemy(unit);
		}
		else
			isEnemy = new IsEnemy((UnitE) unit);
		return isEnemy;
	}
	
	@Override
	public E createIsAlive(E unit, SourceLocation sourceLocation) {
		E isAlive;
		if (unit instanceof ReadVariable) {
			isAlive = new IsAlive(unit);
		}
		else
			isAlive = new IsAlive((UnitE) unit);
		return isAlive;
	}
	
	@Override
	public E createCarriesItem(E unit, SourceLocation sourceLocation) {
		E carriesItem;
		if (unit instanceof ReadVariable) {
			carriesItem = new CarriesItem(unit);
		}
		else
			carriesItem = new CarriesItem((UnitE) unit);
		return carriesItem;
	}
	
	@Override
	public E createNot(E expression, SourceLocation sourceLocation) {
		E notCondition;
		if (expression instanceof ReadVariable) {
			notCondition = new NotCondition(expression);
		}
		else
			notCondition = new NotCondition((BooleanE) expression);
		return notCondition;
	}
	
	@Override
	public E createAnd(E left, E right, SourceLocation sourceLocation) {
		E andCondition;
		if (left instanceof ReadVariable || right instanceof ReadVariable) {
			andCondition = new AndCondition(left,right);
		}
		else
			andCondition = new AndCondition((BooleanE) left, (BooleanE) right);
		return andCondition;
	}
	
	@Override
	public E createOr(E left, E right, SourceLocation sourceLocation) {
		E orCondition;
		if (left instanceof ReadVariable || right instanceof ReadVariable) {
			orCondition = new OrCondition(left,right);
		}
		else
			orCondition = new OrCondition((BooleanE) left, (BooleanE) right);
		return orCondition;
	}
	
	@Override
	public E createHerePosition(SourceLocation sourceLocation) {
		E here = new Here();
		return here;
	}
	
	@Override
	public E createLogPosition(SourceLocation sourceLocation) {
		E logPosition = new LogExpression();
		return logPosition;
	}
	
	@Override
	public E createBoulderPosition(SourceLocation sourceLocation) {
		E boulderExpression = new BoulderExpression();
		return boulderExpression;
	}
	
	@Override
	public E createWorkshopPosition(SourceLocation sourceLocation) {
		E workshopPosition = new Workshop();
		return workshopPosition;
	}
	
	@Override
	public E createSelectedPosition(SourceLocation sourceLocation) {
		E selectedPosition = new SelectedPosition();
		return selectedPosition;
	}
	
	@Override
	public E createNextToPosition(E position, SourceLocation sourceLocation) {
		E nextTo;
		if (position instanceof ReadVariable) {
			nextTo = new NextTo(position);
		}
		else
			nextTo = new NextTo((PositionE)position);
		return nextTo;
	}
	
	@Override
	public E createPositionOf(E unit, SourceLocation sourceLocation) {
		E positionOf;
		if (unit instanceof ReadVariable) {
			positionOf = new PositionOf(unit);
		}
		else
			positionOf = new PositionOf((UnitE)unit);
		return positionOf;
	}
	
	@Override
	public E createLiteralPosition(int x, int y, int z, SourceLocation sourceLocation) {
		int[] position = {x,y,z};
		E positionExpression = new Position(position);
		return positionExpression;
	}
	
	@Override
	public E createThis(SourceLocation sourceLocation) {
		E thisE = new This();
		return thisE;
	}
	
	@Override
	public E createFriend(SourceLocation sourceLocation) {
		E friend = new Friend();
		return friend;
	}
	
	@Override
	public E createEnemy(SourceLocation sourceLocation) {
		E enemy = new Enemy();
		return enemy;
	}
	
	@Override
	public E createAny(SourceLocation sourceLocation) {
		E any = new Any();
		return any;
	}
	
	@Override
	public E createTrue(SourceLocation sourceLocation) {
		E trueE = new True();
		return trueE;
	}
	
	@Override
	public E createFalse(SourceLocation sourceLocation) {
		E falseE = new False();
		return falseE;
	}
}