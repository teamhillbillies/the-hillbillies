package hillbillies.model;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import hillbillies.part2.listener.DefaultTerrainChangeListener;

public class LogTest {

	private Log someLog, logWithUnit, terminatedLog;
	private int[] position0;
	private int[] position1;
	private int[] position2;
	private World world;
	private int nbX;
	private int nbY;
	private int nbZ;
	private Unit unit;
	private double[] unitPosition;

	@Before
	public void setUp() throws Exception {
		nbX = 10;
		nbY = 20;
		nbZ = 30;
		
		world = new World(new int[nbX][nbY][nbZ], new DefaultTerrainChangeListener());
		
		
		for (int x=0; x<nbX; x++){
			for (int y=0; y<nbY; y++){
				for (int z=0; z<nbZ; z++){
					int[] myPosition = {x,y,z};
					if (world.isValidItemPosition(myPosition)){
						
						if (position1!=null && position2 == null){
							position2 = new int[] {x,y,z};
						}
						
						if (position0!=null && position1 == null){
							position1 = new int[] {x,y,z};
							unitPosition = new double[] {x,y,z};
						}
						
						if (position0==null){
							position0 = new int[] {x,y,z};

						}
					}
				}
			}
		}
		
		someLog = new Log(position0);
		
		logWithUnit = new Log(position1);
		unit = new Unit("Unit Low",unitPosition,25,25,25,25,false);
		world.addUnit(unit);
		logWithUnit.receiveUnit(unit);
		
		terminatedLog = new Log(position2);
		terminatedLog.terminate();
	}
	
	//BLACKBOX + WHITEBOX
	@Test
	public void extendedConstructor_legalCase() throws Exception{
		Log myLog1 = new Log(position0);
		Log myLog2 = new Log(position1);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void extendedConstructor_falseCase() throws Exception{
		int[] invalidPositionInt = {-10,-10,-10};
		double[] invalidPositionDouble = {-10+0.5,-10+0.5,-10+0.5};
		if (!Log.isValidPosition(invalidPositionDouble)){
			Log myLog1 = new Log(invalidPositionInt);
		}
		else{
			throw new IllegalArgumentException();
		}
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void extendedConstructor_maxValues() throws Exception{
		int[] invalidPositionInt = {Integer.MAX_VALUE,Integer.MAX_VALUE,Integer.MAX_VALUE};
		double[] invalidPositionDouble = {Integer.MAX_VALUE+0.5,Integer.MAX_VALUE+0.5,Integer.MAX_VALUE+0.5};
		if (!Log.isValidPosition(invalidPositionDouble)){
			Log myLog1 = new Log(invalidPositionInt);
		}
		else{
			throw new IllegalArgumentException();
		}
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void extendedConstructor_minValues() throws Exception{
		int[] invalidPositionInt = {Integer.MIN_VALUE,Integer.MIN_VALUE,Integer.MIN_VALUE};
		double[] invalidPositionDouble = {Integer.MIN_VALUE+0.5,Integer.MIN_VALUE+0.5,Integer.MIN_VALUE+0.5};
		if (!Log.isValidPosition(invalidPositionDouble)){
			Log myLog1 = new Log(invalidPositionInt);
		}
		else{
			throw new IllegalArgumentException();
		}
	}
	
	@Test
	public void getWeight_legalCase() throws Exception{
		assertTrue(10<=someLog.getWeight() && someLog.getWeight()<=50);
	}
}
