package hillbillies.model;

import ogp.framework.util.ModelException;

/**
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 * @invar  	The position of the Boulder must be a valid position for this Boulder.
 * @invar 	The weight of this Boulder must be a valid weight for this Boulder.
 */
public class Boulder extends Item {

	/**
	 * Initialize this new Boulder with given position and weight.
	 *
	 * @param  position
	 *         The position for this new Boulder.
	 * @effect The position of this new Boulder is set to
	 *         the given position.
	 *       | this.setPosition(position)
	 */
	public Boulder(int[] position) throws ModelException {
		setWeight((int)(Math.random()*(MAX_WEIGHT-MIN_WEIGHT)+MIN_WEIGHT));
		double[] truePosition = {0.5,0.5,0.5};
		for (int i=0;i<position.length;i++) {
			truePosition[i] += position[i];
		}
		try{
			this.setPosition(truePosition);
			this.setnewPosition(truePosition);
		}
		catch (IllegalArgumentException e){
			throw new ModelException();
		}
	}
	/**
	 * The minimum weight of a boulder
	 */
	private final static int MIN_WEIGHT = 10;
	
	/**
	 * The maximum weight of a boulder
	 */
	private final static int MAX_WEIGHT = 50;
	
	/**
	 * Returns the weight of the boulder
	 */
	@Override
	protected int getWeight() {
		return this.weight;
	}
	
	/**
	 * 
	 * @param 	weight
	 * 			The weight to be checked
	 * @return 	Return true if the given weight is between the minimum and maximum weight of the boulder
	 */
	private static boolean isValidWeight(int weight) {
		return (MIN_WEIGHT <= weight && weight <= MAX_WEIGHT);
	}
	
	/**
	 * @param	weight
	 * 			The new weight of the boulder
	 * @post	If the given weight is a valid weight for the boulder then the given weight
	 * 			of this boulder is equal to the given weight
	 * @throws	IllegalArgumentException
	 * 			The given weight is not a valid weight for this boulder.
	 */
	protected void setWeight(int weight) throws IllegalArgumentException {
		if (!isValidWeight(weight)) {
			throw new IllegalArgumentException("this is not a valid boulder weight");
		}
		this.weight = weight;

	}

}
