package hillbillies.model;

enum Status {

	WAITING, WORKING, ATTACKING, MOVING, RESTING, FALLING, FOLLOWING
	
}
