package hillbillies.model;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;
import ogp.framework.util.ModelException;


/**
 * A class that defines a scheduler
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 *
 */
public class Scheduler {
	
	/**
	 * Initializes the scheduler with the given faction
	 * 
	 * @param 	faction
	 * 			The given faction
	 * @effect	The given faction will be linked with
	 * 			|this.receiveFaction(faction) 
	 * @throws	ModelException
	 * 			If the scheduler can't receive the given faction as faction
	 * 			|(! this.canHaveAsFaction(faction) || faction == null)||
	 * 			|(faction.hasScheduler() && faction.getScheduler()!=this) ||
	 * 			|(this.getFaction()!= null && this.getFaction()!= faction)
	 * 		
	 */
	public Scheduler(Faction faction) throws ModelException {
		receiveFaction(faction);
	}
	
	/**
	 * Replaces the given original task with the given replacement task
	 * @param 	original
	 * 			The original task to be replaced
	 * @param 	replacement
	 * 			The replacement task that replaces the original
	 * @throws 	ModelException
	 * 			if this scheduler can't have the given replacement as a task
	 * 			|(!canHaveAsTask(replacement))
	 */
	public void replaceTask(Task original, Task replacement) throws ModelException{
		removeTask(original);
		addTask(replacement);
	}
	
	/**
	 * Returns whether all tasks of the given collection are part of this scheduler
	 * 
	 * @param 	tasks
	 * 			the tasks to be checked
	 * @return	true if all the tasks in the collection are part of this scheduler
	 * 			|(for Task task: tasks)
	 * 			|	if !TaskInScheduler(task)
	 * 			|		result == false
	 * 			|	result == true			
	 */
	public boolean areTasksPartOf(Collection<Task> tasks) {
		for (Task task : tasks) {
			if(!TaskInScheduler(task)) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Returns a iterator of all tasks
	 */
	public Iterator<Task> getAllTasksIterator() {
		Iterator<Task> it = tasks.iterator();
		return it;
		
	}
	
	
	/**
	 * Return the set of all Tasks associated with this Scheduler.
	 * 
	 * @invar	The set of Tasks is effective
	 * 			|Tasks != null
	 * @invar	Each Task in this set of Tasks is a valid Task for this
	 * 			Scheduler.
	 * 			|for each Task in Tasks:
	 * 			|	canHaveAsTask(Task)
	 * @invar	Each Task in this set contains this Scheduler in its set of Schedulers.
	 * 			|for each Task in Tasks:
	 * 			|	(Task.containsScheduler == true)
	 */
	private final TreeSet<Task> tasks = new TreeSet<Task>(new Comparator<Task>() {

	    public int compare(Task task1, Task task2) {
	    	int difference = task1.getPriority() - task2.getPriority();
	    	if (difference < 0)
	    		return 1;
	    	else if (difference == 0) {
	    		int difference2 = task1.getUniqueInstance() - task2.getUniqueInstance();
		    	if (difference2 < 0)
		    		return 1;
		    	else if (difference2 == 0)
		    		return 0;
		    	else{
		    		return -1;
	    		}
	    	}
	    	else
	    		return -1;
	    }
	});


	/**
	 * Return the number of Tasks of this Scheduler.
	 */
	@Basic @Raw
	public int getNbTasks() {
		return tasks.size();
	}
	
	
	/**
	 * 
	 * @return a copy of the set of Tasks
	 */
	public TreeSet<Task> getTasks(){
		TreeSet<Task> copyOfTasks = new TreeSet<Task>(new Comparator<Task>() {

		    public int compare(Task task1, Task task2) {
		    	int difference = task1.getPriority() - task2.getPriority();
		    	if (difference < 0)
		    		return 1;
		    	else if (difference == 0) {
		    		int difference2 = task1.getUniqueInstance() - task2.getUniqueInstance();
			    	if (difference2 < 0)
			    		return 1;
			    	else if (difference2 == 0)
			    		return 0;
			    	else{
			    		return -1;
		    		}
		    	}
		    	else
		    		return -1;
		    }
		});
		for (Task currentTask : tasks){
			copyOfTasks.add(currentTask);
		}
		return copyOfTasks;
	}
	
	/**
	 * Check whether this Scheduler has proper Tasks attached to it.
	 * 
	 * @return 	True if this Scheduler can have each Task as its Task and
	 * 			if each Task of this Scheduler has this Scheduler in its set of Schedulers.
	 * 			|result == 
	 * 			|	for each Task in Tasks:
	 * 			|		if (!canHaveAsTask(currentTask)||(currentTask.SchedulerInSet(this)==false))
	 * 			|			return false
	 * 			|	return true
	 */
	public boolean hasProperTasks(){
		for (Task currentTask : tasks){
			if (!canHaveAsTask(currentTask)||(currentTask.SchedulerInSet(this)==false))
				return false;
		}
		return true;
	}
	
	/**
	 * Check whether the given nbTasks is a valid nbTasks for
	 * any Scheduler.
	 *  
	 * @param  nbTasks
	 *         The nbTasks to check.
	 * @return 
	 *       | result == true if the number of Tasks is bigger or equal than zero but less or equal than the maximum 
	 *       | number of Tasks allowed in a Scheduler
	*/
	public static boolean isValidNbTasks(int nbTasks) {
		return (0 <= nbTasks && nbTasks <= MAXNBTasks);
	
	}
	
	/**
	 * the maximum number of Tasks of the Scheduler
	 */
	private final static int MAXNBTasks = 50;
	
	/**
	 * Check whether this Scheduler can have the given Task 
	 * as one of its Tasks
	 * 
	 * @param 	task
	 * 			The Task to check
	 * @return	True if the Task is different from null and is not terminated and if this Scheduler is not terminated
	 * 			and if the maximum number of tasks will not be exceeded by adding this task.
	 * 			| result == (task!=null && !task.isTerminated && !this.isTerminated 
	 * 			| && (isvalidNbTasks(this.getNbTasks()+1) || TaskInScheduler(task)))
	 */
	public boolean canHaveAsTask(Task task){
		return 
				(task!=null)
				&& (!task.isTerminated())
				&& (!this.isTerminated())
				&& (isValidNbTasks(this.getNbTasks()+1) || TaskInScheduler(task));

	}
	
	
	/**
	 * Checks if the Task belongs to this Scheduler
	 * 
	 * @param 	task
	 * 			The Task to check
	 * @return	True if the Task is in this Scheduler.
	 * 			|result == getTasks().contains(task)
	 * 			
	 */
	public boolean TaskInScheduler(Task task){
		return this.getTasks().contains(task);
	}
	
	/**
	 * Add the given Task to this Scheduler and add this Scheduler
	 * to the list of Schedulers of Task
	 * 
	 * @param 	task
	 * 			The Task to be added
	 * @post	The given Task has this Scheduler as one of its Schedulers.
	 * 			|Task.schedulerInSet(task)
	 * 
	 * @post	This Scheduler has the given Task as one of its Tasks.
	 * 			|this.TaskInScheduler(task)
	 * 
	 * @throws 	IllegalArgumentException
	 * 			The given Task can't be added to this Scheduler.
	 * 			|(!this.canHaveAsTask(task) || !task.canHaveAsScheduler(this))
	 */
	public void addTask(Task task) throws ModelException{
		if (!this.canHaveAsTask(task))
			throw new IllegalArgumentException("Not a valid Task");
		try{
			tasks.add(task);
			task.addSchedulerToSet(this);
		}
		catch (IllegalArgumentException e){
			e.printStackTrace();
			throw new ModelException("Can't add Task to Scheduler");
			
		}
		
	}
	
	/**
	 * Remove the given Task from this Scheduler and removes this Scheduler 
	 * from the set of Schedulers of Task.
	 * 
	 * @param 	task
	 * 			The Task to be removed
	 * @effect	The Task no longer belongs to this Scheduler
	 * 			|!TaskInScheduler(task)
	 * 
	 * @effect	This Scheduler is removed from the set of Schedulers of Task
	 * 			|task.removeScheduler(this)
	 * 
	 * @throws 	IllegalArgumentException
	 * 			The given Task isn't in the right state to be removed from
	 * 			this Scheduler
	 * 			
	 */
	void removeTask(Task task) throws IllegalArgumentException{
		try{
			if (TaskInScheduler(task)) {
				tasks.remove(task);
			}
			task.removeScheduler(this);
		}
		catch (IllegalArgumentException e){
			e.printStackTrace();
			throw new IllegalArgumentException();
		}
	}
	
	/**
	 * Remove all bindings between this Scheduler and its Tasks.
	 * 
	 * @effect	Each Task of this Scheduler no longer has this Scheduler
	 * 			in its set of Schedulers
	 * 			|(for Task task : tasks)
	 * 				task.getScheduler!=this
	 * @effect	The set of Tasks is emptied
	 * 			|this.getNbTasks() == 0
	 * 
	 * @throws 	IllegalArgumentException
	 * 			The method failed to remove the binding between a Task and this Scheduler
	 */
	void removeAllTasks() throws IllegalArgumentException{
		for (Task task : getTasks()) {
			try{
				removeTask(task);
			}
			catch (IllegalArgumentException e){
				throw new IllegalArgumentException();
			}
		}

	}
	
	/**
	 * Returns the task with the highest priority
	 * 
	 * @return 	the next task with the highest priority
	 * 			|if (nextTask.getUnit()==null && nextTask.getPriority() >= -1000 && !nextTask.isTerminated())
	 * 			|	result == nextTask
	 * @return	if none tasks are found return null
	 * 			|else
	 * 			|	result == null
	 */
	public Task getHighestPriorityTask() {
		Iterator<Task> iterator = getAllTasksIterator();
		while(iterator.hasNext()) {
			Task nextTask = iterator.next();
			if (nextTask.getUnit()==null && nextTask.getPriority() >= -1000 && !nextTask.isTerminated()) {
				return nextTask;
			}
		}
		return null;
	}
	
	/**
	 * Return the Faction of this Scheduler.
	 */
	@Basic @Raw
	public Faction getFaction() {
		return this.faction;
	}
	
	/**
	 * Check whether the given Faction is a valid Faction for
	 * any Scheduler.
	 *  
	 * @param  Faction
	 *         The Faction to check.
	 * @return 
	 *       | result == 	true if the Scheduler is terminated and Faction is null,
	 *       				or the Faction isn't null and the Faction isn't terminated.
	*/
	@Raw
	public boolean canHaveAsFaction(@Raw Faction faction) {
		if (isTerminated()){
			return (faction==null);
		}
		else{
			return(faction!=null &&!faction.isTerminated());
		}
			
	}
	
	/**
	 * Set the Faction of this Scheduler to the given Faction.
	 * 
	 * @param  Faction
	 *         The new Faction for this Scheduler.
	 * @post   The Faction of this new Scheduler is equal to
	 *         the given Faction.
	 *       | new.getFaction() == Faction
	 * @throws IllegalArgumentException
	 *         The given Faction is not a valid Faction for any
	 *         Scheduler.
	 *       | ! isValidFaction(getFaction())
	 */
	@Raw
	private void setFaction(@Raw Faction faction){
		this.faction = faction;
	}
	
	@Raw
	/**
	 * Checks whether this Scheduler has a valid Faction attached to it
	 * 
	 * @return 	true if the Scheduler can have the Faction as it's Faction,
	 * 			and the Faction has to have this Scheduler as it's Scheduler or is null.
	 * 			|result == (canHaveAsFaction(getFaction) 
	 * 			|			&& (getFaction().getScheduler == this || getFaction() == null))
	 */
	public boolean hasProperFaction(){
		return (canHaveAsFaction(getFaction()) && (getFaction().getScheduler()==this || getFaction() == null));
	}
	
	/**
	 * Initiates a connection between the given Faction and this Scheduler
	 * 
	 * @param 	faction
	 * 			The Faction to be connected with.
	 * 
	 * @post	if the Faction is a valid Faction for this Scheduler and the Faction
	 * 			can have this Scheduler as its Scheduler then set the Faction of this Scheduler
	 * 			to the given Faction.
	 * 			|new.getFaction == faction
	 * @post	if the given Faction is set as this Schedulers Faction, then set the Scheduler 
	 * 			of the given Faction to this Scheduler.
	 * 			|new.getScheduler == this
	 * @throws 	IllegalArgumentException
	 * 			If the Faction is not a valid Faction for this Scheduler or is null
	 * 			If the Faction already has an Scheduler but it isn't this Scheduler
	 * 			if the Scheduler already has a Faction but it isn't the given Faction
	 * 			|!this.canHaveAsFaction(faction) ||
	 * 			|(faction.hasScheduler() && faction.getScheduler()!=this) ||
	 * 			|(this.getFaction()!=null && this.getFaction()!=faction)
	 */
	public void receiveFaction(Faction faction) throws ModelException{
		if (! this.canHaveAsFaction(faction) || faction == null)
			throw new IllegalArgumentException("Can't have as Faction");
		if (faction.hasScheduler() && faction.getScheduler()!=this)
			throw new IllegalArgumentException("Faction already has another Scheduler");
		if (this.getFaction()!= null && this.getFaction()!= faction)
			throw new IllegalArgumentException("Scheduler already belongs to another Faction");
		
		try{
		setFaction(faction);
		faction.transferScheduler(this);
		}
		catch (IllegalArgumentException e){
			throw new ModelException("invalid Faction-Scheduler connection");
		}
	}
	
	/**
	 * Breaks the connection between this Scheduler and the Faction
	 * this Scheduler is attached to.
	 * 
	 * @effect	The Faction attached to this Scheduler will no longer be
	 * 			attached to this Scheduler
	 * 			|new.getFaction() == null
	 * @effect	The Faction attached to this Scheduler will break it's association
	 * 			with this Scheduler.
	 * 			|this.getFaction().getScheduler() == null
	 */
	private void loseFaction(){
		try{
			Faction formerFaction = getFaction();
			setFaction(null);
			formerFaction.transferScheduler(null);
		}
		catch(NullPointerException e){
		}
	}
	
	/**
	 * Variable registering the Faction of this Scheduler.
	 */
	protected Faction faction;

	/**
	 * Terminate this Scheduler
	 * 
	 * @post	This Scheduler is terminated
	 * 			|this.isTerminated
	 * 
	 * @effect 	This Scheduler will lose its association with the faction it is attached to
	 * 			|new.getFaction() == null
	 * 
	 * @effect	Each Task of this Scheduler loses its association with this Scheduler
	 * 			|new.getNbTask() == 0
	 * 
	 */
	public void terminate(){
		if (!isTerminated()) {
			loseFaction();
			removeAllTasks();
			this.isTerminated = true;
		}
	}
	
	/**
	 * Check whether this Scheduler is terminated.
	 */
	public boolean isTerminated(){
		return isTerminated;
	}
	
	/**
	 * Variable registering whether or not this Scheduler 
	 * is terminated.
	 */
	private boolean isTerminated = false;
	
	

}
