package hillbillies.model;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import hillbillies.model.pathfinding.Position;
import hillbillies.part2.listener.DefaultTerrainChangeListener;

public class ItemTest {
	
	private Item someBoulder, itemWithUnit, someLog, terminatedItem;
	private int[] position0;
	private int[] position1;
	private int[] position2;
	private int[] position3;
	private World world;
	private int nbX;
	private int nbY;
	private int nbZ;
	private Unit unit;
	private double[] unitPosition;


	@Before
	public void setUp() throws Exception {
		nbX = 10;
		nbY = 20;
		nbZ = 30;
		
		world = new World(new int[nbX][nbY][nbZ], new DefaultTerrainChangeListener());
		
		
		for (int x=0; x<nbX; x++){
			for (int y=0; y<nbY; y++){
				for (int z=0; z<nbZ; z++){
					int[] myPosition = {x,y,z};
					if (world.isValidItemPosition(myPosition)){
						
						if (position2!=null && position3 == null){
							position3 = new int[] {x,y,z};
						}
						
						if (position1!=null && position2 == null){
							position2 = new int[] {x,y,z};
						}
						
						if (position0!=null && position1 == null){
							position1 = new int[] {x,y,z};
							unitPosition = new double[] {x,y,z};
						}
						
						if (position0==null){
							position0 = new int[] {x,y,z};

						}
					}
				}
			}
		}
		
		someBoulder = new Boulder(position0);
		
		itemWithUnit = new Boulder(position1);
		unit = new Unit("Unit Low",unitPosition,25,25,25,25,false);
		world.addUnit(unit);
		itemWithUnit.receiveUnit(unit);
		
		someLog = new Log(position2);
		
		terminatedItem = new Log(position3);
		terminatedItem.terminate();
		
		
	}


	//BLACKBOX + WHITEBOX
		
	@Test
	public void getPosition_legalCase() throws Exception{
		double[] truePosition = {0.5,0.5,0.5};
		for (int i=0;i<position0.length;i++) {
			truePosition[i] += position0[i];
		}
		assertTrue(Arrays.equals(truePosition, someBoulder.getPosition()));
	}
	
	@Test
	public void isValidPosition_LegalCase() throws Exception{
		double[] positivePosition = {2,2,2};
		assertTrue(Boulder.isValidPosition(positivePosition));
		double[] negativePosition = {-2,-2,-2};
		assertTrue(Boulder.isValidPosition(negativePosition));
	}
	
	@Test
	public void isValidPosition_FalseCase() throws Exception{
		assertFalse(Boulder.isValidPosition(null));
	}
	
	@Test
	public void setNewPosition() throws Exception{
		double[] newPosition = {5.0,5.0,5.0};
		someLog.setnewPosition(newPosition);
		assertTrue(Arrays.equals(newPosition, someLog.getnewPosition()));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void extendedConstruction_falseCase() throws Exception{
		double[] invalidPosition = {-10,-10,-10};
		if (!Boulder.isValidPosition(invalidPosition)){
			int[] invalidPositionInt = {-10,-10,-10};
			Boulder invalidBoulder = new Boulder(invalidPositionInt);
		}
		else{
			throw new IllegalArgumentException();
		}
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void extendedConstructor_maxValues() throws Exception{
		double[] invalidPosition = {Integer.MAX_VALUE,Integer.MAX_VALUE,Integer.MAX_VALUE};
		if (!Boulder.isValidPosition(invalidPosition)){
			int[] invalidPositionInt = {Integer.MAX_VALUE,Integer.MAX_VALUE,Integer.MAX_VALUE};
			Boulder invalidBoulder = new Boulder(invalidPositionInt);
		}
		else{
			throw new IllegalArgumentException();
		}
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void extendedConstructor_minValues() throws Exception{
		double[] invalidPosition = {Integer.MIN_VALUE,Integer.MIN_VALUE,Integer.MIN_VALUE};
		if (!Boulder.isValidPosition(invalidPosition)){
			int[] invalidPositionInt = {Integer.MIN_VALUE,Integer.MIN_VALUE,Integer.MIN_VALUE};
			Boulder invalidBoulder = new Boulder(invalidPositionInt);
		}
		else{
			throw new IllegalArgumentException();
		}
	}
	
	@Test
	public void extendedConstructor_legalCase() throws Exception{
		int[] validPosition = position0;
		Boulder validBoulder = new Boulder(validPosition);
	}
	
	@Test
	public void getUnit_dontHaveUnit() throws Exception{
		assertEquals(someLog.getUnit(),null);
		assertEquals(someBoulder.getUnit(),null);
	}
	
	@Test
	public void getUnit_hasUnit() throws Exception{
		assertEquals(itemWithUnit.getUnit(),unit);
	}
	
	@Test
	public void canHaveAsUnit_legalCase() throws Exception{
		Unit unitWithoutItem = new Unit("Unit Low",unitPosition,25,25,25,25,false);
		assertTrue(someBoulder.canHaveAsUnit(unitWithoutItem));
		assertTrue(someLog.canHaveAsUnit(unitWithoutItem));
	}
	
	@Test
	public void canHaveAsUnit_ItemTerminatedTrue() throws Exception{
		Unit nullUnit = null;
		assertTrue(terminatedItem.canHaveAsUnit(null));
	}
	
	@Test
	public void canHaveAsUnit_ItemTerminatedFalse() throws Exception{
		Unit legalUnit = new Unit("Unit Low",unitPosition,25,25,25,25,false);
		assertFalse(terminatedItem.canHaveAsUnit(legalUnit));
	}
	
	@Test 
	public void canHaveAsUnit_UnitTerminated() throws Exception{
		
		Unit terminatedUnit = new Unit("Unit Low",unitPosition,25,25,25,25,false);
		world.addUnit(terminatedUnit);
		world.removeUnit(terminatedUnit);
		assertTrue(terminatedUnit.isTerminated());
		assertFalse(someLog.canHaveAsUnit(terminatedUnit));
		
	}
	
	@Test
	public void canHaveAsUnit_UnitNull() throws Exception{
		Unit illegalUnit = null;
		assertFalse(someLog.canHaveAsUnit(illegalUnit));
	}
	
	@Test 
	public void hasProperUnit_legalCase() throws Exception{
		assertTrue(itemWithUnit.hasProperUnit());
	}
	
	@Test 
	public void hasProperUnit_falseCase() throws Exception{
		assertFalse(someBoulder.hasProperUnit());
	}
	
	@Test
	public void receiveUnit_legalCase() throws Exception{
		Unit legalUnit = new Unit("Unit Low",unitPosition,25,25,25,25,false);
		someBoulder.receiveUnit(legalUnit);
		assertEquals(someBoulder.getUnit(),legalUnit);
		assertEquals(legalUnit.getItem(), someBoulder);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void receiveUnit_cantHaveAsUnit() throws Exception{
		Unit illegalUnit = null;
		someBoulder.receiveUnit(illegalUnit);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void receiveUnit_unitHasItem() throws Exception{
		someBoulder.receiveUnit(unit);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void receiveUnit_itemHasUnit() throws Exception{
		Unit legalUnit = new Unit("Unit Low",unitPosition,25,25,25,25,false);
		itemWithUnit.receiveUnit(legalUnit);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void receiveUnit_itemTerminated() throws Exception{
		Unit legalUnit = new Unit("Unit Low",unitPosition,25,25,25,25,false);
		terminatedItem.receiveUnit(legalUnit);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void receiveUnit_unitTerminated() throws Exception{
		world.removeUnit(unit);
		assertFalse(unit.hasItem());
		assertTrue(unit.isTerminated());
		someBoulder.receiveUnit(unit);
	}
	
	@Test
	public void loseUnit_itemWithoutUnit() throws Exception{
		assertTrue(someBoulder.getUnit()==null);
		someBoulder.loseUnit();
		assertTrue(someBoulder.getUnit()==null);
	}
	
	@Test
	public void loseUnit_itemWithUnit() throws Exception{
		assertEquals(itemWithUnit.getUnit(),unit);
		assertEquals(unit.getItem(),itemWithUnit);
		itemWithUnit.loseUnit();
		assertEquals(itemWithUnit.getUnit(),null);
		assertFalse(unit.hasItem());
	}
	
	@Test
	public void loseUnit_terminatedItem() throws Exception{
		assertEquals(terminatedItem.getUnit(),null);
		terminatedItem.loseUnit();
		assertEquals(terminatedItem.getUnit(),null);
	}
	
	@Test
	public void terminate_itemWithUnit() throws Exception{
		itemWithUnit.terminate();
		assertEquals(itemWithUnit.getUnit(),null);
		assertTrue(itemWithUnit.isTerminated());
		assertFalse(unit.hasItem());
		assertFalse(unit.isTerminated());
	}
	
	@Test
	public void terminate_itemWithoutUnit() throws Exception{
		someBoulder.terminate();
		assertEquals(someBoulder.getUnit(),null);
		assertTrue(someBoulder.isTerminated());
	}
	

}		

