package hillbillies.model.statement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import hillbillies.model.expression.E;
import hillbillies.model.expression.ReadVariable;

public class Sequence extends S {
	
	public Sequence(List<? extends S> sequence){
		this.sequence = sequence;
	}
	
	private final List<? extends S> sequence;

	@Override
	public boolean hasAsSubStatement(S statement) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public int getSize(){
		return sequence.size();
	}

	@Override
	public boolean executeStatement() throws IllegalArgumentException{
		if(isDone()==true) {
			setIsDone(false);
			Iterator<? extends S> iterator = sequence.iterator();
			S nextStatement = iterator.next();
			while (nextStatement.isDone() && iterator.hasNext()) {
				iterator.next().setIsDone(false);
			}
		}
		boolean hasRealWork;
		Iterator<? extends S> iterator = sequence.iterator();
		S nextStatement = iterator.next();
		while (nextStatement.isDone() && iterator.hasNext()) {
			nextStatement = iterator.next();
		}
		nextStatement.transferTask(this.getTask());
		hasRealWork = nextStatement.executeStatement();
		if(!iterator.hasNext() && nextStatement.isDone())
			setIsDone(true);
		return hasRealWork;
		//for (S statement: sequence){
		//	statement.transferTask(this.getTask());
		//	statement.executeStatement();
		//}

	}

	@Override
	public boolean isMutable() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean equals(Object other) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String toString() {
		
		String result = "{";
		for (S statement: sequence){
			statement.transferTask(this.getTask());
			result = result + statement.toString() + ",";
		}
		return result.substring(0,result.lastIndexOf(",")) + "}" ;
	}
	
	public void Reset() {
		setIsDone(false);
		Iterator<? extends S> iterator = sequence.iterator();
		while(iterator.hasNext()) {
			S nextStatement = iterator.next();
			nextStatement.transferTask(this.getTask());
			nextStatement.Reset();
		}
	}

	
	@Override
	public S clone() {
		List<S> sequenceCopy = new ArrayList<S>();
		Iterator<? extends S> iterator = sequence.iterator();
		while(iterator.hasNext()) {
			S nextClone = iterator.next().clone();
			sequenceCopy.add(nextClone);
		}
		Sequence sequenceClone = new Sequence(sequenceCopy);
		return sequenceClone;
	}
	
	@Override
	public boolean isWellFormed(Integer depthLevel,HashMap<String, E> variableMap) {
		for (S statement: sequence) {
			int valueBeforeLoop = depthLevel.intValue();
			if(statement.isWellFormed(depthLevel, variableMap)==false) {
				return false;
			}
			int valueAfterLoop = depthLevel.intValue();
			if(valueAfterLoop<valueBeforeLoop) {
				break;
			}

		}
		return true;
	}

}
