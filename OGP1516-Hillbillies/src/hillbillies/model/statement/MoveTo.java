package hillbillies.model.statement;

import hillbillies.model.expression.*;

public class MoveTo extends PositionTask {

	public MoveTo(E position) {
		super(position);
	}
	

	@Override
	public boolean hasAsSubStatement(S statement) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean executeStatement() throws IllegalArgumentException{
		//getUnit().MoveTo(getExpression().getValue()) //Unit of the task and MoveTo(int [])
		getExpression().transferStatement(this);
		int[] target = (int[])getExpression().getValue();
		int[] currentCube = getTask().getUnit().getOccupiedCube();
		setIsDone(true);
		if(target[0]==currentCube[0] && target[1]==currentCube[1] && target[2]==currentCube[2]) {
			return false;
		}
		else {
			getTask().getUnit().moveTo(target);
			return true;
		}

	}

	@Override
	public String toString() {
		getExpression().transferStatement(this);
		return "moveTo" + getExpression().toString();
	}

	@Override
	public boolean equals(Object other){
		return ((other instanceof MoveTo) && (this.getExpression().equals(((ExpressionStatement) other).getExpression())));
	}
	
	
	@Override
	public S clone() {
		MoveTo moveToClone = new MoveTo(getExpression().clone());
		moveToClone.getExpression().transferStatement(moveToClone);
		return moveToClone;
	}

}
