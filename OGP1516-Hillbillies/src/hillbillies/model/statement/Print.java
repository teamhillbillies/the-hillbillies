package hillbillies.model.statement;

import java.util.HashMap;

import hillbillies.model.expression.E;
import hillbillies.model.expression.ReadVariable;

public class Print extends Variable {

	public Print(E expression) {
		super(expression);
	}

	@Override
	public boolean hasAsSubStatement(S statement) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean executeStatement() {
		getExpression().transferStatement(this);
		System.out.println(getExpression().getValue());
		setIsDone(true);
		return false;

	}

	@Override
	public String toString() {
		getExpression().transferStatement(this);
		return "print (" + getExpression().toString() + ")";
	}

	@Override
	public boolean equals(Object other){
		return ((other instanceof Print) && (this.getExpression().equals(((ExpressionStatement) other).getExpression())));
	}
	
	@Override
	public S clone() {
		Print printClone = new Print(getExpression().clone());
		printClone.getExpression().transferStatement(printClone);
		return printClone;
	}
	
	@Override
	public boolean isWellFormed(Integer depthLevel,HashMap<String, E> variableMap) {
		if(getExpression() instanceof ReadVariable) {
			if (variableMap.containsKey(((ReadVariable) getExpression()).getStringVariable())) {
				return true;
			}
			return false;
		}
		return true;
	}


}
