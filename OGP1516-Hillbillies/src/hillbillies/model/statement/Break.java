package hillbillies.model.statement;

import java.util.HashMap;

import hillbillies.model.expression.E;

public class Break extends S {

	@Override
	public boolean hasAsSubStatement(S statement) {
		return (statement == this);
	}

	@Override
	public boolean executeStatement() {
		if (getTask().getWhileDepth()>=1)
			getTask().setStopWhileDepth(getTask().getStopWhileDepth()+1);
		setIsDone(true);
		return false;

	}

	@Override
	public boolean isMutable() {
		return false;
	}

	@Override
	public boolean equals(Object other) {
		if (other instanceof Break)
			return true;
		return false;
	}

	@Override
	public String toString() {
		return "break";
	}

	@Override
	public void Reset() {
		setIsDone(false);
		getTask().setStopWhileDepth(0);
	}

	@Override
	public S clone() {
		Break breakClone = new Break();
		return breakClone;
	}

	@Override
	public boolean isWellFormed(Integer depthLevel,HashMap<String, E> variableMap) {
		if (depthLevel>=1) {
			depthLevel -= 1;
			return true;
		}
		return false;
	}
	

}
