package hillbillies.model.statement;

import java.util.HashMap;

import hillbillies.model.Task;
import hillbillies.model.expression.E;

/*
 * A class of statements units that will be used in tasks so units will execute them
 * 
* @author Maarten Pieck
* @author Thomas Vandendijk
* @version 1.0
*
*/
public abstract class S implements ResetInterface, Cloneable{
	
	/**
	 * Check whether this statement has the given statement as one
	 * of its substatements.
	 * 
	 * @param 	statement
	 * 			statement to be checked
	 * @return	true if the given statement is the same statement as this statement
	 * @return 	false if the given statement is not effective
	 */
	public abstract boolean hasAsSubStatement(S statement);
	
	/**
	 * Runs this statement
	 * 
	 * @post	This statement is runned and had his impact on the world
	 */
	public abstract boolean executeStatement();
	
	/**
	 * Check whether the state of this statement can be changed
	 */
	public abstract boolean isMutable();
	
	/**
	 * Check whether this statement is equal to the given object
	 * 
	 * 
	 * @return 	true if the given object is the same as this statement
	 * @return 	if this statement is mutable, true if and only if this
	 *         	statement and the given object are the same.
	 * @return	false if the given object is not effective
	 * @return	false if the given object and this expression do not belong
	 * 			to the same concrete class	
	 */
	public abstract boolean equals(Object other);
	
	
	/**
	 * 
	 * @param 	other
	 * 			the statement to compare with
	 * @return	true if the given statement is equal to this expression
	 * @return 	false if the given statement is not effective
	 * @return	false if the given statement is from a different concrete
	 * 			class than this statement
	 */
	public boolean isIdenticalTo(S other){
		if (other == null){
			return false;
		}
		
		if (this.getClass() != other.getClass()){
			return false;
		}
		
		if (this.equals(other)){
			return true;
		}
		return false;
			
	}
	
	/**
	 * Return a clone of this statement
	 * 
	 * @return	the returning statement is identical to this statement
	 * @return 	the returning statement is the same as this statement
	 * 			if the statement is immutable
	 */
	@Override
	public abstract S clone();
	
	/**
	 * Return the hashCode of this statement
	 */
	@Override
	public int hashCode(){
		return super.hashCode();
	}
	
	/**
	 * Return the textual representation of this statement
	 * 
	 * @return 	the resulting statement is non-empty
	 */
	@Override
	public abstract String toString();
	
	/**
	 * Returns the task of this statement
	 */
	public Task getTask(){
		return this.task;
	}
	
	/**
	 * Sets the task of this statement to the given task
	 * @param 	task
	 * 			The new task of this statement
	 * @pre		The given task must be a valid task for this statement
	 * @post	the new task of this statement is the given task
	 */
	protected void setTask(Task task){
		this.task = task;
	}
	
	/**
	 * Checks whether the given task is a valid task for this statement
	 * @param 	task
	 * 			the task to be checked
	 * @return	false if the given task is not effective, otherwise return true
	 */
	public boolean canHaveAsTask(Task task){
		if (task == null)
			return false;
		return true;
	}
	
	/**
	 * Checks whether this statement has a proper task
	 * 
	 * @return	false if this statement can't have his task as a task
	 * @return 	false if the task of this statement points to another statement
	 * @return 	otherwise return true 
	 */
	public boolean hasProperTask(){
		if (!canHaveAsTask(this.getTask()))
			return false;
		if (this.getTask().getActivity()!=this)
			return false;
		return true;
	}
	
	/**
	 * Gives this statement the given task as it's task
	 * 
	 * @param 	task
	 * 			The new task of this statement
	 * @post	The new task of this statement is the given task
	 * @throws 	IllegalArgumentException
	 * 			If this statement can't have the given task as it's task
	 * 			or if the given task already points to another statement
	 */
	public void transferTask(Task task) throws IllegalArgumentException{
		if (!canHaveAsTask(task))
			throw new IllegalArgumentException("statement can't have the task");
		//if (task.getActivity()!=this)
		//	throw new IllegalArgumentException("not a valid bidirectional relation");
		setTask(task);
		
	}
	

	/**
	 * Variable registering the task of this statement
	 */
	private Task task;
	
	private boolean isDone;
	
	public boolean isDone() {
		return isDone;
	}
	
	public void setIsDone(boolean newIsDone) {
		isDone = newIsDone;
	}
	
	public abstract boolean isWellFormed(Integer value, HashMap<String, E> variableMap);
	
//	public abstract void terminate();
//	
//
//	public boolean isTerminated(){
//		return isTerminated;
//	}
//	
//
//	protected boolean isTerminated = false;
	
}
