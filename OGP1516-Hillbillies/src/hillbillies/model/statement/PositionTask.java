package hillbillies.model.statement;

import java.util.HashMap;

import hillbillies.model.expression.E;
import hillbillies.model.expression.ReadVariable;

/**
 * A class using the PositionE as it's basic expression.
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 */
public abstract class PositionTask extends BasicStatement {
	
	/**
	 * 
	 * @param 	expression
	 * 			The PositionE this PositionTask will get as it's basic expression
	 * @post	The given expression is the new PositionE of this PositionTask statement
	 */
	public PositionTask(E expression) {
		super(expression);
		
	}
	
	@Override
	public boolean isWellFormed(Integer depthLevel,HashMap<String, E> variableMap) {
		if(getExpression() instanceof ReadVariable) {
			if (variableMap.containsKey(((ReadVariable) getExpression()).getStringVariable())) {
				return true;
			}
			return false;
		}
		return true;
	}
	

}
