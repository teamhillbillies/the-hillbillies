package hillbillies.model.statement;

import static org.junit.Assert.*;

import java.util.HashMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import hillbillies.model.Task;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.model.expression.E;
import hillbillies.model.expression.IsSolid;
import hillbillies.model.expression.Position;
import hillbillies.model.expression.PositionE;
import hillbillies.model.expression.ReadVariable;
import hillbillies.part2.facade.Facade;
import hillbillies.part2.listener.DefaultTerrainChangeListener;

public class IfStatementTest {
	
	private static final int TYPE_AIR = 0;
	private static final int TYPE_ROCK = 1;
	private static final int TYPE_TREE = 2;
	private static final int TYPE_WORKSHOP = 3;
	private Facade facade;
	private World world;
	private static PositionE passablePosition;
	private static IsSolid trueIsSolid;
	private static S statement, statement2;
	private Unit unit0;
	private Task task1;
	private IfStatement someIfStatement;

	@Before
	public void setUp() throws Exception {
		
		int[][][] types = new int[3][3][3];
		types[1][1][0] = TYPE_AIR;
		types[1][1][1] = TYPE_TREE;
		types[1][1][2] = TYPE_WORKSHOP;
		
		this.facade = new Facade();
		
		double[] unitPosition = {1,1,1};
		unit0 = new Unit("Unit Low0",unitPosition,25,25,25,25,false);

		world = facade.createWorld(types, new DefaultTerrainChangeListener());;
		
		world.addUnit(unit0);
	

		int[] position2 = {1,1,1};
		passablePosition = new Position(position2);
		trueIsSolid = new IsSolid(passablePosition);

		//trueIsSolid.transferStatement((ExpressionStatement) statement);
		statement = new MoveTo(new ReadVariable("variable"));
		statement2 = new MoveTo(trueIsSolid);
	
		
		someIfStatement = new IfStatement(trueIsSolid, statement, statement2);
		
		task1 = new Task("TaskTest1",10,someIfStatement, position2);
		task1.linkUnit(unit0);
		
	}

	@Test
	public void extendedConstructor() throws Exception{
		IfStatement myIfStatement = new IfStatement(trueIsSolid,statement, statement2);
	}
	
	@Test
	public void isWellFormed() throws Exception{
		HashMap<String, E> variableMap = new HashMap<String, E>();
		variableMap.put("variable", passablePosition);
		assertTrue(someIfStatement.isWellFormed(0, variableMap));
	}
	
	@Test
	public void isWellFormed_FalseCase() throws Exception{
		HashMap<String, E> variableMap = new HashMap<String, E>();
		assertFalse(someIfStatement.isWellFormed(0, variableMap));
	}

}
