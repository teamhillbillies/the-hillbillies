package hillbillies.model.statement;

import hillbillies.model.expression.*;

/**
 * A class of basic statements, it contains a single action.
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 */
public abstract class BasicStatement extends ExpressionStatement {
	
	/**
	 * Initializes the basic statement with the given basic expression
	 * 
	 * @param 	expression
	 * 			the basic expression the statement uses
	 * @post	the statement has the basic expression as it's basic expression
	 */
	public BasicStatement(E expression){
		super(expression);
	}
	

	/**
	 * Return whether the state of this basic statement can be changed
	 * @return	always returns false.
	 */
	@Override
	public boolean isMutable() {
		return false;
	}
	
	/**
	 * @return true if the given statement is equal to this basic statement
	 */
	@Override
	public boolean hasAsSubStatement(S statement) {
		return statement == this;
	}
	
	
	@Override
	public void Reset() {
		setIsDone(false);
	}
	
	public abstract S clone();

}
