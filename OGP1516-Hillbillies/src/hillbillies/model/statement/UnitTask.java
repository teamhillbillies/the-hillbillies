package hillbillies.model.statement;

import java.util.HashMap;

import hillbillies.model.expression.E;
import hillbillies.model.expression.ReadVariable;

/**
 * A class using the UnitE as it's basic expression.
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 */
public abstract class UnitTask extends BasicStatement {
	
	/**
	 * 
	 * @param 	unit
	 * 			The UnitE this UnitTask will get as it's basic expression
	 * @post	The given expression is the new UnitE of this UnitTask statement
	 */
	public UnitTask(E unit){
		super(unit);
	}

	
	public boolean hasRealWork = false;
	
	@Override
	public boolean isWellFormed(Integer depthLevel,HashMap<String, E> variableMap) {
		if(getExpression() instanceof ReadVariable) {
			if (variableMap.containsKey(((ReadVariable) getExpression()).getStringVariable())) {
				return true;
			}
			return false;
		}
		return true;
	}
	
		
	@Override
	public boolean equals(Object other){
		return ((other instanceof UnitTask) && (this.getExpression()==((ExpressionStatement) other).getExpression()));
	}


}
