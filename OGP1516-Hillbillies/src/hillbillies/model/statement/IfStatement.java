package hillbillies.model.statement;

import java.util.HashMap;

import hillbillies.model.expression.E;
import hillbillies.model.expression.ReadVariable;


public class IfStatement extends ConditionStatement {

	public IfStatement(E condition,S ifBody,S elseBody) {
		super(condition);
		this.ifBody = ifBody;
		this.elseBody = elseBody;
	}
	
	private final S ifBody;
	private final S elseBody;
	
	public S getIfBody(){
		return this.ifBody;
	}
	
	public S getElseBody(){
		return this.elseBody;
	}

	@Override
	public boolean executeStatement() {
		boolean hasRealWork;
		if (hasPassedCheck == 1) {
			hasRealWork = getIfBody().executeStatement();
			if(getIfBody().isDone()) {
				setIsDone(true);
			}
		}
		else if (hasPassedCheck == 2) {
			hasRealWork = getElseBody().executeStatement();
			if(getElseBody().isDone()) {
				setIsDone(true);
			}
		}
		getExpression().transferStatement(this);
		if ((boolean) getExpression().getValue()){
			hasPassedCheck = 1;
			getIfBody().transferTask(this.getTask());
			hasRealWork = getIfBody().executeStatement();
			if(getIfBody().isDone()) {
				setIsDone(true);
			}
		}
		else{
			if (getElseBody()!= null) {
				hasPassedCheck = 2;
				getElseBody().transferTask(this.getTask());
				hasRealWork = getElseBody().executeStatement();
				if(getIfBody().isDone()) {
					setIsDone(true);
				}
			}
			else {
				setIsDone(true);
				hasRealWork = false;
			}
		}
		return hasRealWork;
		
	}

	@Override
	public String toString() {
		getExpression().transferStatement(this);
		getIfBody().transferTask(this.getTask());
		if (getElseBody()!=null) {
			getElseBody().transferTask(this.getTask());
		return "if" + getExpression().toString() + "then" + getIfBody().toString()
				+ "| else" + getElseBody().toString() + "| fi";
		}
		else {
			return "if" + getExpression().toString() + "then" + getIfBody().toString()
					+ "| fi";
		}

	}

	@Override
	public boolean hasAsSubStatement(S statement) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean equals(Object other) {
		return ((other instanceof IfStatement) && (this.getIfBody().equals(((IfStatement) other).getIfBody()))
				&& (this.getElseBody().equals(((IfStatement) other).getElseBody())) 
				&& (this.getExpression().equals(((ExpressionStatement) other).getExpression())));
	}
	
	@Override
	public void Reset() {
		hasPassedCheck = 0;
		getIfBody().transferTask(this.getTask());
		getIfBody().Reset();
		if (getElseBody()!=null)
			getElseBody().transferTask(this.getTask());
			getElseBody().Reset();
		setIsDone(false);
	}
	
	/**
	 * = 0 -> no check yet
	 * =1 -> if statement
	 * =2 -> else statement
	 */
	private int hasPassedCheck = 0;
	
	@Override
	public S clone() {
		S ifBodyClone = getIfBody().clone();
		IfStatement ifStatementClone;
		if (getElseBody() != null) {
			S elseBodyClone = getElseBody().clone();
			ifStatementClone = new IfStatement(getExpression().clone(),ifBodyClone,elseBodyClone);
		}
		else {
			ifStatementClone = new IfStatement(getExpression().clone(),ifBodyClone,null);
		}
		ifStatementClone.getExpression().transferStatement(ifStatementClone);
		return ifStatementClone;
	}
	
	@Override
	public boolean isWellFormed(Integer depthLevel,HashMap<String, E> variableMap) {
		if(getExpression() instanceof ReadVariable) {
			if (!variableMap.containsKey(((ReadVariable) getExpression()).getStringVariable())) {
				return false;
			}
		}
		HashMap<String, E> mapCopy = new HashMap<String, E>(variableMap);
		Integer depthCopy = new Integer(depthLevel.intValue());
		if(getIfBody().isWellFormed(depthLevel, variableMap)==false)
			return false;
		if(getElseBody().isWellFormed(depthCopy, mapCopy)==false)
			return false;
		if(depthCopy>depthLevel) {
			depthLevel = depthCopy;
			variableMap = mapCopy;
		}
		return true;
	}

	
}
