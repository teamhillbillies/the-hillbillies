package hillbillies.model.statement;

import hillbillies.model.expression.E;
import hillbillies.model.expression.PositionE;

public class Work extends PositionTask {

	public Work(PositionE expression) {
		super(expression);
	}
	
	public Work(E expression) {
		super(expression);
	}

	@Override
	public boolean hasAsSubStatement(S statement) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean executeStatement() {
		//getUnit().work(getExpression().getValue()) // getUnit is unit of the task and work(int [])
		getExpression().transferStatement(this);
		getTask().getUnit().work((int[])getExpression().getValue());
		setIsDone(true);
		return true;
	}

	@Override
	public String toString() {
		getExpression().transferStatement(this);
		return "work" + getExpression().toString();
	}
	
	@Override
	public boolean equals(Object other){
		return ((other instanceof Work) && (this.getExpression().equals(((ExpressionStatement) other).getExpression())));
	}

	
	@Override
	public S clone() {
		Work workClone = new Work((PositionE) getExpression().clone());
		workClone.getExpression().transferStatement(workClone);
		return workClone;
	}
}
