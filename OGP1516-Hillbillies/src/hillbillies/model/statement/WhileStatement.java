package hillbillies.model.statement;

import java.util.HashMap;

import hillbillies.model.expression.E;
import hillbillies.model.expression.ReadVariable;

public class WhileStatement extends ConditionStatement {

	public WhileStatement(E condition, S body) {
		super(condition);
		this.body = body;
	}
	
	private final S body;
	
	public S getBody(){
		return this.body;
	}

	@Override
	public boolean executeStatement() {
		boolean hasRealWork;
		if (!depthLevelIncreased) {
			getTask().setWhileDepth(getTask().getWhileDepth()+1);
			depthLevelIncreased = true;
			getExpression().transferStatement(this);
			getBody().transferTask(this.getTask());
		}
		if( ((boolean) getExpression().getValue()) && (getTask().getStopWhileDepth() == 0)) {
			hasRealWork = this.body.executeStatement();
		}
		else {
			if(getTask().getStopWhileDepth()>=1) {
				getTask().setStopWhileDepth(getTask().getStopWhileDepth()-1);
			}
			getTask().setWhileDepth(getTask().getWhileDepth()-1);
			setIsDone(true);
			depthLevelIncreased = false;
			hasRealWork = false;
		}
		return hasRealWork;

	}

	@Override
	public String toString() {
		getExpression().transferStatement(this);
		getBody().transferTask(this.getTask());
		return "while" + getExpression().toString() + "do" + getBody().toString() + "done";
	}

	@Override
	public boolean hasAsSubStatement(S statement) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean equals(Object other) {
		return ((other instanceof WhileStatement) && (this.getBody().equals(((WhileStatement) other).getBody()))
				&& (this.getExpression().equals(((ExpressionStatement) other).getExpression())));
	}
	
	private boolean depthLevelIncreased = false;
	
	@Override
	public void Reset() {
		setIsDone(false);
		getTask().setWhileDepth(0);
		depthLevelIncreased = false;
		getBody().transferTask(this.getTask());
		this.body.Reset();
	}
	
	
	@Override
	public S clone() {
		S bodyClone = getBody().clone();
		WhileStatement whileStatementClone = new WhileStatement(getExpression().clone(),bodyClone);
		whileStatementClone.getExpression().transferStatement(whileStatementClone);
		return whileStatementClone;
	}
	
	@Override
	public boolean isWellFormed(Integer depthLevel,HashMap<String, E> variableMap) {
		if(getExpression() instanceof ReadVariable) {
			if (!variableMap.containsKey(((ReadVariable) getExpression()).getStringVariable())) {
				return false;
			}
		}
		depthLevel += 1;
		int valueBeforeLoop = depthLevel.intValue();
		boolean bodyIsWellFormed = getBody().isWellFormed(depthLevel, variableMap);
		int valueAfterLoop = depthLevel.intValue();
		if(valueBeforeLoop==valueAfterLoop)
			depthLevel -= 1;
		return bodyIsWellFormed;
	}

}
