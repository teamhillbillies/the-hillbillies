package hillbillies.model.statement;

import java.util.HashMap;

import hillbillies.model.expression.E;

public class Assign extends Variable {

	public Assign(E expression, String variableName) {
		super(expression);
		this.variableName = variableName;
	}
	
	private final String variableName;
	
	public String getVariableName(){
		return this.variableName;
	}
	
	@Override
	public boolean hasAsSubStatement(S statement) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean executeStatement() {
		getExpression().transferStatement(this);
		getTask().addVariable(getVariableName(), getExpression());
		setIsDone(true);
		return false;
	}

	@Override
	public String toString() {
		getExpression().transferStatement(this);
		return getVariableName() + ":=" + getExpression().toString();
	}

	@Override
	public boolean equals(Object other){
		return ((other instanceof Assign) && (this.getExpression().equals(((ExpressionStatement) other).getExpression())));
	}
	
	@Override
	public S clone() {
		Assign assignClone = new Assign(getExpression().clone(),getVariableName());
		assignClone.getExpression().transferStatement(assignClone);
		return assignClone;
	}
	
	@Override
	public boolean isWellFormed(Integer depthLevel,HashMap<String, E> variableMap) {
		variableMap.put(getVariableName(),getExpression());
		return true;
	}

}
