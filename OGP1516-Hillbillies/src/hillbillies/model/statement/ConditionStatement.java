package hillbillies.model.statement;

import hillbillies.model.expression.E;


/**
 * A class of statements that use a condition.
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 */
public abstract class ConditionStatement extends ExpressionStatement {
	
	/**
	 * Initializes this condition statement with the given condition expression
	 * 
	 * @param 	condition
	 * 			the composed condition this statement will use
	 * @post	The given condition is the new condition of this statement
	 */
	public ConditionStatement(E condition){
		super(condition);
	}
	

	@Override
	public boolean isMutable() {
		return false;
	}

}
