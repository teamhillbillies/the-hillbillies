package hillbillies.model.statement;

import hillbillies.model.Unit;
import hillbillies.model.expression.E;
import hillbillies.model.expression.UnitE;

public class Follow extends UnitTask {

	public Follow(E unit) {
		super(unit);
	}

	@Override
	public boolean hasAsSubStatement(S statement) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean executeStatement() {
		getExpression().transferStatement(this);
		getTask().getUnit().follow((Unit) getExpression().getValue());
		setIsDone(true);
		return true;

	}

	@Override
	public String toString() {
		getExpression().transferStatement(this);
		return "follow" + getExpression().toString();
	}

	@Override
	public boolean equals(Object other){
		return ((other instanceof Follow) && (this.getExpression().equals(((ExpressionStatement) other).getExpression())));
	}

	
	@Override
	public S clone() {
		Follow followClone = new Follow((UnitE) getExpression().clone());
		followClone.getExpression().transferStatement(followClone);
		return followClone;
	}
}
