package hillbillies.model.statement;

import hillbillies.model.Unit;
import hillbillies.model.expression.E;
import hillbillies.model.expression.UnitE;

public class Attack extends UnitTask {

	public Attack(E unit) {
		super(unit);
	}


	@Override
	public boolean executeStatement() {
		//getUnit().fight(getExpression().getValue()); //must be connected with an unit
		getExpression().transferStatement(this);
		getTask().getUnit().fight((Unit)getExpression().getValue());
		setIsDone(true);
		return true;
	}

	@Override
	public String toString() {
		getExpression().transferStatement(this);
		return "attack" + getExpression().toString();
	}


	@Override
	public boolean hasAsSubStatement(S statement) {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public boolean equals(Object other){
		return ((other instanceof UnitTask) && (this.getExpression().equals(((ExpressionStatement) other).getExpression())));
	}

	@Override
	public S clone() {
		Attack attackClone = new Attack((UnitE) getExpression().clone());
		attackClone.getExpression().transferStatement(attackClone);
		return attackClone;
	}
}
