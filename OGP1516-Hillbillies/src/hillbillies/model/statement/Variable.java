package hillbillies.model.statement;

import hillbillies.model.expression.E;

/**
 * A class using the Number of expression as it's basic expression.
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 */
public abstract class Variable extends BasicStatement {
	
	/**
	 * 
	 * @param 	number
	 * 			The Number this Variable will get as it's basic expression
	 * @post	The given number is the new Number of this Variable statement
	 */
	public Variable(E number){
		super(number);
	}
	
	@Override
	public boolean equals(Object other){
		return ((other instanceof Variable) && (this.getExpression()==((ExpressionStatement) other).getExpression()));
	}


}
