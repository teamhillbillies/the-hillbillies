package hillbillies.model.statement;

import hillbillies.model.expression.E;

public abstract class ExpressionStatement extends S {

	/**
	 * Initializes the expression statement with the given expression
	 * 
	 * @param 	expression
	 * 			the expression the statement uses
	 * @post	the statement has the expression as it's expression
	 * @post	the given expression has this statement as it's statement
	 * 
	 */
	public ExpressionStatement(E expression){
		this.expression = expression;
	}
	
	/**
	 * Variable registering the expression of this expression statement
	 */
	private final E expression;
	
	/**
	 * Returns the expression this expression statement uses
	 */
	public E getExpression(){
		return this.expression;
	}
	
//	private void destroyExpression() {
//		getExpression().terminate();
//	}
//	
//	private void loseTask() {
//		setTask(null);
//	}
//	public void terminate(){
//		if (!isTerminated()) {
//			destroyExpression();
//			loseTask();
//			this.isTerminated = true;
//		}
//	}

}
