package hillbillies.model;

public enum TerrainType {

	AIR(true),WORKSHOP(true),ROCK(false),TREE(false),BORDER(true);
	private boolean isPassable;
	private TerrainType(boolean isPassable) {
		this.isPassable = isPassable;
	}
	public boolean isPassable() {
		return isPassable;
	}
	
}
