package hillbillies.model;

import java.util.*;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;
import ogp.framework.util.ModelException;
import ogp.framework.util.Util;
import hillbillies.model.pathfinding.AStarAlgorithm;
import hillbillies.model.pathfinding.Position;
import hillbillies.part2.listener.TerrainChangeListener;
import hillbillies.util.ConnectedToBorder;

/**
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 * 
 * @invar The units attached to this world must be acceptable units
 * @invar The factions attached to this world must be acceptable factions
 * @invar The items attached to this world must be acceptable factions
 * @invar The world matrix must be a valid world matrix
 */
public class World {
	/**
	 * Initializes the world with the given matrix size and terrainchangelistener
	 * 
	 * @param 	intWorld
	 * 			The size of the matrix of the world
	 * @param 	modelListener1
	 * 			The terrainchangelistener of this world
	 * @throws	ModelException
	 * 			if the worldMatrix can't be set
	 * 			(!isValidWorldMatrix(newWorld)
	 * 			
	 */
	public World(int[][][] intWorld, TerrainChangeListener modelListener1) throws ModelException{
		X_LENGTH = intWorld.length;
		Y_LENGTH = intWorld[0].length;
		Z_LENGTH = intWorld[0][0].length;
		TerrainType[][][] newWorld = new TerrainType[X_LENGTH][Y_LENGTH][Z_LENGTH];
		borderConnectionAlgorithm = new ConnectedToBorder(X_LENGTH,Y_LENGTH,Z_LENGTH);

		for (int i=0;i<X_LENGTH;i++)  {
			for (int j=0;j<Y_LENGTH;j++)  {
				for (int k=0;k<Z_LENGTH;k++)  {
					switch (intWorld[i][j][k]) {
						case 0:
							newWorld[i][j][k] = TerrainType.AIR;
							borderConnectionAlgorithm.changeSolidToPassable(i,j,k);
							break;
						case 1:
							newWorld[i][j][k] = TerrainType.ROCK;
							break;
						case 2:
							newWorld[i][j][k] = TerrainType.TREE;
							break;
						case 3:
							newWorld[i][j][k] = TerrainType.WORKSHOP;
							borderConnectionAlgorithm.changeSolidToPassable(i,j,k);
							allWorkShops.add(new Position(i,j,k));
							break;
						default:
							newWorld[i][j][k] = TerrainType.AIR;
							borderConnectionAlgorithm.changeSolidToPassable(i,j,k);
							break;
					}
				}
			}
		}
		pathFinder = new AStarAlgorithm();
		try{
		this.setWorldMatrix(newWorld);
		}
		catch(IllegalArgumentException e){
			throw new ModelException();
		}
		modelListener = modelListener1;
		
	}
	
	TerrainChangeListener modelListener;
	
	ConnectedToBorder borderConnectionAlgorithm;
	
	AStarAlgorithm pathFinder;
	
	/**
	 * Return the worldMatrix of this world.
	 */
	@Basic @Raw
	public TerrainType[][][] getWorldMatrix() {
		return this.worldMatrix;
	}
	
	/**
	 * Check whether the given worldMatrix is a valid worldMatrix for
	 * any world.
	 *  
	 * @param  worldMatrix
	 *         The worldMatrix to check.
	 * @return 
	 *       | result == true
	*/
	public static boolean isValidWorldMatrix(TerrainType[][][] worldMatrix) {
		return true;
	}
	
	/**
	 * Set the worldMatrix of this world to the given worldMatrix.
	 * 
	 * @param  worldMatrix
	 *         The new worldMatrix for this world.
	 * @pre    The given worldMatrix must be a valid worldMatrix for any
	 *         world.
	 *       | isValidworldMatrix(worldMatrix)
	 * @post   The worldMatrix of this world is equal to the given
	 *         worldMatrix.
	 *       | new.getworldMatrix() == worldMatrix
	 */
	@Raw
	private void setWorldMatrix(TerrainType[][][] worldMatrix) throws IllegalArgumentException{
		if(! isValidWorldMatrix(worldMatrix)) {
			throw new IllegalArgumentException("this is not a valid world matrix");
		}
		this.worldMatrix = worldMatrix;
	}
	
	/**
	 * Variable registering the worldMatrix of this world.
	 */
	private TerrainType[][][] worldMatrix;
	
	public TerrainType getTerrainType(int[] position) {
		if (0 <= position[0] && position[0] < X_LENGTH && 0 <= position[1] && position[1] < Y_LENGTH && 0 <= position[2] && position[2] < Z_LENGTH)
			return worldMatrix[position[0]][position[1]][position[2]];
		return TerrainType.BORDER;
	}
	/**
	 * Sets the terrain to the given type at the given position in the world matrix
	 * 
	 * @param 	position
	 * 			The position where the terrain has to be set
	 * @param 	type
	 * 			The type of the terrain that needs to be set
	 * @post	The given position in the matrix will be set to the given type
	 * 			
	 */
	public void setTerrainType(int[] position,TerrainType type) {

		if (worldMatrix[position[0]][position[1]][position[2]] == TerrainType.WORKSHOP) {
			allWorkShops.remove(new Position(position[0],position[1],position[2]));
		}
		if (type==TerrainType.WORKSHOP) {
			allWorkShops.add(new Position(position[0],position[1],position[2]));
		}
		worldMatrix[position[0]][position[1]][position[2]] = type;
		this.modelListener.notifyTerrainChanged(position[0], position[1], position[2]);
	}
	
	/**
	 * A set of all the workshops in the world
	 */
	private final Set<Position> allWorkShops = new HashSet<Position>();
	
	/**
	 * 
	 * @return all workshops in the world
	 */
	public Set<Position> getAllWorkshops() {
		return this.allWorkShops;
	}
	
	/**
	 * Destroys a solid block and sets the terrain type to air.
	 * @param 	position
	 * 			The position where the solid block needs to be destroyed
	 * @effect	The type of the terrain at the given position is changed to air
	 */
	public void destroySolidBlock(int[] position) throws ModelException{
		List<int[]> extraCubes = borderConnectionAlgorithm.changeSolidToPassable(position[0],position[1],position[2]);
		ArrayList<int[]>changedCubes = new ArrayList<int[]>();
		changedCubes.add(position);
		if (!extraCubes.isEmpty()) {
			changedCubes.addAll(extraCubes);
		}
		
		for (int[] changedCube : changedCubes) {
			TerrainType oldTerrainType = getTerrainType(changedCube);
			setTerrainType(changedCube,TerrainType.AIR);
			int[] aboveCube = changedCube.clone();
			aboveCube[2] += 1;
			Position positionObject = new Position(aboveCube);
			if (itemsInWorld.containsKey(positionObject)) {
				List<Item> listOfItems = new ArrayList<Item>(itemsInWorld.get(positionObject));
				for (Item item : listOfItems) {
					item.setIsFalling(true);
					double[] newPosition = findAllowedPosition(item.getPosition(), false);
					item.setnewPosition(newPosition);
					Position newPositionObject = new Position(getOccupiedCube(newPosition));
					changeItemPosition(item, positionObject, newPositionObject);
				}
			}
			for (Unit unit : unitsInWorld) {
				if (!isValidUnitPosition(unit.getPosition())) {
					unit.startFalling();
					try {
						unit.setNewPosition(findAllowedPosition(unit.getPosition(), false));
					}
					catch (IllegalArgumentException e) {
					}
				}
			}
			changedCubes.size();
			double checkIfItemAppears = Math.random();
			if (checkIfItemAppears < 0.25) {
				Position itemPositionObject = new Position(changedCube);
				if (oldTerrainType == TerrainType.TREE) {
					Log newLog = new Log(changedCube);
					placeItem(newLog, itemPositionObject);
				}
				else if (oldTerrainType == TerrainType.ROCK) {
					Boulder newBoulder = new Boulder(changedCube);
					placeItem(newBoulder, itemPositionObject);
				}
			}
		}
	}
	
	/**
	 * 
	 * @return Returns the number of cubes along the x-coordinate
	 */
	public int getNbCubesX(){
		return X_LENGTH;
	}
	
	/**
	 * Variable registering the number of the cubes along the x-coordiate
	 */
	private final int X_LENGTH;
	
	/**
	 * 
	 * @return Returns the number of cubes along the y-coordinate
	 */
	public int getNbCubesY(){
		return Y_LENGTH;
	}
	
	/**
	 * Variable registering the number of cubes along the y-coordinate
	 */
	private final int Y_LENGTH;
	
	/**
	 * 
	 * @return Returns the number of cubes along the z-coordinate
	 */
	public int getNbCubesZ(){
		return Z_LENGTH;
	}
	
	/**
	 * Variable registering the number of cubes along the z-coordinate
	 */
	private final int Z_LENGTH;
	
	/**
	 * @param 	time
	 * 			The duration with which to advance time
	 * @effect	Advances the time with the given duration of every unit attached to this World
	 * @effect	Advances the time with the given duration of every item attached to this World
	 */
	public void advanceTime(double time) throws ModelException {
		for (Unit unit : unitsInWorld) {
			unit.advanceTime(time);
		}
		for (List<Item> value : itemsInWorld.values()) {
		    for (Item item : value) {
				item.advanceTime(time);
		    }
		}
		checkItemPositions += time;
		if (checkItemPositions > 3) {
			checkItemPositions = 0;
			Set<Item> setOfItems = getAllItems();
			for (Item item : setOfItems) {
			    int[] cubeOfItem = getOccupiedCube(item.getPosition());
				if (!isValidItemPosition(cubeOfItem)) {
					item.setIsFalling(true);
					if (!isValidItemPosition(getOccupiedCube(item.getnewPosition()))) {
						double[] newPosition = findAllowedPosition(item.getPosition(), false);
						item.setnewPosition(newPosition);
						Position newPositionObject = new Position(getOccupiedCube(newPosition));
						changeItemPosition(item, new Position(cubeOfItem), newPositionObject);
					}
			    }
			}
		}
	}
	
	/**
	 * Variable used to make sure that all items have correct positions every 3 seconds, if not make them fall
	 */
	static double checkItemPositions = 0;
	
	/**
	 * Gets all adjacent enemies of the given unit
	 * 
	 * @param 	unit
	 * 			The unit to be checked
	 * @return	List with all units that are adjacent enemies of the given unit.
	 */
	public List<Unit> getAdjacentEnemies(Unit unit) {
		List<Unit> adjacentEnemies = new ArrayList<Unit>();
		for (Unit enemy : unitsInWorld) {
			if (canAttackUnit(unit, enemy)) {
				adjacentEnemies.add(enemy);
			}
		}
		return adjacentEnemies;
	}
	
	/**
	 * Checks whether the given attacker can attack the given defender
	 * 
	 * @param 	attacker
	 * 			The attacker to be checked
	 * @param 	defender
	 * 			The defender to be checked
	 * @return	true if the attacker belongs to a different faction than
	 * 			the defender, and if the distance between them is small enough
	 * 			|result == (attacker.getFaction!=defender.getFaction &&
	 * 			|			 ( Math.abs(dx) <= 1 && Math.abs(dy) <= 1 && Math.abs(dz) <= 1))
	 */
	public boolean canAttackUnit(Unit attacker, Unit defender) {
		int[] defenderCube = defender.getOccupiedCube();
		int[] attackerCube = attacker.getOccupiedCube();
		if (attacker.getFaction() == defender.getFaction()) {
			return false;
		}
		double dx = attackerCube[0] - defenderCube[0];
		double dy = attackerCube[1] - defenderCube[1];
		double dz = attackerCube[2] - defenderCube[2];
		if ( Math.abs(dx) <= 1 && Math.abs(dy) <= 1 && Math.abs(dz) <= 1) {
			return true;
		}
		return false;	
	}
	/**
	 * Return the number of units of this world.
	 */
	@Basic @Raw
	public int getNbUnits() {
		return getAllUnits().size();
			
	}
	
//	public int getNbUnit2(){
//		int nbUnitsSoFar = 0;
//		for(Unit currentUnit : unitsInWorld){
//			if (canHaveAsUnit(currentUnit))
//				nbUnitsSoFar++;
//		}
//		return nbUnitsSoFar;
//	}
//	
	
	/**
	 * Checks whether the given unit can be attached to this World
	 * @param 	unit
	 * 			unit to be checked
	 * @return	true if the given unit isn't null and isn't terminated
	 * 			and this World can't be terminated and the maximum number 
	 * 			of units of this World can't be reached if the given unit doesn't
	 * 			already belong to this World
	 */
	public boolean canHaveAsUnit(Unit unit){
		return 
				((unit!=null)
				&& (!this.isTerminated())
				&& (!unit.isTerminated())
				&& (isValidNbUnits(getNbUnits()+1)||hasAsUnit(unit)));
	}
	
	
	/**
	 * Return the set of units of this world
	 */
	public Set<Unit> getAllUnits() {
		Set<Unit> copyOfUnitsInWorld = new HashSet<Unit>();
		for (Unit unit: unitsInWorld){
			copyOfUnitsInWorld.add(unit);
		}
		return copyOfUnitsInWorld;
	}
	
	/**
	 * 
	 * @param 	unit
	 * 			The unit that will be checked
	 * @return 	true if the unit is in the world
	 */
	public boolean hasAsUnit(Unit unit) {
		return this.unitsInWorld.contains(unit);
	}
	
	/**
	 * checks if this world has proper units assigned to it
	 * @return 	true if the world can have all its unit as units and if every unit has a faction and the unit
	 * 			has this world as world
	 */
	public boolean hasProperUnits(){
		for (Unit currentUnit : unitsInWorld){
			if (!canHaveAsUnit(currentUnit) || !currentUnit.hasFaction() || currentUnit.getWorld()!=this)
				return false;
		}
		return true;
	}
	
	/**
	 * Set collecting references to units attached to this world
	 * 
	 * @invar 	The set of units is active
	 * @invar 	Each unit in the set of units references a unit
	 * 			that is an acceptable unit for this World
	 * @invar	Each unit in the set of units has this World as its World
	 * 			attached to it
	 * 
	 */
	private final Set<Unit> unitsInWorld = new HashSet<Unit>();
	
	/**
	 * Check whether the given number of units is a valid number of units for
	 * any world.
	 *  
	 * @param  nbUnits
	 *         The number of units to check.
	 * @return 
	 *       | result == true if the nbUnits <= MAXNBUNITS
	*/
	public static boolean isValidNbUnits(int nbUnits) {
		return (nbUnits <= MAXNBUNITS);
	}
	
	/**
	 * maximum number of units allowed in this world
	 */
	private final static int MAXNBUNITS = 100;
	
	/**
	 * Creates a unit in this world
	 * 
	 * @effect	creates a new unit in this world
	 * @post	adds the created unit to this world
	 * @post	the created unit has this world as it's world
	 * @return	the created unit
	 * @throws 	ModelException
	 * 			if there can't be created another unit
	 */
	public Unit createUnit() throws ModelException {
			
		int strength = createRandom(Unit.getMinInitStrength(),Unit.getMaxInitStrength());
		int weight = createRandom(Unit.getMinInitWeight(),Unit.getMaxInitWeight());
		int agility = createRandom(Unit.getMinInitAgility(), Unit.getMaxInitAgility());
		int toughness = createRandom(Unit.getMinInitToughness(), Unit.getMaxInitToughness());
			
		double enableDefaultBehaviorChecker = Math.random();
		boolean enableDefaultBehavior = false;
			
		if (Util.fuzzyGreaterThanOrEqualTo(enableDefaultBehaviorChecker , 0.5));
			enableDefaultBehavior = true;
			
		double[] position = {createRandom(0,getNbCubesX()*LENGTHCUBE)+0.5,createRandom(0,getNbCubesY()*LENGTHCUBE)+0.5,createRandom(0,getNbCubesX()*LENGTHCUBE)+0.5};
			
			
		String unitName = "Unit" + Integer.toString(this.getNbUnits()+1);

		if (!isValidUnitPosition(position)){
			try {
				double[] allowedPosition = findAllowedPosition(position, true);
				Unit someUnit = new Unit(unitName, allowedPosition, weight, agility,strength,toughness,enableDefaultBehavior);
				addUnit(someUnit);
				return someUnit;
			}
			catch (IllegalArgumentException e) {
				return createUnit();
			}
		}
		Unit someUnit = new Unit(unitName, position, weight, agility,strength,toughness,enableDefaultBehavior);

		return someUnit;
	}
	
	public ArrayList<int[]> findPath(int[] startPosition, int[] goal) {
		return pathFinder.aStar(startPosition, goal, this);
	}
	
	/**
	 * 
	 * @param underLimit
	 * 			The underlimit of the random generator
	 * @param upperLimit
	 * 			The upperlimit of the random generator
	 * @return a random number between underlimit and upperlimit
	 */
	private int createRandom(int underLimit, int upperLimit){
		return (int)(Math.random()*(upperLimit-underLimit)+underLimit);
	}
	
	/**
	 * Adds a Unit to the world
	 * @param 	unit
	 * 			The Unit to be added to the world
	 * @effect	if the maximum number of factions isn't reached a new faction is created
	 * @post	This world has the given unit as one of its units.
	 * @post	The given unit references this World as the World to which 
	 * 			it is attached
	 * @post 	The given unit is added to a faction belonging to this world
	 * @throws 	ModelException
	 */
	public void addUnit(Unit unit) throws ModelException {

		if (!canHaveAsUnit(unit))
			throw new ModelException();
		try{		

			if (isValidNbFactions(this.getNbFactions()+1)){
					newFaction = new Faction(unit);
					addFaction(newFaction);
			}
			else {
				Faction smallestFaction = factionsInWorld.get(0);
				//@invar	smallestFaction is the smallest faction in factionsInWorld with index smaller than 
				//			i
				for (int i = 1; i < this.getAllFactionsInWorld().size(); i++){
					Faction currentFaction = this.getAllFactionsInWorld().get(i);
					smallestFaction = smallestFaction.getNbUnits()<currentFaction.getNbUnits() ? smallestFaction : currentFaction;
				}
				smallestFaction.addUnitToFaction(unit);
			}
			this.unitsInWorld.add(unit);
			unit.transferWorld(this);
		}	
		catch(IllegalArgumentException e){
			throw new ModelException(e.getMessage());
		}
				
	}
	
	/**
	 * Variable for creating a new Faction in the World
	 */
	private Faction newFaction;
	
	
	/**
	 * Removes the given unit from this World
	 * 
	 * @param 	unit
	 * 			The unit to be removed
	 * @pre		The given unit must belong to this world
	 * @post	If the given unit is removed from this World, then
	 * 			the unit is terminated and doesn't belong to this World
	 * 			anymore.
	 */
	public void removeUnit(Unit unit) throws ModelException{
		assert(this.hasAsUnit(unit));
		if (!unit.isTerminated())
			unitsInWorld.remove(unit);
			unit.getFaction().removeUnit(unit);
	}

	
	
	/**
	 * the length of a cube
	*/
	private final static int LENGTHCUBE = 1;
	
	
	/**
	 * 
	 * @param position
	 * @return true if the position is a valid position so if the item is allowed to stand on that position
	 */
	public boolean isValidItemPosition(int[] position){
		if (getTerrainType(position).isPassable() == false)
			return false;
		if (position[2] == 0 || getTerrainType(new int[]{position[0],position[1],position[2]-1}).isPassable() == false)
			return true;
		return false;
	}
	
	
	/**
	 * Check whether the given position is a valid position for any Unit.
	 * 
	 * @param position
	 *            The position to check.
	 * @return returns true if and only if each position coordinate is between 0 and their respective MAXPOSITION 
	 * 			| result == ((0+eps <= position[0] <= XMAXPOSITION - eps) &&
	 * 			|			(0+eps <= position[1] <= YMAXPOSITION - eps) &&
	 * 			|			(0+eps <= position[2] <= ZMAXPOSITION - eps))
	 */
	public boolean isValidUnitPosition(double[] position) {
		int[] cubePosition = getOccupiedCube(position);
		if (!isValidUnitCube(cubePosition))
			return false;
		if (Util.fuzzyGreaterThanOrEqualTo(position[0], 0) && Util.fuzzyLessThanOrEqualTo(position[0], getNbCubesX()) && !Double.isNaN(position[0])
			&& Util.fuzzyGreaterThanOrEqualTo(position[1], 0) && Util.fuzzyLessThanOrEqualTo(position[1], getNbCubesY()) && !Double.isNaN(position[1])
			&& Util.fuzzyGreaterThanOrEqualTo(position[2], 0) && Util.fuzzyLessThanOrEqualTo(position[2], getNbCubesZ()) && !Double.isNaN(position[2])) {

				return true;
			}

		return false;
	}
	
	/**
	 * Returns the cube of the position.
	 */
	public int[] getOccupiedCube(double[] position) {
		int[] cubePosition = { 0, 0, 0 };
		for (int i = 0; i < position.length; i++) {
			cubePosition[i] = (int) Math.floor(position[i]);
		}
		return cubePosition;
	}
	
	/**
	 * 
	 * @param cubePosition
	 * @return true if the position is a valid cubePosition so if the unit is allowed to stand on that cubePosition
	 */
	public boolean isValidUnitCube(int[] cubePosition){
		if (getTerrainType(cubePosition).isPassable() == false || getTerrainType(cubePosition) == TerrainType.BORDER)
			return false;
		if (cubePosition[2] == 0 || 
				getTerrainType(new int[]{cubePosition[0]+1,cubePosition[1],cubePosition[2]}).isPassable() == false || 
				getTerrainType(new int[]{cubePosition[0]-1,cubePosition[1],cubePosition[2]}).isPassable() == false || 
				getTerrainType(new int[]{cubePosition[0],cubePosition[1]+1,cubePosition[2]}).isPassable() == false || 
				getTerrainType(new int[]{cubePosition[0],cubePosition[1]-1,cubePosition[2]}).isPassable() == false || 
				getTerrainType(new int[]{cubePosition[0],cubePosition[1],cubePosition[2]+1}).isPassable() == false || 
				getTerrainType(new int[]{cubePosition[0],cubePosition[1],cubePosition[2]-1}).isPassable() == false)
			return true;
		return false;
	}
	
	/**
	 * 
	 * @return A random position that is a valid position of a unit.
	 */
	public int[] selectRandomPosition() {
		int[] target = {0,0,0};
		target[0] = (int) Math.floor(Math.random() * getNbCubesX());
		target[1] = (int) Math.floor(Math.random() * getNbCubesY());
		target[2] = (int) Math.floor(Math.random() * getNbCubesZ());
		double[] truePosition = {0.5,0.5,0.5};
		for (int i =0;i<target.length;i++) {
			truePosition[i] += (double) target[i];
		}
		if (isValidUnitPosition(truePosition))
			return target;
		return selectRandomPosition();
	}

	
	/**
	 * 
	 * @param position
	 * @return the new position where the unit or item is allowed to stand, with the distance to position as short as possible
	 */
	public double[] findAllowedPosition(double[] position, boolean canMoveUp) throws IllegalArgumentException {
		int[] cubePosition = getOccupiedCube(position);
		int [] positionCopy = cubePosition.clone();
		positionCopy[2] += 1;
		while (positionCopy[2] > -1) {
			positionCopy[2] -= 1;
			if (getTerrainType(new int[]{positionCopy[0],positionCopy[1],positionCopy[2]+1}).isPassable() == true && (getTerrainType(positionCopy).isPassable() == false || positionCopy[2] == -1)) {
				return new double[]{position[0],position[1],positionCopy[2]+1.5};
			}
		}
		if (canMoveUp) {
			positionCopy[2] = cubePosition[2];
			while (positionCopy[2] <= getNbCubesZ()) {
				if (getTerrainType(new int[]{positionCopy[0],positionCopy[1],positionCopy[2]+1}).isPassable() == true && (getTerrainType(positionCopy).isPassable() == false))
					return new double[]{position[0],position[1],positionCopy[2]+1.5};
				positionCopy[2] += 1;
			}
		}
		throw new IllegalArgumentException();
	}
	
	/**
	 * 
	 * @param 	x
	 * 			The x-coordinate of the cube
	 * @param 	y
	 * 			The y-coordinate of the cube
	 * @param 	z
	 * 			The z-coordinate of the cube
	 * @return	True if the cube is connected to the border.
	 */
	public boolean isSolidConnectedToBorder(int x, int y, int z) {
		return borderConnectionAlgorithm.isSolidConnectedToBorder(x,y,z);
	}
	
	/**
	 * Return the number of factions of this world.
	 */
	@Basic @Raw
	public int getNbFactions() {
		return factionsInWorld.size();
	}
	
//	public int getNbFactions2() {
//		int nbFactionsSoFar = 0;
//		
//		for (Faction currentFaction : factionsInWorld){
//			if (canHaveAsFaction(currentFaction))
//				nbFactionsSoFar++;
//		}
//		return nbFactionsSoFar;
//	}
	
	/**
	 * Checks whether this World can have the given facion as its faction
	 * @param 	faction
	 * 			The faction to be checked
	 * @return	True if the given faction isn't null and isn't terminated
	 * 			and the world isn't terminated and if the faction doesn't already belong to the 
	 * 			world the maximum numbers of factions isn't reached
	 */
	public boolean canHaveAsFaction(Faction faction){
		return
				((faction!=null)
				&&(!this.isTerminated())
				&&(!faction.isTerminated())
				&&(isValidNbFactions(getNbFactions()+1)||hasAsFaction(faction))
				&& (faction.getNbUnits()>0));
	}
	
	/**
	 * 
	 * @param 	index
	 * 			The index of the faction to be returned
	 * @return	The faction at the given index
	 * @throws 	IllegalArgumentException
	 * 			If the given index is a invalid index
	 */
	public Faction getFactionAt(int index) throws ModelException{
		if ((index < 0) || (index >= getNbFactions()))
				throw new ModelException("illegal index");
		return factionsInWorld.get(index);
	}
	
	/**
	 * Checks whether the World can have the given faction at the given index
	 * @param 	faction
	 * 			The faction to be checked
	 * @param 	index
	 * 			the index where the faction has to be checked
	 * @return	true if the world can have the given faction as it's faction
	 * 			and the given index is a valid index and the given faction isn't 
	 * 			already in the faction
	 */
	public boolean canHaveAsFactionAt(Faction faction, int index) throws ModelException{
		if (!canHaveAsFaction(faction))
			return false;
		if ((index < 0) || (index > getNbFactions()))
			return false;
		for (int i=0; i<getNbFactions(); i++)
			if ((i!=index) && (getFactionAt(i) == faction))
				return false;
		
		return true;
	}
	
	/**
	 * Checks whether the world has the given faction attached to it
	 * @param 	faction
	 * 			The faction to be checked
	 * @return	true if the World contains this faction
	 */
	public boolean hasAsFaction(Faction faction){
		return this.factionsInWorld.contains(faction);
	}
	
	/**
	 * Checks whether the World has the given faction at the given index
	 * 
	 * @param 	faction
	 * 			The faction to be checked
	 * @param 	index
	 * 			The place where the faction needs to be checked
	 * @return	true if the world has the given faction at the given index
	 */
	public boolean hasAsFactionAt(Faction faction, int index) throws ModelException{
		
		if (getFactionAt(index)==faction)
			return true;
		
		return false;
					
	}
	
	/**
	 * Check whether the World has proper factions attached to it
	 * 
	 * @return true if the world can have all of its faction as factions.
	 */
	public boolean hasProperFactions() throws ModelException{
		for (int i=0; i<getNbFactions(); i++){
			if (!canHaveAsFactionAt(getFactionAt(i),i))
				return false;
		}
		return true;
	}
	
	/**
	 * Check whether the given number of factions is a valid number of factions for
	 * any world.
	 *  
	 * @param  nbFactions
	 *         The number of units to check.
	 * @return 
	 *       | result == true if the number of factions is bigger or equal than 0 and less or equal than the maximum
	 *       | number of factions allowed in the world
	*/
	public static boolean isValidNbFactions(int nbFactions) {
		return (nbFactions <= MAXNBFACTIONS);
	}
	
	/**
	 * Variable registering the maximum number of factions.
	 */
	private final static int MAXNBFACTIONS = 5;
	
	/**
	 * @return A list of all the factions in the world
	 */
	@Basic @Raw
	public List<Faction> getAllFactionsInWorld(){
		List<Faction> copyFactionsInWorld = new ArrayList<Faction>();
		for (Faction currentFaction: factionsInWorld){
			copyFactionsInWorld.add(currentFaction);
		}
		return copyFactionsInWorld;
	}
	
	/**
	 * @return A set of all the active factions in the world
	 */
	public Set<Faction> getAllActiveFactions() {
		Set<Faction> activeFactions = new HashSet<Faction>();
		for (Faction currentFaction : getAllFactionsInWorld()){
			if (!currentFaction.getUnitsInFaction().isEmpty())
				activeFactions.add(currentFaction);
		}
		return activeFactions;
	}
	
	/**
	 * A list registering all the factions in this World
	 * 
	 * @invar	The list of factions is effective
	 * @invar	the list can't be bigger than the maximum numbers
	 * 			of factions allowed in this world
	 * @invar	each faction in the list of factions must be an acceptable
	 * 			faction for this World
	 */
	private final List<Faction> factionsInWorld = new ArrayList<Faction>();
	
	
	
	/**
	 * Adds the given faction to the world
	 * 
	 * @param 	faction
	 * 			The faction to be added
	 * @post	This World has the given faction attached to it
	 * 
	 * @throws 	IllegalArgumentException
	 * 			If the world can't have the given faction as its faction or
	 * 			if the faction is already attached to this world
	 */
	private void addFaction(Faction faction) throws ModelException{
		if (!canHaveAsFaction(faction))
			throw new ModelException("can't add the faction");
		if (hasAsFaction(faction))
			throw new ModelException("faction is already in the world");
		factionsInWorld.add(faction);	
		
	}
	
	/**
	 * Adds the given faction at the given index to this World
	 * 
	 * @param 	faction
	 * 			The faction to be added
	 * @param 	index
	 * 			The index where the faction needs to be added
	 * @post	This world has the given faction attached to it at the given faction
	 * @throws 	IllegalArgumentException
	 * 			If this World can't have the given faction at the given index or
	 * 			if this faction is already attached to this world
	 */
	public void addFactionAt(Faction faction, int index) throws ModelException{
		Faction factionToSet = faction;
		
		if (!canHaveAsFactionAt(faction, index))
			throw new ModelException("can't add the faction at the given index");
		if (!hasAsFaction(faction))
			throw new ModelException("Faction is already active in this world");
		
		for (int i=index; i <= getNbFactions()-1; i++){
			Faction oldFaction = getFactionAt(i);
			factionsInWorld.set(i, factionToSet);
			if (i == getNbFactions()-1)
				addFaction(oldFaction);
			
			factionToSet = oldFaction;	
				
		}
			
	}
	
	/**
	 * Removes the faction at the given index
	 * 
	 * @param 	index
	 * 			The index where a faction has to be removed
	 * @post	The faction at the given index is no longer attached to this world
	 * 
	 * @throws 	IllegalArgumentException
	 * 			if the faction isn't terminated, or the index isn't a valid index
	 * 			for this world.
	 */
	public void removeFactionAt(int index) throws ModelException{
		
		try{
			if (!getFactionAt(index).isTerminated())
				throw new ModelException("faction isn't terminated yet");
			factionsInWorld.remove(index);
		}
		catch(IndexOutOfBoundsException | ModelException e){
			throw new ModelException("can't remove the faction at the given index");
		}
	}
	
	/**
	 * Removes the given faction
	 * @param 	faction
	 * 			The faction to be removed
	 * @post	The given faction is no longer attached to this world
	 * 
	 * @throws 	IllegalArgumentException
	 * 			if the unit isn't terminated yet
	 */
	public void removeFaction(Faction faction) throws ModelException{
		try{
		for (int i=0; i<getNbFactions(); i++)
			if (getFactionAt(i) == faction)
				removeFactionAt(i);
		}
		catch(ModelException e){
			throw new ModelException("can't remove the faction");
		}
			
	}
	
	/**
	 * Checks whether this world can have the given item as its item
	 * 
	 * @param 	item
	 * 			The item to be checked
	 * @return	True if item isn't null and isn't terminated and this world isn't terminated.
	 */
	public boolean canHaveAsItem(Item item){
		return ((item!=null)
				&& (!this.isTerminated())
				&& (!item.isTerminated()));
	}
	
	/**
	 * @return	The number of items in this world
	 */
	public int getNbItems(){
		int itemsSoFar = 0;
		for (List<Item> value : itemsInWorld.values()) {
		    for (Item currentItem : value) {
				if (canHaveAsItem(currentItem))
					itemsSoFar++;
		    }
		}
		return itemsSoFar;
	}
	
	/**
	 * Checks whether this world has the given item attached to it
	 * 
	 * @param 	item
	 * 			Item to be checked
	 * @return	true if this world contains the given item
	 */
	public boolean HasAsItem(Item item){
		Set<Item> setOfItems = getAllItems();
		return setOfItems.contains(item);
	}
	
	/**
	 * @return	A set of all the items attached to this world.
	 */
	public Set<Item> getAllItems(){
		Set<Item> copyOfItemsInWorld = new HashSet<Item>();
		for (List<Item> value : itemsInWorld.values()) {
		    for (Item currentItem : value) {
				//if (canHaveAsItem(currentItem))
					copyOfItemsInWorld.add(currentItem);
		    }
		}
		return copyOfItemsInWorld;
	}
	
	/**
	 * Adds the given item to the given position
	 * 
	 * @param 	item
	 * 			The item to be added
	 * @param 	position
	 * 			The position to be added
	 * @post	The given item is attached to this World
	 * @throws 	IllegalArgumentException
	 * 			if this world can't have the given item as its item
	 * 			
	 */
	public void addItem(Item item, Position position) throws ModelException{
		if (!canHaveAsItem(item))
			throw new ModelException("can't add the item");
		List<Item> listOfItems;
		if (itemsInWorld.containsKey(position))
			listOfItems = itemsInWorld.get(position);
		else 
			listOfItems = new ArrayList<Item>();
		listOfItems.add(item);
		itemsInWorld.put(position, listOfItems);
	}
	
	/**
	 * Places the given item at the given position
	 * 
	 * @param 	item
	 * 			the item to be placed
	 * @param 	position
	 * 			the position where the item has to be placed
	 * @throws 	ModelException
	 * 			if the given item can't be placed at the given position
	 */
	public void placeItem(Item item, Position position) throws ModelException{

		item.setPosition(new double[]{position.getX()+0.5,position.getY()+0.5,position.getZ()+0.5});
		addItem(item,position);	
		if (!isValidItemPosition(new int[]{position.getX(),position.getY(),position.getZ()})) {
			item.setIsFalling(true);
			double[] newPosition = findAllowedPosition(item.getPosition(), false);
			item.setnewPosition(newPosition);
			Position newPositionObject = new Position(getOccupiedCube(newPosition));
			changeItemPosition(item, position, newPositionObject);
		}
	}
	
	/**
	 * Removes the given item from the world
	 * @param 	item
	 * 			Item to be removed
	 * @param 	position
	 * 			The position of the item
	 * @post	The given item is no longer attached to this world
	 * 
	 */
	public void removeItem(Item item, Position position){
		List<Item> listOfItems = itemsInWorld.get(position);
		//item.terminate();
		listOfItems.remove(item);
	}
	
	/**
	 * Checks whether this world has proper items attached to it
	 * 
	 * @return 	true if each item attached to this world is an acceptable
	 * 			item in this world.
	 */
	public boolean hasProperItems(){
		for (List<Item> value : itemsInWorld.values()) {
		    for (Item currentItem : value) {
				if (!canHaveAsItem(currentItem))
					return false;
		    }
		}
		return true;
	}
	
	/**
	 * Returns a list of all the items at the given position
	 * 
	 * @param 	position
	 * 			the position to be checked
	 * @return	a list of all the items at the given position
	 */
	public List<Item> getItemsAtPos(int[] position) {
		Position positionObject = new Position(position);
		Map<Position,List<Item>> itemsInWorldCopy = getMapOfItems();
		if (itemsInWorldCopy.containsKey(positionObject))
			return itemsInWorldCopy.get(positionObject);
		return null;
	}
	
	public Map<Position,List<Item>> getMapOfItems(){
		 Map<Position,List<Item>> itemsInWorldCopy = new HashMap<Position,List<Item>>();
		 for (Position key : itemsInWorld.keySet()){
			 itemsInWorldCopy.put(key, itemsInWorld.get(key));
		 }
		 return itemsInWorldCopy;
	}
	
	/**
	 * Changes the position of the given item from the given oldPosition to the given newPosition
	 * @param 	item
	 * 			Item that changes position
	 * @param 	oldPosition
	 * 			Old position of the unit
	 * @param 	newPosition
	 * 			New position of the unit
	 * @pre		The given oldPosition is the old position of the unit
	 * @pre		The given newPosition is a valid item position
	 * @post	The position of the given item is changed from oldPosition to newPosition
	 */
	private void changeItemPosition(Item item, Position oldPosition, Position newPosition) throws ModelException {
		//assert (isValidItemPosition(newPosition));
		itemsInWorld.get(oldPosition).remove(item);
		addItem(item,newPosition);
	}
	
	/**
	 * A map registering all the items of this world as a list as values at
	 * the given position as key
	 * 
	 * @invar	The map of position as keys and list of items as values is effective
	 * @invar	Each item in the map is an acceptable item attached to this world
	 * @invar	Each position must be a valid position in this world
	 * @invar	Each position must belong to the position of each item in the list
	 * 			items with its appointing value
	 */
	private final Map<Position,List<Item>> itemsInWorld = new HashMap<Position,List<Item>>();
	
	/**
	 * @return True if the world is terminated
	 */
	public boolean isTerminated(){
		
		return isTerminated;
	}
	
	
	/**
	 * Terminate this world
	 * 
	 * @post 	This world is terminated
	 * @post	All units attached to this world are 
	 * 			removed from this world and are terminated
	 * @post	All factions attached to this world are 
	 * 			removed from this world and are terminated
	 * @post	All items attached to this world are removed
	 * 			from this world
	 */
	public void terminate() throws ModelException{
		if (!isTerminated())
			for (Unit currentUnit : getAllUnits()){
				removeUnit(currentUnit);
			}
			for (Faction currentFaction : getAllFactionsInWorld()){
				removeFaction(currentFaction);
			}
			for (Position currentPosition : itemsInWorld.keySet()){
				List<Item> itemList = itemsInWorld.get(currentPosition);
				for (int i=0; i<itemList.size();i++){
					Item currentItem = itemList.get(i);
					removeItem(currentItem,currentPosition);
				}
					
			}
			this.isTerminated = true;
	}
	
	/**
	 * Variable registering whether or not this 
	 * world is terminated
	 */
	private boolean isTerminated = false;

}
