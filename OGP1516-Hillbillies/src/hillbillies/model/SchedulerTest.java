package hillbillies.model;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import hillbillies.model.expression.*;
import hillbillies.model.statement.*;
import ogp.framework.util.ModelException;

//NOTE waarom IllegalArgumentException bij remove
//NOTE waarom krijgen we illegalArgumentExceptions ipv modelExceptions

public class SchedulerTest {
	
	private double[] unitPosition = {1,1,1};
	private Unit unit1;
	private Faction faction;
	private Scheduler scheduler;
	private int[] position = {0,0,0};
	private PositionE positionExpression;
	private S statement;
	private Task task1;

	@Before
	public void setUp() throws Exception {
		unit1 = new Unit("Unit",unitPosition, 25,50,70,100,false);
		faction = new Faction(unit1);
		scheduler = faction.getScheduler();
		PositionE positionExpression = new Position(position);
		S statement = new MoveTo(positionExpression);
		task1 = new Task("Task",10,statement,position);
	}

	@Test
	public void ExtendedConstructor() throws Exception{
		Unit myUnit = new Unit("Unit",unitPosition, 25,50,70,100,false);
		Faction myFaction = new Faction(myUnit);
		Scheduler myScheduler = myFaction.getScheduler();
		assertEquals(myScheduler.getFaction(), myFaction);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void ExtendedConstructor_FalseCase() throws Exception{
		Scheduler myScheduler = new Scheduler(null);
	}
	
	@Test
	public void canHaveAsTask_LegalCase() throws Exception{
		assertTrue(scheduler.canHaveAsTask(task1));
	}
	
	@Test
	public void canHaveAsTask_NullCase() throws Exception{
		assertFalse(scheduler.canHaveAsTask(null));
	}
	
	@Test
	public void canHaveAsTask_TaskTerminated() throws Exception{
		task1.terminate();
		assertFalse(scheduler.canHaveAsTask(task1));
	}
	
	@Test
	public void TaskInScheduler_legalCase() throws Exception{
		scheduler.addTask(task1);
		assertTrue(scheduler.TaskInScheduler(task1));
	}
	
	@Test
	public void TaskInScheduler_FalseCase() throws Exception{
		assertFalse(scheduler.TaskInScheduler(task1));
	}
	
	@Test
	public void addTask() throws Exception{
		Unit myUnit = new Unit("Unit",unitPosition, 25,50,70,100,false);
		Faction myFaction = new Faction(myUnit);
		Scheduler myScheduler = myFaction.getScheduler();
		myScheduler.addTask(task1);
		assertTrue(myScheduler.TaskInScheduler(task1));
		assertEquals(myScheduler.getNbTasks(),1);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void addTask_NullCase() throws Exception{
		Unit myUnit = new Unit("Unit",unitPosition, 25,50,70,100,false);
		Faction myFaction = new Faction(myUnit);
		Scheduler myScheduler = myFaction.getScheduler();
		myScheduler.addTask(null);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void addTask_TaskTerminated() throws Exception{
		task1.terminate();
		scheduler.addTask(task1);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void addTask_SchedulerTerminated() throws Exception{
		scheduler.terminate();
		scheduler.addTask(task1);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void addTask_Overflow() throws Exception{
		PositionE positionExpression = new Position(position);
		S statement = new MoveTo(positionExpression);
		for (int i=0; i<51; i++){
			Task myTask = new Task("Task",i,statement,position);
			scheduler.addTask(myTask);
			System.out.println(i);
		}
		
	}
	
	@Test
	public void removeTask() throws Exception{
		scheduler.addTask(task1);
		scheduler.removeTask(task1);
		assertFalse(scheduler.TaskInScheduler(task1));
	}
	
	//NOTE : why illegalArgumentException at removeTask?
	@Test
	public void removeTask_TaskNotInScheduler() throws Exception{
		scheduler.removeTask(task1);
		assertFalse(scheduler.TaskInScheduler(task1));
	}
	
	@Test
	public void replaceTask() throws Exception{
		PositionE positionExpression = new Position(position);
		S statement = new MoveTo(positionExpression);
		Task task2 = new Task("Task",5,statement,position);
		scheduler.addTask(task1);
		assertTrue(scheduler.TaskInScheduler(task1));
		assertFalse(scheduler.TaskInScheduler(task2));
		scheduler.replaceTask(task1, task2);
		assertTrue(scheduler.TaskInScheduler(task2));
		assertFalse(scheduler.TaskInScheduler(task1));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void replaceTask_NullCase() throws Exception{
		scheduler.addTask(task1);
		scheduler.replaceTask(task1, null);		
	}
	
	@Test
	public void areTasksPartOf() throws Exception{
		PositionE positionExpression = new Position(position);
		S statement = new MoveTo(positionExpression);
		Task task2 = new Task("Task",5,statement,position);
		Task task3 = new Task("Task",11,statement,position);
		scheduler.addTask(task1);
		scheduler.addTask(task2);
		scheduler.addTask(task3);
		Set<Task> tasks = new HashSet<Task>();
		tasks.add(task1);
		tasks.add(task2);
		tasks.add(task3);
		assertTrue(scheduler.areTasksPartOf(tasks));
	}
	
	@Test
	public void hasProperTasks_LegalCase() throws Exception{
		PositionE positionExpression = new Position(position);
		S statement = new MoveTo(positionExpression);
		Task task2 = new Task("Task",5,statement,position);
		Task task3 = new Task("Task",11,statement,position);
		scheduler.addTask(task1);
		scheduler.addTask(task2);
		scheduler.addTask(task3);
		assertTrue(scheduler.hasProperTasks());
	}
	
	@Test
	public void hasProperTasks_FalseCase() throws Exception{
		//can't happen
	}
	
	@Test
	public void areTasksPartOf_falseCase() throws Exception{
		Set<Task> tasks = new HashSet<Task>();
		tasks.add(task1);
		scheduler.areTasksPartOf(tasks);
	}
	
	@Test
	public void removeAllTasks() throws Exception{
		PositionE positionExpression = new Position(position);
		S statement = new MoveTo(positionExpression);
		Task task2 = new Task("Task",5,statement,position);
		Task task3 = new Task("Task",11,statement,position);
		scheduler.addTask(task1);
		scheduler.addTask(task2);
		scheduler.addTask(task3);
		scheduler.removeAllTasks();
		assertFalse(scheduler.TaskInScheduler(task1));
		assertFalse(scheduler.TaskInScheduler(task2));
		assertFalse(scheduler.TaskInScheduler(task3));
	}
	
	@Test
	public void getHighestPriorityTask() throws Exception{
		PositionE positionExpression = new Position(position);
		S statement = new MoveTo(positionExpression);
		Task task2 = new Task("Task",15,statement,position);
		Task task3 = new Task("Task",11,statement,position);
		scheduler.addTask(task1);
		scheduler.addTask(task2);
		scheduler.addTask(task3);
		assertEquals(scheduler.getHighestPriorityTask(),task2);
	}
	
	@Test
	public void getHighestPriorityTask_EmptyScheduler() throws Exception{
		assertEquals(scheduler.getHighestPriorityTask(),null);
	}
	
	@Test
	public void canHaveAsFaction_LegalCase() throws Exception{
		assertTrue(scheduler.canHaveAsFaction(faction));
	}
	
	@Test
	public void canHaveAsFaction_SchedulerTerminatedLegal() throws Exception{
		scheduler.terminate();
		assertTrue(scheduler.canHaveAsFaction(null));
	}
	
	@Test
	public void canHaveAsFaction_SchedulerTerminatedFalse() throws Exception{
		scheduler.terminate();
		assertFalse(scheduler.canHaveAsFaction(faction));
	}
	
	@Test
	public void canHaveAsFaction_NullCase() throws Exception{
		assertFalse(scheduler.canHaveAsTask(null));
	}
	
	
	@Test
	public void getFaction() throws Exception{
		assertEquals(scheduler.getFaction(), faction);
	}
	
	@Test
	public void hasProperFaction() throws Exception{
		assertTrue(scheduler.hasProperFaction());
	}
	
	@Test
	public void receiveFaction() throws Exception{
		//can only be tested in constructor, so we did
	}
	
	@Test
	public void terminate() throws Exception{
		scheduler.addTask(task1);
		scheduler.terminate();
		assertEquals(scheduler.getNbTasks(),0);
		assertEquals(scheduler.getFaction(), null);
		assertTrue(scheduler.isTerminated());
	}
	
	@Test
	public void isTerminated_legalCase() throws Exception{
		scheduler.terminate();
		assertTrue(scheduler.isTerminated());
	}
	
	@Test
	public void isTerminated_falseCase() throws Exception{
		assertFalse(scheduler.isTerminated());
	}
}
