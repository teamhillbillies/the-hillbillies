package hillbillies.model.pathfinding;

import java.util.ArrayList;

/**
 * A class of nodes, to be used by the A star algorithm
 *
 */
public class Node {

	public Node(Node parentNode, int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.parentNode = parentNode;
	}
	
	ArrayList<Node> neightbors = new ArrayList<Node>();
	Node parentNode;
	double f;
	double g;
	double h;
	int x;
	int y;
	int z;
	

}
