package hillbillies.model.pathfinding;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import hillbillies.model.World;

public class AStarAlgorithm {

	/**
	 * Finds a path between the startPosition and the goal using the A star algorithm. Nodes are currently 
	 * generated on-the-fly. If a path is found an ArrayList of adjacent positions is returned, which describes 
	 * the path between startPosition and goal. If no path could be found an IllegalArgumentException is thrown
	 * 
	 * @param startPosition
	 * 			The current position of the unit
	 * @param goal
	 *          The requested end position of the unit
	 * @param world
	 *          The world in which the unit moves
	 * @return  An arrayList of adjacent positions which starts at the startPosition and ends at the goal
	 * @throws  IllegalArgumentException 
	 *          no path could be found
	 * 
	 */
	public ArrayList<int[]> aStar(int[] startPosition, int[] goal, World world) {
		Position beginPosition = new Position(startPosition[0],startPosition[1],startPosition[2]);
		
		this.world = world;
		Node startNode = new Node(null, startPosition[0],startPosition[1],startPosition[2]);
		Node goalNode = new Node(null, goal[0], goal[1], goal[2]);
		startNode.g = 0;
		startNode.h = distance(startNode,goalNode);
		startNode.f = startNode.h;
		open.put(beginPosition, startNode);
		
		while(true) {
			Node current = null;
			
			if (open.size() == 0) {
				throw new IllegalArgumentException("there is no route");
			}
			Iterator<Entry<Position, Node>> it = open.entrySet().iterator();
			
			while(it.hasNext()) {
				Map.Entry<Position, Node> pair = (Map.Entry<Position, Node>) it.next();
				if (current == null || pair.getValue().f < current.f) {

					
					current = pair.getValue();
				}
			}
			if (current.x==goalNode.x && current.y==goalNode.y && current.z==goalNode.z) {
				goalNode = current;
				break;
			}
			Position currentPosition = new Position(current.x,current.y,current.z);

			open.remove(currentPosition);
			closed.put(currentPosition,current);
			
			current.neightbors = getSuccessors(current);
			for (Node neightbor : current.neightbors) {
				double nextG = current.g + distance(current, neightbor);

				if (neightbor == null) {
					continue;
				}
				Position neightborPosition = new Position(neightbor.x,neightbor.y,neightbor.z);
				if (nextG < neightbor.g) {
					open.remove(neightborPosition);
					closed.remove(neightborPosition);
				}
				if (!open.containsKey(neightborPosition) && !closed.containsKey(neightborPosition)) {
					neightbor.g = nextG;
					neightbor.h = (int) distance(neightbor, goalNode);
					neightbor.f = neightbor.g + neightbor.h;
					neightbor.parentNode = current;
					open.put(neightborPosition, neightbor);
				}
				
			}
			
			
		}
		ArrayList<int[]> nodes = new ArrayList<int[]>();
		Node current = goalNode;
		while (current.parentNode != null) {
			int[] currentPosition = {current.x,current.y,current.z};
			nodes.add(currentPosition);
			current = current.parentNode;
		}
		open = new HashMap<Position,Node>();
		closed = new HashMap<Position,Node>();
		return nodes;
		
	}
	
	/**
	 * A map of all positions currently considered for evaluation
	 */
	private Map<Position,Node> open = new HashMap<Position,Node>();
	
	/**
	 * A map of all positions that have been evaluated
	 */
	private Map<Position,Node> closed = new HashMap<Position,Node>();
	World world;
	
	/**
	 * 
	 * @return the euclidean distance between node1 and node2
	 */
	public double distance(Node node1, Node node2) {
		double xd = node1.x - node2.x;
		double yd = node1.y - node2.y;
		double zd = node1.z - node2.z;
		return Math.sqrt(xd*xd+yd*yd+zd*zd);
	}
	
	/**
	 * 
	 * @param node
	 *        The node for which to find the successors
	 * @return The successors to a certain node, which are the adjacent passable nodes
	 */
	public ArrayList<Node> getSuccessors(Node node) {

		ArrayList<Node> successors = new ArrayList<Node>();
		Position parentPosition = (node.parentNode != null) ? new Position(node.parentNode.x,node.parentNode.y,node.parentNode.z): null;
		for (int xd = -1; xd <= 1; xd++) {
			for (int yd = -1; yd <= 1; yd++) {
				for (int zd = -1; zd <= 1; zd++) {
					Position position = new Position(node.x+xd,node.y+yd,node.z+zd);
					double[] fakePosition = {position.getX(),position.getY(),position.getZ()};
					if ((xd==0 && yd==0 && zd==0) || position.equals(parentPosition)) {
						continue;
					}
					if (world.isValidUnitPosition(fakePosition)) {
						if (open.containsKey(position)) {
							successors.add(open.get(position));
						}
						else if (closed.containsKey(position)) {
							successors.add(closed.get(position));
						}
						else {
							Node currentNode = new Node(node,position.getX(),position.getY(),position.getZ());
							successors.add(currentNode);
						}
					}
				}
			}
		}
		return successors;
	}
}
