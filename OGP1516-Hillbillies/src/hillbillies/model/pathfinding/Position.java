package hillbillies.model.pathfinding;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Value;

/**
 * A class of positions for which two positions with the same coordinates in the 
 * world are equal to each other. This makes it possible to use coordinates as key in a data structure
 * @author Maarten
 *
 *
 */
@Value
public class Position {



    /**
     * Initializes this new Position with the given coordinates
     * @param x
     *        The x coordinate for this new Position
     * @param y
     *        The y coordinate for this new Position     
     * @param z
     *        The z coordinate for this new Position
     */
    public Position(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     *  Initializes this new Position with the given coordinates
     * @param position
     *        The coordinates, given as an array
     */
    public Position(int[] position) {
    	this.x = position[0];
    	this.y = position[1];
    	this.z = position[2];
    }
    
    @Basic
    /**
     * @return The X coordinate of this position
     */
    public int getX() {
    	return x;
    }
    
    @Basic
    /**
     * @return The Y coordinate of this position
     */
    public int getY() {
    	return y;
    }    
    
    @Basic
    /**
     * @return The Z coordinate of this position
     */
    public int getZ() {
    	return z;
    }
    
	/**
	 * The coordinates of this position
	 */
    private final int x,y,z;
    

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		result = prime * result + z;
		return result;
	}

    /**
     * Two positions are equal if and only if they share the same coordinates
     * @return true if the two Positions have the same value, false otherwise
     */
    @Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Position other = (Position) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		if (z != other.z)
			return false;
		return true;
	}
	/**
	 * Returns a textual representation of the Position
	 * 
	 * @return "A position with as coordinates (x,y,z)" with x,y,z the corresponding variables of this class
	 */
    @Override
    public String toString() {
    	return "A position with as coordinates (" + getX() + "," + getY() + "," + getZ() + ")";
    }
}
