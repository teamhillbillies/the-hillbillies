package hillbillies.model;

import java.util.*;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;
import ogp.framework.util.ModelException;

/**
 * 
 * @author Maarten Pieck
 * @author Thomas Vandendijk
 * @version 1.0
 * @invar  	The nbUnits of each Faction must be a valid nbUnits for any
 *         	Faction.
 *       	| isValidNbUnits(getNbUnits())
 * @invar	The units associated with each Faction must be proper units
 * 			for that Faction
 * 			|hasProperUnits()
 */
public class Faction {
	
	/**
	 * Initialize this Faction with 1 unit attached to it.
	 * 
	 * @param 	unit
	 * 			The unit attached to this Faction
	 * @post	1 Unit is already attached to this Faction.
	 * 			|new.getNbUnits() == 1
	 * @throws 	IllegalArgumentException
	 * 			The given unit is not a valid unit for this faction.
	 */
	public Faction(Unit unit) throws ModelException {
			
		addUnitToFaction(unit);
		Scheduler scheduler = new Scheduler(this);
	}
	/**
	 * Return the set of all units associated with this Faction.
	 * 
	 * @invar	The set of units is effective
	 * 			|unitsInWorld != null
	 * @invar	Each unit in this set of units are valid units for this
	 * 			Faction.
	 * 			|for each unit in unitsInWorld:
	 * 			|	canHaveAsUnit(unit)
	 * @invar	Each unit in this set has his Faction set to this Faction.
	 * 			|for each unit in unitsInWorld:
	 * 			|	(units.getFaction == this)
	 */
	private final Set<Unit> unitsInFaction = new HashSet<Unit>();


	/**
	 * Return the number of units of this Faction.
	 */
	@Basic @Raw
	public int getNbUnits() {
		//maybe iterate over the collection and if the unit isnt dead, count += 1
		//or destroy all bonds when an unit dies so that no dead units are in this set
		return unitsInFaction.size();
	}
	
//	public int getNbUnits2(){
//		int nbUnitsSoFar = 0;
//		for (Unit currentUnit : unitsInFaction){
//			if (canHaveAsUnit(currentUnit))
//				nbUnitsSoFar++ ;
//		}
//		return nbUnitsSoFar;
//	}
	
	/**
	 * 
	 * @return a copy of the set of units
	 */
	public Set<Unit> getUnitsInFaction(){
		Set<Unit> copyOfUnitsInFaction = new HashSet<Unit>();
		for (Unit currentUnit : unitsInFaction){
			copyOfUnitsInFaction.add(currentUnit);
		}
		return copyOfUnitsInFaction;
	}
	
	/**
	 * Check whether this faction has proper units attached to it.
	 * 
	 * @return 	True if this faction can have each unit as its unit and
	 * 			if each unit of this faction has this faction as its faction.
	 * 			|result == 
	 * 			|	for each unit in unitsInWorld:
	 * 			|		if (!canHaveAsUnit(currentUnit)||(currentUnit.getFaction()!=this))
	 * 			|			return false
	 * 			|	return true
	 */
	public boolean hasProperUnits(){
		for (Unit currentUnit : unitsInFaction){
			if (!canHaveAsUnit(currentUnit)||(currentUnit.getFaction()!=this))
				return false;
		}
		return true;
	}
	
	/**
	 * Check whether the given nbUnits is a valid nbUnits for
	 * any Faction.
	 *  
	 * @param  nbUnits
	 *         The nbUnits to check.
	 * @return 
	 *       | result == true if the number of units is bigger or equal than zero but less or equal than the maximum 
	 *       | number of units allowed in a faction
	*/
	public static boolean isValidNbUnits(int nbUnits) {
		return (0 <= nbUnits && nbUnits <= MAXNBUNITSINFACTION);
	
	}
	
	/**
	 * the maximum number of units of the Faction
	 */
	private final static int MAXNBUNITSINFACTION = 50;
	
	/**
	 * Check wheter this Faction can have the given unit 
	 * as one of its units
	 * 
	 * @param 	unit
	 * 			The unit to check
	 * @return	True if the unit is different from null and isn't terminated and if this faction isn't terminated
	 * 			and if this unit doesn't already belong to the faction than the maximum number of units can't be 
	 * 			reached in this faction
	 */
	public boolean canHaveAsUnit(Unit unit){
		return 
				(unit!=null)
				&& (!unit.isTerminated())
				&& (!this.isTerminated())
				&& (isValidNbUnits(this.getNbUnits()+1) || unitInFaction(unit));

	}
	
	
	/**
	 * Checks if the unit belongs to this Faction
	 * 
	 * @param 	unit
	 * 			The unit to check
	 * @return	True if the unit is in this faction.
	 */
	public boolean unitInFaction(Unit unit){
		return this.getUnitsInFaction().contains(unit);
	}
	
	/**
	 * Add the given unit to the this Faction
	 * 
	 * @param 	unit
	 * 			The unit to be added
	 * @post	This unit has the given unit as one of its units.
	 * 
	 * @post	The given unit has this faction as its faction.
	 * 
	 * @throws 	IllegalArgumentException
	 * 			The given unit can't be added to this faction.
	 */
	void addUnitToFaction(Unit unit) throws ModelException{
		if (!this.canHaveAsUnit(unit))
			throw new IllegalArgumentException("Not a valid unit");
		try{
		unitsInFaction.add(unit);
		unit.transferFaction(this);
		}
		catch (IllegalArgumentException e){
			//never happens
			throw new ModelException("Can't add unit to faction");
			
		}
		
	}
	
	/**
	 * Remove the given unit from this faction, and terminate this unit. If the given unit is
	 * the last unit of this faction then the faction will also be terminated.
	 * 
	 * @param 	unit
	 * 			The unit to be removed
	 * @effect	The unit no longer belongs to this Faction, if the unit
	 * 			was the last unit of this Faction then this Faction will be
	 * 			terminated
	 * @effect	The unit is terminated
	 * 
	 * @throws 	IllegalArgumentException
	 * 			The given unit isn't in the right state to be removed from
	 * 			this Faction
	 */
	void removeUnit(Unit unit) throws ModelException{
		try{
			if (unitInFaction(unit)){
				unitsInFaction.remove(unit);
				unit.terminate();
			
				if (getNbUnits()==0)
					this.terminate();
			}
		}
		catch (IllegalArgumentException e){
			throw new ModelException();
		}
	}
		

//	public void clearTerminatedUnits(){
//		for (Unit currentUnit : unitsInFaction){
//			if (currentUnit.isTerminated())
//				removeUnit(currentUnit); 
//		}
//	}
	
	/**
	 * 
	 * @return Returns the Scheduler of the Faction
	 */
	@Basic @Raw
	public Scheduler getScheduler(){
		return this.Scheduler;
	}
	
	/**
	 * 
	 * @param 	Scheduler
	 * 			The Scheduler the Faction obtains
	 * @pre		the given Scheduler must be a valid Scheduler
	 * 			|canHaveAsScheduler(Scheduler)
	 * @post	The Scheduler of this Faction will be set to the given Scheduler
	 * 			|new.getScheduler() == Scheduler
	 */
	@Raw
	private void setScheduler(Scheduler Scheduler){
		assert canHaveAsScheduler(Scheduler);
		this.Scheduler = Scheduler;
	}
	/**
	 * 
	 * @param Scheduler
	 * 			the Scheduler assigned to the Faction
	 */
	@Raw
	void transferScheduler(@Raw Scheduler Scheduler) throws IllegalArgumentException{		

		if (Scheduler == null && this.hasScheduler() && getScheduler().getFaction()==null){
			setScheduler(null);
			return;
		}
		if (!canHaveAsScheduler(Scheduler) || Scheduler == null)
			throw new IllegalArgumentException("Not a valid Scheduler");
		if (hasScheduler()) {
			throw new IllegalArgumentException("the Faction already has an Scheduler");
		}
		if(Scheduler.getFaction()!=this)
			throw new IllegalArgumentException("not a valid bidirectional association");
		setScheduler(Scheduler);
	}
	
	
	/**
	 * Checks of the Faction can use the Scheduler
	 * @param Scheduler
	 * 			Scheduler the Faction wants to use
	 * @return true if the Scheduler can be used by the Faction and the Scheduler already references the
	 * 			the Faction
	 * 			|Scheduler.canHaveAsFaction(this) && Scheduler.getFaction() == this
	 */
	@Raw
	public boolean canHaveAsScheduler(@Raw Scheduler Scheduler){
		return (Scheduler == null || (Scheduler.canHaveAsFaction(this) && Scheduler.getFaction()== this));
	}
	
	/**
	 * 
	 * @return True if the Faction has an Scheduler
	 */
	@Raw
	public boolean hasScheduler(){
		return getScheduler() != null;
	}
	/**
	 * 
	 * @return true if the Faction has a proper Scheduler attached to it
	 */
	@Raw
	public boolean hasProperScheduler(){
		return (canHaveAsScheduler(getScheduler()) && (getScheduler()==null || getScheduler().getFaction()==this));
	}
	
	
	private Scheduler Scheduler;
	
	/**
	 * Terminate this Faction
	 * 
	 * @post	This Faction is terminated
	 * 
	 * @post	No units are longer attached 
	 * 			to this Faction
	 * @effect	every unit attached to this Faction 
	 * 			is removed and terminated.
	 * 
	 */
	public void terminate(){
		if (!isTerminated())
		
			if (getNbUnits() == 0)
				this.isTerminated = true;
	}
	
	/**
	 * Check whether this Faction is terminated.
	 */
	public boolean isTerminated(){
		return isTerminated;
	}
	
	/**
	 * Variable registering whether or not this Faction 
	 * is terminated.
	 */
	private boolean isTerminated = false;
	
}
	

