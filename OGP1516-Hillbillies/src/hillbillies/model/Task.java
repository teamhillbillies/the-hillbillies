package hillbillies.model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Raw;
import hillbillies.model.expression.E;
import hillbillies.model.statement.S;
import ogp.framework.util.ModelException;

/**
 * @invar  The name of each Task must be a valid name for any
 *         Task.
 *       | isValidName(getName())
 * @invar  The priority of each Task must be a valid priority for any
 *         Task.
 *       | isValidPriority(getPriority())
 * @invar  The activity of each Task must be a valid activity for any
 *         Task.
 *       | isValidactivity(getActivity())
 * @invar  Each Task can have its location as location .
 *       | canHaveAsLocation(getLocation())
 * @invar  Each Task has an unique ID that distinguishes it from other Tasks
 *       | Task1.getUniqueInstance() == Task2.getUniqueInstance() --> Task1 == Task2

 */
public class Task {
	
	
	
	
	/**
	 * Initialize this new Task with given name, priority, activity and location.
	 * 
	 * @param  name
	 *         The name for this new Task.
	 * @param  priority
	 *         The priority for this new Task.
	 * @param  activity
	 *         The activity for this new Task.
	 * @param  location
	 * 		   The location for this new Task
	 * @post   If the given name is a valid name for any Task,
	 *         the name of this new Task is equal to the given
	 *         name. Otherwise, the name of this new Task is equal
	 *         to "defaultName".
	 *       | if (isValidName(name))
	 *       |   then new.getName() == name
	 *       |   else new.getName() == "defaultName" 
	 * @post   If the given priority is a valid priority for any Task,
	 *         the priority of this new Task is equal to the given
	 *         priority. Otherwise, the priority of this new Task is equal
	 *         to 0.
	 *       | if (isValidPriority(priority))
	 *       |   then new.getPriority() == priority
	 *       |   else new.getPriority() == 0
	 * @effect The activity of this new Task is set to
	 *         the given activity.
	 *       | this.setactivity(activity)
	 * @post   If the given location is a valid location for any Task,
	 *         the location of this new Task is equal to the given
	 *         location. Otherwise, the location of this new Task is equal
	 *         to {0,0,0}.
	 *       | if (isValidLocation(location))
	 *       |   then new.getLocation() == location
	 *       |   else new.getLocation() == {0,0,0}
	 * @effect The Task is assigned an unique Id
	 *       | new.uniqueIdInstance = assignUniqueId();
	 * @throws ModelException
	 * 		   if the given activity is not a valid activity for any Task
	 * 		   |!isValidActivity(activity)
	 */
	public Task(String taskName, int taskPriority, S activity, int[] location) throws ModelException {
		
		if (! isValidName(taskName))
			taskName = "defaultName";
		setName(taskName);
		
		if (! isValidPriority(taskPriority))
			taskPriority = 0;
		setPriority(taskPriority);
		
		if(! isValidActivity(activity))
			throw new IllegalArgumentException("statement is null");
		setActivity(activity);
		
		if (! canHaveAsLocation(location))
			location = new int[]{0,0,0};
		this.location = location;
		
		uniqueIdInstance = assignUniqueId();
	}
	


	/**
	 * Return the name of this Task.
	 */
	@Basic @Raw
	public String getName() {
		return this.name;
	}

	/**
	 * Check whether the given name is a valid name for
	 * any Task.
	 *  
	 * @param  name
	 *         The name to check.
	 * @return 
	 *       | result == (name != null)
	*/
	public static boolean isValidName(String name) {
		return name != null;
	}

	/**
	 * Set the name of this Task to the given name.
	 * 
	 * @param  name
	 *         The new name for this Task.
	 * @post   If the given name is a valid name for any Task,
	 *         the name of this new Task is equal to the given
	 *         name.
	 *       | if (isValidName(name))
	 *       |   then new.getName() == name
	 */
	@Raw
	private void setName(String name) {
		if (isValidName(name))
			this.name = name;
	}

	/**
	 * Variable registering the name of this Task.
	 */
	private String name;

	
	/**
	 * Return the priority of this Task.
	 */
	@Basic @Raw
	public int getPriority() {
		return this.priority;
	}
	
	/**
	 * Check whether the given priority is a valid priority for
	 * any Task.
	 *  
	 * @param  priority
	 *         The priority to check.
	 * @return 
	 *       | result == true
	*/
	public static boolean isValidPriority(int priority) {
		return true; //any integer is acceptable
	}
	
	/**
	 * Set the priority of this Task to the given priority.
	 * 
	 * @param  priority
	 *         The new priority for this Task.
	 * @post   If the given priority is a valid priority for any Task,
	 *         the priority of this new Task is equal to the given
	 *         priority.
	 *       | if (isValidPriority(priority))
	 *       |   then new.getPriority() == priority
	 */
	@Raw
	void setPriority(int priority) {
		if (isValidPriority(priority))
			this.priority = priority;
	}
	
	/**
	 * Variable registering the priority of this Task.
	 */
	private int priority;

	/**
	 * Return the activity of this Task.
	 */
	@Basic @Raw
	public S getActivity() {
		return this.activity;
	}
	
	/**
	 * Check whether the given activity is a valid activity for
	 * any Task.
	 *  
	 * @param  activity
	 *         The activity to check.
	 * @return 
	 *       | result == true
	*/
	public static boolean isValidActivity(S activity) {
		return activity!=null;
	}
	
	/**
	 * Set the activity of this Task to the given activity.
	 * 
	 * @param  activity
	 *         The new activity for this Task.
	 * @post   The activity of this new Task is equal to
	 *         the given activity.
	 *       | new.getActivity() == activity
	 * @post   The given activity gets this task as it's task
	 *       | activity.getTask == this
	 * @throws IllegalArgumentException
	 *         The given activity is not a valid activity for any
	 *         Task.
	 *       | ! isValidActivity(getActivity())
	 */
	@Raw
	void setActivity(S activity) 
			throws ModelException {
		if (! isValidActivity(activity))
			throw new ModelException();
		this.activity = activity;
		activity.transferTask(this);
	}
	
	/**
	 * Variable registering the activity of this Task.
	 */
	private S activity;
	


	
	/**
	 * Return the location of this Task.
	 */
	@Basic @Raw @Immutable
	public int[] getLocation() {
		return this.location;
	}
	
	/**
	 * Check whether this Task can have the given location as its location.
	 *  
	 * @param  location
	 *         The location to check.
	 * @return 
	 *       | result == true
	*/
	@Raw
	public boolean canHaveAsLocation(int[] location) {
		if (location == null)
			return false;
		return true;
	}
	
	/**
	 * Variable registering the location of this Task.
	 */
	private final int[] location;

	/**
	 * 
	 * @return whether this Task has a valid name, priority, list of activity, unit and set of schedulers
	 * 		| result == true if this task has a valid name, priority, list of activity, unit and set of schedulers. False otherwise
	 */
	public boolean isWellFormed() {
		
		return activity.isWellFormed(new Integer(0), new HashMap<String,E>());
	}
	


	/**
	 * Return the Unit of this Task.
	 */
	@Basic @Raw
	public Unit getUnit() {
		return this.unit;
	}
	
	/**
	 * Check whether the given Unit is a valid Unit for
	 * any Task.
	 *  
	 * @param  Unit
	 *         The Unit to check.
	 * @return 
	 *       | result == 	true if the Task is terminated and unit is null,
	 *       				or the unit isn't null and the unit isn't terminated.
	*/
	@Raw
	public boolean canHaveAsUnit(@Raw Unit unit) {
		if (isTerminated()){
			return (unit==null);
		}
		else{
			return(unit!=null &&!unit.isTerminated());
		}
			
	}
	
	/**
	 * Set the Unit of this Task to the given Unit.
	 * 
	 * @param  unit
	 *         The new Unit for this Task.
	 * @post   The Unit of this new Task is equal to
	 *         the given Unit.
	 *       | new.getUnit() == unit
	 */
	@Raw
	private void setUnit(@Raw Unit unit){
		this.unit = unit;
	}
	
	@Raw
	/**
	 * Checks whether this Task has a valid unit attached to it
	 * 
	 * @return 	true if the Task can have the unit as it's unit,
	 * 			and the unit has to have this Task as it's Task or is null.
	 */
	public boolean hasProperUnit(){
		return (canHaveAsUnit(getUnit()) && (getUnit().getTask()==this || getUnit() == null));
	}
	
	/**
	 * Initiates a connection between the given unit and this Task
	 * 
	 * @param 	unit
	 * 			The unit to be connected with.
	 * 
	 * @post	if the unit is a valid unit for this Task and the unit
	 * 			can have this Task as its Task then set the unit of this Task
	 * 			to the given unit.
	 * 			|new.getUnit() == unit
	 * @post	if the given unit is set as this Schedulers unit, then set the Task 
	 * 			of the given unit to this Task.
	 * 			|unit.getTask() == new
	 * @throws 	IllegalArgumentException
	 * 			If the unit is not a valid unit for this Task or is null
	 * 			If the unit already has an Task but it isn't this Task
	 * 			if the Task already has a unit but it isn't the given unit
	 * 			|(! this.canHaveAsUnit(unit) || unit == null) ||
	 * 			|(unit.hasTask() && unit.getTask()!=this) ||
	 * 			|(this.getUnit()!= null && this.getUnit()!= unit)
	 */
	public void linkUnit(Unit unit) throws ModelException{
		if (! this.canHaveAsUnit(unit) || unit == null)
			throw new IllegalArgumentException("Can't have as unit");
		if (unit.hasTask() && unit.getTask()!=this)
			throw new IllegalArgumentException("Unit already has another Task");
		if (this.getUnit()!= null && this.getUnit()!= unit)
			throw new IllegalArgumentException("Task already belongs to another unit");
		
		try{
			setUnit(unit);
			unit.transferTask(this);
		}
		catch (IllegalArgumentException e){
			throw new ModelException("invalid unit-Task connection");
		}
	}
	
	/**
	 * Breaks the connection between this Task and the Unit
	 * this Task is attached to.
	 * 
	 * @effect	The unit attached to this Task will no longer be
	 * 			attached to this Task
	 * 			|new.getUnit() == null
	 * @effect	The unit attached to this Task will break it's association
	 * 			with this Task.
	 * 			|this.getUnit().getTask() == null
	 */
	public void loseUnit(){
		try{
			Unit formerUnit = getUnit();
			setUnit(null);
			formerUnit.transferTask(null);
		}
		catch(NullPointerException e){
			e.printStackTrace();
		}
	}
	
	/**
	 * The unit this task is assigned to
	 */
	private Unit unit;
	

	/**
	 * Return the set of all Schedulers associated with this Task.
	 * 
	 * @invar	The set of Schedulers is effective
	 * 			|result != null
	 * @invar	Each Scheduler in this set of Schedulers are valid Schedulers for this
	 * 			Task.
	 * 			|for each scheduler in Schedulers:
	 * 			|	canHaveAsScheduler(scheduler)
	 * @invar	Each Scheduler in this set has this Task in their set of Schedulers.
	 * 			|for each scheduler in Schedulers:
	 * 			|	(scheduler.containsTask(Task) == true)
	 */
	private final Set<Scheduler> schedulers = new HashSet<Scheduler>();


	/**
	 * Return the number of Schedulers of this Task.
	 */
	@Basic @Raw
	public int getNbSchedulers() {
		return schedulers.size();
	}
	
	
	/**
	 * 
	 * @return a copy of the set of Schedulers
	 */
	public Set<Scheduler> getSchedulers(){
		Set<Scheduler> copyOfSchedulers = new HashSet<Scheduler>();
		for (Scheduler currentScheduler : schedulers){
			copyOfSchedulers.add(currentScheduler);
		}
		return copyOfSchedulers;
	}
	
	/**
	 * Check whether this Task has proper Schedulers attached to it.
	 * 
	 * @return 	True if this Task can have each Scheduler as its Scheduler and
	 * 			if each Scheduler of this Task contains this Scheduler in their set of Schedulers.
	 * 			|result == 
	 * 			|	for each Scheduler in SchedulersInWorld:
	 * 			|		if (!canHaveAsScheduler(currentScheduler)||(currentScheduler.containsScheduler(this)==false))
	 * 			|			return false
	 * 			|	return true
	 */
	public boolean hasProperSchedulers(){
		for (Scheduler currentScheduler : schedulers){
			if (!canHaveAsScheduler(currentScheduler)||(currentScheduler.TaskInScheduler(this)==false))
				return false;
		}
		return true;
	}
	
	/**
	 * Check whether the given nbSchedulers is a valid nbSchedulers for
	 * any Task.
	 *  
	 * @param  nbSchedulers
	 *         The nbSchedulers to check.
	 * @return 
	 *       | result == true if the number of Schedulers is bigger or equal than zero but less or equal than the maximum 
	 *       | number of Schedulers a Task is allowed to have
	*/
	public static boolean isValidNbSchedulers(int nbSchedulers) {
		return (0 <= nbSchedulers && nbSchedulers <= MAXNBSchedulers);
	
	}
	
	/**
	 * the maximum number of Schedulers of the Task
	 */
	private final static int MAXNBSchedulers = 5;
	
	/**
	 * Check whether this Task can have the given Scheduler 
	 * as one of its Schedulers
	 * 
	 * @param 	scheduler
	 * 			The Scheduler to check
	 * @return	True if the Scheduler is different from null and is not terminated and if this Task is not terminated
	 *          and if the Scheduler contains this Task in its set and if the number of Schedulers doesn't 
	 *          go above the limit or this Scheduler is already contained in the set of Schedulers 
	 *          |result == ((scheduler!=null) && (!scheduler.isTerminated()) && (!this.isTerminated())
	 *          |			&& (scheduler.TaskInScheduler(this)) 
	 *          |			&& (isValidNbSchedulers(this.getNbSchedulers()+1) || SchedulerInSet(Scheduler)))
	 */
	public boolean canHaveAsScheduler(Scheduler scheduler){
		return 
				(scheduler!=null)
				&& (!scheduler.isTerminated())
				&& (!this.isTerminated())
				&& (scheduler.TaskInScheduler(this))
				&& (isValidNbSchedulers(this.getNbSchedulers()+1) || SchedulerInSet(scheduler));

	}
	
	
	/**
	 * Checks if the Scheduler is contained in the set of Schedulers
	 * 
	 * @param 	scheduler
	 * 			The Scheduler to check
	 * @return	True if the Scheduler is in the set of Schedulers.
	 */
	public boolean SchedulerInSet(Scheduler scheduler){
		return this.getSchedulers().contains(scheduler);
	}
	
	/**
	 * Add the given Scheduler to the set of Schedulers
	 * 
	 * @param 	scheduler
	 * 			The Scheduler to be added
	 * @post	This Task has the given Scheduler as one of its Schedulers.
	 * 			|new.SchedulerInSet(scheduler)==true
	 * 
	 * @pre	    The given Scheduler contains this Task in its set of Tasks.
	 * 			|scheduler.TaskInScheduler == new(this)
	 * 
	 * @throws 	IllegalArgumentException
	 * 			The given Scheduler can't be added to the set of Tasks.
	 * 			|(!this.canHaveAsScheduler(scheduler))
	 */
	void addSchedulerToSet(Scheduler scheduler) throws ModelException, IllegalArgumentException{
		if (!this.canHaveAsScheduler(scheduler))
			throw new IllegalArgumentException("Not a valid Scheduler");
		try{
		schedulers.add(scheduler);
		}
		catch (IllegalArgumentException e){
			//never happens
			throw new ModelException("Can't add Scheduler to Task");
			
		}
		
	}
	
	/**
	 * Remove all bindings between this Task and its Schedulers.
	 * 
	 * @effect	Each Scheduler of this Task no longer has this Task
	 * 			in its set of Tasks
	 * 			|(for Scheduler scheduler : getSchedulers){
	 * 			|	(new)scheduler.TaskInScheduler(this)) == false
	 * @effect	The set of Schedulers is emptied
	 * 			|new.getNbTasks() == 0
	 * @throws 	IllegalArgumentException
	 * 			The method failed to remove the binding between a Scheduler and this Task
	 */
	void removeSchedulersBinding() throws IllegalArgumentException{
		for (Scheduler Scheduler : getSchedulers()) {
			try{
				Scheduler.removeTask(this);
			}
			catch (IllegalArgumentException e){
				e.getStackTrace();
				throw new IllegalArgumentException();
			}
		}

	}
		
	/**
	 * Remove the given Scheduler from this Task. If the given Scheduler is
	 * the last Scheduler of this Task then the Task will also be terminated.
	 * 
	 * @param 	Scheduler
	 * 			The Scheduler to be removed
	 * @effect	The Scheduler no longer belongs to this Task, if the Scheduler
	 * 			was the last Scheduler of this Task then this Task will be
	 * 			terminated
	 * 			|new.SchedulerInSet(scheduler) == false
	 * 			|if (getNbSchedulers()==0 && getUnit() == null)
	 * 			|	new.isTerminated == true
	 * 
	 * @throws 	IllegalArgumentException
	 * 			The given Scheduler isn't in the right state to be removed from
	 * 			this Task
	 */
	void removeScheduler(Scheduler scheduler) throws IllegalArgumentException{
		try{
			
			if (SchedulerInSet(scheduler))
				schedulers.remove(scheduler);
			
			if (getNbSchedulers()==0 && getUnit() == null)
				this.terminate();
		}
		catch (IllegalArgumentException e){
			throw new IllegalArgumentException();
		}
	}
	
	/**
	 * Add a variable to the map of variables
	 * 
	 * @param 	variableName
	 * 			The name of the variable
	 * @param 	e
	 * 			The value of the variable
	 * @post	The given variable is initialized with the given value
	 * 			|variableName := number
	 */
	public void addVariable(String variableName, E e){
		variableMap.put(variableName, e);
	}
	
	/**
	 * Returns the value of the variable introduced
	 * 
	 * @param 	variableName
	 * 			The name of the variable
	 * @return	the value of the variable
	 * 			|variableMap.get(variableNam).getValue()
	 */
	public E getVariable(String variableName){
		return variableMap.get(variableName);
	}
	
	/**
	 * Variable registering the map of all the variables introduced in the tasks
	 */
	private HashMap<String, E> variableMap = new HashMap<String, E>();
	
	/**
	 * Returns how many while statements are running
	 */
	public int getWhileDepth(){
		return this.whileDepth;
	}
	
	/**
	 * Sets the while depth to the given value
	 */
	public void setWhileDepth(int whileDepth){
		this.whileDepth = whileDepth;
		
	}
	
	/**
	 * registers the depth of the while statements running
	 */
	private int whileDepth = 0;
	
	/**
	 * Returns the while depth level that will be stopped
	 */
	public int getStopWhileDepth(){
		return this.stopWhileDepth;
	}
	
	/**
	 * Sets the stopWhileDepth to the given value
	 */
	public void setStopWhileDepth(int stopWhileDepth){
		this.stopWhileDepth = stopWhileDepth;
		
	}
	
	/**
	 * registers the depth of the while statement that will be stopped (0 for no stop)
	 */
	private int stopWhileDepth = 0;

	
	static int uniqueId = 0;
	
	int uniqueIdInstance;

	static int assignUniqueId()
	{
	    return uniqueId++;
	}
	
	int getUniqueInstance() {
		return uniqueIdInstance;
	}
	
	/**
	 * Terminate this Task
	 * 
	 * @post	This Task is terminated
	 * 
	 * @effect 	This Task will lose it's association with it's Unit attached 
	 * 			to it.
	 * 			|new.getUnit() == null
	 * 			|this.getUnit().getTask() == null
	 * @effect	This Task is removed from every scheduler
	 * 			that contained the task
	 * 			|(for Scheduler scheduler: getSchedulers()){
	 * 				scheduler.TaskInScheduler(new)==false
	 * 
	 */
	public void terminate(){
		if (!isTerminated()) {
			removeSchedulersBinding();
			if(getUnit()!=null)
				loseUnit();
			this.isTerminated = true;
		}
	}
	
	/**
	 * Check whether this Task is terminated.
	 */
	public boolean isTerminated(){
		return isTerminated;
	}
	
	/**
	 * Variable registering whether or not this Task 
	 * is terminated.
	 */
	private boolean isTerminated = false;
	
	

}
