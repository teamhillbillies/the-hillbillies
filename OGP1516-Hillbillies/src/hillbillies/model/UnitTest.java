package hillbillies.model;



import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import hillbillies.model.expression.*;
import hillbillies.model.statement.*;
import hillbillies.part2.facade.Facade;
import hillbillies.part2.listener.DefaultTerrainChangeListener;
import ogp.framework.util.ModelException;
import ogp.framework.util.Util;

public class UnitTest {

private static final double DELTA = 1e-6;
	
	private Unit unit0 , unit1, unit2;
	private double[] position;
	private int[] cubePosition;
	private int[] nextCubePosition;
	private int[] lastCubePosition;
	private World world;
	private int nbX;
	private int nbY;
	private int nbZ;
	private double[] nextPosition;
	private double[] lastPosition;


	

	@Before
	public void setUp() throws Exception {
		nbX = 10;
		nbY = 20;
		nbZ = 30;
		
		world = new World(new int[nbX][nbY][nbZ], new DefaultTerrainChangeListener());
		
		
		for (int x=0; x<nbX; x++){
			for (int y=0; y<nbY; y++){
				for (int z=0; z<nbZ; z++){
					double[] myPosition = {x,y,z};
					if (world.isValidUnitPosition(myPosition)){
						if (nextPosition == null && position!=null){
							nextPosition = new double[] {x,y,z};
							nextCubePosition = new int[] {x,y,z};
						}
						if (position==null){
							position = new double[] {x,y,z};
							cubePosition = new int[] {x,y,z};
						}
						lastPosition = new double[] {x,y,z};
						lastCubePosition = new int[] {x,y,z};
					}
				}
			}
		}
		
		
		unit0 = new Unit("Unit Low",position,25,25,25,25,false);
		unit1 = new Unit("Unit O'Middle",position, 50,50,50,50,false);
		unit2 = new Unit("UnitHigh",position, 100,100,100,100,false);
		world.addUnit(unit0);
		world.addUnit(unit1);
		world.addUnit(unit2);
		
		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void unitTransferWorld_UnitNotInWorld() throws Exception{
		Unit myUnit = new Unit("Unit Low",position,25,25,25,25,false);
		myUnit.transferWorld(world);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void unitTransferWorld_AlreadyHasWorld() throws Exception{
		World world2 = new World(new int[nbX][nbY][nbZ], new DefaultTerrainChangeListener());
		unit0.transferWorld(world2);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void unitTransferWorld_NullCase() throws Exception{
		unit0.transferWorld(null);
	}
	
	
	@Test
	public void units(){
		assertEquals(25,unit0.getAgility());
	}	
	

	//BLACKBOX-TESTING
	@Test
	public void extendedConstructor_LegalCase() throws Exception {

		Unit myUnit1 = new Unit("Unit O'Check",position, 25,50,70,100,false);
		assertEquals(position,myUnit1.getPosition());
		assertArrayEquals(cubePosition,myUnit1.getOccupiedCube());
		assertEquals("Unit O'Check", myUnit1.getName());
		assertEquals(60,myUnit1.getWeight());
		assertEquals(50, myUnit1.getAgility());
		assertEquals(70, myUnit1.getStrength());
		assertEquals(100, myUnit1.getToughness());
	}

	@Test
	public void extendedConstructor_InvalidValues() throws Exception {
		
		Unit myUnit2 = new Unit("Unit Invalid Values",position,Integer.MAX_VALUE,Integer.MAX_VALUE,Integer.MAX_VALUE,Integer.MAX_VALUE,false);
		assertEquals(100, myUnit2.getWeight());
		assertEquals(100, myUnit2.getAgility());
		assertEquals(100, myUnit2.getStrength());
		assertEquals(100, myUnit2.getToughness());
		
		Unit myUnit3 = new Unit("Unit Invalid Values",position,Integer.MIN_VALUE,Integer.MIN_VALUE,Integer.MIN_VALUE,Integer.MIN_VALUE,false);
		assertEquals(25, myUnit3.getWeight());
		assertEquals(25, myUnit3.getAgility());
		assertEquals(25,myUnit3.getStrength());
		assertEquals(25, myUnit3.getToughness());
		
	}
	
	@Test
	public void extendedConstructor_IllegalNumbers() throws Exception {

		Unit myUnit4 = new Unit("Unit Invalid Values",position,101,300,450,608,false);
		assertEquals(100, myUnit4.getWeight());
		assertEquals(100, myUnit4.getAgility());
		assertEquals(100,myUnit4.getStrength());
		assertEquals(100, myUnit4.getToughness());
		
		
		Unit myUnit5 = new Unit("Unit Invalid Values",position,1,10,15,24,false);
		assertEquals(25, myUnit5.getWeight());
		assertEquals(25, myUnit5.getAgility());
		assertEquals(25,myUnit5.getStrength());
		assertEquals(25, myUnit5.getToughness());
	}

	@Test
	public void extraSetterConditions_LegalCase() throws Exception {
		unit0.setWeight(120,false);
		assertEquals(120, unit0.getWeight());
		
		unit0.setAgility(150,false);
		assertEquals(150, unit0.getAgility());
		
		unit0.setStrength(180,false);
		assertEquals(180, unit0.getStrength());
		
		unit0.setToughness(200,false);
		assertEquals(200, unit0.getToughness());
	}
	

	
	@Test
	public void isValidName_TrueCase() throws Exception {
		assertTrue(Unit.isValidName(unit0.getName()));
		assertTrue(Unit.isValidName(unit1.getName()));
		assertTrue(Unit.isValidName(unit2.getName()));
		assertTrue(Unit.isValidName("Unit InvalidName2"));
		
	}
	
	@Test
	public void isValidName_FalseCase() throws Exception {
		assertFalse(Unit.isValidName("Unit_InvalidName"));
		assertFalse(Unit.isValidName("unit InvalidName"));
		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void extendedConstructor_IllegalName() throws Exception {
		if (! Unit.isValidName("unit_InvalidName1"))
			unit0.setName("unit_InvalidName1");
		else
			throw new IllegalArgumentException("null");	
	}
	
	@Test
	public void hitpoints_LegalCase() throws Exception {
		assertEquals(13, unit0.getMaxHitpoints());
		assertEquals(13, unit0.getHitpoints(),DELTA);
		assertEquals(50, unit1.getMaxHitpoints());
		assertEquals(50, unit1.getHitpoints(),DELTA);
	}
	
	@Test
	public void isValidHitpoints_LegalCase() throws Exception {
		assertTrue(unit0.isValidHitpoints(10));
		assertTrue(unit1.isValidHitpoints(30));
	}
	
	@Test
	public void isValidHitpoints_FalseCase() throws Exception {
		assertFalse(unit0.isValidHitpoints(20));
		assertFalse(unit0.isValidHitpoints(-10));
	}
	
	@Test
	public void consumeHitpoints_LegalCase() throws Exception {
		unit0.consumeHitpoints();
		unit1.consumeHitpoints();
		assertEquals(12, unit0.getHitpoints(),DELTA);
		assertEquals(49, unit1.getHitpoints(),DELTA);
	}
	
	@Test
	public void consumeMultipleHitpoints_LegalCase() throws Exception {
		unit0.consumeHitpoints(10);
		unit1.consumeHitpoints(5);
		assertEquals(3, unit0.getHitpoints(),DELTA);
		assertEquals(45, unit1.getHitpoints(),DELTA);
	}
	
	
	@Test
	public void stamina_LegalCase() throws Exception {
		assertEquals(13, unit0.getMaxStamina());
		assertEquals(13, unit0.getStamina(),DELTA);
		assertEquals(50, unit1.getMaxStamina(),DELTA);
		assertEquals(50, unit1.getStamina(),DELTA);
	}
	
	@Test
	public void isValidStamina_LegalCase() throws Exception {
		assertTrue(unit0.isValidStamina(10));
		assertTrue(unit1.isValidStamina(30));
	}
	
	@Test
	public void isValidStamina_FalseCase() throws Exception {
		assertFalse(unit0.isValidStamina(20));
		assertFalse(unit0.isValidStamina(-10));
	}
	
	@Test
	public void consumeStamina_LegalCase() throws Exception {
		unit0.consumeStamina();
		unit1.consumeStamina();
		assertEquals(12, unit0.getStamina(),DELTA);
		assertEquals(49, unit1.getStamina(),DELTA);
	}
	
	@Test
	public void consumeMultipleStamina_LegalCase() throws Exception {
		unit0.consumeStamina(10);
		unit1.consumeStamina(5);
		assertEquals(3, unit0.getStamina(),DELTA);
		assertEquals(45, unit1.getStamina(),DELTA);
	}
	
	@Test
	public void orientation_LegalCase() throws Exception {
		assertEquals(Math.PI/2, unit0.getOrientation(),DELTA);
	}
	
	@Test
	public void orientation_IllegalAmount() throws Exception {
		unit0.setOrientation(Double.MAX_VALUE);
		assertTrue((unit0.getOrientation()>=0)&&(unit0.getOrientation()<=2*Math.PI));
		unit0.setOrientation(Double.MIN_VALUE);
		assertTrue((unit0.getOrientation()>=0)&&(unit0.getOrientation()<=2*Math.PI));
	}
	
	@Test
	public void orientation_IllegalNumber() throws Exception {
		unit0.setOrientation(6*Math.PI);
		assertEquals(0, unit0.getOrientation(),DELTA);
		unit0.setOrientation(-Math.PI);
		assertEquals(Math.PI, unit0.getOrientation(),DELTA);
	}
	
	@Test
	public void currentActivity_LegalCase() throws Exception {
		assertEquals(Status.WAITING, unit0.getCurrentActivity());
	}
	
	@Test
	public void isValidDuration_LegalCase() throws Exception {
		assertTrue(Unit.isValidDuration(0.1));
		assertTrue(Unit.isValidDuration(0));
		assertTrue(Unit.isValidDuration(0.2));
	}
	
	@Test
	public void isValidDuration_FalseCase() throws Exception {
		assertFalse(Unit.isValidDuration(Double.NaN));
		assertFalse(Unit.isValidDuration(0.3));
		assertFalse(Unit.isValidDuration(-0.1));
	}
	
	@Test(expected=ModelException.class)
	public void advancetime_IllegalDuration() throws Exception {
		if (!Unit.isValidDuration(-0.2))
			unit0.advanceTime(-0.2);
		if (!Unit.isValidDuration(Double.NaN))
			unit0.advanceTime(Double.NaN);
		else
			throw new IllegalArgumentException("null");
	}
	
	@Test
	public void baseSpeed_LegalCase() throws Exception {
		assertEquals(1.5, unit0.getBaseSpeed(),DELTA);
		assertEquals(1.5, unit1.getBaseSpeed(),DELTA);
	}
	
	
	@Test
	public void isValidWalkingSpeed_LegalCase() throws Exception {
		assertTrue(Unit.isValidWalkingSpeed(2.0));
		assertTrue(Unit.isValidWalkingSpeed(Double.MAX_VALUE));
	}
	
	@Test
	public void isValidWalkingSpeed_FalseCase() throws Exception {
		assertFalse(Unit.isValidWalkingSpeed(-5.0));
		assertFalse(Unit.isValidWalkingSpeed(Double.NaN));
		//assertFalse(Unit.isValidWalkingSpeed(Double.MIN_VALUE));
	}
	
	@Test
	public void isValidSprintSpeed_LegalCase() throws Exception {
		assertTrue(Unit.isValidSprintSpeed(2.0));
		assertTrue(Unit.isValidSprintSpeed(Double.MAX_VALUE));
	}
	
	@Test
	public void isValidSprintSpeed_FalseCase() throws Exception {
		assertFalse(Unit.isValidSprintSpeed(-5.0));
		assertFalse(Unit.isValidSprintSpeed(Double.NaN));
		//assertFalse(Unit.isValidSprintSpeed(Double.MIN_VALUE));
	}
	
	@Test
	public void sprintingAndWalking_LegalCase() throws Exception {
		
		if (nextPosition!=null){
		
		//newPosition = new double[] {newPosition[0]-position[0],newPosition[1]-position[1],newPosition[2]-position[2]};
		unit0.moveToAdjacentFinal(nextPosition);
		assertTrue(unit0.isMoving());
		unit0.startSprinting();
		assertTrue(unit0.isSprinting());
		unit0.stopSprinting();
		assertFalse(unit0.isSprinting());
		}
		else{
			System.out.println("sprintingAndWalking_LegalCase() isn't tested, run again");
		}
	}
	
	@Test
	public void staminaConsumptionSprinting_LegalCase() throws Exception {
		
		if (nextPosition != null){
			//newPosition = new double[] {newPosition[0]-position[0],newPosition[1]-position[1],newPosition[2]-position[2]};
			unit0.moveToAdjacentFinal(nextPosition);
			double originalStamina = unit0.getStamina();
			unit0.startSprinting();
			assertEquals(originalStamina-1, unit0.getStamina(),DELTA);
			/*consumes 1 stamina*/
			unit0.advanceTime(0.11);
			assertEquals(originalStamina-2, unit0.getStamina(),DELTA);
			/*consumes 2 stamina*/
			unit0.advanceTime(0.2);
			assertEquals(originalStamina-4, unit0.getStamina(),DELTA);
		}
		else{
			System.out.println("staminaConsumptionSprinting_LegalCase() isn't tested, run again");
		}
	}
	
	
	
	@Test
	public void sprinting_FalseCase() throws Exception {
		assertFalse(unit0.isMoving());
		assertFalse(unit0.isSprinting());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void moveToAdjacent_IllegalPosition() throws Exception {
		
		double[] newPositionInt = {-1,6,5};
		
		unit0.moveToAdjacentFinal(newPositionInt);
	}
	
	@Test
	/* only legalcase because the duration has to be a valid duration because it is only
	 * called in advancetime where the given duration is a valid duration
	 */
	public void move_LegalCase() throws Exception {
		
		if (nextPosition != null){
			//newPosition = new double[] {newPosition[0]-position[0],newPosition[1]-position[1],newPosition[2]-position[2]};
			unit0.moveToAdjacentFinal(nextPosition);
			double[] oldPosition = unit0.getPosition();
			unit0.advanceTime(0.1);
			assertFalse(Arrays.equals(oldPosition, unit0.getPosition()));
		}
		else
			System.out.println("move_LegalCase isn't tested run again");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void moveTo_IllegalPosition() throws Exception {
		int[] newPositionInt = {-1,0,0};

		unit0.moveTo(newPositionInt);
			
	}
	
	@Test
	public void work_LegalCase() throws Exception {
		unit0.work(null);
		assertTrue(unit0.isWorking());
	}
	
	@Test
	public void work_FalseCase() throws Exception {
		assertFalse(unit0.isWorking());
	}
	
	@Test
	public void isAttacked_FalseCase() throws Exception {
		assertFalse(unit0.isAttacked());
	}
	
	/**@Test
	public void attack_LegalCase() throws Exception {
		unit0.attack(unit1);
		for (int i=0; i<9; i++){
			unit0.advanceTime(0.1);
			assertTrue(unit0.isAttacking());
		}
		unit0.advanceTime(0.1);
		assertFalse(unit0.isAttacking());
	}**/
	
	@Test
	public void rest_LegalCase() throws Exception {
		
		if (nextPosition != null){
			//newPosition = new double[] {newPosition[0]-position[0],newPosition[1]-position[1],newPosition[2]-position[2]};
			unit0.moveToAdjacentFinal(nextPosition);
			unit0.advanceTime(0.1);
			unit0.rest();
			assertFalse(unit0.isResting());
			assertEquals(Status.MOVING,unit0.getCurrentActivity());
			unit0.startSprinting();
			for (int i=0; i<8; i++){
				unit0.advanceTime(0.1);
			}
			unit0.rest();
			assertTrue(unit0.isResting());
			for (int i=0;i<200;i++) {
				unit0.advanceTime(0.1);
			}
			assertEquals(unit0.getMaxStamina(),unit0.getStamina(),DELTA);
		}
		else
			System.out.println("rest_legalCase() isn't tested, run again");
			
	}
	
	@Test
	public void startFalling_LegalCase() throws Exception{
		unit0.startFalling();
		assertTrue(unit0.isFalling());
		assertTrue(unit0.getCurrentActivity()==Status.FALLING);
		
	}
	
	@Test
	public void extendedConstructorCheckFacAndExp_legalCase()throws Exception{
		Unit myUnit1 = new Unit("Unit O'Check",position, 25,50,70,100,false);
		assertTrue(myUnit1.getFaction()==null);
		assertEquals(myUnit1.getExperiencePoints(),0);
	}
	
	@Test
	public void addFaction_legalCase() throws Exception{
		Faction firstFaction = new Faction(unit0);
		unit0.transferFaction(firstFaction);
		assertEquals(unit0.getFaction(),firstFaction);
		assertTrue(unit0.hasProperFaction());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void addFaction_falseCase() throws Exception{
		Faction illegalFaction = null;
		unit0.transferFaction(illegalFaction);
			
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void addFaction_factionFull() throws Exception{
		Faction alreadyFullFaction = new Faction(unit1);
		for (int i=0; i<49; i++){
			Unit myUnit1 = new Unit("Unit O'Check",position, 25,50,70,100,false);
			alreadyFullFaction.addUnitToFaction(myUnit1);
		}
		assertTrue(alreadyFullFaction.getNbUnits()==50);
		alreadyFullFaction.addUnitToFaction(unit0);
	}
	
	
	@Test
	public void addItem_LegalCase() throws Exception{
		int[] position = {0,0,0};
		Boulder boulder = new Boulder(position);
		boulder.receiveUnit(unit0);
		assertEquals(unit0.getItem(),boulder);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void addItem_falseCase() throws Exception{
		Boulder boulder = null;
		unit0.transferItem(boulder);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void addItem_FalseBidirectionalAssociation() throws Exception{
		int[] position = {0,0,0};
		Boulder boulder = new Boulder(position);
		unit0.transferItem(boulder);
	}
	
	@Test
	public void isValidEverything() throws Exception{
		//TODO
	}
	
	@Test(expected=ModelException.class)
	public void terminate_falseCase() throws Exception{
		unit0.terminate();
	}
	
	
//	@Test
//	public void isValidTimeSpendResting_LegalCase() throws Exception {
//		assertTrue(Unit.isValidTimeSpendResting(0.1));
//		assertTrue(Unit.isValidTimeSpendResting(Double.MAX_VALUE));
//		assertTrue(Unit.isValidTimeSpendResting(5));
//		assertTrue(Unit.isValidTimeSpendResting(0));
//	}
//	
//	@Test
//	public void isValidTimeSpendResting_FalseCase() throws Exception {
//		assertFalse(Unit.isValidTimeSpendResting(-0.1));
//		assertFalse(Unit.isValidTimeSpendResting(Double.NaN));
//		assertFalse(Unit.isValidTimeSpendResting(Double.MIN_VALUE));
//		assertTrue(Unit.isValidTimeSpendResting(-5));
//	}
//	
	@Test
	public void defaultBehavior_LegalCase() throws Exception {
		unit0.startDefaultBehaviour();
		assertTrue(unit0.hasDefaultBehaviour());
		unit0.stopDefaultBehaviour();
		assertFalse(unit0.hasDefaultBehaviour());
		
	}
	
	//WHITE-BOX TESTING
	
	
	@Test
	public void startSprinting_scenarios() throws Exception {
		
		if (lastPosition != null){
			
			unit0.startSprinting();
			assertFalse(unit0.isSprinting());
			unit0.moveToAdjacentFinal(lastPosition);
			unit0.startSprinting();
	

			//make sure the stamina is equal to 1 at the end of the loop
			for (int i=0; i < unit0.getMaxStamina(); i++){
				if (Util.fuzzyLessThanOrEqualTo(unit0.getStamina(), 1.0))
					break;
				assertTrue(unit0.isMoving());
				assertTrue(unit0.isSprinting());
				unit0.advanceTime(0.1);
				
			}
			unit0.advanceTime(0.1);
			assertFalse(unit0.isSprinting());
			unit0.startSprinting();
			assertFalse(unit0.isSprinting());
		}
		else{
			System.out.println("startSprinting_scenarios() isn't tested run again");
		}
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void moveToAdjacentFinal_FalseCase() throws Exception{
		double[] newPosition = {50,50,50};
		unit0.moveToAdjacentFinal(newPosition);
	}
	
//	@Test
//	public void moveToAdjacentFinal_LegalCase() throws Exception{
//		unit0.moveToAdjacentFinal(nextPosition);
//		double[] currentPosition = unit0.getPosition();
//		
//		while (!Arrays.equals(lastPosition, currentPosition)){
//			//assertTrue(unit0.isMoving());
//			unit0.advanceTime(0.1);
//			currentPosition = unit0.getPosition();
//		}
//		Arrays.equals(unit0.getPosition(),lastPosition);
//		
//	}
	
	@Test(expected=IllegalArgumentException.class)
	public void moveTo_FalseCase() throws Exception{
		int[] newPosition = {50,50,50};
		unit0.moveTo(newPosition);
		int[] negativePosition = {-5,-5,-5};
		unit0.moveTo(negativePosition);
	}
	
	
	@Test
	public void moveTo_isRestingAndIsntRecovered() throws Exception{
		unit0.attack(unit1);
		unit0.moveTo(nextCubePosition);
		assertTrue(unit0.getCurrentActivity()==Status.WAITING);
		unit2.consumeHitpoints(5.0);
		unit2.rest();
		assertTrue(unit2.getCurrentActivity()==Status.RESTING);
		unit2.advanceTime(0.1);
		assertFalse(unit2.getCurrentActivity()==Status.WAITING);
		
	}
	
	
	@Test
	public void work_cantWork() throws Exception{
		double[] newPosition = {0.0,0.0,0.0};
		unit0.moveToAdjacentFinal(newPosition);
		assertTrue(unit0.getCurrentActivity()== Status.MOVING);
		unit0.work(null);
		assertTrue(unit0.getCurrentActivity()== Status.MOVING);
		unit1.consumeHitpoints(5.0);
		unit1.rest();
		unit1.work(null);
		assertTrue(unit1.getCurrentActivity()==Status.RESTING);
	}
	
	@Test
	public void work_canWork() throws Exception{
		unit0.work(null);
		assertTrue(unit0.getCurrentActivity()==Status.WORKING);
	}
	
	@Test
	public void attack_cantAttack() throws Exception{
		unit0.moveToAdjacentFinal(nextPosition);
		assertTrue(unit0.getCurrentActivity()== Status.MOVING);
		unit0.attack(unit2);
		assertTrue(unit0.getCurrentActivity()== Status.MOVING);
		unit1.consumeHitpoints(5.0);
		unit1.rest();
		unit1.attack(unit2);
		assertTrue(unit1.getCurrentActivity()==Status.RESTING);
	}
	
	@Test
	public void attack_canAttack() throws Exception{
		unit0.attack(unit1);
		assertTrue(unit0.getCurrentActivity()==Status.ATTACKING);
	}
	
	@Test
	public void rest_cantRest() throws Exception{
		double[] newPosition = {0.0,0.0,0.0};
		unit0.moveToAdjacentFinal(newPosition);
		unit0.rest();
		assertTrue(unit0.getCurrentActivity()== Status.MOVING);
	}
	
	@Test
	public void rest_canRest() throws Exception{
		unit0.rest();
	}
	
	@Test
	public void getTask_NotAssigned() throws Exception{
		assertEquals(unit0.getTask(),null);
	}
	
	@Test
	public void taskBinding() throws Exception{
		int[] myPosition = {0,0,0};
		PositionE positionExpression = new Position(myPosition);
		S statement = new MoveTo(positionExpression);
		Task task1 = new Task("task1", 100, statement, myPosition);
		task1.linkUnit(unit0);
		assertEquals(unit0.getTask(),task1);
		assertTrue(unit0.canHaveAsTask(task1));
		assertTrue(unit0.hasProperTask());
		assertTrue(unit0.hasTask());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void taskAssign_TaskNotControlling() throws Exception{
		int[] myPosition = {0,0,0};
		PositionE positionExpression = new Position(myPosition);
		S statement = new MoveTo(positionExpression);
		Task task1 = new Task("task1", 100, statement, myPosition);
		unit0.transferTask(task1);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void taskBinding_NonEffectiveUnit() throws Exception{
		int[] myPosition = {0,0,0};
		PositionE positionExpression = new Position(myPosition);
		S statement = new MoveTo(positionExpression);
		Task task1 = new Task("task1", 100, statement, myPosition);
		task1.linkUnit(null);	
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void taskBinding_NonEffectiveTask() throws Exception{
		unit0.transferTask(null);
	}
	
	@Test
	public void canHaveAsTask() throws Exception{
		unit0.canHaveAsTask(null);
	}
	

}
